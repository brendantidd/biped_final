import os     
import sys
import psutil
from tensorflow.python.framework.tensor_shape import as_dimension
os.environ["TF_CPP_MIN_LOG_LEVEL"] = "3" 
import tensorflow as tf
tf.get_logger().setLevel("DEBUG")
import numpy as np
import argparse
import tensorboardX
from collections import deque     
from scripts.utils import *
from baselines.common.mpi_util import sync_from_root
import random
from mpi4py import MPI
import time
from pathlib import Path
home = str(Path.home())
from baselines import logger
import matplotlib.pyplot as plt
plt.switch_backend("agg")
import pybullet as p
import cv2
import json
import git
np.set_printoptions(precision=3, suppress=True)
import gc
from six.moves import shlex_quote

def all_gather(data, name, rank, num_workers):
    all_d = MPI.COMM_WORLD.allgather(data)
    temp_d = np.concatenate(all_d)
    length = temp_d.shape[0]//num_workers
    start = rank*length
    end = start + length
    if name == "done" and rank == 0:
        print("data shape", data.shape, [len(ad) for ad in all_d])
    if len(data.shape) == 1:
        return temp_d[start:end]
    else:
        return temp_d[start:end, ::]

def run(args):

    PATH = home + "/results/biped_final/latest/" + args.folder + "/" + args.exp + "/"

    comm = MPI.COMM_WORLD
    rank = comm.Get_rank()
    num_workers = comm.Get_size()
    myseed = int(args.seed + 10000 * rank)
    np.random.seed(myseed)
    random.seed(myseed)
    tf.set_random_seed(myseed)
    
    logger.configure(dir=PATH)
    if rank == 0:
        writer = tensorboardX.SummaryWriter(log_dir=PATH)
        repo = git.Repo(search_parent_directories=True)
        sha = repo.head.object.hexsha
        with open(PATH + "commandline_args.txt", "w") as f:
            f.write("Hash:")
            f.write(str(sha) + "\n")
            json.dump(args.__dict__, f, indent=2)
        print("Save git commit and diff to {}/git.txt".format(PATH))
        cmds = ["echo `git rev-parse HEAD` >> {}".format(
                        shlex_quote(os.path.join(PATH, "git.txt"))),
                        "git diff >> {}".format(
                        shlex_quote(os.path.join(PATH, "git.txt")))]
        os.system("\n".join(cmds))
    else: 
        writer = None 

    gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction= 0.1)
    sess = tf.InteractiveSession(config=tf.ConfigProto(inter_op_parallelism_threads=1,
                                                                                    intra_op_parallelism_threads=1,                         
                                                                                    gpu_options=gpu_options), graph=None)
 
    horizon = 2048

    from assets.env_pb_biped_setup import Env
    # from assets.env_pb_biped_old import Env
    # from models.dqn_adv import DQN
    if args.use_classifier:
        from models.classifier import Classifier
        classifier = Classifier(args, im_size=[48,48,1], dense_size=128, sess=sess) 
        classifier.load("./weights/classifier/")

    if args.flat_setup:
        obstacle_types = [args.obstacle_type, args.obstacle_type + "_setup", "flat", "flat_setup"]
    else:
        obstacle_types = [args.obstacle_type, args.obstacle_type + "_setup", "flat"]

    env = Env(render=args.render, PATH=PATH, args=args, display_im=args.display_im, obstacle_type=args.obstacle_type, vis=args.vis, doa=args.doa, disturbances=False, multi=args.multi, dqn=True, difficulty=args.difficulty)
    # env = Env(PATH=PATH, args=args)

    if args.eval_e2e:
        from models.ppo import Model
        e2e_pol = Model(args.obstacle_type, env=env, ob_size=env.ob_size, ac_size=env.ac_size, im_size=env.im_size, args=args, PATH=PATH, horizon=horizon, writer=writer, max_timesteps=args.max_ts, vis=args.vis, const_std=args.const_std)
    else:
        # from models import ppo_with_setup
        from models import ppo
        from models import ppo_setup
        pol = {name:ppo.Model(name, env=env, ob_size=env.ob_size, ac_size=env.ac_size, im_size=env.im_size, args=args, PATH=PATH, horizon=horizon, writer=writer, max_timesteps=args.max_ts, vis=args.vis, const_std=args.const_std) for name in obstacle_types if "setup" not in name}    
        pol[args.obstacle_type + "_setup"] = ppo_setup.Model(args.obstacle_type + "_setup", env=env, ob_size=env.ob_size, ac_size=env.ac_size, im_size=env.im_size, args=args, PATH=PATH, horizon=horizon, writer=writer, max_timesteps=args.max_ts, vis=args.vis, const_std=args.const_std)
    
    
    if args.flat_setup:
        pol["flat_setup"] = ppo_setup.Model("flat_setup", env=env, ob_size=env.ob_size, ac_size=env.ac_size, im_size=env.im_size, args=args, PATH=PATH, horizon=horizon, writer=writer, max_timesteps=args.max_ts, vis=args.vis, const_std=args.const_std)

    from models import q
    # args.max_ts = 3e6
    # pol = q.Model(args.obstacle_type + "_q", env=env, ob_size=env.ob_size, ac_size=1, im_size=env.im_size, vae_im_size=env.im_size, args=args, PATH=PATH, horizon=horizon, writer=writer, max_timesteps=args.max_ts, vis=args.vis, const_std=args.const_std)
    # im_size = env.im_size
    # im_size = [96,48,1]
    im_size = [48,48,1]
    # print(args.obstacle_type, args.max_ts, args.vis, args.const_std, args.ensemble_count); exit()

    q_pols = [q.Model(args.obstacle_type + "_q" + str(i), env=env, ob_size=env.ob_size, ac_size=2, im_size=im_size, vae_im_size=im_size, args=args, PATH=PATH, horizon=horizon, writer=writer, max_timesteps=args.max_ts, vis=args.vis, const_std=args.const_std) for i in range(args.ensemble_count)]

    if args.hpc:
        WEIGHTS_PATH = home + "/hpc-home/results/biped_model/weights/" + args.folder 
    else:
        # WEIGHTS_PATH = home + "/results/biped_model/weights/" + args.folder 
        WEIGHTS_PATH = "./weights"
    
    if args.eval_e2e:
        # e2e_pol.load(home + "/hpc-home/results/biped_model/latest/all/high_jumps_e2e_flat_cur/")            
        # e2e_pol.load(home + "/hpc-home/results/biped_model/latest/all_final1/high_jumps_e2e_flat_cur/")         
        e2e_pol.load(home + "/hpc-home/results/biped_model/latest/all_final1/high_jumps_e2e_flat_no_terrain_cur/")         
        # high_jumps_e2e_flat
        # high_jumps_e2e_flat_cur
        # high_jumps_e2e_flat_no_terrain_cur 
    else:
        for name in obstacle_types:
            if "setup" in name:
                if name == "flat_setup":
                    pol[name].load(WEIGHTS_PATH="./weights/flat_select/")
                else:
                    # if args.test_pol:
                    pol[name].load(WEIGHTS_PATH="./weights/" + args.obstacle_type + "_setup/")
                        # pol[name].load(WEIGHTS_PATH="./weights/high_jumps_vpred_42/")
                        # pol[name].load(WEIGHTS_PATH=home + "/results/biped_model/latest/dqn5/terminate2/")
                    # else:
                    #     if not args.blank:
                    #         if args.train_select and not args.train_setup or args.just_eval:
                    #             pol[name].load_pol(base_name=name, WEIGHTS_PATH="./weights/" + name + "/", vf=False)
                    #         else:
                    #             pol[name].load_pol(base_name="flat", WEIGHTS_PATH="./weights/" + "flat" + "/", vf=False)
            else:
                pol[name].load_pol(base_name=name, WEIGHTS_PATH="./weights/" + name + "/")
            
    initialize_uninitialized()
    # if not args.eval_e2e:
    #     sync_from_root(sess, pol[args.obstacle_type + "_setup"].vars, comm=comm)

    if not args.test_pol:
        for policy in q_pols:
            sync_from_root(sess, policy.vars, comm=comm)
            policy.set_training_params(max_timesteps=args.max_ts, learning_rate=args.lr, horizon=horizon)

    # Throw an error if the graph grows (shouldn"t change once everything is initialised)
    tf.get_default_graph().finalize()
    
    if args.obstacle_type == "flat":
        env.difficulty = 10

    prev_done = True
    prev_setup_done = True
    prev_select_done = True
    ob = env.reset()
    im = q_im = env.get_im()
    # if args.render: 
    #     while True: pass
    
    eval_success = []
    eval_distance = []
    eval_lengths = []
    if args.eval_first or args.test_pol:
        eval_next = True
        t_eval = time.time()
        evaluate = True
        stochastic = False
    else:
        eval_next = False
        evaluate = False
        stochastic = True
        if rank == 0:
            pol[args.obstacle_type + "_setup"].writer.add_scalar("Eval_success", 0, 0)
            pol[args.obstacle_type + "_setup"].writer.add_scalar("Eval_distance", 0, 0)
            pol[args.obstacle_type + "_setup"].writer.add_scalar("Eval_lengths", 0, 0)

    ep_ret = 0
    ep_len = 0
    ep_rets = []
    ep_lens = []
    setup_ep_ret = 0
    setup_ep_len = 0
    setup_ep_rets = []
    setup_ep_lens = []
    select_ep_ret = 0
    select_ep_len = 0
    select_ep_rets = []
    select_ep_lens = []
    ep_steps = 0
    ep_count = 0
    # timesteps_so_far = 0

    vpred_data = []
    vpred_orig_data = []
    probs_data = []
    stable_vpred = []
    terrain_data = []
    current_pol_data = []
    baseline_data = []
    setup_rew_data = []
    adv_data = []
    select_raw_data = []
    success = []

    if args.obstacle_type == "flat":
        next_pol = desired_pol = prev_pol = current_pol = obstacle_types.index("flat_setup")
    else:
        next_pol = desired_pol = prev_pol = current_pol = obstacle_types.index("flat")

    if args.render:
        replace_Id2 = p.addUserDebugText(obstacle_types[current_pol],[env.body_xyz[0], env.body_xyz[1], env.body_xyz[2]+0.5],[0,0,1],textSize=3)

    pols = [i for i in range(len(obstacle_types))]                         
    if args.obstacle_type != "flat":
        box_cross_steps = None
    else:
        box_cross_steps = 1

    # max_setup_length = 90
    max_setup_length = 75
    if not args.eval_e2e:
        _, exp_vpred, _, _ = pol[args.obstacle_type].step(ob, im, stochastic=False)    
    setup_done = False
    select_done = False
    prev_avg_success = avg_success = 0
    train_steps = 0
    eps = 1.0
    switched = False
    initial_setup = 150
    if args.train_select and args.train_setup:
        ready_to_train_select = False
    elif args.train_select:
        ready_to_train_select = True
    total_steps = 0
    eval_step = 0
    select_desired_pol = current_pol
    terminate_setup = False
    terminate_flat_setup = False
    max_disturbance = 500
    returns = []
    vpreds = []
    train_steps = 0
    env.set_current_pol(obstacle_types[current_pol])

    while True:

        # ===================================================================================
        # Evaluate stuff ====================================================================
        # ===================================================================================
        total_steps += 1
        if eval_next and total_steps % 1000 == 0:
            all_success = MPI.COMM_WORLD.allgather(eval_success)
            all_success = [h for d in all_success for h in d]
            all_distance = MPI.COMM_WORLD.allgather(eval_distance)
            all_distance = [h for d in all_distance for h in d]
            all_lengths = MPI.COMM_WORLD.allgather(eval_lengths)
            all_lengths = [h for d in all_lengths for h in d]
            # if rank == 0:
            #     print(len(all_success))
            if len(all_success) > num_workers*2:
                eval_next = False
                if rank == 0:
                    # print(pol[args.obstacle_type + "_setup"].iters_so_far)
                    if not args.eval_e2e:
                        q_pols[0].writer.add_scalar("Eval_success", np.mean(all_success), q_pols[0].iters_so_far)
                        q_pols[0].writer.add_scalar("Eval_distance", np.mean(all_distance), q_pols[0].iters_so_far)
                        q_pols[0].writer.add_scalar("Eval_lengths", np.mean(all_lengths), q_pols[0].iters_so_far)
                    print("resuming training", len(all_success), "evaluation took ", time.time() - t_eval, "eval success", np.mean(all_success), "eval distance", np.mean(all_distance), "lengths", np.mean(all_lengths))
                if args.eval_e2e:
                    break
        # ===================================================================================
        if q_pols[0].timesteps_so_far > q_pols[0].max_timesteps:
        # if q_pols[0].iters_so_far > q_pols[0].max_timesteps:
            print([(q_pols[i].timesteps_so_far, q_pols[i].max_timesteps) for i in range(args.ensemble_count)])
            print("exiting because of timesteps")
            break 

        # if not args.eval_e2e and (pol[args.obstacle_type + "_setup"].timesteps_so_far > args.max_ts):
        #     break 

        # if avg_success > prev_avg_success:
        #     prev_avg_success = avg_success
        # # eps = max(1 - 2*prev_avg_success, 0.01)
        # eps = max(1 - prev_avg_success, 0.01)
        # if not args.no_disturbance and random.random() < 0.02 and (not (args.test_pol or evaluate)) and obstacle_types[current_pol] != args.obstacle_type:
        #     env.add_disturbance(max_disturbance)
            # print("adding disturbance", max_disturbance)

        # ===================================================================================
        # Step env ==========================================================================
        # ===================================================================================
        if args.eval_e2e:
            act, _, _, _ = e2e_pol.step(ob, im, stochastic=False)
        else:
            adm, vpred, qs, recon_loss, recon_im = q_pols[train_steps%args.ensemble_count].step(ob, q_im, stochastic=stochastic)
            admissible = adm[0] == 1
            
            if obstacle_types[current_pol] == args.obstacle_type + "_setup":
                act, setup_vpred, _, nlogp, select_raw, nlogp_select = pol[args.obstacle_type + "_setup"].step(ob, im, stochastic=False)
                
                # terminate_setup = select_raw > 2
                terminate_setup = select_raw[0] > 3
                # print(terminate_setup, select_raw)
            else:
                if args.flat_setup and obstacle_types[current_pol] == "flat_setup":
                    act, _, _, _, flat_select_raw, _ = pol[obstacle_types[current_pol]].step(ob, im, stochastic=False)
                    terminate_flat_setup = flat_select_raw[0] > 3
                    print(terminate_flat_setup, flat_select_raw)
                else:
                    act, _, _, _ = pol[obstacle_types[current_pol]].step(ob, im, stochastic=False)

        # if args.render and args.debug:
        #     # print(terminate_setup)
        #     if terminate_setup:
        #         print(select_raw, env.steps, box_cross_steps, env.steps - box_cross_steps)
        #     if args.flat_setup and terminate_flat_setup:
        #         print(select_raw, env.steps, box_cross_steps, env.steps - box_cross_steps)

        torques = act
        
        next_ob, rew, done, _ = env.step(torques)
        next_im = next_q_im = env.get_im()
        if (args.obstacle_type == "flat" and env.box_num == (len(env.box_info[1])-1)) or (args.obstacle_type != "flat" and (env.box_info[1][-1][0] - env.box_info[2][-1][0]) < env.x_min):
            done = True
        # orig_done = done
        # if (env.steps < env.max_steps):
            # done = False

        if not setup_done and ((args.train_select and terminate_setup) or done or (prev_pol == obstacle_types.index(args.obstacle_type + "_setup") and current_pol != obstacle_types.index(args.obstacle_type + "_setup"))):
        # if not setup_done and (prev_pol == obstacle_types.index(args.obstacle_type + "_setup") and current_pol != obstacle_types.index(args.obstacle_type + "_setup")) or done:
            # print("setup_done", terminate_setup, done,    (prev_pol == obstacle_types.index(args.obstacle_type + "_setup") and current_pol != obstacle_types.index(args.obstacle_type + "_setup")))
            # print(setup_ep_len)
            # if args.render and args.debug:
            #     print(terminate_setup, env.steps - box_cross_steps)

            setup_done = True
            last_next_ob = next_ob
            last_next_im = next_im
        # ===================================================================================

        terrain_data.append(env.z_offset)

        if args.render:
            replace_Id2 = p.addUserDebugText(obstacle_types[current_pol],[env.body_xyz[0], env.body_xyz[1], env.body_xyz[2]+0.5],[0,0,1],textSize=3,replaceItemUniqueId=replace_Id2)
        
        # ===================================================================================
        # Setup stuff =======================================================================
        # ===================================================================================
        # if (orig_done and not done):  
        #     q_rew = 0.0 - 0.9*(np.clip((admissible - 0.0)**2, 0, 1)) 
        # if env.get_terrain("current") == args.obstacle_type or (env.get_terrain("previous") == args.obstacle_type and env.get_terrain("current") == "flat" and env.x_min > (env.box_info[1][env.box_num][0] - env.box_info[2][env.box_num][0]) and (not env.ob_dict["right_foot_left_ground"] and not env.ob_dict["left_foot_left_ground"])):
        #     if terminate_setup:
        #         q_rew = 1.0 - 0.9*(np.clip((adm[0] - 0.0)**2, 0, 1)) 
        #     else:
        #         q_rew = 1.0 - 0.9*(np.clip((adm[0] - 1.0)**2, 0, 1)) 
        # else:
        #     q_rew = 1.0 - 0.9*(np.clip((adm[0] - 2.0)**2, 0, 1)) 
        if env.get_terrain("current") == args.obstacle_type or (env.get_terrain("previous") == args.obstacle_type and env.get_terrain("current") == "flat" and env.x_min > (env.box_info[1][env.box_num][0] - env.box_info[2][env.box_num][0]) and (not env.ob_dict["right_foot_left_ground"] and not env.ob_dict["left_foot_left_ground"])):
            q_rew = 1.0 - 0.9*(np.clip((admissible - 1.0)**2, 0, 1)) 
        else:
            q_rew = 1.0 - 0.9*(np.clip((admissible - 0.0)**2, 0, 1)) 

        if not args.eval_e2e:
            exp_act, exp_next_vpred, _, _ = pol[args.obstacle_type].step(next_ob, next_im, stochastic=False)    
        else:
            exp_act, exp_next_vpred, exp_vpred = 0, 0, 0
        adv = rew + (1-done)*0.99*exp_next_vpred - exp_vpred
        scaled_adv = adv**2/15
        setup_rew = (1 - min(scaled_adv, 1))*exp_vpred/100
        # if args.rew == "adv":
        #     setup_rew = (1 - min(adv**2, 1))*exp_vpred/100
        # elif args.rew == "wide_adv":
        #     # setup_rew = (10 - min(adv**2, 10))*exp_vpred/1000
        #     setup_rew = (20 - min(adv**2, 20))*exp_vpred/2000
        # elif args.rew == "scaled_adv":
        #     scaled_adv = adv**2/25
        #     setup_rew = (1 - min(scaled_adv, 1))*exp_vpred/100
        # elif args.rew == "scaled_adv2":
        #     scaled_adv = adv**2/10
        #     setup_rew = (1 - min(scaled_adv, 1))*exp_vpred/100
        # elif args.rew == "scaled_adv3":
        #     scaled_adv = adv**2/15
        #     setup_rew = (1 - min(scaled_adv, 1))*exp_vpred/100
        # elif args.rew == "scaled_adv4":
        #     scaled_adv = adv**2/30
        #     setup_rew = (1 - min(scaled_adv, 1))*exp_vpred/100
        # elif args.rew == "exp_adv":
        #     setup_rew = np.exp(-args.fact*(adv**2))*exp_vpred/100
        # elif args.rew == "abs_adv":
        #     setup_rew = np.exp(-2*(abs(adv)))*exp_vpred/100
        # elif args.rew == "exp_adv10":
        #     setup_rew = np.exp(-args.fact*(adv**2))*exp_vpred/10
        # elif args.rew == "const":
        #     setup_rew = 1.5
        # elif args.rew == "tor":
        #     setup_rew = np.exp(-2.0*np.sum((act - exp_act)**2))*1.5
        # elif args.rew == "rew":
        #     setup_rew = rew
        # elif args.rew == "vpred":
        #     setup_rew = exp_vpred/100
        # elif args.rew == "just_adv":
        #     setup_rew = adv
        # elif args.rew == "just_exp_adv":
        #     setup_rew = (1 - np.exp(-(adv**2)))*1.5
        # elif args.rew == "lv_adv":
        #     # returns.append( rew + (1-done)*0.99*exp_next_vpred )
        #     returns.append( rew )
        #     vpreds.append(exp_vpred)
        #     adv = sum(returns)+(1-done)*0.99*exp_next_vpred    - exp_vpred
        #     setup_rew = np.exp(-(adv**2))*exp_vpred/100
            # setup_rew = (1 - np.exp(-(adv**2)))*1.5

        # if not args.test_pol:
        # print(setup_done)
        # if obstacle_types[current_pol] == args.obstacle_type + "_setup":
        #     setup_ep_ret += setup_rew
        #     setup_ep_len += 1
        # elif setup_done:                
        #     # print(not args.no_extra_rew)
        #     # if len(pol[args.obstacle_type + "_setup"].data["rew"]) > 0:
        #         # print("need to figure this out..")
        #         # print("also add gamma?")
        #     if not args.no_extra_rew and not args.test_pol:
        #         # print(terminate_setup)
        #         if (args.extra_on_terminate and terminate_setup and setup_ep_len > 50) or not args.extra_on_terminate:
        #             # print(terminate_setup, pol[args.obstacle_type + "_setup"].data[rew"][-1])
        #             pol[args.obstacle_type + "_setup"].data["rew"][-1] += setup_rew
        #             setup_ep_ret += setup_rew
        #             # print("extra: ", setup_rew)
        #             # if args.debug and rank == 0: print(pol[args.obstacle_type + "_setup"].data["rew"][-1])

        # ===================================================================================


        # ===================================================================================
        # Training stuff ====================================================================
        # ===================================================================================
        if not args.test_pol and not evaluate:
            # if args.train_setup and obstacle_types[current_pol] == args.obstacle_type + "_setup":
            #     # print(nlogp_select, nlogp, select_raw)
            #     pol[args.obstacle_type + "_setup"].add_to_buffer([ob, im, act, select_raw, setup_rew, prev_setup_done, setup_vpred, nlogp, nlogp_select])
            q_pols[train_steps%args.ensemble_count].add_to_buffer([ob, q_im, adm, q_rew, prev_done, vpred])

        # train_steps += 1
        # if not args.test_pol and not evaluate and train_steps % (args.ensemble_count*10) == 0 and train_steps > 0: 
        if not args.test_pol and not evaluate and len(q_pols[0].data["rew"]) > 1440: 
            # if len(pol[args.obstacle_type + "_setup"].data["rew"]) > 1440 and train_steps % 100 == 0:
            # if len(dqn.data["rew"]) > 2048 and train_steps % 100 == 0:
                # setup_data_lengths = MPI.COMM_WORLD.allgather(len(pol[args.obstacle_type + "_setup"].data["rew"]))
                # if (np.array(setup_data_lengths) > 1440).all():
                    # if rank == 0:
                        # print("setup", setup_data_lengths)

                # print(len(q_pols[0].data["rew"]), train_steps, args.ensemble_count*10, train_steps%args.ensemble_count)   
                for ens in range(args.ensemble_count): 
                    # successes = MPI.COMM_WORLD.allgather(success)
                    # successes = np.array([d for h in successes for d in h], dtype=np.float32)
                    # avg_success = np.mean(successes)
                    # things = {"Avg_success":np.mean(successes)}
                    # things["eps"] = eps
                    # if rank == 0:
                    #     process = psutil.Process(os.getpid())
                    #     things["RAM"] = num_workers*process.memory_info().rss/(1024.0 ** 3)

                    # if not evaluate:      
                        # ep_lens.append(ep_len)
                        # ep_rets.append(ep_ret)
                    _, vpred, _, _, _  = q_pols[ens].step(next_ob, next_q_im, stochastic=True)

                    q_pols[ens].finalise_buffer(data={"ep_rets":ep_rets, "ep_lens":ep_lens}, last_value=vpred, last_done=done)

                    for d in q_pols[ens].training_input:
                        all_d = MPI.COMM_WORLD.allgather(q_pols[ens].data[d])
                        temp_d = np.concatenate(all_d)
                        length = temp_d.shape[0]//num_workers
                        start = rank*length
                        end = start + length
                        if len(q_pols[ens].data[d].shape) == 1:
                            q_pols[ens].data[d] = temp_d[start:end]
                        else:
                            q_pols[ens].data[d] = temp_d[start:end, ::]

                    # print(ens, [q_pols[ens].data[d].shape for d in q_pols[ens].training_input])

                    # 
                    # temp_d = None
                    # all_d = None


                    if ens == 0:
                        q_pols[ens].run_train(data={"ep_rets":ep_rets, "ep_lens":ep_lens}, last_value=vpred, last_done=done, evaluate=True)
                    else:
                        q_pols[ens].run_train(data={"ep_rets":ep_rets, "ep_lens":ep_lens}, last_value=vpred, last_done=done, evaluate=False)



                    # if args.debug and rank == 0: print(rank, "training setup, total steps", len(pol[args.obstacle_type + "_setup"].data["rew"]), train_steps)
                    
                    # if setup_done:
                    #     _, setup_next_vpred, _, _, _, _ = pol[args.obstacle_type + "_setup"].step(last_next_ob, last_next_im, stochastic=True)
                    # else:
                    #     _, setup_next_vpred, _, _, _, _ = pol[args.obstacle_type + "_setup"].step(next_ob, next_im, stochastic=True)
                    
                    # Calculate advantage with local data
                    # pol[args.obstacle_type + "_setup"].finalise_buffer({"ep_rets":ep_rets, "ep_lens":ep_lens}, last_value=setup_next_vpred, last_done=setup_done)        

                    # # Lots of smaller calls to allgather
                    # for d in pol[args.obstacle_type + "_setup"].training_input:
                    #     all_d = MPI.COMM_WORLD.allgather(pol[args.obstacle_type     + "_setup"].data[d])
                    #     temp_d = np.concatenate(all_d)
                    #     length = temp_d.shape[0]//num_workers
                    #     start = rank*length
                    #     end = start + length
                    #     if len(pol[args.obstacle_type + "_setup"].data[d].shape) == 1:
                    #         pol[args.obstacle_type + "_setup"].data[d] = temp_d[start:end]
                    #     else:
                    #         pol[args.obstacle_type + "_setup"].data[d] = temp_d[start:end, ::]
                    # temp_d = None
                    # all_d = None

                    # Add additional things that should be logged and added to tensorboard
                    # pol[args.obstacle_type + "_setup"].log_stuff(things)

                    # pol[args.obstacle_type + "_setup"].run_train(setup_ep_rets, setup_ep_lens)

                # if args.eval and not eval_next and pol[args.obstacle_type + "_setup"].iters_so_far % 10 == 0 and pol[args.obstacle_type + "_setup"].iters_so_far != 0 and eval_step != pol[args.obstacle_type + "_setup"].iters_so_far: 
                # For now, eval every second update..
                if args.eval and q_pols[0].iters_so_far % 10 == 0: 
                    eval_step = q_pols[0].iters_so_far
                    eval_next = True
                    eval_success = []
                    eval_distance = []
                    eval_lengths = []
                    t_eval = time.time()
                    if rank == 0: 
                        print("evaluating.. ")

                success = []
                ep_rets = []
                ep_lens = []
                    # success = []
                    # ep_rets = []
                    # ep_lens = []
                    # setup_ep_rets = []
                    # setup_ep_lens = []
                    # select_ep_rets = []
                    # select_ep_lens = []

        # ===================================================================================
        current_pol_data.append(current_pol)
        baseline_data.append(desired_pol)
        if not args.eval_e2e and obstacle_types[current_pol] == args.obstacle_type + "_setup":
            select_raw_data.append(select_raw[0])
        else:
            select_raw_data.append(0)
        setup_rew_data.append(setup_rew)
        if "exp" in args.rew:
            adv_data.append(np.exp(-(adv**2)))
        elif "scaled" in args.rew:
            adv_data.append(scaled_adv)
        else:
            adv_data.append(min(adv**2, 20))

        # ===================================================================================
        # Terrain oracle ====================================================================
        # ===================================================================================
        
        # if admissible:
        #     next_base_pol = args.obstacle_type
        # else:
        #     next_base_pol = "flat"
        # if admissible:

        if not box_cross_steps and env.order[env.box_num] == args.obstacle_type:
            box_cross_steps = env.steps

        if not admissible:
            desired_pol = obstacle_types.index("flat") 
        # elif box_cross_steps and (box_cross_steps + max_setup_length > env.steps):
        #     desired_pol = obstacle_types.index(args.obstacle_type + "_setup") 
        # elif box_cross_steps and (box_cross_steps + max_setup_length <= env.steps):
        else:
            # if (env.order[env.box_num] == args.obstacle_type):
                # print(max_setup_length + box_cross_steps, env.steps - box_cross_steps)
            desired_pol = obstacle_types.index(args.obstacle_type) 
            # elif (env.order[env.box_num-1] == args.obstacle_type and env.order[env.box_num] != args.obstacle_type and env.body_xyz[0] < env.box_info[1][env.box_num][0]):
                # desired_pol = obstacle_types.index(args.obstacle_type) 
            # else:
                # desired_pol = obstacle_types.index("flat") 

        # ORIGINAL SETUP
        # if not box_cross_steps:
        #     desired_pol = obstacle_types.index("flat") 
        # elif box_cross_steps and (box_cross_steps + max_setup_length > env.steps):
        #     desired_pol = obstacle_types.index(args.obstacle_type + "_setup") 
        # elif box_cross_steps and (box_cross_steps + max_setup_length <= env.steps):
        #     if (env.order[env.box_num] == args.obstacle_type):
        #         # print(max_setup_length + box_cross_steps, env.steps - box_cross_steps)
        #         desired_pol = obstacle_types.index(args.obstacle_type) 
        #     elif (env.order[env.box_num-1] == args.obstacle_type and env.order[env.box_num] != args.obstacle_type and env.body_xyz[0] < env.box_info[1][env.box_num][0]):
        #         desired_pol = obstacle_types.index(args.obstacle_type) 
        #     else:
        #         desired_pol = obstacle_types.index("flat") 

        # ===================================================================================
        # Select stuff ======================================================================
        # ===================================================================================
        
        # print(abs(env.vz) < 0.05, abs(env.vz))
        # elif "flat" not in obstacle_types[current_pol] and (env.order[env.box_num-1] == args.obstacle_type and env.order[env.box_num] != args.obstacle_type and (env.ob_dict["left_foot_on_ground"] and env.ob_dict["right_foot_on_ground"] and abs(env.vz) < 0.1)):



        # next_pol = adm[0]


        if admissible and terminate_setup:
            next_pol = obstacle_types.index(args.obstacle_type) 
        elif admissible:
            next_pol = obstacle_types.index(args.obstacle_type + "_setup") 
        else:
            next_pol = obstacle_types.index("flat") 
            # terminate_setup = False
           
        # if env.get_terrain("current") == args.obstacle_type or (env.get_terrain("previous") == args.obstacle_type and env.get_terrain("current") == "flat" and env.x_min > (env.box_info[1][env.box_num][0] - env.box_info[2][env.box_num][0]) and (not env.ob_dict["right_foot_left_ground"] and not env.ob_dict["left_foot_left_ground"])):
        #     # q_rew = 1.0 - 0.9*(np.clip((admissible - 1.0)**2, 0, 1)) 
        #     if terminate_setup:
        #         next_pol = obstacle_types.index(args.obstacle_type) 
        #     else:
        #         next_pol = obstacle_types.index(args.obstacle_type + "_setup") 

        # else:
        #     # q_rew = 1.0 - 0.9*(np.clip((admissible - 0.0)**2, 0, 1))            
        #     next_pol = obstacle_types.index("flat") 


        # # ORIGINAL SETUP
        # if not box_cross_steps:
        #     next_pol = obstacle_types.index("flat") 
        # elif box_cross_steps and (box_cross_steps + max_setup_length > env.steps) and not setup_done:
        #     next_pol = obstacle_types.index(args.obstacle_type + "_setup") 
        # elif box_cross_steps and (box_cross_steps + max_setup_length <= env.steps or setup_done):
        #     if (env.order[env.box_num] == args.obstacle_type):
        #         next_pol = obstacle_types.index(args.obstacle_type) 
        #     # elif (env.order[env.box_num-1] == args.obstacle_type and env.order[env.box_num] != args.obstacle_type and env.body_xyz[0] < env.box_info[1][env.box_num][0]):
        #     # elif "flat" in obstacle_types[current_pol] or (env.order[env.box_num-1] == args.obstacle_type and env.order[env.box_num] != args.obstacle_type and (env.ob_dict["left_foot_on_ground"] and env.ob_dict["right_foot_on_ground"] and abs(env.vz) < 0.05)):
        #     elif "flat" in obstacle_types[current_pol] or (env.order[env.box_num-1] == args.obstacle_type and env.order[env.box_num] != args.obstacle_type and (env.ob_dict["left_foot_on_ground"] and env.ob_dict["right_foot_on_ground"] and abs(env.vz) < 0.05 and env.vx > 0)):
        #         if args.flat_setup and obstacle_types[current_pol] != "flat" and not terminate_flat_setup:
        #             next_pol = obstacle_types.index("flat_setup") 
        #             # print("running flat setup")
        #         else:
        #             next_pol = obstacle_types.index("flat") 
        #     else:
        #         next_pol = obstacle_types.index(args.obstacle_type) 

        prev_pol = current_pol
        current_pol = next_pol
        env.set_current_pol(obstacle_types[current_pol])

        exp_vpred = exp_next_vpred

        if obstacle_types[current_pol] == args.obstacle_type + "_setup":
            prev_setup_done = setup_done
        prev_done = done
        ob = next_ob
        im = next_im
        q_im = next_q_im
        ep_ret += rew
        ep_len += 1
        ep_steps += 1

        # if args.eval and not eval_next and pol[args.obstacle_type + "_setup"].iters_so_far % 10 == 0 and pol[args.obstacle_type + "_setup"].iters_so_far != 0 and eval_step != pol[args.obstacle_type + "_setup"].iters_so_far: 
        #     eval_step = pol[args.obstacle_type + "_setup"].iters_so_far
        #     eval_next = True
        #         eval_success = []
        #     eval_distance = []
        #     eval_lengths = []
        #     t_eval = time.time()

        # ====================================================================================================================
        # Done
        # ====================================================================================================================
        if done:  
            if not evaluate:      
                ep_lens.append(ep_len)
                ep_rets.append(ep_ret)
            #     _, vpred, _, _, _  = q_pols[train_steps%args.ensemble_count].step(next_ob, next_q_im, stochastic=True)

            #     if train_steps%args.ensemble_count == 0:
            #         q_pols[train_steps%args.ensemble_count].run_train(data={"ep_rets":ep_rets, "ep_lens":ep_lens}, last_value=vpred, last_done=done, evaluate=True)
            #     else:
            #         q_pols[train_steps%args.ensemble_count].run_train(data={"ep_rets":ep_rets, "ep_lens":ep_lens}, last_value=vpred, last_done=done, evaluate=False)
            
            #     success = []
            #     ep_rets = []
            #     ep_lens = []

            t1 = time.time()
            # if (ep_count % (args.ensemble_count*10) == 0) or args.test_pol:
            #     if rank == 0:
            #         print("evaluating from now..", ep_count, args.test_pol)
            #     eval_next = True
            #     eval_step = q_pols[0].iters_so_far
            #     eval_next = True
            #     eval_success = []
            #     eval_distance = []
            #     eval_lengths = []
            #     t_eval = time.time()
                # eval_next = False

            # if rank == 0:
                # if eval_next:
                #     print("eval next", train_steps, ep_count)
                # else:
                #     print("training next", train_steps, ep_count)


            if (args.obstacle_type == "flat" and env.box_num == (len(env.box_info[1])-1)) or (args.obstacle_type != "flat" and (env.box_info[1][-1][0] - env.box_info[2][-1][0]) < env.x_min):
            # if ((env.box_info[1][-1][0] - env.box_info[2][-1][0]) < env.body_xyz[0]) and env.steps >= env.max_steps - 2:
                label = True    
                if rank == 0:
                    env.save_sim_data(tag=str(rank)+"_success")
            else:
                label = False
            success.append(label)
            # if args.render and args.debug:
            # print("success", label, np.mean(success))

            if evaluate:
                eval_success.append(label)
                eval_distance.append(env.x_min/(env.box_info[1][-1][0] - env.box_info[2][-1][0]))
                eval_lengths.append(setup_ep_len)

            if eval_next:
                evaluate = True
                stochastic = False
            else:
                evaluate = False
                stochastic = True

            if not evaluate:
                train_steps += 1

            # if args.plot or (rank == 0 and ep_count % 100 == 0) or label:
            # if args.plot or (rank == 0 and (ep_count % 100 == 0 or (ep_count % 10 == 0 and label and not evaluate))):
            if args.plot or (rank == 0 and (ep_count % 10 == 0 or (ep_count % 10 == 0 and label and not evaluate))):
                num_axes = 5
                fig, axes = plt.subplots(num_axes,    figsize=(10, 3*num_axes))
                axes[0].plot([_ for _ in range(len(vpred_data))], vpred_data, alpha=1.0)    
                
                colours = [np.array([0, 102, 204])/255.0, np.array([0, 102, 204])/255.0, np.array([255,165,0])/255.0, np.array([51, 204, 51])/255.0, np.array([100, 204, 0])/255.0, np.array([200, 100, 51])/255.0]
                
                print(np.array(current_pol_data).shape, len(current_pol_data))
                axes[0].plot([_ for _ in range(len(current_pol_data))], current_pol_data, c=np.array([0, 102, 204])/255.0, alpha=1.0)    
                axes[0].plot([_ for _ in range(len(baseline_data))], baseline_data, c=np.array([0, 204, 102])/255.0, alpha=1.0)    
                axes[0].set_ylim([-0.9,len(obstacle_types) + 0.1])
                axes[0].legend(["current", "baseline"])
                axes[0].set_title("Current policy", loc="left")
                for s, n in enumerate(obstacle_types):
                    axes[0].text(0,s*0.5+0.5,str(s) + ". " + n)

                # axes[1].set_ylim([-0.9,len(obstacle_types) + 0.1])
                # for s, n in enumerate(obstacle_types):
                #     axes[1].text(0,s*0.5+0.5,str(s) + ". " + n)
                # axes[1].set_title("baseline data", loc="left")

                axes[1].plot([_ for _ in range(len(select_raw_data))], select_raw_data, c=np.array([0, 102, 204])/255.0, alpha=1.0)    
                # axes[2].set_ylim([-0.9, 1.1])
                axes[1].set_title("terminate setup data", loc="left")


                axes[2].plot([_ for _ in range(len(setup_rew_data))], setup_rew_data, c=np.array([0, 102, 204])/255.0, alpha=1.0)    
                # axes[2].set_ylim([-0.9, 1.1])
                axes[2].set_title("setup reward", loc="left")

                axes[3].plot([_ for _ in range(len(adv_data))], adv_data, c=np.array([0, 102, 204])/255.0, alpha=1.0)    
                # axes[2].set_ylim([-0.9, 1.1])
                axes[3].set_title("advantage data", loc="left")

                axes[-1].plot([_ for _ in range(len(terrain_data))], terrain_data, c=np.array([0, 102, 204])/255.0, alpha=1.0)    
                axes[-1].set_title("terrain height", loc="left")
                fig.tight_layout(pad=4.0)
                if label:
                    print(PATH + str(rank) + "_success" + str(ep_count) + ".png")
                    plt.savefig(PATH + str(rank) + "_success" + str(ep_count) + ".png", bbox_inches="tight")
                else:
                    print(PATH + "ep" + str(ep_count) + ".png")
                    plt.savefig(PATH + "ep" + str(ep_count) + ".png", bbox_inches="tight")
                plt.close()


            vpred_data = []
            vpred_orig_data = []
            probs_data = []
            terrain_data = []
            current_pol_data = []
            baseline_data = []
            adv_data = []
            setup_rew_data = []
            select_raw_data = []
            switch_data = []
            
            ob = env.reset()
            im = q_im = env.get_im()

            # ep_rets.append(ep_ret)    
            # ep_lens.append(ep_len)         
            ep_ret = 0
            ep_len = 0
            setup_ep_rets.append(setup_ep_ret)    
            setup_ep_lens.append(setup_ep_len)         
            setup_ep_ret = 0
            setup_ep_len = 0
            # if not evaluate:
            ep_count += 1    
            if args.obstacle_type != "flat":
                box_cross_steps = None
            else:
                box_cross_steps = 1
            if args.obstacle_type == "flat":
                next_pol = desired_pol = prev_pol = current_pol = obstacle_types.index("flat_setup")
            else:
                next_pol = desired_pol = prev_pol = current_pol = obstacle_types.index("flat")
            if not args.eval_e2e:
                _, exp_vpred, _, _ = pol[args.obstacle_type].step(ob, im, stochastic=False)    
            setup_done = False
            select_done = False
            switched = False
            select_raw = 0
            terminate_setup = False
            terminate_flat_setup = False
            returns = []
            vpreds = []
            env.set_current_pol(obstacle_types[current_pol])
            

if __name__ == "__main__":
    import defaults
    from dotmap import DotMap
    args1, unknown1 = defaults.get_defaults() 
    parser = argparse.ArgumentParser()
    # Arguments that are specific for this run (including run specific defaults, ignore unknown arguments)
    parser.add_argument("--folder", default="ens")
    parser.add_argument("--difficulty", default=10, type=int)
    parser.add_argument("--single_pol", default=True, action="store_false")
    parser.add_argument("--obstacle_type", default="high_jumps")
    parser.add_argument("--adv", default=True, action="store_false")
    parser.add_argument("--dqn", default=True, action="store_false")
    parser.add_argument("--baseline_reward", default=True, action="store_false")
    parser.add_argument("--advantage2", default=False, action="store_true")
    parser.add_argument("--use_classifier", default=False, action="store_true")
    parser.add_argument("--old_rew", default=True, action="store_false")
    parser.add_argument("--eval", default=True, action="store_false")
    parser.add_argument("--eval_first", default=False, action="store_true")
    parser.add_argument("--eval_e2e", default=False, action="store_true")
    parser.add_argument("--run_state", default="train_setup")
    parser.add_argument("--flat_setup", default=False, action="store_true")
    parser.add_argument("--just_eval", default=False, action="store_true")
    parser.add_argument("--same_value", default=False, action="store_true")
    parser.add_argument("--train_setup", default=True, action="store_false")
    parser.add_argument("--no_extra_rew", default=False, action="store_true")
    parser.add_argument("--train_select", default=True, action="store_false")
    parser.add_argument("--select_desired", default=False, action="store_true")
    parser.add_argument("--success_explore", default=False, action="store_true")
    parser.add_argument("--delay_select", default=False, action="store_true")
    parser.add_argument("--blank", default=False, action="store_true")
    parser.add_argument("--categorical", default=False, action="store_true")
    # parser.add_argument("--share_vis", default=False, action="store_true")
    parser.add_argument("--share_vis", default=True, action="store_false")
    # parser.add_argument("--no_disturbance", default=False, action="store_true")
    parser.add_argument("--no_disturbance", default=True, action="store_false")
    # parser.add_argument("--extra_on_terminate", default=False, action="store_true")
    parser.add_argument("--extra_on_terminate", default=True, action="store_false")
    parser.add_argument("--mem_test", default="small")
    # parser.add_argument("--rew", default="vpred")
    parser.add_argument("--rew", default="scaled_adv3")
    parser.add_argument("--setup_length", default=0, type=int)
    parser.add_argument("--ensemble_count", default=3, type=int)
    parser.add_argument("--fact", default=1.0, type=float)
    parser.add_argument("--max_ts", default=1e7, type=int)
    parser.add_argument("--latent_dim", default=16, type=int)
    # parser.add_argument("--max_ts", default=2e7, type=int)
    args2, unknown2 = parser.parse_known_args()
    args2 = vars(args2)
    # Replace any arguments from defaults with run specific defaults
    for key in args2:
        args1[key] = args2[key]
    # Look for any changes to defaults (unknowns) and replace values in args1
    for n, unknown in enumerate(unknown2):
        if "--" in unknown and n < len(unknown2)-1 and "--" not in unknown2[n+1]:
            arg_type = type(args1[unknown[2:]])
            args1[unknown[2:]] = arg_type(unknown2[n+1])
    args = DotMap(args1)
    # Check for dodgy arguments
    unknowns = []
    # print(unknown1)
    # print(unknown2)
    # print(args.same_value)
    for unknown in unknown1 + unknown2:
        if "--" in unknown and unknown[2:] not in args:
            unknowns.append(unknown)
    if len(unknowns) > 0:
        print("Dodgy argument")
        print(unknowns)
        exit()
    # exit()
    os.environ["CUDA_VISIBLE_DEVICES"]="-1"
    run(args)