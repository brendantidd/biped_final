##############################################
# Created from template ros2.dockerfile.jinja
##############################################

###########################################
# Base image 
###########################################
FROM ubuntu:18.04 AS base

ENV DEBIAN_FRONTEND=noninteractive

# Install language
RUN apt-get update && apt-get install -y \
  locales \
  && locale-gen en_US.UTF-8 \
  && update-locale LC_ALL=en_US.UTF-8 LANG=en_US.UTF-8 \
  && apt-get autoremove -y  && apt-get clean -y && rm -rf /var/lib/apt/lists/*

ENV LANG en_US.UTF-8

# Install timezone
RUN ln -fs /usr/share/zoneinfo/UTC /etc/localtime \
  && export DEBIAN_FRONTEND=noninteractive \
  && apt-get update && apt-get install -y \
  tzdata \
  && dpkg-reconfigure --frontend noninteractive tzdata \
  && apt-get autoremove -y  && apt-get clean -y && rm -rf /var/lib/apt/lists/*



# Install ROS2
# RUN apt-get update && apt-get install -y \
#    curl \
#    gnupg2 \
#    lsb-release \
#    sudo \
#    # && curl -sSL https://raw.githubusercontent.com/ros/rosdistro/master/ros.key -o /usr/share/keyrings/ros-archive-keyring.gpg \
#    && echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/ros-archive-keyring.gpg] http://packages.ros.org/ros2/ubuntu $(lsb_release -cs) main" | tee /etc/apt/sources.list.d/ros2.list > /dev/null \
#    && apt-get update && apt-get install -y \
#    # ros-foxy-ros-base \
#    python3-argcomplete \
#    && apt-get autoremove -y  && apt-get clean -y && rm -rf /var/lib/apt/lists/*

# ENV ROS_DISTRO=foxy
# ENV AMENT_PREFIX_PATH=/opt/ros/foxy
# ENV COLCON_PREFIX_PATH=/opt/ros/foxy
# ENV LD_LIBRARY_PATH=/opt/ros/foxy/lib
# ENV PATH=/opt/ros/foxy/bin:$PATH
# ENV PYTHONPATH=/opt/ros/foxy/lib/python3.8/site-packages
ENV ROS_PYTHON_VERSION=3
# ENV ROS_VERSION=2
ENV DEBIAN_FRONTEND=

# ###########################################
# #  Headless development 
# ###########################################
FROM base AS dev_headless

ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update && apt-get install -y \
   curl \
   gnupg2 \
   lsb-release \
   bash-completion \
   build-essential \
   cmake \
   gdb \
   git \
   pylint3 \
   python3-argcomplete \
   # python3-colcon-common-extensions \
   python3-pip \
   # python3-rosdep \
   # python3-vcstool \
   python3-argcomplete \
   vim \
   wget \
   htop \
   tmux \
   # Install ros distro testing packages
   # ros-foxy-ament-lint \
   # ros-foxy-launch-testing \
   # ros-foxy-launch-testing-ament-cmake \
   # ros-foxy-launch-testing-ros \
   # ros-foxy-xacro \
   # ros-foxy-ros2-control \
   # python3-autopep8 \
   # && rosdep init || echo "rosdep already initialized" \
   && apt-get autoremove -y  && apt-get clean -y && rm -rf /var/lib/apt/lists/*

ARG USERNAME=biped
ARG USER_UID=1000
ARG USER_GID=$USER_UID

# Create a non-root user
RUN groupadd --gid $USER_GID $USERNAME \
   && useradd -s /bin/bash --uid $USER_UID --gid $USER_GID -m $USERNAME \
   # [Optional] Add sudo support for the non-root user
   && apt-get update && apt-get install -y sudo \
   && echo $USERNAME ALL=\(root\) NOPASSWD:ALL > /etc/sudoers.d/$USERNAME\
   && chmod 0440 /etc/sudoers.d/$USERNAME \
   # Cleanup
   && rm -rf /var/lib/apt/lists/* \
   && echo "source /usr/share/bash-completion/completions/git" >> /home/$USERNAME/.bashrc \
   # && echo "if [ -f /opt/ros/${ROS_DISTRO}/setup.bash ]; then source /opt/ros/${ROS_DISTRO}/setup.bash; fi" >> /home/$USERNAME/.bashrc
ENV DEBIAN_FRONTEND=

# ENV variables only during the build process
ARG DEBIAN_FRONTEND=noninteractive

# Casadi depends https://github.com/casadi/casadi/wiki/InstallationLinux
RUN apt-get update && apt-get -y install --no-install-recommends \
   gcc \
   g++ \
   gfortran \
   git \
   cmake \
   liblapack-dev \
   pkg-config \
   coinor-libipopt-dev \
   && apt-get autoremove -y  && apt-get clean -y && rm -rf /var/lib/apt/lists/*

# # Build and install casadi
# RUN git clone https://github.com/casadi/casadi.git \
#    && cd casadi \
#    && git submodule update --init --recursive \
#    && mkdir build && cd build \
#    && cmake -DWITH_IPOPT=ON -DWITH_COMMON=ON .. \
#    && make -j$(nproc) \
#    && make install \
#    && make clean \
#    && ldconfig


# Set up auto-source of workspace for ros user
# ARG WORKSPACE
# RUN echo "if [ -f ${WORKSPACE}/install/setup.bash ]; then source ${WORKSPACE}/install/setup.bash; fi" >> /home/ros/.bashrc

# ###########################################
# #  Jetson development 
# ###########################################
# FROM dev_headless AS jetson_dev

# ###########################################
# #  GUI development 
# ###########################################
FROM dev_headless AS dev


# ROS installs
# RUN apt-get update && apt-get -y install --no-install-recommends \
#    ros-foxy-rviz2 \
#    ros-foxy-gazebo-ros-pkgs \
#    libgl1-mesa-dev \
#    && apt-get autoremove -y  && apt-get clean -y && rm -rf /var/lib/apt/lists/*

# PCAN python api
# RUN pip3 install python-can

# TODO: Remove (this is for testing)
RUN apt-get update && apt-get -y install --no-install-recommends \
   mesa-utils \
   clinfo \
   && apt-get autoremove -y  && apt-get clean -y && rm -rf /var/lib/apt/lists/*

# nvidia-container-runtime
# As documented in http://wiki.ros.org/docker/Tutorials/Hardware%20Acceleration
ENV NVIDIA_VISIBLE_DEVICES \
    ${NVIDIA_VISIBLE_DEVICES:-all}
ENV NVIDIA_DRIVER_CAPABILITIES \
    ${NVIDIA_DRIVER_CAPABILITIES:+$NVIDIA_DRIVER_CAPABILITIES,}all

# Use software rendering for video stuff
# ENV LIBGL_ALWAYS_SOFTWARE=true

# Install RL spinup stuff
# https://spinningup.openai.com/en/latest/user/installation.html#installation
RUN apt-get update && apt-get -y install --no-install-recommends \
  libopenmpi-dev \
  && apt-get autoremove -y  && apt-get clean -y && rm -rf /var/lib/apt/lists/*

RUN apt-get update && apt-get -y install --no-install-recommends \
   libgtk2.0-dev \
   pkg-config \ 
   && apt-get autoremove -y  && apt-get clean -y && rm -rf /var/lib/apt/lists/*


# pip install 

ADD https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh /tmp/
COPY sha256sums.txt /tmp/.
RUN cd /tmp \
  && sha256sum -c sha256sums.txt \   
  && chmod +x Miniconda3-latest-Linux-x86_64.sh \
  && chown biped:biped Miniconda3-latest-Linux-x86_64.sh
  
# USER ros
USER biped
# Use miniconda to manage the python environments
RUN cd /tmp && ls -lah\
  && /tmp/Miniconda3-latest-Linux-x86_64.sh -b -p /home/biped/miniconda3
# Setup the conda paths for the biped user and create the environment
RUN eval "$(/home/biped/miniconda3/bin/conda shell.bash hook)" \
  && conda init \
  && conda config --set auto_activate_base false \
  && conda create --name biped python=3.6 \
  && conda install -y pip 
  # && cd $HOME \
  # && git clone https://github.com/openai/spinningup.git \
  #  && cd spinningup \
  #  && conda activate biped \
  #  && pip install -e .

# Yeah I know we already got pybullet but we need it this time in the python 3.6 environment.
RUN eval "$(/home/biped/miniconda3/bin/conda shell.bash hook)" \
  && conda activate biped \
  && conda install tensorboardX \
  && pip install pybullet \
  && pip install tensorflow==1.14 \
  && cd $HOME \
  && git clone https://github.com/openai/baselines.git \
  && cd baselines \
  && pip install -e .
 
RUN eval "$(/home/biped/miniconda3/bin/conda shell.bash hook)" \
  && conda activate biped \
  && pip install git-python \
  && pip install dotmap \
  && pip install matplotlib \
  && pip install pandas \
  && pip install mpi4py 
  # && pip install numpy \
  
# RUN pip3 install pandas mpi4py numpy==1.16 git-python dotmap 

#  RUN pip3 install pybullet pyserial tensorflow==1.14 tensorboardX 
# RUN pip3 install scikit-build 
# RUN pip3 install opencv-python matplotlib

# # Build and install baselines
# RUN git clone https://github.com/openai/baselines.git \
#    && cd baselines \
#    && pip3 install -e .


