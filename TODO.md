### IROS switch paper: ###
- Verify code works by training with current method
- test switch estimator by always running base policy first
- train new set of policies without RoA 
    - No pause time, smaller run up to terrain
    - get data and train switch estimator
    - plot of roa
- train e2e and dqn and get wall clock times
    - get wall clock time for training a single policy and switch estimator
- Other comments:
    -  average torque


### IROS titan paper: ###



This is your final paper, there's going to be a lot in there.
One class classification for open set modular RL for complex terrain traversal
- static and dynamic robot: biped and titan
- Comparisons: 
    - OCGAN
    - Dim's paper
    - Supervised learning
    - RL
    - end-to-end, DQN
- Require setup policies 
- New behaviours? Single terrains (this will help when you need to train switching policies)

KEEP THIS CODE CLEAN

Step 1.
- Make sure curriculum still works
- Train 5 terrain types on single terrains: gaps, hurdles, stairs, steps, high_jumps. 
    - Stop before, stop after.
- Train setup policies for the single terrains (make sure still works)

Step 2.
- Train terrain classifier from single terrain
    - Verify classifier trains (MNIST?)

TODO:
- KEEP CODE CLEAN
- clean up terrain detection
- re-train low level, single policies
- re-train setup policies
- re-train switch policies (for thesis)
- train classifier online:
    - RL
    - Supervised
    - OCGAN
    - Dim's distance thing
- Titan! Should be able simple adjustments to titan_rl
- Other terrains: steps with gaps, inverse steps, steering (avoiding obstacles), ducking, narrow windy path (one foot width)
- can still have 2 terrains, as long as they are close
- curriculum for step initial y?
- Kp either finer, or more overlap?
- x force keeps com over feet, but tries to push forward

Prioritise:
- Classifier online
- Low level policies: start close, no stop bit (reward for no velocity until something happens), cross the terrain, reward for no velocity.
- Two settings: multi_train (leave for setup/switch), and something else (single_train)?


How the code works:
- everything runs from run.py, there are 3 levels, single policy, switch/select, and multi:
    - no arguments - single terrain, single policy. 
        - Starting location: if args.single: num_artifacts == 1, start close to artifact (small RoA).
        - Once over, stop. By reward, no control bit.
        - set original_speed = 0, but don't have it in the policy
    - setup, or switch: train setup or switch policy
        - These start from single_pol
        - Once over, switch back to flat policy? or stop
    - classifier: use setup or switch (arguments use_setup or use_switch) to train classifier
        - These start from single_pol
    - multi: use setup or switch (arguments use_setup or use_switch) on multi terrain
    - dqn: train dqn on multi
    - e2e: train e2e policy on multi