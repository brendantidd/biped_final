import matplotlib.pyplot as plt

class Plots():
    def __init__(self, PATH, plot_dict=["current_pol", "terrain"], plot_freq=10):
        self.PATH = PATH
        self.plot_dict = plot_dict
        self.plot_freq = plot_freq
        self.reset()

    def reset(self):
        self.data = {}
        for dl in self.plot_dict:
            if type(self.plot_dict[dl]) == dict:
                self.data[dl] = {}
                for d in self.plot_dict[dl]:
                    self.data[dl][d] = []
            else:
                self.data[dl] = []

    def update(self, data):
        for dl in self.data:
            if type(self.data[dl]) == dict:
#                 print(self.data[dl])
                for d in self.data[dl]:
#                     print(self.data[dl], d)
                    self.data[dl][d].append(data[dl][d])
            else:
                self.data[dl].append(data[dl])

    def plot(self, ep_count):
        if ep_count % self.plot_freq == 0:
        # if args.plot or (rank == 0 and ep_count % 100 == 0) or label:
            num_axes = len(self.data)
            fig, axes = plt.subplots(num_axes, figsize=(10, 3*num_axes))
            for i,dl in enumerate(self.data):
                if type(self.data[dl]) == dict:
                    for d in self.data[dl]:
                        axes[i].plot([_ for _ in range(len(self.data[dl][d]))], self.data[dl][d], alpha=1.0)    
                        # axes[i].plot([_ for _ in range(len(self.data[dl]))], self.data[dl], alpha=1.0)    
                else:
                    axes[i].plot([_ for _ in range(len(self.data[dl]))], self.data[dl], alpha=1.0)    
            plt.savefig(self.PATH + "ep" + str(ep_count) + ".png", bbox_inches="tight")
            plt.close()
        self.reset()


        #     axes[0].plot([_ for _ in range(len(vpred_data))], vpred_data, alpha=1.0)    
            
        #     colours = [np.array([0, 102, 204])/255.0, np.array([0, 102, 204])/255.0, np.array([255,165,0])/255.0, np.array([51, 204, 51])/255.0, np.array([100, 204, 0])/255.0, np.array([200, 100, 51])/255.0]
            
        #     # print(np.array(current_pol_data).shape, len(current_pol_data))
        #     axes[0].plot([_ for _ in range(len(current_pol_data))], current_pol_data, c=np.array([0, 102, 204])/255.0, alpha=1.0)    
        #     axes[0].plot([_ for _ in range(len(baseline_data))], baseline_data, c=np.array([0, 204, 102])/255.0, alpha=1.0)    
        #     axes[0].set_ylim([-0.9,len(obstacle_types) + 0.1])
        #     for s, n in enumerate(obstacle_types):
        #         axes[0].text(0,s*0.5+0.5,str(s) + ". " + n)
        #     axes[0].set_title("Current policy", loc='left')
        #     axes[0].legend(['current', 'baseline'])

        #     # axes[1].set_ylim([-0.9,len(obstacle_types) + 0.1])
        #     # for s, n in enumerate(obstacle_types):
        #     #     axes[1].text(0,s*0.5+0.5,str(s) + ". " + n)
        #     # axes[1].set_title("baseline data", loc='left')

        #     axes[1].plot([_ for _ in range(len(select_raw_data))], select_raw_data, c=np.array([0, 102, 204])/255.0, alpha=1.0)    
        #     # axes[2].set_ylim([-0.9, 1.1])
        #     axes[1].set_title("select data", loc='left')


        #     axes[2].plot([_ for _ in range(len(setup_rew_data))], setup_rew_data, c=np.array([0, 102, 204])/255.0, alpha=1.0)    
        #     # axes[2].set_ylim([-0.9, 1.1])
        #     axes[2].set_title("select data", loc='left')

        #     axes[3].plot([_ for _ in range(len(adv_data))], adv_data, c=np.array([0, 102, 204])/255.0, alpha=1.0)    
        #     # axes[2].set_ylim([-0.9, 1.1])
        #     axes[3].set_title("select data", loc='left')

        #     axes[-1].plot([_ for _ in range(len(terrain_data))], terrain_data, c=np.array([0, 102, 204])/255.0, alpha=1.0)    
        #     axes[-1].set_title("terrain height", loc='left')
        #     fig.tight_layout(pad=4.0)
        #     if label:
        #         # print(PATH + str(rank) + '_success.png')
        #         plt.savefig(PATH + str(rank) + '_success.png', bbox_inches='tight')
        #     else:
        #         # print(PATH + 'ep.png')
        #         plt.savefig(PATH + 'ep.png', bbox_inches='tight')
        #     plt.close()
        # self.reset()

