import os, inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
os.environ["TF_CPP_MIN_LOG_LEVEL"] = "3" 
import tensorflow as tf
tf.get_logger().setLevel("DEBUG")
import numpy as np
import argparse
import tensorboardX
from collections import deque   
from scripts.utils import *
from scripts.mpi_utils import sync_from_root
import random
from mpi4py import MPI
import time
from pathlib import Path
home = str(Path.home())
from scripts import logger
import json
import git
import pybullet as p
from plots import Plots
np.set_printoptions(precision=3, suppress=True)

def run(args):

    PATH = home + "/results/biped_final/latest/" + args.folder + "/" + args.exp + "/"

    comm = MPI.COMM_WORLD
    rank = comm.Get_rank()
    myseed = args.seed + 10000 * rank
    np.random.seed(myseed)
    random.seed(myseed)
    tf.set_random_seed(myseed)
    
    logger.configure(dir=PATH)
    if rank == 0:
        writer = tensorboardX.SummaryWriter(log_dir=PATH)
        repo = git.Repo(search_parent_directories=True)
        sha = repo.head.object.hexsha
        with open(PATH + "commandline_args.txt", "w") as f:
            f.write("Hash:")
            f.write(str(sha) + "\n")
            json.dump(args.__dict__, f, indent=2)
    else: 
            writer = None 

    gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction= 0.1)
    sess = tf.InteractiveSession(config=tf.ConfigProto(inter_op_parallelism_threads=1,
                                            intra_op_parallelism_threads=1,             
                                            gpu_options=gpu_options), graph=None)
    
    horizon = 2048

    if args.old_env or args.run_state in ["run_setup","train_setup","run_switch","train_switch","train_admis","run_admis","multi"]:
        from assets.env_pb_biped_old import Env
    else:
        from assets.env_pb_biped import Env

    env = Env(PATH=PATH, args=args)

    if not args.run_state in ["run_single", "train_single"]:
        # plots = Plots(PATH, plot_list=["current_pol","terrain","term"] , plot_freq=args.plot_freq)
        plots = Plots(PATH, plot_dict={"current_pol":[],"terrain":{"z":[],"robot_z":[]},"term":[]}, plot_freq=args.plot_freq)

    from models.ppo import Model
    if args.run_state in ["run_setup","train_setup","run_switch","train_switch","train_admis","run_admis"]:
        env.obstacle_types = ["high_jumps"]
        if args.run_state in ["train_setup","train_switch"]:
            obstacle_types = [args.obstacle_type, "flat"]  
            if "setup" in args.run_state:
                pol = Model(args.obstacle_type + "_setup", env=env, ob_size=env.ob_size, ac_size=env.ac_size, im_size=env.im_size, args=args, PATH=PATH, horizon=horizon, writer=writer, max_timesteps=args.max_ts, vis=args.vis, const_std=args.const_std)
            elif "switch" in args.run_state:
                pol = Model(args.obstacle_type + "_switch", env=env, ob_size=env.ob_size, ac_size=env.ac_size, im_size=env.im_size, args=args, PATH=PATH, horizon=horizon, writer=writer, max_timesteps=args.max_ts, vis=args.vis, const_std=args.const_std)
            
        else:
            if "admis" in args.run_state:
                # print("IMPORTING admis")
                # from models import admis
                from models import ppo_ae
                pol = ppo_ae.Model(args.obstacle_type + "_admis", env=env, ob_size=env.ob_size, ac_size=1, im_size=env.im_size, args=args, PATH=PATH, horizon=horizon, writer=writer, max_timesteps=args.max_ts, vis=args.vis, const_std=args.const_std)
            obstacle_types = [args.obstacle_type, args.obstacle_type + "_setup", "flat"]  
        all_pols = {name:Model(name, env=env, ob_size=env.ob_size, ac_size=env.ac_size, im_size=env.im_size, args=args, PATH=PATH, horizon=horizon, writer=writer, max_timesteps=args.max_ts, vis=args.vis, const_std=args.const_std) for name in obstacle_types}    
    elif args.run_state in ["multi"]:
        env.args.obstacle_type = "mix"
        env.obstacle_types = ["flat","jumps","gaps","stairs","steps","high_jumps"]
        obstacle_types = []
        for ob in env.obstacle_types:
            obstacle_types.append(ob)
            if ob is not "flat":
                obstacle_types.append(ob + "_setup")
        all_pols = {name:Model(name, env=env, ob_size=env.ob_size, ac_size=env.ac_size, im_size=env.im_size, args=args, PATH=PATH, horizon=horizon, writer=writer, max_timesteps=args.max_ts, vis=args.vis, const_std=args.const_std) for name in obstacle_types}    
        # pol = Model("gan", env=env, ob_size=env.ob_size, ac_size=env.ac_size, im_size=env.im_size, args=args, PATH=PATH, horizon=horizon, writer=writer, max_timesteps=args.max_ts, vis=args.vis, const_std=args.const_std)
    else:
        # env.args.difficulty = 1
        # env.args.num_artifacts = 1
        env.obstacle_types = [args.obstacle_type]
        pol = Model(args.obstacle_type, env=env, ob_size=env.ob_size, ac_size=env.ac_size, im_size=env.im_size, args=args, PATH=PATH, horizon=horizon, writer=writer, max_timesteps=args.max_ts, vis=args.vis, const_std=args.const_std)
    
    initialize_uninitialized()
    
    if not args.run_state in ["run_single","run_setup","run_switch","run_admis","multi"]:
        sync_from_root(sess, pol.vars, comm=comm)
        pol.set_training_params(max_timesteps=args.max_ts, learning_rate=args.lr, horizon=horizon)

    if args.run_state in ["run_setup","train_setup","run_switch","train_switch","multi","run_admis","train_admis"]:
        for ob in obstacle_types:
            # if "setup" in ob:
            #     all_pols[ob].load("./weights/" + ob + "/")
            # else:
            all_pols[ob].load_pol(ob, "./weights/" + ob + "/")

    # if args.test_pol:
    if "run_single" in args.run_state:
        if args.hpc:
            pol.load(home + "/hpc-home/results/biped_final/latest/" + args.folder + "/" + args.exp + "/")      
        else:
            # pol.load(home + "/results/biped_final/latest/" + args.folder + "/" + args.exp + "/")      
            # pol.load("./weights/" + args.obstacle_type + "/")      
            # pol.load("/home/brendan/Dropbox/robot/mybot_ws/src/biped_final/weights/" + args.obstacle_type + "/")      
            pol.load_pol(args.obstacle_type, "./weights/" + args.obstacle_type + "/")

    # Throw an error if the graph grows (shouldn"t change once everything is initialised)
    tf.get_default_graph().finalize()
    
    prev_done = True

    if args.run_state in ["run_setup","train_setup","run_switch","train_switch","multi","run_admis","train_admis"]:
        current_base_pol = next_base_pol = current_pol = "flat"
    env.set_current_pol(current_pol)
    ob = env.reset()
    im = env.get_im()
    # im = 1.0 - im 

    if args.run_state in ["run_admis", "train_admis"]:  
        env.set_images(im, im)
    ep_ret = 0
    ep_len = 0
    ep_rets = []
    ep_lens = []
    ep_steps = 0
    ep_count = 0

    if "run" in args.run_state:
        stochastic = False
    else:
        stochastic = True
    select_output = [0]
    
    # prev_artifact = artifact = None
    box_cross_steps = None
    while True: 
        if not args.run_state in ["run_single","run_setup","run_switch","run_admis","multi"]:
            if pol.timesteps_so_far > pol.max_timesteps:
                break 
        # print(current_pol)
        if args.run_state in ["run_setup","train_setup","run_switch","train_switch","multi","run_admis","train_admis"]:
            if "setup" in current_pol:
                if args.run_state in ["train_setup"]:
                    act, vpred, _, nlogp, select_output, nlogp_select = pol.step(ob, im, stochastic=True)
                else:
                    act, vpred, _, nlogp, select_output, _ = all_pols[current_pol].step(ob, im, stochastic=False)
                # terminate_setup = select_output[0] > 3
                terminate_setup = select_output[0] > 2
                # print(select_output[0])
            else:
                act, vpred, _, nlogp = all_pols[current_pol].step(ob, im, stochastic=False)
        
        if args.run_state in ["run_admis", "train_admis"]:
            adm, vpred, _, nlogp, recon_im = pol.step(ob, im, stochastic=stochastic)
            # else:
            #     adm, vpred, _, nlogp = pol.step(ob, im, stochastic=stochastic)
            # admissible = adm[0] > 3
            admissible = adm[0] > 0
            # print(admissible, adm[0])
            # current_base_pol = classification[0] > 3
        elif args.run_state in ["run_single","train_single"]:
            act, vpred, _, nlogp = pol.step(ob, im, stochastic=stochastic)

        # print(act, current_pol)
        torques = act

        next_ob, rew, done, _ = env.step(torques)
        next_im = env.get_im()
        # next_im = 1.0 - next_im 

        if args.run_state in ["run_admis", "train_admis"]:
            env.set_images(next_im, recon_im)

        # ========================================================================================================
        # Terrain detection stuff: TODO clean up!!
        # ========================================================================================================
        if args.run_state in ["train_admis"]:
            if admissible:
                if "flat" in current_pol:
                    current_pol = args.obstacle_type + "_setup"
                elif "setup" in current_pol and terminate_setup:
                    current_pol = args.obstacle_type
            else:
                current_pol = "flat"
                
        elif args.run_state in ["run_setup","train_setup","run_switch","train_switch","run_admis","train_admis","multi"]:
            # if artifact is None and env.order[env.box_num] != "flat":
                # prev_artifact = artifact = env.order[env.box_num]
                # prev_artifact = artifact = next_pol = env.get_next_pol()
            if args.run_state in ["run_admis", "train_admis"]:
                if admissible:
                    next_base_pol = args.obstacle_type
                else:
                    next_base_pol = "flat"
            else:
                next_base_pol = env.get_terrain()

            # if (current_pol == "high_jumps" and env.order[env.box_num] == "flat") or (current_pol != "high_jumps" and env.order[env.box_num] != "flat"):
            #     artifact = env.order[env.box_num]

            # if artifact is not None:
            #     admis_current_pol = artifact
            # else:
            #     admis_current_pol = "flat"
            # current_base_pol = admis_current_pol
            if next_base_pol != current_base_pol and "setup" not in current_pol:
            # if current_base_pol not in current_pol and "setup" not in current_pol:
                if (current_pol == "high_jumps" and env.x_min > (env.box_info[1][env.box_num][0] - env.box_info[2][env.box_num][0]) and (not env.ob_dict["left_foot_left_ground"] and not env.ob_dict["right_foot_left_ground"]) and env.vx > 0) or (current_pol != "high_jumps"):
                    if "flat" in next_base_pol:
                        current_pol = current_base_pol = next_base_pol
                    else:
                        current_pol = current_base_pol = next_base_pol + "_setup"
                    terminate_setup = False
                    box_cross_steps = env.steps
            elif "setup" in current_pol and (terminate_setup or (box_cross_steps and env.steps - box_cross_steps > 75)):
                # print("current_pol ", current_pol, " timeout", env.steps - box_cross_steps > 75, "terminate", terminate_setup)
                current_pol = current_base_pol = next_base_pol
                box_cross_steps = None

        # ========================================================================================================
        # This is necessary to set the desired speed, new policies will fix this
        if args.old_env and args.multi:
            if 'high_jumps' in current_pol:
                env.obstacle_type = 'high_jumps'
            else:
                env.obstacle_type = 'mix'
                env.original_speed = 1.0
        # ========================================================================================================

            # if args.no_setup and "setup" in current_pol:
            #     current_pol = current_base_pol
            # if args.no_flat_setup and current_pol == "flat_setup":
            #     current_pol = "flat"

            # next_pol = env.get_next_pol()
            # # if current_base_pol not in current_pol and "setup" not in current_pol:

            # if next_pol not in current_pol:
            #     current_pol = next_pol + "_setup"
            # elif "setup" in current_pol and terminate_setup:nd "setup" in current_pol:
            #     current_pol = current_base_pol
            # if args.no_flat_setup and current_pol == "flat_setup":
            #     current_pol = 
            #     current_pol = next_pol
            # print(current_pol)
        # ========================================================================================================

        if not args.run_state in ["run_single","run_setup","run_switch","run_admis","multi"]:
            if args.run_state in ["train_setup"]:
                pol.add_to_buffer([ob, im, act, select_output, setup_rew, prev_setup_done, setup_vpred, nlogp, nlogp_select])
            elif args.run_state in ["train_admis"]:
                if env.get_terrain("current") == args.obstacle_type or (env.get_terrain("previous") == args.obstacle_type and env.get_terrain("current") == "flat" and env.x_min > (env.box_info[1][env.box_num][0] - env.box_info[2][env.box_num][0]) and (not env.ob_dict["right_foot_left_ground"] and not env.ob_dict["left_foot_left_ground"])):
                    # rew = np.exp(-(1 - np.clip(adm[0], -1, 1))**2)
                    # admis_rew = 1.0 - 1.0*(np.clip(adm[0], -1, 1) - 3.0)**2
                    rew = 1.0 - (np.clip((adm[0] - 3.0)**2, 0, 1))
                    # rew = np.exp(-(adm[0] - 5.0)**2)
                else:
                    # admis_rew = 1.0 - 1.0*(adm[0] - -3.0)**2
                    # rew = np.exp(-(0 - adm[0] > 3)**2)
                    # reward = 1.0 - 1.0*(run_pol - -1.0)**2
                    rew = 1.0 - (np.clip((adm[0] - -3.0)**2, 0, 1))

                    # rew = np.exp(-(adm[0] - -5.0)**2)
                # print(rew)
                # print("this is right")

                pol.add_to_buffer([ob, im, adm, rew, prev_done, vpred, nlogp])
            else:
                pol.add_to_buffer([ob, im, act, rew, prev_done, vpred, nlogp])
        prev_done = done
        ob = next_ob
        im = next_im
        ep_ret += rew
        ep_len += 1
        ep_steps += 1
        if not args.run_state in ["run_single","run_setup","run_switch","run_admis","multi"] and ep_steps % horizon == 0:
            if args.run_state in ["run_admis", "train_admis"]:
                _, vpred, _, _, _ = pol.step(next_ob, next_im, stochastic=True)
            else:
                _, vpred, _, _ = pol.step(next_ob, next_im, stochastic=True)
            pol.run_train(data={"ep_rets":ep_rets, "ep_lens":ep_lens}, last_value=vpred, last_done=done)
            ep_rets = []
            ep_lens = []
        
        # plots.update({"current_pol": obstacle_types.index(current_pol), "terrain": terrain})
        if rank == 0 and not args.run_state in ["run_single", "train_single"]:
            # plots.update({"current_pol": obstacle_types.index(current_pol), "terrain": {"z_offset":env.z_offset + env.body_xyz[2], "terrain_z":env.box_info[1][env.box_num][2] + env.box_info[2][env.box_num][2]}})
            # plots.update({"current_pol": obstacle_types.index(current_pol), "terrain": env.z_offset + env.body_xyz[2], "term":select_output[0]})
            # plots.update({"current_pol": obstacle_types.index(current_pol), "terrain": env.z_offset + env.body_xyz[2], "term":select_output[0]})
            plots.update({"current_pol":obstacle_types.index(current_pol),"terrain":{"z":env.box_info[1][env.box_num][2] + env.box_info[2][env.box_num][2],"robot_z":env.body_xyz[2]},"term":select_output[0]})
        env.set_current_pol(current_pol)

        if done:
            if not args.run_state in ["run_single", "train_single"] and rank == 0:
                plots.plot(ep_count)

            if args.run_state in ["run_setup","train_setup","run_switch","train_switch","multi","run_admis","train_admis"]:
                current_base_pol = next_base_pol = current_pol = "flat"
            env.set_current_pol(current_pol)
            ob = env.reset()
            im = env.get_im()
            # im = 1.0 - im 

            ep_rets.append(ep_ret)  
            ep_lens.append(ep_len)     
            ep_ret = 0
            ep_len = 0
            ep_count += 1        
            # prev_artifact = artifact = None
            box_cross_steps = None
            select_output = [0]

if __name__ == "__main__":
    # Hack for loading defaults, but still accepting run specific defaults
    import defaults
    from dotmap import DotMap
    args1, unknown1 = defaults.get_defaults() 
    parser = argparse.ArgumentParser()
    # Arguments that are specific for this run (including run specific defaults, ignore unknown arguments)
    parser.add_argument("--folder", default="b")
    parser.add_argument("--difficulty", default=10, type=int)
    parser.add_argument("--obstacle_type", default="gaps", help="Obstacle types are: flat, gaps, jumps, stairs, steps, high_jumps, one_leg_hop, hard_steps, hard_high_jumps, or mix (for a combination)")
    parser.add_argument("--sim_type", default="pb", help="pb or gz (pybullet or gazebo)")
    parser.add_argument("--robot", default="biped", help="biped or hexapod")
    parser.add_argument("--alg", default="ppo", help="ppo, eventually sac as well") 
    parser.add_argument("--setup", default=False, action="store_true")
    parser.add_argument("--no_flat_setup", default=True, action="store_false")
    parser.add_argument("--advantage2", default=False, action="store_true")
    parser.add_argument("--multi", default=False, action="store_true")
    parser.add_argument("--admis_type", default="RL")
    parser.add_argument("--share_vis", default=True, action="store_false")
    parser.add_argument("--train_vae", default=True, action="store_false")
    parser.add_argument("--debug", default=True, action="store_false")
    parser.add_argument("--run_state", default="train_single", help="train_single, run_single, train_setup, run_setup, train_switch, run_switch, train_admis, run_admis, multi")
    parser.add_argument("--old_env", default=False, action="store_true")
    parser.add_argument("--num_artifacts", default=1, type=int)
    parser.add_argument("--ent_coef", default=0.0, type=float)
    # parser.add_argument("--std_from_state", default=False, action="store_true")
    parser.add_argument("--std_from_state", default=True, action="store_false")
    parser.add_argument("--plot_freq", default=20, type=int)
    args2, unknown2 = parser.parse_known_args()
    args2 = vars(args2)
    # Replace any arguments from defaults with run specific defaults
    for key in args2:
        args1[key] = args2[key]
    # Look for any changes to defaults (unknowns) and replace values in args1
    for n, unknown in enumerate(unknown2):
        if "--" in unknown and n < len(unknown2)-1 and "--" not in unknown2[n+1]:
            arg_type = type(args1[unknown[2:]])
            args1[unknown[2:]] = arg_type(unknown2[n+1])
    args = DotMap(args1)
    # Check for dodgy arguments
    unknowns = []
    for unknown in unknown1 + unknown2:
        if "--" in unknown and unknown[2:] not in args:
            unknowns.append(unknown)
    if len(unknowns) > 0:
        print("Dodgy argument")
        print(unknowns)
        exit()

    os.environ["CUDA_VISIBLE_DEVICES"]="-1"
    run(args)
