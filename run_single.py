import os, inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
os.environ["TF_CPP_MIN_LOG_LEVEL"] = "3" 
import tensorflow as tf
tf.get_logger().setLevel("DEBUG")
import numpy as np
import argparse
import tensorboardX
from collections import deque   
from scripts.utils import *
from scripts.mpi_utils import sync_from_root
import random
from mpi4py import MPI
import time
from pathlib import Path
home = str(Path.home())
from scripts import logger
import json
import git
np.set_printoptions(precision=3, suppress=True)
from six.moves import shlex_quote

def run(args):

    PATH = home + "/results/biped_final/latest/" + args.folder + "/" + args.exp + "/"

    comm = MPI.COMM_WORLD
    rank = comm.Get_rank()
    myseed = args.seed + 10000 * rank
    np.random.seed(myseed)
    random.seed(myseed)
    tf.set_random_seed(myseed)
    
    logger.configure(dir=PATH)
    if rank == 0:
        writer = tensorboardX.SummaryWriter(log_dir=PATH)
        repo = git.Repo(search_parent_directories=True)
        sha = repo.head.object.hexsha
        with open(PATH + "commandline_args.txt", "w") as f:
            f.write("Hash:")
            f.write(str(sha) + "\n")
            json.dump(args.__dict__, f, indent=2)
        print("Save git commit and diff to {}/git.txt".format(PATH))
        cmds = ["echo `git rev-parse HEAD` >> {}".format(
                        shlex_quote(os.path.join(PATH, "git.txt"))),
                        "git diff >> {}".format(
                        shlex_quote(os.path.join(PATH, "git.txt")))]
        os.system("\n".join(cmds))
    else: 
            writer = None 

    # sess = tf.Session()
    gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction= 0.1)
    sess = tf.InteractiveSession(config=tf.ConfigProto(inter_op_parallelism_threads=1,
                                            intra_op_parallelism_threads=1,             
                                            gpu_options=gpu_options), graph=None)
    
    horizon = 2048

    # if args.train_setup:
    obstacle_types = [args.obstacle_type, "flat"]
    obs_except_flat = [args.obstacle_type]

    from assets.env_pb_biped_setup1 import Env
    env = Env(render=args.render, PATH=PATH, args=args, display_im=args.display_im, obstacle_type=args.obstacle_type, obstacle_types=obs_except_flat, vis=args.vis, doa=args.doa, disturbances=True, initial_disturbance=args.initial_disturbance, final_disturbance=args.final_disturbance, multi=args.multi, dqn=True, difficulty=args.difficulty, cur=args.cur)

    # from assets.env_pb_biped_setup2 import Env
    # env = Env(render=args.render, PATH=PATH, args=args, cur=args.cur, obstacle_type=args.obstacle_type, control_type=args.control_type, display_im=args.display_im, vis=args.vis, speed_cur=args.speed_cur, comparison=args.comparison, initial_disturbance=args.initial_disturbance, final_disturbance=args.final_disturbance, vis_type=args.vis_type, difficulty=args.difficulty, num_artifacts=args.num_artifacts, early_stop=args.early_stop)
      
    if args.train_setup and args.obstacle_type == "high_jumps":
        env.obstacle_type = "flat"

    # else:
    #     from assets.env_pb_biped import Env
    #     env = Env(PATH=PATH, args=args)

    from models.ppo import Model

    pol = Model(args.obstacle_type, env=env, ob_size=env.ob_size, ac_size=env.ac_size, im_size=env.im_size, args=args, PATH=PATH, horizon=horizon, writer=writer, max_timesteps=args.max_ts, vis=args.vis, const_std=args.const_std)
    
    # initialize()
    initialize_uninitialized()
    sync_from_root(sess, pol.vars, comm=comm)
    pol.set_training_params(max_timesteps=args.max_ts, learning_rate=args.lr, horizon=horizon)

    if args.test_pol or args.fine_tune:
        if args.hpc:
            pol.load(home + "/hpc-home/results/biped_final/latest/" + args.folder + "/" + args.exp + "/")      
        else:
            # pol.load(home + "/results/biped_final/latest/" + args.folder + "/" + args.exp + "/")      
            pol.load(home + "/results/biped_final/latest/single/hj_1/")      
            # pol.load("./weights/" + args.obstacle_type + "/")      
            # pol.load("/home/brendan/Dropbox/robot/mybot_ws/src/biped_final/weights/" + args.obstacle_type + "/")      
            # pol.load_pol(args.obstacle_type, "./weights/" + args.obstacle_type + "/")

    # Throw an error if the graph grows (shouldn"t change once everything is initialised)
    tf.get_default_graph().finalize()
    
    prev_done = True
    ob = env.reset(cur_params=None)
    im = env.get_im()
    ep_ret = 0
    ep_len = 0
    ep_rets = []
    ep_lens = []
    ep_steps = 0

    if args.test_pol:
        stochastic = False
    else:
        stochastic = True
    
    # eval_success = []
    # eval_distance = []
    # eval_lengths = []
    # eval_single_success = []
    # eval_step = 0
    # if args.eval_only:
    #     eval_next = True
    #     t_eval = time.time()
    #     evaluate = True
    #     stochastic = False
    # else:
    #     eval_next = False
    #     evaluate = False
    #     stochastic = True
    #     if rank == 0:
    #         pol.writer.add_scalar("Eval_success", 0, 0)
    #         pol.writer.add_scalar("Eval_distance", 0, 0)
    #         pol.writer.add_scalar("Eval_lengths", 0, 0)
    if args.train_setup:
        env.obstacle_type = "flat"

    while True: 
        if pol.timesteps_so_far > pol.max_timesteps:
            break 

        # if eval_next and ep_steps % 1000 == 0:
        #     all_success = MPI.COMM_WORLD.allgather(eval_success)
        #     all_success = [h for d in all_success for h in d]
        #     all_distance = MPI.COMM_WORLD.allgather(eval_distance)
        #     all_distance = [h for d in all_distance for h in d]
        #     all_lengths = MPI.COMM_WORLD.allgather(eval_lengths)
        #     all_lengths = [h for d in all_lengths for h in d]
        #     all_single_success = MPI.COMM_WORLD.allgather(eval_single_success)
        #     all_single_success = [h for d in all_single_success for h in d]
        #     if len(all_success) > 32:
        #         if rank == 0:
        #             if not args.eval_e2e:
        #                 pol.writer.add_scalar("Eval_success", np.mean(all_success), pol.iters_so_far)
        #                 pol.writer.add_scalar("Eval_distance", np.mean(all_distance), pol.iters_so_far)
        #                 pol.writer.add_scalar("Eval_lengths", np.mean(all_lengths), pol.iters_so_far)
        #                 pol.writer.add_scalar("Eval_single_success", np.mean(all_single_success), pol.iters_so_far)
        #             print("resuming training", len(all_success), "evaluation took ", time.time() - t_eval, "eval success", np.mean(all_success), "eval distance", np.mean(all_distance), "lengths", np.mean(all_lengths))
        #         if not args.eval_only:
        #             eval_next = False

        act, vpred, _, nlogp = pol.step(ob, im, stochastic=stochastic)

        torques = act

        next_ob, rew, done, _ = env.step(torques)
        next_im = env.get_im()

        # if args.train_setup:
        #     env.obstacle_type = env.order[env.box_num] if env.order[env.box_num] != "zero" else "flat"

        if not args.test_pol:
            pol.add_to_buffer([ob, im, act, rew, prev_done, vpred, nlogp])
        prev_done = done
        ob = next_ob
        im = next_im
        ep_ret += rew
        ep_len += 1
        ep_steps += 1
        
        if not args.test_pol and ep_steps % horizon == 0:
            _, vpred, _, _ = pol.step(next_ob, next_im, stochastic=True)
            pol.run_train(data={"ep_rets":ep_rets, "ep_lens":ep_lens}, last_value=vpred, last_done=done)
            ep_rets = []
            ep_lens = []
        
        # if args.eval and not eval_next and pol.iters_so_far % 10 == 0 and pol.iters_so_far != 0 and eval_step != pol.iters_so_far: 
        #     eval_step = pol.iters_so_far
        #     eval_next = True
        #     eval_success = []
        #     eval_distance = []
        #     eval_lengths = []
        #     eval_single_success = []
        #     t_eval = time.time()

        if done:
            # if evaluate:
            #     single_success = False
            #     label = False
            #     if env.difficulty == 10:
            #         if env.box_num > (len(env.order) - 3):
            #             label = True
            #         first_terrain = False                            
            #         for i in env.order:
            #             if i != "flat":
            #                 first_terrain = True
            #             if i == "flat" and first_terrain:
            #                 single_success = True
            #                 break
            #     eval_success.append(label)
            #     eval_distance.append(env.x_min/(env.box_info[1][-1][0] - env.box_info[2][-1][0]))
            #     eval_lengths.append(ep_len)
            #     eval_single_success.append(single_success)


            ob = env.reset()
            im = env.get_im()

            # if not evaluate:
            ep_rets.append(ep_ret)  
            ep_lens.append(ep_len)     
            ep_ret = 0
            ep_len = 0        

            # if eval_next or args.eval_only:
            #     evaluate = True
            #     stochastic = False
            # else:
            #     evaluate = False
            #     stochastic = True            




if __name__ == "__main__":
    # Hack for loading defaults, but still accepting run specific defaults
    import defaults
    from dotmap import DotMap
    args1, unknown1 = defaults.get_defaults() 
    parser = argparse.ArgumentParser()
    # Arguments that are specific for this run (including run specific defaults, ignore unknown arguments)
    parser.add_argument("--longer", default=False, action="store_true")
    parser.add_argument("--fewer_start", default=False, action="store_true")
    parser.add_argument("--ten", default=False, action="store_true")
    parser.add_argument("--new_cur", default=False, action="store_true")
    parser.add_argument("--wider", default=False, action="store_true")
    parser.add_argument("--one_jump", default=False, action="store_true")
    parser.add_argument("--no_base", default=False, action="store_true")
    parser.add_argument("--no_start", default=False, action="store_true")
    parser.add_argument("--short", default=False, action="store_true")
    parser.add_argument("--mean_kp", default=False, action="store_true")
    parser.add_argument("--broken", default=False, action="store_true")
    parser.add_argument("--decay_clip", default=False, action="store_true")
    parser.add_argument("--set_done", default=False, action="store_true")
    parser.add_argument("--kp_first", default=False, action="store_true")
    parser.add_argument("--mean_success", default=False, action="store_true")
    parser.add_argument("--no_x_and_z", default=False, action="store_true")
    parser.add_argument("--joint_cur_first", default=False, action="store_true")
    parser.add_argument("--new_image", default=False, action="store_true")
    parser.add_argument("--no_forces", default=False, action="store_true")
    parser.add_argument("--linear_decay", default=False, action="store_true")
    parser.add_argument("--adaptive_decay", default=False, action="store_true")
    parser.add_argument("--use_duty_cycle", default=False, action="store_true")
    parser.add_argument("--no_joint_forces", default=False, action="store_true")
    parser.add_argument("--use_phase", default=False, action="store_true")
    parser.add_argument("--guide_only", default=False, action="store_true")
    parser.add_argument("--kill_vz", default=False, action="store_true")
    parser.add_argument("--foot_pen", default=False, action="store_true")
    parser.add_argument("--clip_difficulty", default=True, action="store_false")
    parser.add_argument("--linear_decay_rate", default=40, type=int)
    parser.add_argument("--max_duty_cycle", default=20, type=int)
    parser.add_argument("--success_mean", default=0.8, type=float)
    parser.add_argument("--initial_Kp", default=400.0, type=float)
    parser.add_argument("--more_power", default=1.0, type=float)
    parser.add_argument("--joint_gain", default=80, type=float)


    parser.add_argument("--ent_coef", default=0.0, type=float)
    parser.add_argument("--body_decay_rate", default=0.75, type=float)
    parser.add_argument("--decay", default=0.85, type=float)
    # parser.add_argument("--jump_height", default=0.8, type=float)
    parser.add_argument("--jump_height", default=0.5, type=float)
    parser.add_argument("--run_state", default="train_single")
    parser.add_argument("--no_body_forces", default=False, action="store_true")
    parser.add_argument("--full_hj", default=False, action="store_true")
    parser.add_argument("--joint_cur", default=False, action="store_true")
    parser.add_argument("--slow_cur", default=False, action="store_true")
    parser.add_argument("--guide_first", default=False, action="store_true")
    # parser.add_argument("--vel_limit", default=False, action="store_true")
    # parser.add_argument("--pitch_limit", default=False, action="store_true")
    parser.add_argument("--use_max_steps", default=False, action="store_true")
    parser.add_argument("--fine_tune", default=False, action="store_true")
    parser.add_argument("--new_rand", default=False, action="store_true")
    parser.add_argument("--no_x", default=False, action="store_true")
    parser.add_argument("--train_hj", default=False, action="store_true")
    parser.add_argument("--eval_only", default=False, action="store_true")
    parser.add_argument("--train_setup", default=False, action="store_true")
    parser.add_argument("--terrain_count", default=1, type=int)
    parser.add_argument("--num_artifacts", default=1, type=int)
    parser.add_argument("--max_ts", default=50000000, type=int)
    parser.add_argument("--folder", default="single")
    # parser.add_argument("--difficulty", default=1, type=int)
    parser.add_argument("--success_len", default=1, type=int)
    parser.add_argument("--difficulty", default=1, type=int)
    parser.add_argument("--obstacle_type", default="high_jumps", help="Obstacle types are: flat, gaps, jumps, stairs, steps, high_jumps, one_leg_hop, hard_steps, hard_high_jumps, or mix (for a combination)")
    parser.add_argument("--sim_type", default="pb", help="pb or gz (pybullet or gazebo)")
    parser.add_argument("--robot", default="biped", help="biped or hexapod")
    parser.add_argument("--alg", default="ppo", help="ppo, eventually sac as well") 
    args2, unknown2 = parser.parse_known_args()
    args2 = vars(args2)
    # Replace any arguments from defaults with run specific defaults
    for key in args2:
        args1[key] = args2[key]
    # Look for any changes to defaults (unknowns) and replace values in args1
    for n, unknown in enumerate(unknown2):
        if "--" in unknown and n < len(unknown2)-1 and "--" not in unknown2[n+1]:
            arg_type = type(args1[unknown[2:]])
            args1[unknown[2:]] = arg_type(unknown2[n+1])
    args = DotMap(args1)
    # Check for dodgy arguments
    unknowns = []
    for unknown in unknown1 + unknown2:
        if "--" in unknown and unknown[2:] not in args:
            unknowns.append(unknown)
    if len(unknowns) > 0:
        print("Dodgy argument")
        print(unknowns)
        exit()

    os.environ["CUDA_VISIBLE_DEVICES"]="-1"
    run(args)
