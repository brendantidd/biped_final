import numpy as np
import pybullet as p
import copy

class Obstacles():
  def __init__ (self):
    self.box_id = []
    self.positions = []
    self.sizes = []
    self.colours = []
    
    self.circle_id = []
    self.circle_sizes = []
    self.circle_colours = []

  def remove_obstacles(self):
    if self.box_id:
      for b in self.box_id:
        p.removeBody(b)
      self.box_id = []
      self.positions = []
      self.sizes = []
      self.colours = []

    if self.circle_id:
      for b in self.circle_id:
        p.removeBody(b)
      self.circle_id = []
      self.circle_sizes = []
      self.circle_colours = []

  def test_world(self):
    self.stairs(pos=[0,0,0], width=1.0, length=0.2, height_coeff=0.07,sideways=False, order=['flat','flat','flat','flat','flat','down','down','down','down','down','flat','flat','up','up','up','up','up','flat','flat','flat','flat'])
    self.stairs(pos=[1.0,1.0,0], width=1.0, length=0.2, height_coeff=0.07,sideways=True, order=['down','down','down','down','down'])
    self.stairs(pos=[7.4,1.0,0], width=1.0, length=0.2, height_coeff=0.07,sideways=True, order=['down','down','down','down','down'])
    self.path(pos=[0,3.5,0],path=[[1.5,0.5,0.5,1],[1.0,0.5,0.5,-1],[0.75,0.5,0.5,1],[1.0,0.5,0.5,1],[1.95,0.5,0.5,-1]])


  def train_world(self):
    self.add_box(pos=[1.5,0.75,0.75], orn=[0,0,0], size=[1.5,0.75,0.75])
    self.stairs(pos=[1.5+1.5,0.75,0], width=0.75, length=0.25, height_coeff=0.08,sideways=False, order=['down','down','down','down','flat','flat','up','up','up','up','up','up','flat','flat'])
    self.stairs(pos=[1.5,0.75+0.75,0], width=1.5, length=0.20, height_coeff=0.06, sideways=True, order=['up','up','up','up','down','down','down','down','up','up','down','down','down','down','down'])
    self.path(pos=[0,8.0,0],path=[[2,0.5,0.5,1],[1.5,0.5,0.5,-1],[1.0,0.5,0.5,1],[1.0,0.5,0.5,1],[1.5,0.5,0.5,-1],[2.5,0.5,0.5,-1],[3.5,0.5,0.5,-1]])

  def easy_path(self, difficulty=1):
    pos = [0,0,0.5]
    size = [1,1,0.5]
    self.add_box(pos=pos, orn=[0,0,0], size=size)
    for n in range(6):
      # print(pos)
      if n % 2 == 0:
        pos = [pos[0] + size[0]*2, 0, 0.5]
        self.add_box(pos=pos, orn=[0,0,0], size=size)
      else:
        # pos = [pos[0] + size[0]*2, 0 + np.random.choice([-1,1],p=[0.5,0.5])*np.random.uniform(0.6+difficulty/10,0.7+difficulty/10), 0.5]
        pos = [pos[0] + size[0]*2, 0 + np.random.choice([-1,1],p=[0.5,0.5])*difficulty/3, 0.5]
        self.add_box(pos=pos, orn=[0,0,0], size=size)


  def path(self, size=1, width=1, pos=[-0.5,0,0], difficulty=1, path=None):
    
    if path is not None:
      box_sizes = path
    else:
      if difficulty < 4:
        self.easy_path(difficulty)
        return 
      prev_direction = 1
      direction = 1
      box_sizes = []
      width = np.random.uniform(1.2 - difficulty/10,1.25)
      length_min = 5.5 - difficulty/2
      total_length = 0
      count = 0
      while total_length < 20:
        if direction == prev_direction:
          new_direction = direction * -1
        elif difficulty < 5: # Lower difficulty must be straight
          new_direction = direction
        else:
          new_direction = np.random.choice([-1,1],p=[0.5,0.5])
        # if difficulty < 4:
        if count % 2 == 0:
          length = np.random.uniform(width,3)
        else:
          length = np.random.uniform(width,width + difficulty/10)
        # else:
          # length = max(np.random.uniform(1,3), width)
        total_length += length
        box_sizes.append([length, width, 0.5, new_direction])
        prev_direction = direction
        direction = new_direction
        count += 1
      
    height = 0.5 
    length = 3
    width = 0.5
    # box_size = [length, width, height, direction (right hand rule)]
    # box_sizes = [[3,1,0.5,1],[0.5,1,0.5,-1],[3,1,0.5,-1],[0.5,1,0.5,1],[3,1,0.5,1],[0.5,1,0.5,-1],[3,1,0.5,-1]]
    # box_sizes = [[3,1,0.5,1],[1.5,1,0.5,-1],[3,1,0.5,1],[1.5,1,0.5,1]]
    # pos = [pos[0], pos[1], pos[2]+height/2]
    prev_b = [0,0,0,-1]
    poses = [[0,0,0]]
    for n,b in enumerate(box_sizes):
      if n == 0:
        # print(b)
        pos = [b[0] + pos[0],0 + pos[1],b[2] + pos[2]]
        self.add_box(pos, [0,0,0], size=b[:3])
      else:
        if n % 2 == 0:
          direction = poses[-1][1] - poses[-2][1]
          if   direction > 0 and b[3] > 0: pos = [pos[0] + prev_b[1] - b[0] , pos[1] + prev_b[0] + b[1], pos[2]]
          elif direction > 0 and b[3] < 0: pos = [pos[0] - prev_b[1] + b[0] , pos[1] + prev_b[0] + b[1], pos[2]]
          elif direction < 0 and b[3] > 0: pos = [pos[0] - prev_b[1] + b[0] , pos[1] - prev_b[0] - b[1], pos[2]]
          elif direction < 0 and b[3] < 0: pos = [pos[0] + prev_b[1] - b[0] , pos[1] - prev_b[0] - b[1], pos[2]]
          self.add_box(pos, [0,0,0], size=b[:3])
        else:
          direction = poses[-1][0] - poses[-2][0]
          if   direction > 0 and b[3] > 0: pos = [pos[0] + prev_b[0] + b[1] , pos[1] - prev_b[1] + b[0], pos[2]]
          elif direction > 0 and b[3] < 0: pos = [pos[0] + prev_b[0] + b[1] , pos[1] + prev_b[1] - b[0], pos[2]]
          elif direction < 0 and b[3] > 0: pos = [pos[0] - prev_b[0] - b[1] , pos[1] + prev_b[1] - b[0], pos[2]]
          elif direction < 0 and b[3] < 0: pos = [pos[0] - prev_b[0] - b[1] , pos[1] - prev_b[1] + b[0], pos[2]]
          self.add_box(pos, [0,0,0], size=[b[1], b[0], b[2]])
      prev_b = b
      poses.append(pos)

  def doughnut(self, size=1, width=1, pos=[0,0,0]):
    radius = size
    num_blocks = 10
    # width = 0.5
    # if widh
    doughnut_type = 'small_full'
    
    # Full circle
    if doughnut_type == 'full':
      size, width = 1,1
      box_size = [width/2,(radius*np.pi*2/(5*num_blocks))+radius/2,0.5]
    elif doughnut_type == 'small_full':
      size, width = 0.5,0.5
      box_size = [width/2,(radius*np.pi*2/(5*num_blocks)),0.5]

    # size, width = 0.5,1
    # box_size = [width/2,(radius*np.pi*2/(5*num_blocks))+radius/2,0.5]
    for i in range(num_blocks):
      box_name = str(i)
      angle = i*2*np.pi/num_blocks
      x = radius*np.cos(angle) + pos[0]
      y = radius*np.sin(angle) + pos[1]
      self.add_box([x,y,0.25], [0,0,angle], size=box_size)

  def large_world(self):
    difficulty = 10
    height_coeff = 0.07
    self.add_box(pos=[2.5,0,0.5],  size=[2.5,2.5,0.5], orn=[0,0,0])    
    self.add_box(pos=[9,0,0.5],    size=[2.5,2.5,0.5], orn=[0,0,0])    
    self.add_box(pos=[15,0,0.5],   size=[2.5,2.5,0.5], orn=[0,0,0])    
    self.add_box(pos=[22,0,0.5],   size=[2.5,2.5,0.5], orn=[0,0,0])    
    size = [0.18, 0.6, 0.5]
    pos = [-size[0]-0.5,0,size[2]]
    prev_size = copy.copy(size)
    prev_yaw = 0
    # rand_order = [np.random.choice(['jumps','gaps','stairs']) for _ in range(3)]
    rand_order = ['stairs', 'jumps','gaps']
    # order = ['flat','flat','flat']
    # pol_order = ['base','base','base']
    order = []
    pol_order = []
    # order = []
    # print(rand_order)
    for r in rand_order:
      for i in range(7):
        if r in ['jumps', 'gaps']:
          if i == 3:
            order.append(r)
          else:
            order.append('flat')
        else:
          if i == 0 or i == 6:
            order.append('flat')
          else:
            order.append(np.random.choice(['up', 'down']))
        pol_order.append(r)
    # order.extend(['flat','flat','flat'])
    # pol_order.extend(['base','base','base'])

    yaws = [0.0]*len(order)

    count = 0
    lowest = 0
    for name in order:
      if name == 'down': count += 1
      if name == 'up': count -= 1
      if count > lowest:
        lowest = count
    prev_height = (lowest + 2)*height_coeff + 0.4
    pos[2] = prev_height
    size[2] = prev_height
    # self.add_box(pos, [0,0,0], size)
    prev_size = copy.copy(size)
    # pos, prev_yaw = self.add_curved_box(pos, prev_yaw=prev_yaw, size=sze, yaw=0.0)
    # for _ in r ange(20):
    yaw_count = 0
    poses = [[4.5,1.0,0.25],[11,-1.0,0.25],[17,0,0.25]]
    pos_count = 0
    for i,o in enumerate(order):        
      if i % 7 == 0:
        print(pos_count)
        pos = poses[pos_count]
        pos_count += 1
      if o == 'down':
        height = prev_height-height_coeff
        pos[2] = height
        size = [0.18, 0.6, height]
      elif o == 'up':
        height = prev_height+height_coeff
        pos[2] = height
        size = [0.18, 0.6, height]
      elif o == 'jumps':
        pos = [pos[0], pos[1], pos[2] + difficulty*0.015]
        size = [0.05+1*0.03, size[1], pos[2]]
      elif o == 'gaps':
        pos = [pos[0], pos[1], 0.01]
        size = [difficulty*0.035, size[1], pos[2]]
      else:
        height = prev_height
        pos[2] = height
        size = [0.18, 0.6, height]
        
      prev_height = height
      yaw = 0.0
      pos, prev_yaw = self.add_curved_box(pos, prev_yaw=prev_yaw, prev_size=prev_size, size=size, yaw=yaw)
      prev_size = copy.copy(size)
    return pol_order

  def stairs(self, order, pos=[-0.5,0,0], sideways=False, length=0.2, width=2, height_coeff=0.1, colour=[0.8,0.3,0.3,1], rand=False):
    count = 0
    lowest = 0
    positions = [pos]
    sizes = [[0,0,0]]
    if rand:
      length = np.random.uniform(0.15, 0.25)
    for name in order:
      if name == 'down': count += 1
      if name == 'up': count -= 1
      if count > lowest:
        lowest = count
    prev_height = (lowest + 2)*height_coeff + 0.4
    for i,o in enumerate(order):
      if rand:
        if i % 5 == 0:
          width = np.random.uniform(1.5-height_coeff,2)
          if i > 0:
            pos[1] = np.random.random() - 0.5
      if o == 'down':
        height = prev_height-height_coeff
      elif o == 'up':
        height = prev_height+height_coeff
      else:
        height = prev_height
      prev_height = height
      if sideways:
        size = [width, length, height]
        position =  [positions[-1][0], positions[-1][1] + sizes[-1][1] + size[1], 0.0 + size[2] + 0.01]
      else:
        size = [length, width, height]
        position = [positions[i][0] + sizes[-1][0] + size[0], positions[-1][1], 0.0 + size[2] + 0.01]
      self.add_box(position, [0,0,0], size, colour=colour)
      positions.append(position)
      sizes.append(size)

  def rand_world(self, pos):
    orn = [0.0,0.0,0.0]
    # grid = np.array(400).reshape(20,20)
    # grid = np.random.random(size=[20,20])
    
    for x in range(-15,15):
      for y in range(-6,6):
        size = [0.25,0.5,np.random.random()*0.2]
        self.add_box([0.5*x + pos[0],1.0*y + pos[1],0.01 + pos[2]], orn, size)

  def add_domain_training_world(self):
    
    size = [0.2, 0.6, 0.5]
    pos = [-size[0]-0.5,0,size[2]]
    
    pos[2] = size[2]

    height_coeff = 0.07

    # order = ['flat']*10 + ['up']*7 + ['down']*7 + ['flat']*3 + ['up']*7 + ['flat']*3 + ['flat']*7 + ['down']*7 + ['flat']*3 + ['flat']*7 + ['up']*7 + ['down']*7 + ['flat']*2 + ['flat']*3 + ['jump']*1 + ['flat']*3 + ['jump']*1 + ['flat']*3 + ['gap']*1 + ['flat']*3 + ['gap']*1 + ['flat']*3
    # yaws = [0.0]*10 + [0.15]*7 + [0.15]*6 + [0.0]*4 + [-0.15]*7 + [0.0]*3 + [0.15]*7 + [0.0]*7 + [0.0]*3 + [0.15]*7 + [0.0]*7 + [0.15]*7 + [-0.15]*2 + [0.0]*3 + [0.0]*1 + [0.0]*3 + [0.0]*1 + [0.0]*3 + [0.0]*1 + [0.0]*3 + [0.0]*1 + [0.0]*3 
   
    order = ['flat']*4 + ['up']*7 + ['down']*7 + ['flat']*3 + ['up']*7 + ['flat']*3 + ['flat']*7 + ['down']*7 + ['flat']*3 + ['flat']*7 + ['flat']*10 + ['flat']*2 + ['jump']*1 + ['flat']*5 + ['jump']*1 + ['flat']*7 + ['gap']*1 + ['flat']*8 + ['gap']*1 + ['flat']*2 + ['jump']*1 + ['flat']*5
    yaws = [0.0]*4 + [0.15]*7 + [0.15]*6 + [0.0]*4 + [-0.15]*7 + [0.0]*3 + [0.15]*7 + [0.0]*7 + [0.0]*3 + [0.15]*7 + [0.15]*10 + [0.10]*2 + [0.0]*1 + [0.0]*5 + [0.0]*1 + [0.0]*7 + [0.0]*1 + [0.0]*8 + [0.0]*1 + [0.0]*2 + [0.0]*1 + [0.0]*5
   

    heights = [0.07]*len(order)
    sizes = [[0.18, 0.6, 0.5]]*len(order)
    prev_size = sizes[0]
    count = 0
    lowest = 0
    for name in order:
      if name == 'down': count += 1
      if name == 'up': count -= 1
      if count > lowest:
        lowest = count
    prev_height = (lowest + 2)*height_coeff + 0.4
    pos[2] = size[2] = prev_height
    prev_yaw = 0.0
    pos, prev_yaw = self.add_curved_box(pos, prev_yaw=prev_yaw, size=size, yaw=0.0)
    for i,(o,y,h,s) in enumerate(zip(order, yaws, heights, sizes)):      
      if o == 'down':
        height = prev_height-h
      elif o == 'up':
        height = prev_height+h
      else:
        height = prev_height
      prev_height = height
      size = s
      pos[2] = size[2] = height
      if o == 'jump':
        # pos = [pos[0], pos[1], 0.5 + 7*0.04]
        pos = [pos[0], pos[1], 0.5 + 6*0.04]
        size = [0.05+1*0.03, size[1], pos[2]]   
        # pos = [pos[0] + prev_size[0] + size[0], pos[1], pos[2]]
        # self.add_box(pos, orn=[0,0,y], size=size)  

      elif o == 'gap':    
        pos = [pos[0], pos[1], 0.01]
        size = [0.05+6*0.03, size[1], pos[2]]
        # size = [0.18, size[1], pos[2]]
        # pos = [pos[0] + prev_size[0] + size[0], pos[1], pos[2]]
        # self.add_box(pos, orn=[0,0,y], size=size)  
      # else:
      #   pos = [pos[0], pos[1], 0.5]
      #   size = [0.18, size[1], pos[2]]
      # pos = [pos[0] + prev_size[0] + size[0], pos[1], pos[2]]
      # self.add_box(pos, orn, size)
      
      pos, prev_yaw = self.add_curved_box(pos, prev_yaw=prev_yaw, prev_size=prev_size, size=size, yaw=y)
      prev_size = copy.copy(size)


  def add_domain_training_world_no_stairs3(self):
    difficulty = 7
    # size = [0.2, 0.7, 0.5]
    # size = [0.2, 0.6, 0.5]
    size = [0.18, 0.6, 0.5]
    # pos = [-size[0]-0.5,0,size[2]]
    pos = [-size[0] - 0.5,0,size[2]]
    
    pos[2] = size[2]

    height_coeff = 0.07

    # order = ['flat']*5 + ['jump']*1 + ['flat']*8 + ['gap']*1 + ['flat']*8 + ['gap']*1 + ['flat']*8 + ['jump']*1 + ['flat']*5
    # order = ['flat']*5 + ['gap']*1 + ['flat']*8 + ['gap']*1 + ['flat']*8 + ['gap']*1 + ['flat']*8 + ['gap']*1 + ['flat']*5
    # order = ['flat']*5 + ['gap']*1 + ['flat']*8 + ['gap']*1 + ['flat']*8 
    
    yaws = [0.0]*len(order)
   
    heights = [0.07]*len(order)
    sizes = [size]*len(order)

    prev_size = sizes[0]
    count = 0
    lowest = 0
    for name in order:
      if name == 'down': count += 1
      if name == 'up': count -= 1
      if count > lowest:
        lowest = count
    prev_height = (lowest + 2)*height_coeff + 0.4
    pos[2] = size[2] = prev_height
    prev_yaw = 0.0
    pos, prev_yaw = self.add_curved_box(pos, prev_yaw=prev_yaw, size=size, prev_size=prev_size, yaw=0.0)
    for i,(o,y,h,s) in enumerate(zip(order, yaws, heights, sizes)):      
      if o == 'down':
        height = prev_height-h
      elif o == 'up':
        height = prev_height+h
      else:
        height = prev_height
      prev_height = height
      size = s
      pos[2] = size[2] = height
      if o == 'jump':
        pos = [pos[0], pos[1], 0.5 + difficulty*0.03]
        size = [0.05+1*0.03, size[1], pos[2]]   
      elif o == 'gap':    
        pos = [pos[0], pos[1], 0.01]
        size = [0.05+difficulty*0.04, size[1], pos[2]]
      
      pos = [pos[0] + prev_size[0] + size[0], pos[1], pos[2]]
      self.add_box(pos, orn=[0,0,0], size=size)
      
      # pos, prev_yaw = self.add_curved_box(pos, prev_yaw=prev_yaw, prev_size=prev_size, size=size, yaw=y)
      prev_size = copy.copy(size)
  
  def add_domain_training_world_no_stairs(self, difficulty=1):
    # if difficulty > 7:
    difficulty = 7
    size = [0.18, 0.6, 0.5]
    pos = [-size[0]-0.5,0,size[2]]
    prev_size = copy.copy(size)
    prev_yaw = 0
    order = ['flat']*5 + ['jump']*1 + ['flat']*8 + ['gap']*1 + ['flat']*8 
    yaws = [0.0]*len(order)
    
    # order = ['flat']*40 + ['flat']*5 + ['jump']*1 + ['flat']*10 + ['gap']*1 + ['flat']*5 + ['flat']*40 + ['flat']*5 + ['jump']*1 + ['flat']*10 + ['gap']*1 + ['flat']*5
    # yaws = [np.pi/40]*40 + [0.0]*21 + [np.pi/40]*40 + [0.0]*21
   
    for y,o in zip(yaws, order):
      if o == 'gap':
        pos = [pos[0], pos[1], 0.01]
        size = [0.05+difficulty*0.04, size[1], pos[2]]
      elif o == 'jump':
        pos = [pos[0], pos[1], 0.5 + difficulty*0.03]
        size = [0.05+1*0.03, size[1], pos[2]]
      else:
        pos = [pos[0], pos[1], 0.5]
        size = [0.18, size[1], pos[2]]
      pos, prev_yaw = self.add_curved_box(pos, prev_yaw=prev_yaw, prev_size=prev_size, size=size, yaw=y)
      prev_size = copy.copy(size)

  def add_domain_training_world_no_stairs2(self):
    difficulty = 6
    size = [0.2, 0.7, 0.5]
    pos = [-size[0]-0.5,0,size[2]]
    
    pos[2] = size[2]

    height_coeff = 0.07

    order = ['flat']*40 + ['flat']*5 + ['jump']*1 + ['flat']*10 + ['gap']*1 + ['flat']*5 + ['flat']*40 + ['flat']*5 + ['jump']*1 + ['flat']*10 + ['gap']*1 + ['flat']*5
    
    yaws = [np.pi/40]*40 + [0.0]*21 + [np.pi/40]*40 + [0.0]*21
   
    heights = [0.07]*len(order)
    sizes = [size]*len(order)

    prev_size = sizes[0]
    count = 0
    lowest = 0
    for name in order:
      if name == 'down': count += 1
      if name == 'up': count -= 1
      if count > lowest:
        lowest = count
    prev_height = (lowest + 2)*height_coeff + 0.4
    pos[2] = size[2] = prev_height
    prev_yaw = 0.0
    pos, prev_yaw = self.add_curved_box(pos, prev_yaw=prev_yaw, size=size, yaw=0.0)
    for i,(o,y,h,s) in enumerate(zip(order, yaws, heights, sizes)):      
      if o == 'down':
        height = prev_height-h
      elif o == 'up':
        height = prev_height+h
      else:
        height = prev_height
      prev_height = height
      size = s
      pos[2] = size[2] = height
      if o == 'jump':
        pos = [pos[0], pos[1], 0.5 + difficulty*0.03]
        size = [0.05+1*0.03, size[1], pos[2]]   
      elif o == 'gap':    
        pos = [pos[0], pos[1], 0.01]
        size = [0.05+difficulty*0.04, size[1], pos[2]]
      
      pos, prev_yaw = self.add_curved_box(pos, prev_yaw=prev_yaw, prev_size=prev_size, size=size, yaw=y)
      prev_size = copy.copy(size)

  def add_random(self, difficulty=1, height_coeff=0.01):
    
    size = [0.18, 0.6, 0.5]
    pos = [-size[0]-0.5,0,size[2]]
    orn = [0,0,0]
    # x = np.random.random() 
    # if x <= 0.5:
    #   for i in range(6):
    #     if i % 2 == 0 and i > 2:
    #       if np.random.random()< 0.5:
    #         self.add_box([pos[0],pos[1],0.01], orn, [size[0],size[1],0.01])
    #       else:
    #         self.add_box([pos[0],pos[1],0.01], orn, [size[0],size[1],0.01])
    #     else:
    #       self.add_box(pos, orn, size)
    #     pos = [pos[0]+size[0]*2,pos[1],pos[2]]
    # elif x > 0.3 and x < 0.7:
    self.add_curvey_stairs(order=[np.random.choice(['up','down','flat'],p=[0.4,0.4,0.2]) for _ in range(6)], difficulty=difficulty, height_coeff=height_coeff)
    # else:
    #   prev_yaw = 0.0
    #   pos, prev_yaw = self.add_curved_box(pos, prev_yaw=prev_yaw, size=size, yaw=0.0)
    #   yaw_count = 0
    #   prev_size = size
    #   for _ in range(6):
    #     max_yaw = 0.05 + 7*0.05
    #     if yaw_count == 0:
    #       yaw = np.random.choice([-max_yaw, max_yaw, 0.0], p=[0.45,0.45,0.1])
    #       # yaw = np.random.choice([-max_yaw, max_yaw], p=[0.5,0.5])
    #     else:
    #       yaw = yaw
    #     yaw_count += 1
    #     if yaw_count > 10:
    #       yaw_count = 0
    #     yaw = np.random.choice([-max_yaw, max_yaw, 0.0], p=[0.3,0.3,0.4])
    #     pos, prev_yaw = self.add_curved_box(pos, prev_yaw=prev_yaw, prev_size=prev_size, size=size, yaw=yaw)
    #     prev_size = size

  def add_show2(self, difficulty=7, height_coeff=0.07, base=False):
    
    size = [0.18, 0.6, 0.5]
    pos = [-size[0]-0.5,0,size[2]]
    prev_size = copy.copy(size)
    prev_yaw = 0
    rand_order = ['jumps','gaps']
    order = ['flat','flat','flat']
    pol_order = ['base','base','base']
    # order = []
    # print(rand_order)
    if base:
      order = ['flat']*(3*2 + 7*7)
      pol_order = ['base']*(3*2 + 7*7)
    else:
      for r in rand_order:
        for i in range(7):
          if r in ['jumps', 'gaps']:
            if i == 3:
              order.append(r)
            else:
              order.append('flat')
          else:
            if i == 0 or i == 6:
              order.append('flat')
            else:
              order.append(np.random.choice(['up', 'down']))
          pol_order.append(r)
      order.extend(['flat','flat','flat'])
      pol_order.extend(['base','base','base'])

    yaws = [0.0]*len(order)

    count = 0
    lowest = 0
    for name in order:
      if name == 'down': count += 1
      if name == 'up': count -= 1
      if count > lowest:
        lowest = count
    prev_height = (lowest + 2)*height_coeff + 0.4
    pos[2] = prev_height
    size[2] = prev_height
    self.add_box(pos, [0,0,0], size)
    prev_size = copy.copy(size)
    # pos, prev_yaw = self.add_curved_box(pos, prev_yaw=prev_yaw, size=sze, yaw=0.0)
    # for _ in r ange(20):
    yaw_count = 0
    for i,o in enumerate(order):        

      if o == 'down':
        height = prev_height-height_coeff
        pos[2] = height
        size = [0.18, 0.6, height]
      elif o == 'up':
        height = prev_height+height_coeff
        pos[2] = height
        size = [0.18, 0.6, height]
      elif o == 'jumps':
        pos = [pos[0], pos[1], pos[2] + difficulty*0.015]
        size = [0.05+1*0.03, size[1], pos[2]]
      elif o == 'gaps':
        pos = [pos[0], pos[1], 0.01]
        size = [difficulty*0.035, size[1], pos[2]]
      else:
        height = prev_height
        pos[2] = height
        size = [0.18, 0.6, height]
        
      prev_height = height
      yaw = 0.0
      pos, prev_yaw = self.add_curved_box(pos, prev_yaw=prev_yaw, prev_size=prev_size, size=size, yaw=yaw)
      prev_size = copy.copy(size)
    return pol_order + ['base']
    # return pol_order


  def add_show1(self, difficulty=10, height_coeff=0.07, base=False):
    
    size = [0.18, 0.6, 0.5]
    pos = [-size[0]-0.5,0,size[2]]
    prev_size = copy.copy(size)
    prev_yaw = 0
    rand_order = ['jumps','gaps','stairs']
    order = ['flat','flat','flat']
    pol_order = ['base','base','base']
    # order = []
    # print(rand_order)
    if base:
      order = ['flat']*(3*2 + 7*7)
      pol_order = ['base']*(3*2 + 7*7)
    else:
      for r in rand_order:
        for i in range(7):
          if r in ['jumps', 'gaps']:
            if i == 3:
              order.append(r)
            else:
              order.append('flat')
          else:
            if i == 0 or i == 6:
              order.append('flat')
            else:
              order.append(np.random.choice(['up', 'down']))
          pol_order.append(r)
      order.extend(['flat','flat','flat'])
      pol_order.extend(['base','base','base'])

    yaws = [0.0]*len(order)

    count = 0
    lowest = 0
    for name in order:
      if name == 'down': count += 1
      if name == 'up': count -= 1
      if count > lowest:
        lowest = count
    prev_height = (lowest + 2)*height_coeff + 0.4
    pos[2] = prev_height
    size[2] = prev_height
    self.add_box(pos, [0,0,0], size)
    prev_size = copy.copy(size)
    # pos, prev_yaw = self.add_curved_box(pos, prev_yaw=prev_yaw, size=sze, yaw=0.0)
    # for _ in r ange(20):
    yaw_count = 0
    for i,o in enumerate(order):        

      if o == 'down':
        height = prev_height-height_coeff
        pos[2] = height
        size = [0.18, 0.6, height]
      elif o == 'up':
        height = prev_height+height_coeff
        pos[2] = height
        size = [0.18, 0.6, height]
      elif o == 'jumps':
        pos = [pos[0], pos[1], pos[2] + difficulty*0.015]
        size = [0.05+1*0.03, size[1], pos[2]]
      elif o == 'gaps':
        pos = [pos[0], pos[1], 0.01]
        size = [difficulty*0.035, size[1], pos[2]]
      else:
        height = prev_height
        pos[2] = height
        size = [0.18, 0.6, height]
        
      prev_height = height
      yaw = 0.0
      pos, prev_yaw = self.add_curved_box(pos, prev_yaw=prev_yaw, prev_size=prev_size, size=size, yaw=yaw)
      prev_size = copy.copy(size)
    return pol_order + ['base']
    # return pol_order


  def add_straight_world(self, difficulty=10, height_coeff=0.07, terrain_type='mix', base=False, base_before=False, initial_pos=[0,0,0], baseline=False, two=False, rand_flat=False):
    initial_size = [np.random.uniform(0.18,0.22), np.random.uniform(0.55,0.85), 0.5]
    # initial_size = [0.2, 0.7, 0.5]
    # print("fixed size", initial_size)
    size = copy.copy(initial_size)
    pos = [-initial_size[0]-0.5+initial_pos[0],0+initial_pos[1],initial_size[2]+initial_pos[2]]
    prev_size = copy.copy(initial_size)
    prev_yaw = 0
    if terrain_type == 'mix':
      # If not same terrain type twice in a row
      rand_order = []
      all_options = ['jumps','gaps','stairs']
      option = np.random.choice(['jumps','gaps','stairs'])
      for i in range(7):
        rand_order.append(option)
        options = copy.copy(all_options)
        options.remove(option)
        option = np.random.choice(options)

      # rand_order = [np.random.choice(['jumps','gaps','stairs']) for _ in range(7)]
      # rand_order = [np.random.choice(['jumps','stairs']) for _ in range(7)]
    elif terrain_type == 'dqn':
      rand_order = [np.random.choice(['jumps','gaps','stairs']) for _ in range(15)]
    elif terrain_type == 'multi':
      rand_order = ['jumps','gaps','stairs']
    elif terrain_type == 'zero':
      rand_order = []
    else:
      if baseline:
        rand_order = [terrain_type for _ in range(7)]
      else:
        if two:
          rand_order = [terrain_type, terrain_type]
        else:
          rand_order = [terrain_type]
    order = ['flat','flat','flat','flat']
    pol_order = ['base','base','base','base']
    if base:
      order = ['flat']*(3*2 + 7*7)
      pol_order = ['base']*(3*2 + 7*7)
    # elif terrain_type == 'dqn':
    #   for r in rand_order:
    #     for i in range(3):
    #       if r in ['jumps', 'gaps']:
    #         if i == 1:
    #           order.append(r)
    #         else:
    #           order.append('flat')
    #       else:
    #         if i in [0]:
    #           order.append('flat')
    #         else:
    #           order.append(np.random.choice(['up', 'down']))
    #       pol_order.append(r)
    #   order.extend(['flat','flat','flat','flat'])
    #   pol_order.extend(['zero','zero','zero','zero'])
    else:
      for r in rand_order:
        if r == 'stairs':
          up_down = np.random.choice(['up', 'down'])
        # if rand_flat and terrain_type == 'mix':
        #   # artifact_size = np.random.randint(7,9)
        # artifact_size = np.random.randint(5,9)
        # placement = 2

        #   artifact_size = np.random.randint(4,8)
        #   # print(artifact_size)
        # else:
          # artifact_size = 5
        
        if terrain_type == 'mix':        
          # artifact_size = np.random.randint(3,9)
          artifact_size = np.random.randint(4,9)
          # artifact_size = np.random.randint(5,9)
          if r == 'stairs':
            placement = np.random.randint(0,3)
            stair = np.random.choice(['up', 'down'])

          else:
            if artifact_size == 4:
              placement = 2
            else:
              placement = np.random.randint(2,artifact_size-2)
              # placement = np.random.randint(1,artifact_size-3)

        else:
          if r == 'stairs':
            artifact_size = np.random.randint(7,10)
            placement = 2
            stair = np.random.choice(['up', 'down'])

          else:
            artifact_size = 7
            placement = 3

        for i in range(artifact_size):
          if r == 'base':
            order.append('flat')
            # pol_order.append('base')
          elif r in ['jumps', 'gaps']:
            # if i == 3:
            if (i == placement):
              order.append(r)
            else:
              order.append('flat')
          else:
            # if i in [3,4,5,6]:
            # if i in [2,3,4,5,6]:
            if terrain_type == 'mix':
              # if i > placement and i != artifact_size - 1:
              if i > placement:
                order.append(np.random.choice(['up', 'down']))
                # order.append(stair)
              else:
                order.append('flat')
            else:
              if i > placement and i != artifact_size - 1:
                order.append(np.random.choice(['up', 'down']))
                # order.append(stair)
              else:
                order.append('flat')
            

              # order.append(up_down)
          # if base_before and (r != 'stairs' and (i == 6 or i == 0)):
          # if base_before and (r != 'stairs' and (i == 6)):
          # if base_before and (r != 'stairs' and (i == placement)):
          #   pol_order.append('base')
          # else:
          pol_order.append(r)
      order.extend(['flat','flat','flat'])
      pol_order.extend(['zero','zero','zero'])

    colours = []
    for pol in pol_order:
      if pol == 'stairs':
        colours.append([0.8,0.5,0.8,1])
      elif pol == 'jumps':
        colours.append([0.8,0.3,0.3,1])
      elif pol == 'gaps':
        colours.append([0.3,0.8,0.3,1])
      elif pol == 'zero':
        colours.append([0.8,0.5,0.3,1])
      elif pol == 'base':
        colours.append([0.3,0.3,0.8,1])

    yaws = [0.0]*len(order)

    count = 0
    lowest = 0
    for name in order:
      if name == 'down': count += 1
      if name == 'up': count -= 1
      if count > lowest:
        lowest = count
    prev_height = (lowest + 2)*height_coeff + 0.4
    pos[2] = prev_height
    size[2] = prev_height
    # self.add_box(pos, [0,0,0], size)
    prev_size = copy.copy(size)
    # pos, prev_yaw = self.add_curved_box(pos, prev_yaw=prev_yaw, size=sze, yaw=0.0)
    # for _ in r ange(20):
    yaw_count = 0
    # height_coeff += 0.03
    for i,o in enumerate(order):        
      initial_size = [np.random.uniform(0.18,0.22), initial_size[1], 0.5]
      if o == 'down':
        height = prev_height-height_coeff
        pos[2] = height
        size = [initial_size[0], initial_size[1], height]
      elif o == 'up':
        height = prev_height+height_coeff
        pos[2] = height
        size = [initial_size[0], initial_size[1], height]
      elif o == 'jumps':
        pos = [pos[0], pos[1], pos[2] + difficulty*0.015]
        # pos = [pos[0], pos[1], pos[2] + difficulty*0.0165]
        # pos = [pos[0], pos[1], pos[2] + difficulty*0.017]

        # pos = [pos[0], pos[1], pos[2] + difficulty*0.0172]
        
        # pos = [pos[0], pos[1], pos[2] + difficulty*0.018]
        # pos = [pos[0], pos[1], pos[2] + 11*0.015]
        # size = [0.05+1*0.03, size[1], pos[2]]
        size = [0.05+1*0.03, size[1], pos[2]]
      elif o == 'gaps':
        pos = [pos[0], pos[1], 0.01]
        # size = [difficulty*0.0385, size[1], pos[2]]
        # size = [difficulty*0.0382, size[1], pos[2]]
        # size = [difficulty*0.0378, size[1], pos[2]]
        # size = [difficulty*0.037, size[1], pos[2]]
        # size = [difficulty*0.036, size[1], pos[2]]
        # size = [difficulty*0.0355, size[1], pos[2]]
        size = [difficulty*0.035, size[1], pos[2]]
        # size = [difficulty*0.03, size[1], pos[2]]
        # size = [difficulty*0.025, size[1], pos[2]]
        # size = [difficulty*0.02, size[1], pos[2]]
      else:
        height = prev_height
        pos[2] = height
        size = [initial_size[0], initial_size[1], height]
        
      prev_height = height
      yaw = 0.0
      pos, prev_yaw = self.add_curved_box(pos, prev_yaw=prev_yaw, prev_size=prev_size, size=size, yaw=yaw, colour=colours[i])
      prev_size = copy.copy(size)
    # return pol_order + ['base']
    return pol_order

  def add_straight_world2(self, difficulty=7, height_coeff=0.07, base=False):
    size = [np.random.uniform(0.18,0.22), np.random.uniform(0.55,0.85), 0.5]
    # size = [0.18, 0.6, 0.5]
    pos = [-size[0]-0.5,0,size[2]]
    prev_size = copy.copy(size)
    prev_yaw = 0
    rand_order = [np.random.choice(['jumps','gaps','stairs']) for _ in range(7)]
    order = ['flat','flat','flat']
    pol_order = ['base','base','base']
    # order = []
    # print(rand_order)
    if base:
      order = ['flat']*(3*2 + 7*7)
      pol_order = ['base']*(3*2 + 7*7)
    else:
      for r in rand_order:
        for i in range(7):
          if r in ['jumps', 'gaps']:
            if i == 3:
              order.append(r)
            else:
              order.append('flat')
          else:
            if i == 0 or i == 6:
              order.append('flat')
            else:
              order.append(np.random.choice(['up', 'down']))
          pol_order.append(r)
      order.extend(['flat','flat','flat'])
      pol_order.extend(['base','base','base'])

    yaws = [0.0]*len(order)

    count = 0
    lowest = 0
    for name in order:
      if name == 'down': count += 1
      if name == 'up': count -= 1
      if count > lowest:
        lowest = count
    prev_height = (lowest + 2)*height_coeff + 0.4
    pos[2] = prev_height
    size[2] = prev_height
    self.add_box(pos, [0,0,0], size)
    prev_size = copy.copy(size)
    # pos, prev_yaw = self.add_curved_box(pos, prev_yaw=prev_yaw, size=sze, yaw=0.0)
    # for _ in r ange(20):
    yaw_count = 0
    for i,o in enumerate(order):        

      if o == 'down':
        height = prev_height-height_coeff
        pos[2] = height
        size = [0.18, 0.6, height]
      elif o == 'up':
        height = prev_height+height_coeff
        pos[2] = height
        size = [0.18, 0.6, height]
      elif o == 'jumps':
        pos = [pos[0], pos[1], pos[2] + difficulty*0.015]
        size = [0.05+1*0.03, size[1], pos[2]]
      elif o == 'gaps':
        pos = [pos[0], pos[1], 0.01]
        size = [difficulty*0.035, size[1], pos[2]]
      else:
        height = prev_height
        pos[2] = height
        size = [0.18, 0.6, height]
        
      prev_height = height
      yaw = 0.0
      pos, prev_yaw = self.add_curved_box(pos, prev_yaw=prev_yaw, prev_size=prev_size, size=size, yaw=yaw)
      prev_size = copy.copy(size)
    return pol_order + ['base']
    # return pol_order

  def add_stepping_stones(self, obstacle_type='base'):
    num_boxes = 50
    if obstacle_type == 'base':
      # size = [np.random.uniform(0.18,0.2), np.random.uniform(0.6,0.8), 0.5]
      # size = [np.random.uniform(0.1,0.18), np.random.uniform(0.6,0.8), 0.5]
      size = [np.random.uniform(0.1,0.18), np.random.uniform(0.3,0.4), 0.5]
    else:
      size = [np.random.uniform(0.36,0.4), np.random.uniform(0.8,1.0), 0.5]

    pos = [-size[0], 0 ,size[2]]
    prev_yaw = 0.0
    max_yaw = 0.05
    pos[2] = size[2]
    # prev_size = [0,0,0]
    # pos, prev_yaw = self.add_curved_box(pos, prev_yaw=prev_yaw, size=size, yaw=0.0)
    self.add_box(pos, orn=[0,0,0], size=size)
    prev_size = copy.copy(size)
    sign = np.random.choice([-1,1],p=[0.5,0.5])
    for i in range(num_boxes):        
      height = 0.5 + np.random.random()/100
      yaw = np.random.choice([-max_yaw, max_yaw, 0.0], p=[0.45,0.45,0.1])
      pos[2] = height
      size[2] = height
      if i < 3:
        size[1] = 0.6
      else:
        if i%2 == 0 and i > 2:
          size[1] = np.random.uniform(0.3,0.4)
          pos[1] = sign*size[1]
        else:
          size[1] = np.random.uniform(0.3,0.4)
          pos[1] = -1*sign*size[1]  

        # size[1] = np.random.uniform(0.6,0.8)

        
      pos, prev_yaw = self.add_curved_box(pos, prev_yaw=prev_yaw, prev_size=prev_size, size=size, yaw=yaw)
      prev_size = [size[0]+np.random.uniform(0.0, 0.15), size[1], size[2]]
      # prev_size = size

  def add_base(self, difficulty, flat=False):
    if difficulty > 7: 
      difficulty = 7
    # difficulty = 7
    size = [np.random.uniform(0.15,0.25), np.random.uniform(0.6-(0.02*difficulty),0.85), 0.5]
    # size = [np.random.uniform(0.15,0.25), np.random.uniform(0.3,0.4), 0.5]
    pos = [-size[0]-0.5,0,size[2]]
    orn = [0,0,0]
    prev_size = copy.copy(size)
    prev_yaw = yaw = 0.0
    prev_height = size[2]
    for i in range(60):
      # height = 0.5 + np.random.random()/50
      if flat:
        height = prev_height
      else:
        # if i % 2 == 0:
        # height = 0.5 + np.random.random()/(100 - difficulty*5)
        height = prev_height
        # else:
          # height = 0.5
      # if i % 2 == 0:
      yaw = 0.0 + (np.random.random()-0.5)*(0.1*difficulty)
        # print(yaw)
      # else:
        # yaw = 0.0

      pos[2] = height
      size[2] = height
      # print(pos[2]*2, pos[2]+size[2])
      # pos = [pos[0] + prev_size[0] + size[0], pos[1], pos[2]]
      # self.add_box(pos, orn, size)
      pos, prev_yaw = self.add_curved_box(pos, prev_yaw=prev_yaw, prev_size=prev_size, size=size, yaw=yaw)
      prev_size = copy.copy(size)

  # def add_jumps(self, difficulty=1):
  #   if difficulty > 7:
  #     difficulty = 7
  #   size = [np.random.uniform(0.15,0.25), np.random.uniform(0.5,0.8), 0.5]
  #   pos = [-size[0]-0.5,0,size[2]]
  #   orn = [0,0,0]
  #   prev_size = size
  #   for i in range(45):
  #     if i > 3:
  #       x = np.random.random()
  #     else:
  #       x = 1.0
  #     if x < 0.1:
  #     # if i % 2 == 0:
  #       pos = [pos[0], pos[1], 0.5 + difficulty*0.04]
  #       size = [0.18, size[1], pos[2]]
  #     # elif x > 0.3 and x < 0.6:
  #     #   pos = [pos[0], pos[1], 0.6]
  #     #   size = [0.18, size[1], pos[2]]
  #     else:
  #       pos = [pos[0], pos[1], 0.5]
  #       size = [0.18, size[1], pos[2]]
  #     pos = [pos[0] + prev_size[0] + size[0], pos[1], pos[2]]
  #     self.add_box(pos, orn, size)
  #     prev_size = size


  def add_one_stair(self):
    height_coeff = 0.07
    size = [np.random.uniform(0.18,0.22), np.random.uniform(0.55,0.85), 0.5]
    pos = [-size[0],0,size[2]]

    prev_yaw = 0.0
    max_yaw = 0.0
    yaw = 0.0
    pos[2] = size[2]

    order = ['flat', 'flat','flat','flat','flat', 'flat'] + [np.random.choice(['up', 'down']) for _ in range(5)] + ['flat', 'flat', 'flat', 'flat']
    count = 0
    lowest = 0
    # prev_height = (lowest + 2)*height_coeff + 0.4
    prev_height = 0.5
    pos[2] = prev_height
    size[2] = prev_height
    # self.add_box(pos, [0,0,0], size)
    prev_size = copy.copy(size)
    # pos, prev_yaw = self.add_curved_box(pos, prev_yaw=prev_yaw, size=sze, yaw=0.0)
    # for _ in r ange(20):
    yaw_count = 0
    for i,o in enumerate(order):        

      if o == 'down':
        height = prev_height-height_coeff
      elif o == 'up':
        height = prev_height+height_coeff
      else:
        height = prev_height
      prev_height = height
      
      pos[2] = height
      size[2] = height
      pos, prev_yaw = self.add_curved_box(pos, prev_yaw=prev_yaw, prev_size=prev_size, size=size, yaw=yaw)
      prev_size = copy.copy(size)

  def add_one_jump(self, difficulty=1):
    # size = [0.22, 0.75, 0.5]
    size = [np.random.uniform(0.18,0.22), np.random.uniform(0.55,0.85), 0.5]
    # size = [0.2, 0.65, 0.5]
    pos = [-size[0]-0.5,0,size[2]]
    orn = [0,0,0]
    prev_size = copy.copy(size)
    prev_jump = False
    jump_count = 0
    for i in range(15):
      if i == 8:
        pos = [pos[0], pos[1], 0.5 + difficulty*0.015]
        size = [0.05+1*0.03, size[1], pos[2]]
        jump_count = 0
        prev_jump = True
      else:
        pos = [pos[0], pos[1], 0.5]
        size = [0.18, size[1], pos[2]]
        jump_count += 1
        if jump_count == 5:
          prev_jump = False
      pos = [pos[0] + prev_size[0] + size[0], pos[1], pos[2]]
      self.add_box(pos, orn, size)
      prev_size = copy.copy(size)

  def add_one_gap(self, difficulty=1):
    size = [np.random.uniform(0.18,0.22), np.random.uniform(0.55,0.85), 0.5]
    pos = [-size[0]-0.5,0,size[2]]
    orn = [0,0,0]
    prev_size = copy.copy(size)
    prev_gap = False
    gap_count = 0
    for i in range(15):
      if i == 8:
        pos = [pos[0], pos[1], 0.01]
        size = [difficulty*0.035, size[1], pos[2]]
        gap_count = 0
        prev_gap = True
      else:
        pos = [pos[0], pos[1], 0.5]
        size = [0.18, size[1], pos[2]]
        gap_count += 1
        if gap_count == 2:
          prev_gap = False
      pos = [pos[0] + prev_size[0] + size[0], pos[1], pos[2]]
      self.add_box(pos, orn, size)
      prev_size = copy.copy(size)

  def add_jumps(self, difficulty=1, args=None):
    # if difficulty > 7:
    #   difficulty = 7
    size = [np.random.uniform(0.18,0.22), np.random.uniform(0.55,0.85), 0.5]
    pos = [-size[0]-0.5,0,size[2]]
    orn = [0,0,0]
    prev_size = copy.copy(size)
    prev_jump = False
    jump_count = 0
    for i in range(60):
      # if i % 3 == 0 and i > 3:
      # if np.random.random() < 0.5 and i > 5 and not prev_jump:
      # if (args.no_overlap and (i > 4 and i % 5 == 0)) or (not args.no_overlap and np.random.random() < 0.5 and i > 5 and not prev_jump):
      if (args.no_overlap and (i > 2 and (np.random.random() < 0.4 and not prev_jump))) or (not args.no_overlap and np.random.random() < 0.3 and i > 5 and not prev_jump):

        # pos = [pos[0], pos[1], 0.01]
        # pos = [pos[0], pos[1], 0.5 + difficulty*0.015]
        # pos = [pos[0], pos[1], 0.5 + 0.04 + difficulty*0.03]
        # pos = [pos[0], pos[1], 0.5 + difficulty*0.03]
        pos = [pos[0], pos[1], 0.5 + difficulty*0.015]
        # pos = [pos[0], pos[1], 0.5 + difficulty*0.03]
        # size = [0.05+difficulty*0.03, size[1], pos[2]]
        size = [0.05+1*0.03, size[1], pos[2]]
        jump_count = 0
        prev_jump = True
      else:
        pos = [pos[0], pos[1], 0.5]
        size = [0.18, size[1], pos[2]]
        jump_count += 1
        if jump_count == 5:
          prev_jump = False
      pos = [pos[0] + prev_size[0] + size[0], pos[1], pos[2]]
      self.add_box(pos, orn, size)
      prev_size = copy.copy(size)

  def add_gaps(self, difficulty=1, args=None):
    # if difficulty > 7:
    #   difficulty = 7
    size = [np.random.uniform(0.18,0.22), np.random.uniform(0.55,0.85), 0.5]
    pos = [-size[0]-0.5,0,size[2]]
    orn = [0,0,0]
    prev_size = copy.copy(size)
    prev_gap = False
    gap_count = 0
    for i in range(60):
      # if i % 3 == 0 and i > 3:
      if (args.no_overlap and (i > 2 and (np.random.random() < 0.4 and not prev_gap))) or (not args.no_overlap and np.random.random() < 0.3 and i > 5 and not prev_gap):
      # if np.random.random() < 0.3 and i > 5 and not prev_gap:
        # if difficulty == 0:
        #   pos = [pos[0], pos[1], 0.5]
        # else:
        #   pos = [pos[0], pos[1], 0.01]
        # size = [0.05+difficulty*0.03, size[1], pos[2]]
        pos = [pos[0], pos[1], 0.01]
        # size = [0.05+difficulty*0.05, size[1], pos[2]]
        # size = [0.06+difficulty*0.06, size[1], pos[2]]
        # size = [difficulty*0.05, size[1], pos[2]]
        size = [difficulty*0.035, size[1], pos[2]]
        # size = [difficulty*0.07, size[1], pos[2]]
        # print(size)
        # size = [difficulty*0.05, size[1], pos[2]]
        gap_count = 0
        prev_gap = True
      else:
        pos = [pos[0], pos[1], 0.5]
        size = [0.18, size[1], pos[2]]
        gap_count += 1
        if gap_count == 2:
          prev_gap = False
      pos = [pos[0] + prev_size[0] + size[0], pos[1], pos[2]]
      self.add_box(pos, orn, size, colour=[0.3,0.8,0.3,1])
      prev_size = copy.copy(size)

  def add_curvey_stairs(self, order=None, difficulty=1, height_coeff=0.07):
    
    if difficulty > 7:
      difficulty = 7
    if height_coeff > 0.07:
      height_coeff = 0.07
    
    size = [np.random.uniform(0.18,0.22), np.random.uniform(0.55,0.85), 0.5]
    # size = [0.18, np.random.uniform(0.55,0.85), 0.5]
    # size = [0.18, np.random.uniform(0.6-(0.02*difficulty), 0.5]
    # pos = [-size[0]-0.5,0,size[2]]
    pos = [-size[0],0,size[2]]

    prev_yaw = 0.0
    # max_yaw = 0.1 + difficulty*0.05
    # max_yaw = 0.18
    # max_yaw = 0.01 + difficulty*0.02
    max_yaw = difficulty*0.02
    
    # size = [size[0], size[1], size[2]]
    pos[2] = size[2]

    #  Make sure stairs aren't too random, i.e. don't have down into up directly 
    # if yaw
    # if max_yaw > 0.05:
    # new_order = []
    # stair_count = 0    
    # prev_stair = 'flat'
    # for i,o in enumerate(order):        
    #   if stair_count == 0 and ((prev_stair == 'down' and o == 'up') or (prev_stair == 'up' and o == 'down')):
    #     o = 'flat'
    #   if stair_count != 0:
    #     o = prev_stair
    #   stair_count += 1
    #   if stair_count > 1:
    #     stair_count = 0
    #   prev_stair = o
    #   new_order.append(o)
    # order = new_order

    count = 0
    lowest = 0
    for name in order:
      if name == 'down': count += 1
      if name == 'up': count -= 1
      if count > lowest:
        lowest = count
    prev_height = (lowest + 2)*height_coeff + 0.4
    pos[2] = prev_height
    size[2] = prev_height
    self.add_box(pos, [0,0,0], size)
    prev_size = copy.copy(size)
    # pos, prev_yaw = self.add_curved_box(pos, prev_yaw=prev_yaw, size=sze, yaw=0.0)
    # for _ in r ange(20):
    yaw_count = 0
    for i,o in enumerate(order):        

      if o == 'down':
        height = prev_height-height_coeff
      elif o == 'up':
        height = prev_height+height_coeff
      else:
        height = prev_height
      prev_height = height
      
      if prev_yaw > 2:
        yaw = -max_yaw
      elif prev_yaw < -2:
        yaw = max_yaw
      else:
        if yaw_count == 0:
          # yaw = np.random.choice([-max_yaw, max_yaw], p=[0.5,0.5])
          yaw = np.random.choice([-max_yaw, max_yaw, 0.0], p=[0.45,0.45,0.1])
        yaw_count += 1
        if yaw_count > 10:
          yaw_count = 0
      
      pos[2] = height
      size[2] = height
      pos, prev_yaw = self.add_curved_box(pos, prev_yaw=prev_yaw, prev_size=prev_size, size=size, yaw=yaw)
      prev_size = copy.copy(size)

  def add_curves(self, difficulty=1):
    
    if difficulty > 7:
      difficulty = 7
    size = [0.2, 0.5, 0.5]
    pos = [-size[0]-0.5,0,size[2]]

    prev_yaw = 0.0
    max_yaw = 0.05 + difficulty*0.05
    
    # size = [size[0], size[1], size[2]]
    sizes = [[0.18,0.5,0.5], [0.18,0.5,0.5],[0.18,0.5,0.5],[0.18,0.5,0.5],[0.18,0.5,0.5],[0.18,0.5,0.5],[0.18,0.5,0.5],[0.18,0.5,0.5],[0.18,0.5,0.5],[0.18,0.5,0.5]]
    # yaws = [0.25,0.25,0.25,0.25,0.25,0.25, -0.25,-0.25,-0.25,-0.25,-0.25,-0.25]
    # yaws = [-0.25,-0.25,-0.25,-0.25,-0.25,-0.25,-0.25,-0.25,-0.25,-0.25,-0.25,-0.25]
    yaws = [-0.25,-0.25,-0.25,-0.25,0.25,0.25,0.25,0.25,0.25,0.25,0.25,0.25]
    pos[2] = size[2]
    prev_size = [0,0,0]
    for size, yaw in zip(sizes ,yaws):
      pos, prev_yaw = self.add_curved_box(pos, prev_yaw=prev_yaw, prev_size=prev_size, size=size, yaw=yaw)
      prev_size = copy.copy(size)
    # yaw_count = 0
    # # for _ in range(10):
    # prev_size = sizes[0]

    # for size in sizes:
    #   # if yaw_count == 0:
    #   #   yaw = np.random.choice([-max_yaw, max_yaw, 0.0], p=[0.45,0.45,0.1])
    #   #   # yaw = np.random.choice([-max_yaw, max_yaw], p=[0.5,0.5])
    #   # else:
    #   #   yaw = yaw
    #   # yaw_count += 1
    #   # if yaw_count > 10:
    #   #   yaw_count = 0
    #   # yaw = np.random.choice([-max_yaw, max_yaw, 0.0], p=[0.3,0.3,0.4])
    #   yaw = 0.2
    #   pos, prev_yaw = self.add_curved_box(pos, prev_yaw=prev_yaw, prev_size=prev_size, size=size, yaw=yaw)
    #   prev_size = size
    
  def add_curved_box(self, pos=[0,0,0], prev_yaw=0.0, prev_size=[1,0.25,0.5], size=[1,0.25,0.5], yaw=-0.25, colour=[0.8,0.3,0.3,1]):
    length, width, _ = prev_size
    diag = np.sqrt(length**2 + width**2)
    if yaw >= 0:
      yaw = min(yaw, np.arctan2(length*2, width*2)) 
      psi = np.arctan2(-width, length)
    else:
      yaw = max(yaw, -np.arctan2(length*2, width*2)) 
      psi = np.arctan2(width, length) 
    cnr = [pos[0] + diag*np.cos(psi + prev_yaw), pos[1] + diag*np.sin(psi + prev_yaw), pos[2]]
    length, width, _ = size
    diag = np.sqrt(length**2 + width**2)
    if yaw >= 0:
      phi = np.arctan2(width, length) 
    else:
      phi = np.arctan2(-width, length)
    yaw = yaw + prev_yaw
    x = diag*np.cos(yaw + phi)
    y = diag*np.sin(yaw + phi)
  
    pos = [cnr[0] + x, cnr[1] + y, cnr[2]]
    # pos = [cnr[0], cnr[1], cnr[2]]
    # Weird memory bug? size is a list, but is sharing memory with previous sizes? casting it as a list fixes this?
    self.add_box(pos=pos, orn=[0,0,yaw], size=list(size), colour=colour)
    return pos, yaw

  def add_circle(self, pos, size=[0.1,0.01], colour=[0,0,1,1]):
    visBoxId = p.createVisualShape(p.GEOM_CYLINDER, radius=size[0], length=size[1], rgbaColor=colour)
    circle_id = p.createMultiBody(baseMass=0, baseVisualShapeIndex=visBoxId, basePosition=pos)
    self.circle_id.append(circle_id)
    self.circle_sizes.append(size)
    self.circle_colours.append(colour)

  def add_box(self, pos, orn, size, colour=[0.8,0.3,0.3,1]):
    size = copy.copy(size)
    colBoxId = p.createCollisionShape(p.GEOM_BOX, halfExtents=size)
    visBoxId = p.createVisualShape(p.GEOM_BOX, halfExtents=size, rgbaColor=colour)
    q = p.getQuaternionFromEuler(orn)
    box_id = p.createMultiBody(baseMass=0, baseCollisionShapeIndex=colBoxId, baseVisualShapeIndex=visBoxId, basePosition=pos, baseOrientation=q)
    self.box_id.append(box_id)
    self.positions.append(pos+orn)
    self.sizes.append(size)
    self.colours.append(colour)
    # print(pos, size)

  def get_box_info(self):
    # print(self.sizes)
    return [self.box_id, self.positions, self.sizes, self.colours]

if __name__=="__main__":
  import argparse
  parser = argparse.ArgumentParser()
  parser.add_argument('--render', default=False, action='store_true')
  parser.add_argument('--use_env', default=False, action='store_true')
  parser.add_argument('--display_hm', default=False, action='store_true')
  args = parser.parse_args()

  if args.use_env:
    from env import Env
    env = Env(render=args.render, display_hm=args.display_hm, obstacle_type='path')
    # env = Env(render=False, display_hm=True)
    # physicsClientId = p.connect(p.GUI)
    # obstacles = Obstacles()
    # obstacles.stairs(['down','up','down','up','down'])
    # obstacles.stairs(['up' for _ in range(10)])
    # obstacles.doughnut(size=0.5,width=0.5)
    # obstacles.path(path=[[1,1,0.5,1],[1.1,1,0.5,1],[1,1,0.5,1]])
    # obstacles.path(difficulty=4)
    # obstacles.adversarial_world()
    # obstacles.path(difficulty=10)
    # obstacles.stairs(['up','up','up','up','up','up','up','up','up','down','down','down','down','down','down'], height_coeff=0.07)

    env.reset()
    env.obstacles.remove_obstacles()
    # env.obstacles.test_world()
    env.obstacles.test_world()
    # env.obstacles.path(difficulty=1)
    env.world_map = env.get_world_map(env.obstacles.get_box_info())

    while True:
      env.step(np.random.random(env.ac_size))
      env.get_hm()
  else:
    if args.render:
      physicsClientId = p.connect(p.GUI)
    else:
      physicsClientId = p.connect(p.DIRECT) #DIRECT is much faster, but GUI shows the robot
    obstacles = Obstacles()
    p.loadMJCF("ground.xml")
    obstacles.add_show1()
    # obstacles.add_curved_box()
    # obstacles.add_curves(difficulty=7)
    # obstacles.add_curvey_stairs(order=['flat']*3 + [np.random.choice(['up','down','flat'],p=[0.3,0.3,0.4]) for _ in range(30)], height_coeff=0.07, difficulty=7)
    # obstacles.add_curvey_stairs(order=['flat']*3 + [np.random.choice(['up','down'],p=[0.5,0.5]) for _ in range(40)], height_coeff=0.07, difficulty=1)
    # obstacles.add_domain_training_world()
    # obstacles.large_world()
    # obstacles.add_domain_training_world_no_stairs()
    while True:
      pass
