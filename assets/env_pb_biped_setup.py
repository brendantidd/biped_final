import os, inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
import numpy as np
import pybullet as p
from collections import deque
from pathlib import Path
home = str(Path.home())
from mpi4py import MPI
comm = MPI.COMM_WORLD
import time
import cv2
import random
import copy

class Env():
    rank = comm.Get_rank()
    # Sim preferes 240Hz
    # simStep = 1/240
    simStep = 1/120
    timeStep = 1/120
    # Pos and vel, com, contacts + swing_feet, vel commands
    ob_size = (12*2+8 + 7 + 8 + 2 + 4)
    ac_size = 12
    total_steps = 0
    steps = 0
    Kp = 400
    prev_local_Kp = local_Kp = 400
    initial_Kp = Kp
    Kd = 0.1
    height_coeff = 0.011
    max_yaw = 0.0
    episodes = 0
    iters_so_far = 0
    rew_Kp = 1
    max_v = 1.5
    min_v = 0.5
    max_yaw = 1.25
    # Strafe max velocity
    # max_vy = 0.75
    max_vy = 0.4
    pause_time = 50
    replace_Id = None
    grid_size = 0.025    
    eval = False
    dist_difficulty = 0
    current_pol = "flat"

    def __init__(self, render=False, PATH=None,    args=None, record_step=True, display_im=False, cur=False, obstacle_type='None', control_type=None, vis=False, disturbances=True, speed_cur=False, doa=False, multi=False, dqn=False, comparison=None, initial_disturbance=100, final_disturbance=1200, vis_type='depth', difficulty=1, dist_difficulty=0, num_artifacts=1, MASTER=True, early_stop=False, multi_robots=False, obstacle_types=None):
        
        self.render = render
        self.PATH = PATH
        self.args = args
        self.record_step = record_step
        self.display_im = display_im
        self.cur = cur
        self.speed_cur = speed_cur
        self.obstacle_type = obstacle_type
        self.doa = doa
        self.multi = multi
        self.dqn = dqn
        self.comparison = comparison
        self.max_disturbance = initial_disturbance
        self.initial_disturbance = initial_disturbance
        self.final_disturbance = final_disturbance
        self.difficulty = difficulty
        self.dist_difficulty = dist_difficulty
        self.cur_num = self.args.cur_num
        self.num_artifacts = num_artifacts
        self.MASTER = MASTER
        self.early_stop = early_stop
        self.multi_robots = multi_robots
        self.obstacle_types = obstacle_types

        if self.multi_robots:
            self.robots = {}
            self.robot_num = 0
            # self.map_length = 10
            # self.robot_resolution = 0.5
            # # self.num_robots = int(self.map_length/self.robot_resolution)
            # self.num_robots = 10
            # self.robots = {i:{} for i in range(self.num_robots)}
            # self.robots['next'] = {}

        self.control_type = control_type
        self.vis = vis
        self.vis_type = vis_type
        self.stopped_start = True
        # self.stopped_start = False
        if self.doa:
            self.disturbances = False
        else:
            self.disturbances = disturbances

        # self.box_nums = [-1,0,1,2]
        self.box_nums = [-1,0,1,2,3]
        # self.box_nums = [0,1]
        # self.num_boxes = 5
        self.num_boxes = len(self.box_nums)
        # if self.args.nicks:
        #     self.box_dim = 7
        # else:
        self.box_dim = 5
        
        if self.vis_type == 'hm':
            self.im_size = [60,40,1]
        elif self.vis_type == 'state':
            self.im_size = [self.num_boxes*self.box_dim]
        elif self.vis_type == 'rgbd':
            self.im_size = [48, 48, 4]
        elif self.vis_type == 'depth':
            self.im_size = [48, 48, 1]
            # self.im_size = [980, 980, 1]
        elif self.vis_type == 'state':
            self.im_size = [self.num_boxes*self.box_dim]

        self.reward_breakdown = {'goal':deque(maxlen=100), 'pos':deque(maxlen=100), 'vel':deque(maxlen=100),    'neg':deque(maxlen=100), 'tip':deque(maxlen=100), 'com':deque(maxlen=100), 'sym':deque(maxlen=100), 'act':deque(maxlen=100)}
    
        if self.render and self.MASTER:
            self.physicsClientId = p.connect(p.GUI)
        else:
            self.physicsClientId = p.connect(p.DIRECT) #DIRECT is much faster, but GUI shows the robot


        self.sim_data = []
        self.load()

        self.box_info = None
        self.world_map = None
        self.z_offset = 0
        try:
            from assets.obstacles_setup import Obstacles
        except:
            from obstacles_setup import Obstacles
        self.obstacles = Obstacles()
        self.still_pos = np.array([0.0,0.0,-0.1,-0.2,0.0,-0.1,0.0,0.0,-0.1,-0.2,0.0,-0.1])
        self.target_joints = self.still_pos
        
        # if self.obstacle_type in ['hard_high_jumps', 'high_jumps']:
        #     self.num_artifacts = 1

        self.order = None
        self.cur_count = 0
        self.cur_len = self.args.cur_len
        if self.obstacle_type == 'mix':
            self.max_steps = 4047
        elif self.num_artifacts > 2:
            self.max_steps = 2047
            self.cur_len = 1200
        elif self.num_artifacts == 2:
            if self.obstacle_type == 'hard_steps':
                self.max_steps = 1700
            else:
                self.max_steps = 1200
            self.cur_len = 900
        elif self.dqn:
            self.max_steps = 1200
            self.cur_len = 1000    
        elif self.args.single_pol:
            self.max_steps = 1000
            self.cur_len = 700    
        else:
            if self.obstacle_type in ['steps']:
                self.max_steps = 1000
                self.cur_len = 700    
            elif self.obstacle_type in ['gaps', 'jumps', 'hard_high_jumps']:
                self.max_steps = 700
                self.cur_len = 500
            elif self.obstacle_type in ['stairs', 'flat']:
                self.max_steps = 1000
                self.cur_len = 700
            elif self.obstacle_type in ['high_jumps']:
                self.max_steps = 500
                self.cur_len = 400
            else:
                self.max_steps = 1000
                self.cur_len = 700    
        
        self.iters_since_starting = 0
        self.avg_reward = 0
        self.best_reward = 1
        self.adapts = [0.9]
        self.min_difficulty = self.difficulty
        self.prev_cur = self.cur
        self.collect_data = False

    def load(self):
        # if self.multi_robots:
        #     for i in range(self.num_robots):
        #         self.robots[i]['Id'] = p.loadURDF(currentdir + "/robot.urdf",flags = p.URDF_USE_SELF_COLLISION | p.URDF_USE_SELF_COLLISION_EXCLUDE_ALL_PARENTS)
        #     # objs = p.loadURDF(currentdir + "/robot.urdf",flags = p.URDF_USE_SELF_COLLISION )
        #     self.Id = self.robots[0]['Id']
        # else:
        if self.MASTER:
            p.loadMJCF(currentdir + "/ground.xml")
            # objs = p.loadURDF(currentdir + "/robot.urdf",flags = p.URDF_USE_SELF_COLLISION_EXCLUDE_PARENT)
            objs = p.loadURDF(currentdir + "/biped.urdf",flags = p.URDF_USE_SELF_COLLISION | p.URDF_USE_SELF_COLLISION_EXCLUDE_ALL_PARENTS)
            # objs = p.loadURDF(currentdir + "/robot.urdf",flags = p.URDF_USE_SELF_COLLISION )
            self.Id = objs
        else:
            self.Id = 1

        p.setTimeStep(self.simStep)
        p.setGravity(0,0,-9.8)
        # ======================================================================

        numJoints = p.getNumJoints(self.Id)
        # Camera following robot:
        # body_xyz, (qx, qy, qz, qw) = p.getBasePositionAndOrientation(self.Id)
        # p.resetDebugVisualizerCamera(2.0, self.camera_rotation, -10.0, body_xyz)
        # p.resetDebugVisualizerCamera(2.0, 50, -10.0, body_xyz)

        self.jdict = {}
        self.feet_dict = {}
        self.leg_dict = {}
        self.body_dict = {}
        # self.feet = ["left_heel1", "left_heel1", "right_heel1", "right_heel1", "left_toe", "right_toe"]
        self.feet = ["left_heel1", "left_heel2", "left_toe1", "left_toe2", "right_heel1", "right_heel2", "right_toe1", "right_toe2"]
        self.feet_contact = {f:True for f in self.feet}
        self.ordered_joints = []
        self.ordered_joint_indices = []
        self.shin_dict = {}
        self.shins = ["left_shin", "right_shin", "left_thigh_link", "right_thigh_link", "base_link", "torso_link2"]
        # self.shins = ["left_shin", "right_shin", "left_thigh_link", "right_thigh_link", "torso_link"]

        for j in range( p.getNumJoints(self.Id) ):
            
            info = p.getJointInfo(self.Id, j)
            link_name = info[12].decode("ascii")
            if link_name=="left_heel1": self.feet_dict["left_heel1"] = j
            if link_name=="right_heel1": self.feet_dict["right_heel1"] = j
            if link_name=="left_heel2": self.feet_dict["left_heel2"] = j
            if link_name=="right_heel2": self.feet_dict["right_heel2"] = j
            if link_name=="left_toe1": self.feet_dict["left_toe1"] = j
            if link_name=="right_toe1": self.feet_dict["right_toe1"] = j
            if link_name=="left_toe2": self.feet_dict["left_toe2"] = j
            if link_name=="right_toe2": self.feet_dict["right_toe2"] = j
            if link_name in self.shins: self.shin_dict[link_name] = j
            # print(link_name)
            if link_name=="base_link": self.body_dict["body_link"] = j
            self.ordered_joint_indices.append(j)
            if info[2] != p.JOINT_REVOLUTE: continue
            jname = info[1].decode("ascii")
            lower, upper = (info[8], info[9])
            self.ordered_joints.append( (j, lower, upper) )
            self.jdict[jname] = j
        
        self.motor_names = ["right_hip_z", "right_hip_x", "right_hip_y", "right_knee"]
        self.motor_names += ["right_ankle_x", "right_ankle_y"]
        self.motor_names += ["left_hip_z", "left_hip_x", "left_hip_y", "left_knee"]
        self.motor_names += ["left_ankle_x", "left_ankle_y"]
        self.motor_power = [300, 300, 900, 600]
        self.motor_power += [150, 300]
        self.motor_power += [300, 300, 900, 600]
        self.motor_power += [150, 300]
             
        self.motors = [self.jdict[n] for n in self.motor_names]
        
        forces = np.ones(len(self.motors))*240
        self.actions = {key:0.0 for key in self.motor_names}

        p.setJointMotorControlArray(self.Id, self.motors, controlMode=p.VELOCITY_CONTROL, forces=[0.] * len(self.motor_names))

        for key in self.feet_dict:
            p.changeDynamics(self.Id, self.feet_dict[key],lateralFriction=0.9, spinningFriction=0.9)

        self.ep_lens = deque(maxlen=5)

        self.ep_speeds = deque(maxlen=5)
        self.speeds = [0]

        # self.terrain_cur_num = 2
        self.terrain_cur_num = self.args.cur_num

        if self.args.terrain_first:
            self.ep_rewards = deque(maxlen=self.terrain_cur_num)
        else:
            self.ep_rewards = deque(maxlen=self.cur_num)
        self.total_reward = 0

        self.pos_error = deque(maxlen=5)
        self.pos_errors = [0.0]
        self.step_time = {'right':deque(maxlen=100), 'left':deque(maxlen=100)}
        self.box_num = 0
        self.replace_Id = None
        self.cur_history = []
        self.time_of_last_decay = 0
        self.ready_to_stop = False
        self.all_ready_to_stop = False
        self.start_box = 0

    def reset(self, params=None, base_before=False, box_info=None, evaluate=False):
        # self.obstacle_type = 'high_jumps'
        # print(self.dqn, self.args.single_pol)
            # if args.single_pol and args.dqn:

        if self.multi_robots and self.robot_num > 0:
            for r in range(1, self.robot_num+1):
                p.removeBody(self.robots[r]['Id'])
            self.robot_num = 0
            self.robots = {}

        # print(self.steps)
        if self.replace_Id is not None:
            p.removeUserDebugItem(self.replace_Id)
        
        self.foot_on_box = {'left':0,'right':0}     
        self.desired_box = {'left':0,'right':0}
        # if self.episodes != 0 and self.episodes % 20 == 0:
        #     print("Average step time: ", "right: ", np.mean(self.step_time['right']), "left: ", np.mean(self.step_time['left']))
        # self.prev_step_time = {'right':0, 'left':0}
        self.touchdown_pens = {0:[0], 1:[0]}
        self.touchdown_dists = {'left':0, 'right':0}
        self.touchdown_box = {'left':set(), 'right':set()}

        self.foot_box = {'right':{0}, 'left':{0}}
        self.current_foot_box = {'right':0, 'left':0}
        self.desired_box = {'right':0, 'left':0}
        self.step_sign = 0
        self.swing_on_ground_time = 0
        self.prev_thing = 0
        if self.obstacle_type in ['high_jumps','hard_high_jumps']:
            self.first_jump = True
            self.stage = '1'

        self.right_boxes = []
        self.left_boxes = []

        self.ep_lens.append(self.steps)

        # if self.args.expert:
        #     self.difficulty = 10
        #     self.height_coeff = 0.07
        # print(self.num_artifacts, self.obstacle_type, self.cur_len)
        if self.num_artifacts in [1,2,3] and self.box_info is not None:
            if self.steps >= self.max_steps - 2:
                # if self.obstacle_type not in ['flat'] and self.x_min > (self.box_info[1][-1][0] -    self.box_info[2][-1][0]):
                #     self.ep_rewards.append(self.cur_len + 1)
                # elif self.obstacle_type not in ['high_jumps', 'hard_high_jumps', 'flat'] and (self.x_min > (self.box_info[1][-1][0] - self.box_info[2][-1][0])):
                    # self.ep_rewards.append(self.cur_len + 1)
                # elif self.obstacle_type in ['flat'] and (self.box_num >= len(self.box_info[1]) - 1):
                if (self.box_num >= len(self.box_info[1]) - 1):
                    if self.start_box == 0:
                        self.ep_rewards.append(self.cur_len + 1)
            else: 
                self.ep_rewards.append(0)
        else:
            self.ep_rewards.append(self.total_reward)

        # if self.early_stop and not self.cur and self.difficulty >= 10 and len(self.ep_rewards) == self.cur_num and (np.array(self.ep_rewards) > self.cur_len).all():
        if self.early_stop and not self.cur and self.difficulty >= 10 and len(self.ep_rewards) >= self.terrain_cur_num and (np.array(self.ep_rewards)[-self.terrain_cur_num:] > self.cur_len).all():    
            self.ready_to_stop = True

        self.ep_speeds.append(np.mean(self.speeds))
        self.speeds = [0]

        self.pos_error.append(np.mean(self.pos_errors))
        self.pos_errors = []

        # self.height_coeff = 0.07
        # self.difficulty = 5

        if not self.cur and self.comparison != 'no_stage3' and not self.eval and self.disturbances and (self.max_disturbance < self.final_disturbance ) and (self.height_coeff >= 0.07 or self.difficulty >= 10) and len(self.ep_rewards) == self.cur_num and (np.array(self.ep_rewards) > self.cur_len).all():
        # if not self.cur and self.comparison != 'no_stage3' and not self.eval and self.disturbances and (self.dist_difficulty < 10 and self.args.dist_inc > 0) and (self.height_coeff >= 0.07 or self.difficulty >= 10) and len(self.ep_rewards) == self.cur_num and (np.array(self.ep_rewards) > self.cur_len).all():
            self.ep_rewards = deque(maxlen=self.cur_num)
            # self.max_disturbance += 500
            # self.dist_difficulty += 1            
            # self.max_disturbance = self.initial_disturbance
            # self.max_disturbance = self.dist_difficulty*self.args.dist_inc + self.initial_disturbance
            # self.max_disturbance = self.dist_difficulty*(self.final_disturbance - self.initial_disturbance)/10 + self.initial_disturbance
            # self.max_disturbance += (self.final_disturbance - self.initial_disturbance)/5
            self.max_disturbance = self.final_disturbance

        # if self.obstacle_type == 'flat':
        #     self.difficulty = 10


        # if self.obstacle_type in ['up_stairs', 'down_stairs', 'stairs', 'gaps', 'jumps', 'flat', 'turn']:    
        # print(self.obstacle_type)
        if box_info is not None:
            self.obstacles.remove_obstacles()
            self.box_info = box_info
            for pos, size, colour in zip(self.box_info[1], self.box_info[2], self.box_info[3]):
                self.obstacles.add_box(pos=pos[:3], orn=pos[3:], size=size, colour=colour)
        elif self.obstacle_type != 'None':    
            # print("not none")
            # if self.args.cur_decay == 'continuous_adaptive':    
            #     if self.difficulty < 10:
            #         self.difficulty = 1 + 9*(self.iters_so_far/100)
            # if (not self.cur or self.args.terrain_first) and self.difficulty < 10 and len(self.ep_rewards) == self.cur_num and (np.array(self.ep_rewards) > self.cur_len).all():    
            # if self.rank == 0:
                # print( len(self.ep_rewards) >= 3 , (np.array(self.ep_rewards)[:3] > self.cur_len).all())

            # if (not self.cur or self.args.terrain_first) and self.difficulty < 10 and len(self.ep_rewards) >= self.terrain_cur_num and (np.array(self.ep_rewards)[-self.terrain_cur_num:] > self.cur_len).all():    
            if (not self.cur or self.args.terrain_first) and self.difficulty < 10 and len(self.ep_rewards) >= self.terrain_cur_num and (np.array(self.ep_rewards) > self.cur_len).all():    
                
                if self.difficulty < 10:
                    self.difficulty += 1*self.args.inc
                if self.difficulty == 10:
                    self.ep_rewards = deque(maxlen=self.cur_num)
                    self.best_reward = self.avg_reward
                    self.save_sim_data(tag="terrain")

                    # self.best_reward = self.total_reward
                else:
                    self.ep_rewards = deque(maxlen=self.terrain_cur_num)

                self.iters_since_starting = self.iters_so_far

            self.obstacles.remove_obstacles()
            # if self.args.single_pol:
            #     # if self.obstacle_type == 'stairs':
            #     #     # self.obstacles.add_one_stair()
            #     self.order, self.ob_first_step = self.obstacles.add_straight_world(difficulty=self.difficulty, height_coeff=self.height_coeff, base_before=base_before, terrain_type=self.obstacle_type, two=self.args.two)

            # else:
            # print(self.obstacle_types); 

            self.order, self.ob_first_step = self.obstacles.add_straight_world(difficulty=self.difficulty, height_coeff=self.height_coeff, terrain_type=self.obstacle_type, num_artifacts=self.num_artifacts, args=self.args, obstacle_types=self.obstacle_types, dqn=self.dqn)

            self.box_info = self.obstacles.get_box_info()
            self.world_map = self.get_world_map(self.box_info)        
            self.box_num = 0

            # print(len(self.box_info[0]), len(self.order))
            # exit()

            # cv2.imshow('world_map_10', self.world_map)
            # cv2.waitKey(0)
        # print(self.ep_rewards1)

        # if (not self.cur or self.args.terrain_first) and self.difficulty < 10 and len(self.ep_rewards) >= self.terrain_cur_num and (np.array(self.ep_rewards)[-self.terrain_cur_num:] > self.cur_len).all():    


        # if self.cur and (self.min_difficulty >= 10 or not self.args.terrain_first):
        if self.cur and (self.difficulty >= 10 or not self.args.terrain_first):
            if self.args.cur_decay == 'continuous_exp':    
                # 735: 0.995
                if self.args.decay == 0.7:
                    self.local_Kp = self.local_Kp*(0.99)
                elif self.args.decay == 0.8:
                    self.local_Kp = self.local_Kp*(0.992)
                elif self.args.decay == 0.9:
                    self.local_Kp = self.local_Kp*(0.995)
                self.Kp = self.local_Kp
                # 459
                # self.local_Kp = self.initial_Kp*(0.992)**(self.iters_so_far - self.iters_since_starting)
                # 367 - Crashes after 200 steps
                # self.local_Kp = self.initial_Kp*(0.99)**(self.iters_so_far - self.iters_since_starting)
            elif self.args.cur_decay == 'continuous_adaptive':    
                # if self.avg_reward > self.best_reward:
                # adapt = np.clip(((2/self.best_reward)*(self.best_reward/self.avg_reward)), 0, 0.015)
                # adapt = np.clip(np.exp(-0.1*self.best_reward/self.avg_reward), 0, 0.015)
                # adapt = 0.0125*((np.clip(self.avg_reward,self.best_reward/2, self.best_reward)    - self.best_reward/2)/(self.best_reward - self.best_reward/2))
                # self.local_Kp = self.initial_Kp*(0.98 + adapt)**(self.iters_so_far - self.iters_since_starting)
                
                # adapt = 0.095*(1 - ((np.clip(self.avg_reward,self.best_reward/2, self.best_reward)    - self.best_reward/2)/(self.best_reward - self.best_reward/2)))
                min_best = 1*self.best_reward/4
                # clipped_total = np.clip(self.total_reward, min_best, self.best_reward) 
                clipped_total = np.clip(self.avg_reward, min_best, self.best_reward) 
                adapt = (1 - ((clipped_total    - min_best)/(self.best_reward - min_best)))
                if self.rank == 0 and self.iters_so_far % 10 == 0:
                    print(clipped_total, min_best, self.best_reward, self.total_reward, adapt, 0.9+0.095*adapt)
                # self.local_Kp = self.initial_Kp*(0.9 + adapt)**(self.iters_so_far - self.iters_since_starting)
                self.adapts.append(adapt)
                self.local_Kp = self.local_Kp*(0.95 + 0.042*adapt)
                
            elif self.args.cur_decay == 'continuous_linear':
                # self.local_Kp = self.local_Kp - 3
                # 120 = 3*40 (3 milestones, 40 decays)
                self.local_Kp = 400 - (400/120)*(self.iters_so_far - self.iters_since_starting)
                
            # if len(self.ep_rewards) == self.cur_num and (np.array(self.ep_rewards) > self.cur_len).all() or (self.args.cur_decay == 'adaptive' and len(self.cur_history) > 0 and (self.iters_so_far - self.time_of_last_decay) > (self.cur_history[-1] + 15)):
            # elif len(self.ep_rewards) == self.cur_num and (np.array(self.ep_rewards) > self.cur_num).all() or (self.args.cur_decay == 'adaptive' and len(self.cur_history) > 0 and (self.iters_so_far - self.time_of_last_decay) > (self.cur_history[-1] + 15)) and (self.Kp == self.local_Kp):
            elif len(self.ep_rewards) == self.cur_num and (np.array(self.ep_rewards) > self.cur_len).all() or (self.args.cur_decay == 'adaptive' and len(self.cur_history) > 0 and (self.iters_so_far - self.time_of_last_decay) > (self.cur_history[-1] + 15)) and (self.Kp == self.local_Kp):
                
                # self.ep_lens = deque(maxlen=self.cur_num)
                # self.pos_error = deque(maxlen=self.cur_num)
                # if self.args.cur_decay == 'exp':
                #     self.local_Kp = self.local_Kp*(0.7 + self.args.decay_rate)
                # elif self.args.cur_decay == 'linear':
                #     self.local_Kp = max(self.local_Kp - 40/self.args.decay_rate, 0)
                # elif self.args.cur_decay == 'sigmoid':
                #     self.local_Kp = self.initial_Kp*(1 - 1/(1+np.exp(-(self.cur_count - 5*self.args.decay_rate))))
                #     self.cur_count += 1
                if len(self.cur_history) > 1:
                    print(self.rank, self.local_Kp, self.cur_len, self.iters_so_far, self.cur_history[-1], self.time_of_last_decay, len(self.ep_rewards) == self.cur_num and (np.array(self.ep_rewards) > self.cur_len).all(),    (self.iters_so_far - self.time_of_last_decay) > (self.cur_history[-1] + 15))
                if self.args.yu:
                    self.Kp = self.prev_local_Kp = min((1+(1-self.args.decay))*self.local_Kp, self.initial_Kp)

                # self.prev_local_Kp = self.local_Kp
                # print("kp should be greater than local", self.Kp, self.prev_local_Kp, self.local_Kp, 1 + 1- self.args.decay, 1+(1-self.args.decay)*self.local_Kp, self.initial_Kp)
                if self.args.cur_decay == 'adaptive':
                    self.cur_history.append(self.iters_so_far - self.time_of_last_decay)
                    self.time_of_last_decay = self.iters_so_far
                    # 10 == good 5 < make harder,    15 > make easier
                    if self.cur_history[-1] < 5:
                        self.local_Kp = max(self.local_Kp - 60, 0)
                    elif self.cur_history[-1] > 15:
                        self.local_Kp = max(self.local_Kp - 20, 0)
                    else:
                        self.local_Kp = max(self.local_Kp - 40, 0)
                    if len(self.cur_history) > 1:
                        if self.cur_history[-1] < 5 and self.cur_history[-2] < 5:
                            self.cur_num = int(np.clip(self.cur_num - 1, 2, 7))
                        elif self.cur_history[-1] > 15 and self.cur_history[-2] > 15:
                            self.cur_num = int(np.clip(self.cur_num + 1, 2, 7))
                elif self.args.cur_decay == 'exp':
                    # self.local_Kp = self.local_Kp*0.70     
                    self.local_Kp = self.local_Kp*self.args.decay     
                elif self.args.cur_decay == 'linear':
                    # self.local_Kp = max(self.local_Kp - 40, 0)
                    self.local_Kp = max(self.local_Kp - self.args.decay, 0)
                elif self.args.cur_decay == 'sigmoid':
                    self.local_Kp = self.initial_Kp*(1 - 1/(1+np.exp(-(self.cur_count - 5))))
                    self.cur_count += 1

                self.ep_rewards = deque(maxlen=self.cur_num)
            if self.args.yu:
                if self.args.rand_Kp:
                    self.Kp = np.random.uniform(self.local_Kp, self.prev_local_Kp)
                else:
                    # self.Kp = max(self.local_Kp, self.prev_local_Kp-1)
                    # print('before', self.Kp - 1, self.local_Kp)
                    self.Kp = max(self.local_Kp, self.Kp-1.0)
                    # print(self.Kp)
                

        if self.prev_cur != self.cur:
            self.save_sim_data(tag="cur")
        self.prev_cur = self.cur

        if self.episodes > 0 and self.record_step:
            self.save_sim_data()
        self.sim_data = []

        start_rot = [0.0,0.0,0.0]
        start_orn = p.getQuaternionFromEuler(start_rot)
        
        self.body_xyz, self.yaw = [0.0, 0.0, 0.0 + 0.95], start_rot[2]
        self.boxes = np.zeros(self.im_size)
        
        self.ob_dict = {'prev_right_foot_left_ground':False,'prev_left_foot_left_ground':False,'left_foot_left_ground':False,'right_foot_left_ground':False}
        
        self.ob_dict['prev_right_heel1'] = self.ob_dict['right_heel1'] = False
        self.ob_dict['prev_right_heel2'] = self.ob_dict['right_heel2'] = False
        self.ob_dict['prev_right_toe2']     = self.ob_dict['right_toe2']     = False
        self.ob_dict['prev_right_toe1']     = self.ob_dict['right_toe1']     = False
        self.ob_dict['prev_left_heel1']    = self.ob_dict['left_heel1']    = False
        self.ob_dict['prev_left_heel2']    = self.ob_dict['left_heel2']    = False
        self.ob_dict['prev_left_toe2']    = self.ob_dict['left_toe2']    = False
        self.ob_dict['prev_left_toe1']        = self.ob_dict['left_toe1']        = False
        
        # self.ob_dict['ready_to_walk'] = False
        if self.obstacle_type == 'one_leg_hop':
            self.initial_swing_foot = self.ob_first_step > 0
        else:
            self.initial_swing_foot = np.random.randint(2)
        if self.initial_swing_foot:
            self.ob_dict['swing_foot'] = True
        else: 
            self.ob_dict['swing_foot'] = False
        self.ob_dict['cur_swing_foot'] = self.ob_dict['swing_foot']

        self.ob_dict['right_foot_swing'] = False
        self.ob_dict['left_foot_swing'] = False
        self.first_step = True
        self.step_count = 0
        if self.args.single_pol:
            self.hj_steps = None
            # if self.obstacle_type == 'stairs':
                # rand_box = np.random.randint(0,5)
                # rand_box = np.random.randint(0,5)
                # rand_box = np.random.randint(1,3)
            # else:
            if self.dqn:
            #     # rand_box = np.random.randint(1,6)
                # rand_box = 4
                # rand_box = 3
                rand_box = np.random.randint(1,5)
            else:
                rand_box = np.random.randint(1,5)

            if self.collect_data and self.obstacle_type == 'high_jumps':
                # rand_box = 5
                # pos = [self.box_info[1][5][0] + np.random.uniform(-0.23,-0.18), np.random.uniform(-0.1,0.1), 0.95+self.z_offset] 
                pos = [self.box_info[1][5][0] + np.random.uniform(0,0), np.random.uniform(-0.1,0.1), 0.95+self.z_offset] 
                orn, joints, base_vel, joint_vel = list(p.getQuaternionFromEuler([0,0,np.random.uniform(-0.1,0.1)])), [0.]*len(self.motor_names), [[0,0,0],[0,0,0]], [0.]*len(self.motor_names)
            elif self.obstacle_type == 'flat':
                # print("doing flat stuff")
                pos = [self.box_info[1][rand_box][0] + np.random.uniform(0,0), self.box_info[1][rand_box][1] + np.random.uniform(-0.1,0.1), 0.95+self.z_offset] 
                orn, joints, base_vel, joint_vel = list(p.getQuaternionFromEuler([0,0,np.random.uniform(-0.3,0.3)])), [0.]*len(self.motor_names), [[0,0,0],[0,0,0]], [0.]*len(self.motor_names)
                # base_vel = []
                for i,j in enumerate(joints):
                    if i in [0,1,2,6,7,8]:
                        if (i in [7,8]):
                            if (i == 7 and joints[1] > 0) or (i == 8 and joints[2] < 0):
                                joints[i] = np.random.uniform(low=0.0, high=0.5)
                            else:
                                joints[i] = np.random.uniform(low=-0.5, high=0.0)
                        else:
                            joints[i] = np.random.uniform(low=-0.5, high=0.5)
                    elif i in [3,9]:
                        joints[i] = np.random.uniform(low=-0.5, high=0.0)
                # print(joints)
                # if self.disturbances:
                #     c = [0.01, 0.01, 0.01, 0.012, 0.1, 0.01, 0.01]
                #     pos[0] += np.random.uniform(low=0.2, high=c[0])
                #     pos[1] += np.random.uniform(low=-c[0], high=c[0])
                #     # No noise to z, can get stuck in the floor
                #     orn[0] += np.random.uniform(low=-c[2], high=c[2])
                #     orn[1] += np.random.uniform(low=-c[2], high=c[2])
                #     orn[2] += np.random.uniform(low=-c[2], high=c[2])
                #     # Noise on w term does funky things
                # for i in range(len(base_vel[0])):
                #     if i == 0:
                #         base_vel[0][i] += np.random.uniform(low=-0.5, high=1.0)
                #     else:
                #         base_vel[0][i] += np.random.uniform(low=-0.5, high=0.5)
                #     base_vel[1][i] += np.random.uniform(low=-0.5, high=0.5)    

            else:
                # print("here"); exit()
                # This is the random position for training setup policies
                # pos = [self.box_info[1][rand_box][0] + np.random.uniform(-0.23,-0.18), np.random.uniform(-0.2,0.2), 0.95+self.z_offset] 
                # orn, joints, base_vel, joint_vel = list(p.getQuaternionFromEuler([0,0,0])), [0.]*len(self.motor_names), [[0,0,0],[0,0,0]], [0.]*len(self.motor_names)

                # This is what it was:
                # pos = [self.box_info[1][rand_box][0] + np.random.uniform(-0.23,-0.18), np.random.uniform(-0.2,0.2), 0.95+self.z_offset] 
                # orn, joints, base_vel, joint_vel = list(p.getQuaternionFromEuler([0,0,np.random.uniform(-0.3,0.3)])), [0.]*len(self.motor_names), [[0,0,0],[0,0,0]], [0.]*len(self.motor_names)

                # New set: 
                # This one
                # pos = [self.box_info[1][rand_box][0] + np.random.uniform(-0.23,-0.18), np.random.uniform(-0.35,0.35), 0.95+self.z_offset] 
                # orn, joints, base_vel, joint_vel = list(p.getQuaternionFromEuler([0,0,np.random.uniform(-0.7,0.7)])), [0.]*len(self.motor_names), [[0,0,0],[0,0,0]], [0.]*len(self.motor_names)
                if evaluate:
                    pos, orn, joints, base_vel, joint_vel = [self.box_info[1][1][0],self.box_info[1][1][1],0.95 + self.z_offset], list(p.getQuaternionFromEuler([0,0,self.box_info[1][1][5]])), [0.]*len(self.motor_names), [[0,0,0],[0,0,0]], [0.]*len(self.motor_names)
                else:
                    rand_box = np.random.randint(1,5)
                    pos = [self.box_info[1][rand_box][0] + np.random.uniform(-0.23,-0.18), np.random.uniform(-0.24,0.24), 0.95+self.z_offset] 
                    orn, joints, base_vel, joint_vel = list(p.getQuaternionFromEuler([0,0,np.random.uniform(-0.65,0.65)])), [0.]*len(self.motor_names), [[0,0,0],[0,0,0]], [0.]*len(self.motor_names)



                # pos = [self.box_info[1][rand_box][0] + np.random.uniform(-0.23,-0.18), np.random.uniform(-0.24,0.24), 0.95+self.z_offset] 
                # orn, joints, base_vel, joint_vel = list(p.getQuaternionFromEuler([0,0,np.random.uniform(-0.707,0.707)])), [0.]*len(self.motor_names), [[0,0,0],[0,0,0]], [0.]*len(self.motor_names)
                
                # pos = [self.box_info[1][1][0] + np.random.uniform(-0.23,-0.18), 0.24, 0.95+self.z_offset] 
                # orn, joints, base_vel, joint_vel = list(p.getQuaternionFromEuler([0,0,0.707   ])), [0.]*len(self.motor_names), [[0,0,0],[0,0,0]], [0.]*len(self.motor_names)

            
        elif self.doa or self.multi or self.obstacle_type in ['turn','mix']:
            # if self.multi or self.obstacle_type == 'mix':
            #     self.box_num = 1
            # else:
            #     self.box_num = np.random.randint(0, len(self.box_info[0])-1)
            # box_x, box_y, box_yaw = self.box_info[1][self.box_num][0], self.box_info[1][self.box_num][1], self.box_info[1][self.box_num][5]
            # self.body_xyz, self.yaw = [box_x, box_y, 0.0], box_yaw 
            
            # if self.obstacle_type == 'turn':
            #     # Choices are 5 degres from the 90 degree positions for north, south east west (8 possibilities)
            #     if self.box_num < 3:
            #         self.initial_yaw = np.random.choice([1.48, -1.48,    1.65, -1.65, 3.05, -3.05])
            #     elif self.box_num >= len(self.box_info[0]) - 2:
            #         self.initial_yaw = np.random.choice([1.48, -1.48,    1.65, -1.65, 0.08, -0.08])
            #     else:
            #         self.initial_yaw = np.random.choice([1.48, -1.48,    1.65, -1.65])
            #     self.initial_x, self.initial_y = self.body_xyz[0], self.body_xyz[1]
            #     # self.initial_yaw = np.pi
            # else:
            #     self.initial_yaw = 0
            #     self.initial_x, self.initial_y = self.body_xyz[0], self.body_xyz[1]

            # self.body_xyz, self.yaw = [box_x, box_y + np.random.uniform(-0.2, 0.2), 0.0], np.random.uniform(-np.pi, np.pi)
            # self.body_xyz, self.yaw = [0.0, 0.0, 0.0], 0.0
            # print("restoring fgrom here")
            # pos, orn, joints, base_vel, joint_vel = [self.body_xyz[0],self.body_xyz[1],0.95 + self.z_offset], list(p.getQuaternionFromEuler([0,0,self.initial_yaw])), [0.]*len(self.motor_names), [[0,0,0],[0,0,0]], [0.]*len(self.motor_names)
            
            # pos, orn, joints, base_vel, joint_vel = [self.box_info[1][0][0],self.box_info[1][0][1],0.95 + self.z_offset], list(p.getQuaternionFromEuler([0,0,self.box_info[1][0][5]])), [0.]*len(self.motor_names), [[0,0,0],[0,0,0]], [0.]*len(self.motor_names)
            # print("this is where we are", self.order.index("high_jumps"))
            
            pos, orn, joints, base_vel, joint_vel = [self.box_info[1][1][0],self.box_info[1][1][1],0.95 + self.z_offset], list(p.getQuaternionFromEuler([0,0,self.box_info[1][1][5]])), [0.]*len(self.motor_names), [[0,0,0],[0,0,0]], [0.]*len(self.motor_names)

            # rand_box = np.random.randint(1,5)
            # pos = [self.box_info[1][rand_box][0] + np.random.uniform(-0.23,-0.18), np.random.uniform(-0.24,0.24), 0.95+self.z_offset] 
            # orn, joints, base_vel, joint_vel = list(p.getQuaternionFromEuler([0,0,np.random.uniform(-0.65,0.65)])), [0.]*len(self.motor_names), [[0,0,0],[0,0,0]], [0.]*len(self.motor_names)


        else:
            if self.obstacle_type in ['high_jumps']:
                # pos = [self.box_info[1][5][0] + np.random.uniform(-0.23,-0.18), np.random.uniform(-0.15,0.15), 0.95+self.z_offset] 
                if self.num_artifacts == 1:
                    # self.start_box = np.random.choice([0,1], p=[0.8,0.2])
                    # self.start_box = np.random.choice([0,1])
                    self.start_box = 0
                else:
                    self.start_box = 0
                    # self.start_box = np.random.choice([0,1,2,3], p=[0.7,0.1,0.1,0.1])
                # self.start_box = 1
                # pos = [self.box_info[1][self.start_box][0], np.random.uniform(-0.15,0.15), 0.95+self.z_offset] 
                if self.start_box == 0:
                    pos = [self.box_info[1][0][0] + np.random.uniform(-0.23,-0.18), np.random.uniform(-0.15,0.15), 0.95+self.z_offset] 
                else:
                    pos = [self.box_info[1][1][0], np.random.uniform(-0.15,0.15), 0.95+self.z_offset] 


                # if self.start_box == 0:
                #     # pos = [self.box_info[1][0][0] + np.random.uniform(-0.23,-0.18), np.random.uniform(-0.15,0.15), 0.95+self.z_offset] 
                #     pos = [self.box_info[1][0][0] + np.random.uniform(-0.05,0.1), np.random.uniform(-0.15,0.15), 0.95+self.z_offset] 
                # else:
                #     pos = [self.box_info[1][1][0], np.random.uniform(-0.15,0.15), 0.95+self.z_offset] 
            elif self.obstacle_type in ['hard_high_jumps','one_leg_hop']:
                    # pos = [self.box_info[1][5][0] + np.random.uniform(0,0.1), np.random.uniform(-0.15,0.15), 0.95+self.z_offset] 
                if self.obstacle_type in ['hard_high_jumps']:
                    if self.num_artifacts == 1:
                        self.start_box = np.random.choice([0,2], p=[0.8,0.2])
                    else:
                        self.start_box = np.random.choice([0,2,4,6], p=[0.7,0.1,0.1,0.1])
                    # self.start_box = 2
                    pos = [self.box_info[1][self.start_box][0], np.random.uniform(-0.15,0.15), 0.95+self.z_offset] 
                    # if self.start_box == 0:
                    #     pos = [self.box_info[1][0][0] + np.random.uniform(0,0.1), np.random.uniform(-0.15,0.15), 0.95+self.z_offset] 
                    # else:
                    #     pos = [self.box_info[1][2][0] + np.random.uniform(0,0.1), np.random.uniform(-0.15,0.15), 0.95+self.z_offset] 
                else:
                    pos = [self.box_info[1][0][0] + np.random.uniform(0,0.1), np.random.uniform(-0.15,0.15), 0.95+self.z_offset] 
                # pos = [self.box_info[1][5][0] - 0.2, np.random.uniform(-0.15,0.15), 0.95+self.z_offset] 
            else:
                pos = [np.random.uniform(-0.05,0.1), np.random.uniform(-0.15,0.15), 0.95+self.z_offset] 
            
            if self.eval:
                if self.obstacle_type in ['high_jumps']:
                    # pos = [self.box_info[1][5][0] + np.random.uniform(-0.23,-0.18), 0, 0.95+self.z_offset] 
                    pos = [self.box_info[1][0][0] + np.random.uniform(-0.23,-0.18), 0, 0.95+self.z_offset] 
                elif self.obstacle_type in ['hard_high_jumps','one_leg_hop']:
                    # pos = [self.box_info[1][5][0] + np.random.uniform(0,0.1), 0, 0.95+self.z_offset] 
                    pos = [self.box_info[1][0][0] + np.random.uniform(0,0.1), 0, 0.95+self.z_offset] 
                else:
                    pos = [np.random.uniform(-0.05,0.1), 0, 0.95+self.z_offset] 
                orn, joints, base_vel, joint_vel = list(p.getQuaternionFromEuler([0,0,0])), [0.]*len(self.motor_names), [[0,0,0],[0,0,0]], [0.]*len(self.motor_names)
            else:
                orn, joints, base_vel, joint_vel = list(p.getQuaternionFromEuler([0,0,np.random.uniform(-0.3,0.3)])), [0.]*len(self.motor_names), [[0,0,0],[0,0,0]], [0.]*len(self.motor_names)
            
            # pos = [self.box_info[1][4][0], 0, 0.95+self.z_offset] 
            # orn, joints, base_vel, joint_vel = list(p.getQuaternionFromEuler([0,0,0])), [0.]*len(self.motor_names), [[0,0,0],[0,0,0]], [0.]*len(self.motor_names)
            

            base_vel = [[0,0,0],[0,0,0]]        
            joints, joint_vel = [0.]*len(self.motor_names), [0.]*len(self.motor_names)

        if self.obstacle_type in ['hard_high_jumps','high_jumps']:
            mult = 1
            if self.obstacle_type == 'hard_high_jumps':
                mult = 2
            self.high_jump_first_left_ground = False
            self.high_jump_step = False
            self.high_jump_second_on_ground = False
            self.high_jump_second_left_ground = False
            self.second_high_jump_step = False

            if self.start_box >= 1*mult:
                self.high_jump_first_left_ground = True
                self.high_jump_step = True
            if self.start_box >= 2*mult:
                self.high_jump_second_on_ground = True
            if self.start_box >= 3*mult:
                self.high_jump_second_left_ground = True
                self.second_high_jump_step = True

        self.body_xyz[0] = pos[0]
        self.body_xyz[1] = pos[1]
        self.get_z_offset()
        # print(pos)
        pos = [pos[0], pos[1], 0.95+self.z_offset]

        # if self.disturbances:
        #     c = [0.01, 0.01, 0.01, 0.012, 0.1, 0.01, 0.01]
        #     pos[0] += np.random.uniform(low=0.2, high=c[0])
        #     pos[1] += np.random.uniform(low=-c[0], high=c[0])
        #     # No noise to z, can get stuck in the floor
        #     orn[0] += np.random.uniform(low=-c[2], high=c[2])
        #     orn[1] += np.random.uniform(low=-c[2], high=c[2])
        #     orn[2] += np.random.uniform(low=-c[2], high=c[2])
        #     # Noise on w term does funky things
        #     for i in range(len(base_vel[0])):
        #         base_vel[0][i] += np.random.uniform(low=-c[3], high=c[3])
        #         base_vel[1][i] += np.random.uniform(low=-c[4], high=c[4])    

        # print("starting pos", pos)


        self.target_pos = [pos[0], 0, pos[2]]
        self.set_position(pos, orn, joints, base_vel, joint_vel)

        self.prev_speed = self.speed = self.original_speed = self.prev_yaw_speed = self.yaw_speed = self.original_yaw_speed = self.prev_y_speed = self.y_speed = self.original_y_speed = 0.0
        self.prev_time_of_step = self.time_of_step = 0

        self.total_reward = 0
        self.steps = 0
        self.step_count = 0
        self.episodes += 1
        self.prev_artifact = None
        self.get_observation()
        self.prev_state = self.joints + self.joint_vel
        self.prev_joints = self.joints 
        self.prev_joint_vel = self.joint_vel

        # self.action_store = deque(maxlen=self.time_of_step)
        self.action_store = np.zeros([self.time_of_step, 4])
        
        # Work out hip angle needed to evenly walk over blocks
        if self.box_info is None:
            self.x_dist = 0.45
        else:
            self.x_dist = self.box_info[2][0][0]*2
        self.thigh_length = 0.29
        self.shin_length = 0.31
        standing_leg_length = np.sqrt(self.thigh_length**2 + self.shin_length**2 - 2*self.thigh_length*self.shin_length*np.cos(np.pi-0.2))
        angle = np.arccos((self.x_dist/2)/standing_leg_length)
        self.hip_y_total_angle = np.pi - 2*angle 
        self.hip_x_total_angle = 0.05
        # self.hip_z_total_angle = 0.5
        self.hip_z_total_angle = 1.49

        self.state = self.joints + self.joint_vel + self.body + self.contacts + [self.original_speed, self.original_yaw_speed]
        return np.array(self.state)

    def step(self, actions=np.zeros(12), freeze_robot=False, frozen=False, set_position=None):

        # print(self.target_pos)
        # print(self.speed)
        # if self.obstacle_type == 'mix' and self.order[self.box_num] == 'zero':
        # if self.order and self.order[self.box_num] == 'zero' and self.body_xyz[0] > self.box_info[1][self.box_num][0]:
        # if self.order and self.order[self.box_num] == 'zero' and self.x_min > (self.box_info[1][self.box_num][0] - self.box_info[2][self.box_num][0]):
        # print(self.x_min, (self.box_info[1][-1][0] - self.box_info[2][-1][0]))

        # print(self.original_speed)
        if (not self.obstacle_type == 'mix' and not self.multi and (((self.steps*self.timeStep) % 3) == 0 and self.steps != 0)) or self.steps == self.pause_time:
            # if random.random() < 0.05 and not self.doa and self.obstacle_type != 'run':
            # if random.random() < 0.2 and self.obstacle_type not in 'flat' and not self.args.single_pol and self.steps > 100 and not self.eval:
            # # if random.random() < 0.2 and self.obstacle_type == 'flat' and not self.args.single_pol and self.steps > 100 and not self.eval:
            #     self.original_speed = 0
            # else:
            if not self.speed_cur:                
                if self.obstacle_type == 'run':
                    self.original_speed = 4.0
                # elif self.obstacle_type in ['stairs','jumps','gaps']:
                #     self.original_speed = 0.75 
                else:
                    self.original_speed = 1.0 
                # self.original_speed = 2.0
                # self.original_speed = 0.5
                # print(self.steps, self.original_speed)
            else:
                self.original_speed = np.random.uniform(low=self.min_v,high=self.max_v)                     
            
            if random.random() < 0.05 or not self.speed_cur:
                self.yaw_speed = 0
            else:
                self.yaw_speed = np.random.uniform(low=-self.max_yaw,high=self.max_yaw)                     
            # if self.strafe:
            #         self.y_v = np.random.uniform(low=-self.max_vy,high=self.max_vy)                    
        
            if abs(self.original_speed) >= 0.2:
                self.time_of_step = int(60/abs(self.original_speed))
                self.dsp_time = int(20/abs(self.original_speed))
                self.action_store = np.zeros([self.time_of_step, 4])
                # self.action_store = deque(maxlen=self.time_of_step)
            else:
                self.time_of_step = 0

        # if self.steps > 200:
        #     self.original_speed = 0.0

        # if not self.speed_cur and self.steps == self.pause_time:
        #     if abs(self.original_speed) >= 0.2:
        #         self.time_of_step = int(60/abs(self.original_speed))
        #         self.dsp_time = int(20/abs(self.original_speed))
        #         self.action_store = deque(maxlen=self.time_of_step)
        #     else: 
        #         self.time_of_step = 0

        # if not self.speed_cur and self.steps > self.pause_time:
        #     self.yaw_speed = 0
        #     if self.obstacle_type == 'run':
        #         self.original_speed = self.max_v
        #     else:
        #         self.original_speed = 1

        if self.obstacle_type == 'turn':
            if self.steps - self.pause_time < 0:
                self.target_yaw = self.initial_yaw
                self.original_yaw_speed = 0
            else:
                # If on an edge, turn shortest path to correct facing
                if self.box_num < 3:
                    self.target_yaw = 0
                elif self.box_num >= ( len(self.box_info[0]) - 2 ):
                    self.target_yaw = np.pi
                else:
                    if self.initial_yaw < np.pi/2 and self.initial_yaw >= -np.pi/2:
                        self.target_yaw = 0
                    else:
                        self.target_yaw = np.pi
                if self.target_yaw == np.pi and self.initial_yaw <= -np.pi/2:
                    self.target_yaw = -np.pi

                yaw_error = self.target_yaw - self.yaw
                
                if abs(yaw_error) < 0.1:
                    self.original_yaw_speed = 0
                    # self.box_num = np.random.randint(0, len(self.box_info[0])-1)
                    # box_x, box_y, box_yaw = self.box_info[1][self.box_num][0], self.box_info[1][self.box_num][1], self.box_info[1][self.box_num][5]
                    # self.body_xyz, self.yaw = [box_x, box_y, 0.0], box_yaw 
                    # self.ini tial_yaw = np.random.choice([1.48, -1.48,    1.65, -1.65, 0.08, -0.08, 3.05, -3.05])

                    # pos, orn, joints, base_vel, joint_vel = [self.body_xyz[0],self.body_xyz[1],0.95 + self.z_offset], list(p.getQuaternionFromEuler([0,0,self.initial_yaw])), [0.]*len(self.motor_names), [[0,0,0],[0,0,0]], [0.]*len(self.motor_names)
                    # self.set_position(pos, orn, joints, base_vel, joint_vel)
                else:
                    self.original_yaw_speed = np.sign(yaw_error)*1.5
                    self.time_of_step = int(60/abs(self.original_yaw_speed))
                    # self.action_store = np.zeros([self.time_of_step, 2])
                self.original_speed = 0

        speeds = np.array([self.original_speed, self.original_y_speed, self.original_yaw_speed])
        if (speeds != 0).any():
            self.time_of_step = int(60/np.max(abs(speeds)))
        #     self.action_store = np.zeros([self.time_of_step, 2])

        # # print(self.box_num, len(self.box_info[1]) - 1)
        # if self.obstacle_type != 'flat' and self.x_min > (self.box_info[1][-1][0] - self.box_info[2][-1][0]):
        #     # print("setting speed to zero")
        #     self.speed = self.original_speed = 0.0            
        # # elif self.obstacle_type == 'flat' and np.sqrt((self.body_xyz[0] - (self.box_info[1][-1][0]))**2 + (self.body_xyz[1] - (self.box_info[1][-1][1]))**2) < 1.5:
        # elif self.obstacle_type == 'flat' and self.box_num >= len(self.box_info[1]) - 1:
        #     self.speed = self.original_speed = 0.0
        # # print(np.sqrt((self.body_xyz[0] - (self.box_info[1][-1][0]))**2 + (self.body_xyz[1] - (self.box_info[1][-1][1]))**2))
        # elif self.obstacle_type in ['high_jumps', 'hard_high_jumps']:
        #     # print(len(self.box_info[1]))
        #     # print(self.x_min, (self.box_info[1][-1][0] -    self.box_info[2][-1][0]))
        #     # if self.x_min > (self.box_info[1][6][0] +    self.box_info[2][6][0]) and (not self.ob_dict['left_foot_left_ground'] or not self.ob_dict['right_foot_left_ground']):
        #     # if self.x_min > (self.box_info[1][-1][0] -    self.box_info[2][-1][0]) and (not self.ob_dict['left_foot_left_ground'] or not self.ob_dict['right_foot_left_ground']):
        #     print(self.box_num, len(self.box_info[1])-1)
        #     if self.x_min > (self.box_info[1][-1][0] -    self.box_info[2][-1][0]):
        #         print("setting speed to 0", self.box_info)
        #         self.speed = self.original_speed = 0
        # elif self.obstacle_type in ['one_leg_hop']:
        #     if self.x_min > (self.box_info[1][-1][0] -    self.box_info[2][-1][0]):
        #         self.speed = self.original_speed = 0
        # else:
        #     if self.box_num >= (len(self.box_info[1]) - 1):
        #         self.speed = self.original_speed = 0

        # Limit acceleration
        # if self.original_speed > 0.5:
        self.speed = np.clip(self.original_speed, self.prev_speed - 0.01, self.prev_speed + 0.01)
        # self.yaw_speed = np.clip(self.original_yaw_speed, self.prev_yaw_speed - 0.05, self.prev_yaw_speed + 0.05)
        self.yaw_speed = 0
        self.y_speed = 0.0
        # print(self.steps, self.speed)
        # else:
        #     self.speed = self.original_speed

        # if self.steps > self.pause_time:
            # if self.obstacle_type == 'one_leg_hop':
            #     self.speed = 0.75 
            # elif self.obstacle_type == 'high_jump':
            #     # self.speed = 0.75 
            #     self.speed = 1.2 
            # elif self.obstacle_type == 'hard_high_jump':
            #     self.speed = 2.0 

        if self.time_of_step != 0 and (np.array([self.original_speed, self.original_y_speed, self.original_yaw_speed]) == 0).all() and (np.abs([self.speed, self.y_speed, self.yaw_speed]) < 0.01).all():
            self.time_of_step = 0

        # if self.yaw > np.pi/4 or self.yaw < -np.pi/4:
        #     self.yaw_speed = 0
# 
        # print(self.box_num)
        # self.speed = 0
        # if self.disturbances and random.random() < 0.02 and (not self.ob_dict['left_foot_left_ground'] or not self.ob_dict['right_foot_left_ground']) and self.box_num not in    [6,8]:
        if self.args.dist_off_ground:
            if self.disturbances and random.random() < 0.02:
            # if self.disturbances and random.random() < 0.025:
                self.add_disturbance(self.max_disturbance)
        else:
            if self.disturbances and random.random() < 0.02 and (not self.ob_dict['left_foot_left_ground'] or not self.ob_dict['right_foot_left_ground']):
            # if self.disturbances and random.random() < 0.00825 and (not self.ob_dict['left_foot_left_ground'] or not self.ob_dict['right_foot_left_ground']):
                # print("adding disturbance", self.max_disturbance)
                self.add_disturbance(self.max_disturbance)
                    
        self.prev_state = self.joints + self.joint_vel
        self.prev_joints = self.joints    
        self.prev_joint_vel = self.joint_vel
        self.actions = actions
        
        # if self.args.stage3 and not self.eval:
        # if not self.eval and self.dist_difficulty > 0 and self.args.dist_inc > 0:
        #     # self.actions = actions + np.random.uniform(-self.dist_difficulty/30,self.dist_difficulty/30,size=self.ac_size)
        #     # actions = actions + np.random.normal(scale = self.dist_difficulty/30, size=self.ac_size)
        #     actions = actions + np.random.normal(scale = self.dist_difficulty/50, size=self.ac_size)
        #     # print(np.random.uniform(-1,1,size=self.ac_size))
        #     # print(np.random.normal(scale = self.dist_difficulty/30, size=self.ac_size))
        #     # self.actions = actions 
        #     # print(self.actions)
        # # else:
        # #     self.actions = actions
        
        if self.obstacle_type != 'None' and self.speed != 0:        
            # print('before', self.speed)
            if self.obstacle_type not in ['flat','mix']:
                if self.x_min > (self.box_info[1][-1][0] - self.box_info[2][-1][0]):
                    if self.obstacle_type not in ['high_jumps', 'hard_high_jumps']:
                        self.speed = self.original_speed = 0
                    elif self.obstacle_type in ['high_jumps', 'hard_high_jumps'] and (not self.ob_dict['left_foot_left_ground'] or not self.ob_dict['right_foot_left_ground']):
                        self.speed = self.original_speed = 0
                        # print("setting speed to zero")
            elif self.box_num >= (len(self.box_info[1]) - 1):
                self.speed = self.original_speed = 0
            # print('after', self.speed)
        # Calculate desired positions
        if self.obstacle_type == 'None':
            self.target_yaw = 0
        else:
            if self.box_num >= len(self.box_info[1]) - 1:
                self.target_yaw = self.box_info[1][self.box_num][5]
            else:
                self.target_yaw = self.box_info[1][self.box_num+1][5]

        self.get_expert()

        if self.cur:
            exp_forces = self.apply_forces(actions)
        else:
            exp_forces = np.zeros(self.ac_size)
        # print(actions)
        power_mult = 0.082*self.args.more_power
        # print(power_mult)

        if self.comparison in ['no_exp', 'no_link_no_exp']:
            forces = np.array(self.motor_power)*np.array(actions)*power_mult
        else:
            # if self.args.include_actions:
            #     self.actions = actions + exp_forces/(np.array(self.motor_power)*power_mult)
            #     # print(actions)
            #     # print(exp_forces/(np.array(self.motor_power)*power_mult))
            #     # print(self.actions)
            #     # print()
            #     forces = np.array(self.motor_power)*np.array(self.actions)*power_mult
            # else:
            forces = np.array(self.motor_power)*np.array(actions)*power_mult + exp_forces
            # forces = exp_forces
        # forces = exp_forces
        # forces = np.clip(forces, -150, 150)
        if set_position is not None:
            self.set_position(pos=set_position[0], orn=set_position[1], joints=set_position[2])
        else:
            if self.args.expert:
                forces = exp_forces
                # p.setJointMotorControlArray(self.Id, self.motors,controlMode=p.TORQUE_CONTROL, forces=forces)

                # x = self.speed*(self.steps - self.pause_time) * self.timeStep
                # self.set_position(pos=[0,0,1.2+self.z_offset], orn=[0,0,0,1], joints=self.target_joints)
                # print([self.target_pos[0],self.target_pos[1],self.target_pos[2]])
                
                # self.set_position(pos=[self.target_pos[0],self.target_pos[1],self.target_pos[2]], orn=p.getQuaternionFromEuler([0,0,self.target_yaw]), joints=self.target_joints)
                p.setJointMotorControlArray(self.Id, self.motors,controlMode=p.POSITION_CONTROL, targetPositions=self.target_joints)
            else:
                p.setJointMotorControlArray(self.Id, self.motors,controlMode=p.TORQUE_CONTROL, forces=forces)


        if self.multi_robots:
            if freeze_robot:
                # print("freezing at ", self.body_xyz)
                self.robot_num += 1
                self.robots[self.robot_num] = {}
                self.robots[self.robot_num]['Id'] = p.loadURDF(currentdir + "/robot.urdf",flags = p.URDF_USE_SELF_COLLISION | p.URDF_USE_SELF_COLLISION_EXCLUDE_ALL_PARENTS)
                self.robots[self.robot_num]['pos'] = self.body_xyz
                self.robots[self.robot_num]['orn'] = [self.qx, self.qy, self.qz, self.qw]
                self.robots[self.robot_num]['joints'] = self.joints
                
            if frozen:
                self.set_position([0,0,-2], [0,0,0,1], robot_id=self.Id)     


            for i in range(1,self.robot_num+1):
                if self.body_xyz[0]    > self.robots[i]['pos'][0] + 0.4 or frozen:
                    # print('freezing', i)
                    self.set_position(self.robots[i]['pos'], self.robots[i]['orn'], joints=self.robots[i]['joints'], robot_id=self.robots[i]['Id'])     
                # else:
                #     self.set_position([self.robots[i]['pos'][0], self.robots[i]['pos'][1], self.robots[i]['pos'][2]+1.2], self.robots[i]['orn'], joints=self.robots[i]['joints'], robot_id=self.robots[i]['Id'])     


        for _ in range(int(self.timeStep/self.simStep)):
            p.stepSimulation()

        if self.render:
            # if self.args.use_roa:
                # print("roa")
                # time.sleep(0.001)
                # time.sleep(0.000001)
            # else:
            time.sleep(0.0005)
        self.get_observation()
        self.prev_speed = self.speed
        self.prev_yaw_speed = self.yaw_speed
        self.prev_time_of_step = self.time_of_step
        self.prev_box_num = self.box_num

        if self.record_step: 
            self.record_sim_data()
        reward, done = self.get_reward()
        # print("removed reward")
        # reward, done = 0,0
        self.prev_actions = self.actions
        self.total_reward += reward
        self.steps += 1
        self.total_steps += 1
        
        self.state = self.joints + self.joint_vel + self.body + self.contacts + [self.original_speed, self.original_yaw_speed]
        return np.array(self.state), reward, done, None

    def get_reward(self):

        reward = 0
        done = False

        goal, pos, neg, vel, com, sym, tip = 0, 0, 0, 0, 0, 0, 0
        
        act = -0.0001*np.sum(self.actions**2)


        if sum([self.ob_dict[w] for w in self.shin_dict]) > 0 and not self.args.expert:
            done = True


        if self.obstacle_type in ['high_jumps', 'hard_high_jumps'] or (self.args.e2e and not self.args.flat_e2e):

            if not self.args.old_rew:
                if self.stage in ['1','2']:
                    goal = 0.0
                else:
                    goal = np.exp(-2.5*max(0, self.speed - self.vx)**2)

                pos = 0.65*np.exp(-2.0*np.sum((np.array(self.target_joints) - np.array(self.joints))**2))            
                # vel = 0.05*np.exp(-0.1*np.sum((np.zeros(self.target_joint_vel) - np.array(self.joint_vel))**2))
                com = 0.2*np.exp(-10*np.sum((np.array([self.roll, self.pitch, self.body_xyz[2], self.body_xyz[1], self.yaw]) - np.array([0.0, 0.0, self.target_pos[2], 0.0, 0.0]))**2))
                
                # neg -= 0.01*np.sum((self.actions[:6] - self.actions[6:])**2)
                joints = np.array(self.joints)
                neg -= 0.01*np.sum((joints[:6] - joints[6:])**2)
                neg -= 0.05*(not (self.ob_dict['right_foot_left_ground'] == self.ob_dict['left_foot_left_ground']))
                
                if self.time_of_step > 0 and self.stage in ["3","4"] and not (self.ob_dict['right_foot_left_ground'] and self.ob_dict['left_foot_left_ground']):
                    neg -= 0.5
                
                if self.time_of_step > 0 and self.stage in ["1","2"] and (self.ob_dict['right_foot_left_ground'] or self.ob_dict['left_foot_left_ground']):
                    neg -= 0.2
            else:
                if self.args.single_pol:
                    # print(self.box_num)
                    if self.body_xyz[0] < self.box_info[1][5][0]:
                        target_x = self.box_info[1][5][0]
                        goal = np.exp(-1.0*(target_x - self.body_xyz[0])**2)
                    # target_x = self.box_info[1][mult*2][0] 
                    else:
                        # print(self.hj_steps, self.high_jump_step)
                        if not self.hj_steps:
                            self.hj_steps = 0
                        else:
                            self.hj_steps += 1
                        if not self.high_jump_step:
                            target_x = self.box_info[1][6][0]
                        else:
                            target_x = self.box_info[1][7][0]
                        if self.hj_steps < 200 or (self.hj_steps > 200 and self.high_jump_step):
                            goal = np.exp(-2.5*(target_x - self.body_xyz[0])**2)
                        if not self.high_jump_step:
                            z_height = (self.box_info[1][6][2] + self.box_info[2][6][2])
                            if (not self.ob_dict['right_foot_left_ground'] and self.foot_pos['right'][2] > z_height) and (not self.ob_dict['left_foot_left_ground'] and self.foot_pos['left'][2] > z_height) and self.box_num == 7:
                                self.high_jump_step = True
                    
                else:
                    mult = 1
                    if not self.high_jump_step:
                        target_x = self.box_info[1][mult*1][0]
                    else:
                        target_x = self.box_info[1][mult*2][0]
                    if self.steps < 200 or (self.steps > 200 and self.high_jump_step) or (self.steps > 400 and self.high_jump_second_on_ground) or (self.steps > 600 and self.second_high_jump_step):
                        goal = np.exp(-2.5*(target_x - self.body_xyz[0])**2)

                pos = 0.65*np.exp(-2.0*np.sum((np.array(self.target_joints) - np.array(self.joints))**2))            
                vel = 0.05*np.exp(-0.1*np.sum((np.zeros(self.ac_size) - np.array(self.joint_vel))**2))
            
                if self.stage == '1' and self.vx < 0.1:
                    neg -= self.vx

                if not self.high_jump_step and not self.args.single_pol:
                    z_height = (self.box_info[1][mult*1][2] + self.box_info[2][mult*1][2])
                    if (not self.ob_dict['right_foot_left_ground'] and self.foot_pos['right'][2] > z_height) and (not self.ob_dict['left_foot_left_ground'] and self.foot_pos['left'][2] > z_height) and self.box_num == mult*1:
                        self.high_jump_step = True

                com = 0.2*np.exp(-10*np.sum((np.array([self.roll, self.pitch, self.body_xyz[2], self.body_xyz[1], self.yaw]) - np.array([0.0, 0.0, self.target_pos[2], 0.0, 0.0]))**2))

                neg -= 0.01*np.sum((self.actions[:6] - self.actions[6:])**2)
                joints = np.array(self.joints)
                neg -= 0.01*np.sum((joints[:6] - joints[6:])**2)
                neg -= 0.05*(not (self.ob_dict['right_foot_left_ground'] == self.ob_dict['left_foot_left_ground']))
                if self.time_of_step > 0 and self.stage in ["3","4"] and (not self.ob_dict['right_foot_left_ground'] or not self.ob_dict['left_foot_left_ground']):
                    neg -= 0.2
                
                if self.time_of_step > 0 and self.stage in ["1","2"] and (self.ob_dict['right_foot_left_ground'] or self.ob_dict['left_foot_left_ground']):
                    neg -= 0.2
        
        else:
            goal = np.exp(-2.5*max(0, self.speed - self.vx)**2)

            # neg -= 0.2*sum([self.ob_dict[w] for w in self.shin_dict])

            # if self.obstacle_type in ['one_leg_hop', 'hard_steps']:
            #     neg += 0.1*np.exp(-10*((self.foot_pos['right'][1] - -0.1)**2 + (self.foot_pos['left'][1] - 0.1)**2))

            pos = 0.65*np.exp(-2.0*np.sum((np.array(self.target_joints) - np.array(self.joints))**2))            

            vel = 0.05*np.exp(-0.1*np.sum((np.zeros(self.ac_size) - np.array(self.joint_vel))**2))
        
            if self.obstacle_type != 'None':
                if self.obstacle_type == 'flat':
                    com = 0.2*np.exp(-10*np.sum((np.array([self.roll, self.pitch, self.body_xyz[2], self.yaw]) - np.array([0.0, 0.0, 0.95+self.z_offset, self.target_yaw]))**2))
                else:
                    com = 0.2*np.exp(-10*np.sum((np.array([self.roll, self.pitch, self.body_xyz[2], self.body_xyz[1], self.yaw]) - np.array([0.0, 0.0, 0.95+self.z_offset, 0.0,self.box_info[1][self.box_num][5]]))**2))

            if self.ob_dict['right_foot_left_ground'] and self.ob_dict['left_foot_left_ground'] and (self.obstacle_type in ['flat','stairs','steps'] or (self.obstacle_type in ['gaps', 'jumps'] and self.cur)):
                neg -= 0.1            

            # # if self.box_info is not None and self.box_num > 1:
            
            if self.step_count > 2 and self.time_of_step > 0:
                # if min(self.touchdown_pen/50, 5.0) > 0:
                if min(self.touchdown_pen/5, 5.0) > 0:
                    # neg -= min(self.touchdown_pen/50, 5.0)    
                    # print(min(self.touchdown_pen, 5.0) , self.touchdown_pen )
                    neg -= min(self.touchdown_pen/5, 5.0)    
                    # self.touchdown_pens[self.ob_dict['cur_swing_foot']].append(min(self.touchdown_pen/50, 5.0))
                    self.touchdown_pens[self.ob_dict['cur_swing_foot']].append(min(self.touchdown_pen/5, 5.0))
                    if self.ob_dict['cur_swing_foot']:
                        swing = 'right'
                        stance = 'left'
                    else:
                        stance = 'right'
                        swing = 'left'
                


        # x = np.sqrt(max(0,self.target_pos[0] - self.body_xyz[0])**2 + max(self.target_pos[1] - self.body_xyz[1])**2)
        # goal = np.exp(-2.5*x)
        

        # x = np.sqrt((self.target_pos[0] - self.body_xyz[0])**2 + (self.target_pos[1] - self.body_xyz[1])**2)
        # goal = 1.0*(2.0/(np.e**(3*x) + np.e**(-3*x)))
        # goal = 0.65*(2.0/(np.e**(3*x) + np.e**(-3*x)))
        # x = abs(self.yaw_vel - self.yaw_speed)
        # x = self.yaw - self.target_yaw
        # goal += 0.25*(2.0/(np.e**(3*x) + np.e**(-3*x)))


        # x = abs(self.vx - self.speed)
        # goal = 0.75*(2.0/(np.e**(3*x) + np.e**(-3*x)))
        # if self.speed < 0.1:
            # goal -= abs(self.vx)
        # x = abs(self.yaw_vel - self.yaw_speed)
        # goal += 0.25*(2.0/(np.e**(3*x) + np.e**(-3*x)))

            # if self.speed > 0.5:
            #     self.speeds.append(self.vx)

        # if self.obstacle_type in ['high_jumps', 'hard_high_jumps']:
        # if self.obstacle_type != 'None' and self.order[self.box_num] in ['high_jumps', 'hard_high_jumps']:
            # if self.stage == '1' and self.vx < 0.1:
            #     neg -= self.vx
            # print(self.stage, self.box_num == 6, (not self.ob_dict['right_foot_left_ground'] or not self.ob_dict['left_foot_left_ground']))
            # neg -= 0.5*sum([self.ob_dict[w] for w in self.shin_dict])
            # neg -= 0.2*sum([self.ob_dict[w] for w in self.shin_dict])
            # if sum([self.ob_dict[w] for w in self.shin_dict]) > 0 and not self.args.expert:
                # done = True

            # if self.obstacle_type == 'high_jumps':
            # if not self.high_jump_first_left_ground:
            #     if self.steps > 100 and (self.ob_dict['right_foot_left_ground'] and self.ob_dict['left_foot_left_ground']):
            #         self.high_jump_first_left_ground = True

            # if (self.x_max < (self.box_info[1][mult*1][0] - self.box_info[2][mult*1][0])) and ((self.high_jump_first_left_ground and (not self.ob_dict['right_foot_left_ground'] or not self.ob_dict['left_foot_left_ground'])) or self.steps > 200) and not self.args.expert:
            #     done = True
            #     if self.rank == 0 and self.render: print("left ground, but recontacted")
            
            # if self.high_jump_first_left_ground and not self.high_jump_step:
            # if not self.high_jump_step and not self.args.single_pol:
                # # x1 = (self.box_info[1][mult*1][0] - self.box_info[2][mult*1][0])
                # # x2 = (self.box_info[1][mult*1][0] + self.box_info[2][mult*1][0])
                # z_height = (self.box_info[1][mult*1][2] + self.box_info[2][mult*1][2])
                # if (not self.ob_dict['right_foot_left_ground'] and self.foot_pos['right'][2] > z_height) and (not self.ob_dict['left_foot_left_ground'] and self.foot_pos['left'][2] > z_height) and self.box_num == mult*1:
                #     # print('true', (not self.ob_dict['right_foot_left_ground'] and self.foot_pos['right'][2] > z_height+0.1) , (not self.ob_dict['left_foot_left_ground'] and self.foot_pos['left'][2] > z_height+0.1))
                #     self.high_jump_step = True
                #     # print("high_jump_step", self.steps)

            # if self.steps > 200 and not self.high_jump_step and not self.args.expert:
            #     # done = True
            #     neg -= 0.5

            # if (self.x_min > (self.box_info[1][1][0] + self.box_info[2][1][0]) and not self.high_jump_step) and not self.args.expert:
            #     done = True
            #     if self.rank == 0 and self.render: print("didn't touch first jump")
            # if self.num_artifacts == 2:

            #     if self.high_jump_step and not self.high_jump_second_on_ground:
            #         x1 = (self.box_info[1][mult*2][0] - self.box_info[2][mult*2][0])
            #         x2 = (self.box_info[1][mult*2][0] + self.box_info[2][mult*2][0])
            #         if (not self.ob_dict['right_foot_left_ground'] and self.foot_pos['right'][0] < x2 and self.foot_pos['right'][0] > x1) and (not self.ob_dict['left_foot_left_ground'] and self.foot_pos['left'][0] < x2 and self.foot_pos['left'][0] > x1):
            #             self.high_jump_second_on_ground = True
            #             # print("high_jump_on ground", self.steps)


            #     # if self.steps > 400 and not self.high_jump_second_on_ground and not self.args.expert:
            #     #     # done = True
            #     #     neg -= 0.5                

            #     # if not self.high_jump_second_left_ground and self.high_jump_second_on_ground and (self.ob_dict['right_foot_left_ground'] and self.ob_dict['left_foot_left_ground']):
            #     #     self.high_jump_second_left_ground = True

            #     if self.high_jump_second_on_ground and not self.second_high_jump_step:
            #         # x3 = (self.box_info[1][mult*3][0] - self.box_info[2][mult*3][0])
            #         # x4 = (self.box_info[1][mult*3][0] + self.box_info[2][mult*3][0])
            #         z_height = (self.box_info[1][mult*3][2] + self.box_info[2][mult*3][2])
            #         if (not self.ob_dict['right_foot_left_ground'] and self.foot_pos['right'][2] > z_height ) and (not self.ob_dict['left_foot_left_ground'] and self.foot_pos['left'][2] > z_height ) and self.box_num == mult*3:
            #         # if (not self.ob_dict['right_foot_left_ground'] and self.foot_pos['right'][0] < x4 and self.foot_pos['right'][0] > x3) and (not self.ob_dict['left_foot_left_ground'] and self.foot_pos['left'][0] < x4 and self.foot_pos['left'][0] > x3):
            #             self.second_high_jump_step = True
            #             # print("second_high", self.steps)

            # if self.steps > 600 and not self.second_high_jump_step and not self.args.expert:
            #     # done = True
            #     neg -= 0.5                

            # if (self.x_min > (self.box_info[1][3][0] + self.box_info[2][3][0]) and not self.second_high_jump_step) and not self.args.expert:
                #     done = True
                #     if self.rank == 0 and self.render: print("didn't touch second jump")


                # if self.num_artifacts == 2:
                    
                #     # if not self.high_jump_second_on_ground and ((self.foot_pos['left'][0] > (self.box_info[1][6][0] + self.box_info[2][6][0])) and (self.foot_pos['right'][0] > (self.box_info[1][6][0] + self.box_info[2][6][0]))) and (not self.ob_dict['right_foot_left_ground'] and not self.ob_dict['left_foot_left_ground']):
                #     #     self.high_jump_second_on_ground = True
                #     #     # print("why is this true?")

                #     # if self.high_jump_second_on_ground and (self.ob_dict['right_foot_left_ground'] and self.ob_dict['left_foot_left_ground']):
                #     #     self.high_jump_second_left_ground = True

                #     # if self.high_jump_second_left_ground and (not self.ob_dict['right_foot_left_ground'] and not self.ob_dict['left_foot_left_ground']) and ((self.foot_pos['left'][0] < (self.box_info[1][7][0] + self.box_info[2][7][0])) or (self.foot_pos['right'][0] < (self.box_info[1][7][0] + self.box_info[2][7][0]))):
                #     #     done = True
                #     #     if self.rank == 0 and self.render: print("left ground on second part and recontacted")

                #     if (not self.ob_dict['right_foot_left_ground'] and not self.ob_dict['left_foot_left_ground'] and self.box_num == 2):
                #         box_x = self.box_info[1][self.box_num][0]
                #         neg -= 1.0*((self.foot_pos['right'][0] - box_x)**2 + (self.foot_pos['left'][0] - box_x)**2 + (self.body_xyz[0] - box_x)**2)
                #         # print(1.0*((self.foot_pos['right'][0] - box_x)**2 + (self.foot_pos['left'][0] - box_x)**2 + (self.body_xyz[0] - box_x)**2))

                #     x3 = (self.box_info[1][3][0] - self.box_info[2][3][0])
                #     x4 = (self.box_info[1][3][0] + self.box_info[2][3][0])
                #     if (not self.ob_dict['right_foot_left_ground'] and self.foot_pos['right'][0] < x4 and self.foot_pos['right'][0] > x3) and (not self.ob_dict['left_foot_left_ground'] and self.foot_pos['left'][0] < x4 and self.foot_pos['left'][0] > x3):
                #         self.second_high_jump_step = True

                #     if (self.x_min > (self.box_info[1][3][0] + self.box_info[2][3][0]) and not self.second_high_jump_step) and not self.args.expert:
                #         done = True
                #         if self.rank == 0 and self.render: print("didn't touch second jump")

            # if self.obstacle_type == 'hard_high_jumps':
            #     if self.steps > 100 and self.box_num == 0 and (self.ob_dict['right_foot_left_ground'] and self.ob_dict['left_foot_left_ground']):
            #         self.high_jump_first_left_ground = True

            #     if (self.x_max < (self.box_info[1][1][0] - self.box_info[2][1][0])) and ((self.high_jump_first_left_ground and (not self.ob_dict['right_foot_left_ground'] or not self.ob_dict['left_foot_left_ground'])) or self.steps > 200) and not self.args.expert:
            #         done = True
            #         if self.rank == 0 and self.render: print("left ground, but recontacted")

            #     x1 = (self.box_info[1][2][0] - self.box_info[2][2][0])
            #     x2 = (self.box_info[1][2][0] + self.box_info[2][2][0])
            #     if (not self.ob_dict['right_foot_left_ground'] and self.foot_pos['right'][0] < x2 and self.foot_pos['right'][0] > x1) and (not self.ob_dict['left_foot_left_ground'] and self.foot_pos['left'][0] < x2 and self.foot_pos['left'][0] > x1):
            #         self.high_jump_step = True

            #     if self.x_min > (self.box_info[1][2][0] + self.box_info[2][2][0]) and not self.high_jump_step and not self.args.expert:
            #         done = True

            #     if self.num_artifacts == 2:

            #         x3 = (self.box_info[1][6][0] - self.box_info[2][6][0])
            #         x4 = (self.box_info[1][6][0] + self.box_info[2][6][0])
            #         if (not self.ob_dict['right_foot_left_ground'] and self.foot_pos['right'][0] < x4 and self.foot_pos['right'][0] > x3) and (not self.ob_dict['left_foot_left_ground'] and self.foot_pos['left'][0] < x4 and self.foot_pos['left'][0] > x3):
            #             self.second_high_jump_step = True

            #         if (self.x_min > (self.box_info[1][6][0] + self.box_info[2][6][0]) and not self.second_high_jump_step) and not self.args.expert:
            #             done = True
            #             if self.rank == 0 and self.render: print("didn't touch second jump")

                # print("hit jump")
            # print(self.x_min , (self.box_info[1][self.box_num][0] + self.box_info[2][self.box_num][0]), not self.high_jump_step)
            

            # com = 0.2*np.exp(-10*np.sum((np.array([self.roll, self.pitch, self.body_xyz[2], self.body_xyz[1], self.yaw]) - np.array([0.0, 0.0, self.target_pos[2], 0.0, 0.0]))**2))
            # # com = 0.2*np.exp(-10*np.sum((np.array([self.roll, self.pitch, self.body_xyz[1], self.yaw]) - np.array([0.0, 0.0, 0.0, 0.0]))**2))
            # com = 0.2*np.exp(-10*np.sum((np.array([self.roll, self.pitch, self.body_xyz[2], self.body_xyz[1], self.yaw]) - np.array([0.0, 0.0, self.target_pos[2], 0.0, 0.0]))**2))

            # neg -= 0.01*np.sum((self.actions[:6] - self.actions[6:])**2)
            # joints = np.array(self.joints)
            # neg -= 0.01*np.sum((joints[:6] - joints[6:])**2)
            # neg -= 0.05*(not (self.ob_dict['right_foot_left_ground'] == self.ob_dict['left_foot_left_ground']))
            # # print(not (self.ob_dict['right_foot_left_ground'] == self.ob_dict['left_foot_left_ground']))
            # # print(self.ob_dict['stage'])
            # if self.time_of_step > 0 and self.stage in ["3","4"] and (not self.ob_dict['right_foot_left_ground'] or not self.ob_dict['left_foot_left_ground']):
            #     neg -= 0.2
            
            # if self.time_of_step > 0 and self.stage in ["1","2"] and (self.ob_dict['right_foot_left_ground'] or self.ob_dict['left_foot_left_ground']):
            #     neg -= 0.2

            # # if self.stage in ['3'] and (not self.ob_dict['right_foot_left_ground'] or not self.ob_dict['left_foot_left_ground']):
            # #     neg -= 0.5
            
            # # if self.ob_dict['right_foot_left_ground'] != self.ob_dict['left_foot_left_ground']:
            # #     neg -= 0.05            
            
            # # if self.stage == '1' and self.first_jump and self.steps - self.pause_time > 150:
            # #     done = True


        sym_fact = 0.0
        if self.time_of_step != 0 and self.step_count > 2:
            if self.cur_time < self.time_of_step:
                if self.ob_dict['cur_swing_foot']:
                    swing = 'right'
                    stance = 'left'
                else:
                    swing = 'left'
                    stance = 'right'
                # current_pos_torque = np.array([self.ob_dict[a + '_pos'] for a in self.motor_names] + list(self.actions))
                # sym = sym_fact*np.exp(-0.1*np.sum((self.action_store[self.cur_time,:] - current_pos_torque)**2))
                # self.action_store[self.cur_time, :] = current_pos_torque

                current_hip_y = np.array([self.ob_dict[swing + '_hip_y_pos'], self.ob_dict[swing + '_knee_pos'], self.ob_dict[stance + '_hip_y_pos'], self.ob_dict[stance + '_knee_pos']])
                sym = sym_fact*np.exp(-5*np.sum((self.action_store[self.cur_time,:] - current_hip_y)**2))            
                # print(self.cur_time, swing, stance, current_hip_y, self.action_store[self.cur_time, :], np.sum((self.action_store[self.cur_time,:] - current_hip_y)**2))
                # print(sym)
                self.action_store[self.cur_time, :] = current_hip_y

        # if self.obstacle_type == 'high_jumps':
        #     if self.x_min < (self.box_info[1][5][0] + self.box_info[2][5][0]):
        #         reward = 0
        #     else:
        #         reward = goal + pos + com + neg + sym + act + vel + tip
        # else:
        reward = goal + pos + com + neg + sym + act + vel + tip
        self.pos_errors.append(pos)

        self.reward_breakdown['goal'].append(goal)
        self.reward_breakdown['pos'].append(pos)
        self.reward_breakdown['vel'].append(vel)
        self.reward_breakdown['neg'].append(neg)
        self.reward_breakdown['tip'].append(tip)
        self.reward_breakdown['com'].append(com)
        self.reward_breakdown['sym'].append(sym)
        self.reward_breakdown['act'].append(act)
        
        # if self.obstacle_type == 'jump':
        #     if ((self.steps + 1) % 16 == 0 and self.steps != 0) and (self.steps > 8*256-1 or self.body_xyz[2] < (1.7)):     
        #         done = True
        # else:
        if self.doa or self.obstacle_type == 'mix':
            # print(120*(self.box_info[1][-1][0] + self.box_info[2][-1][0]) + 200)
            # if self.body_xyz[2] < (0.7 + self.z_min) or self.steps > (120*(self.box_info[1][-1][0] + self.box_info[2][-1][0]) + 200) or self.body_xyz[0] > (self.box_info[1][-1][0] + self.box_info[2][-1][0]):     
            # if self.body_xyz[2] < (0.7 + self.z_min) or self.body_xyz[0] > (self.box_info[1][-1][0]):     
            if self.body_xyz[2] < (0.6 + self.z_min) or self.box_num == len(self.box_info[1]) - 1 or (self.steps > self.max_steps):
                # print(self.body_xyz[2] < (0.6 + self.z_min) , self.box_num > len(self.box_info[1]) -1 ) , (self.steps > self.max_steps))
                # print(self.body_xyz[2] ,    (0.6 + self.z_min))
                done = True
        else:
            if self.args.advantage2:
                if (self.steps > self.max_steps):
                    # print(self.steps)
                    done = 2
                # print([self.ob_dict[w] for w in self.shin_dict])
                
                # if not self.args.expert and sum([self.ob_dict[w] for w in self.shin_dict]) > 0:
                #     done = True
                # if (self.obstacle_type not in ['high_jumps', 'hard_high_jumps'] and self.body_xyz[2] < (0.7 + self.z_min)) or (self.obstacle_type in ['high_jumps', 'hard_high_jumps'] and self.body_xyz[2] < (0.6 + self.z_min)) or (sum([self.ob_dict[w] for w in self.shin_dict]) > 0):     
                # if (self.obstacle_type not in ['high_jumps', 'hard_high_jumps'] and self.body_xyz[2] < (0.7 + self.z_min)) or (self.obstacle_type in ['high_jumps', 'hard_high_jumps'] and self.body_xyz[2] < (0.6 + self.z_min)):     
                # print(self.body_xyz[2]    self.z_min)
                if self.obstacle_type == 'None':
                    if (self.body_xyz[2] < (0.7 + self.z_min)):     
                        done = 1
                elif (self.order[self.box_num] not in ['high_jumps', 'hard_high_jumps'] and self.body_xyz[2] < (0.7 + self.z_min)) or (self.order[self.box_num] in ['high_jumps', 'hard_high_jumps'] and self.body_xyz[2] < (0.6 + self.z_min)):     
                    # print(done, self.steps, self.body_xyz[2] < (0.7 + self.z_min), self.body_xyz[2] < (0.6 + self.z_min), self.max_steps, sum([self.ob_dict[w] for w in self.shin_dict]) > 0)
                    done = 1
            else:
                # if self.order[self.box_num] not in ['one_leg_hop'] and sum([self.ob_dict[w] for w in self.shin_dict]) > 0:
                if sum([self.ob_dict[w] for w in self.shin_dict]) > 0:
                    done = True
                # if (self.steps > self.max_steps) or (self.obstacle_type not in ['high_jumps', 'hard_high_jumps'] and self.body_xyz[2] < (0.7 + self.z_min)) or (self.obstacle_type in ['high_jumps','hard_high_jumps'] and self.body_xyz[2] < (0.6 + self.z_min)) or (sum([self.ob_dict[w] for w in self.shin_dict]) > 0):     
                # if (self.steps > self.max_steps) or (self.obstacle_type not in ['high_jumps', 'hard_high_jumps'] and self.body_xyz[2] < (0.7 + self.z_min)) or (self.obstacle_type in ['high_jumps','hard_high_jumps'] and self.body_xyz[2] < (0.6 + self.z_min)):     
                if (self.steps > self.max_steps) or (self.order[self.box_num] not in ['high_jumps', 'hard_high_jumps'] and self.body_xyz[2] < (0.7 + self.z_min)) or (self.order[self.box_num] in ['high_jumps','hard_high_jumps'] and self.body_xyz[2] < (0.6 + self.z_min)):     
                    # print(done, self.steps, self.body_xyz[2] < (0.7 + self.z_min), self.body_xyz[2] < (0.6 + self.z_min), self.max_steps, sum([self.ob_dict[w] for w in self.shin_dict]) > 0)
                    done = True
            if self.eval and self.obstacle_type not in ['high_jumps', 'hard_high_jumps']:
            #     if self.obstacle_type == 'high_jumps' and self.body_xyz[0] > (self.box_info[1][6][0] ) and :
            #         done = True
            #     if self.obstacle_type == 'hard_high_jumps' and self.body_xyz[0] > (self.box_info[1][8][0]):
            #         done = True
                if self.obstacle_type == 'flat':
                    if self.box_num == len(self.box_info[1]) - 1:
                        done = True
                else:
                    if self.x_min > (self.box_info[1][-1][0] - self.box_info[2][-1][0]):
                        done = True
            # print(done, (self.steps > self.max_steps))
        return reward, done

    def get_observation(self):
        jointStates = p.getJointStates(self.Id,self.ordered_joint_indices)
        self.joints = list(np.array([jointStates[j[0]][0] for j in self.ordered_joints[:int(self.ac_size)]]))
        self.joint_vel = list(np.array([jointStates[j[0]][1] for j in self.ordered_joints[:int(self.ac_size)]]) + np.random.uniform(-0.5,0.5,self.ac_size))
        
        self.ob_dict.update({n + '_pos':j for n,j in zip(self.motor_names, self.joints)})


        self.body_xyz, (self.qx, self.qy, self.qz, self.qw) = p.getBasePositionAndOrientation(self.Id)
        self.roll, self.pitch, self.yaw = p.getEulerFromQuaternion([self.qx, self.qy, self.qz, self.qw])

        self.body_vxyz, self.base_rot_vel = p.getBaseVelocity(self.Id)
        
        self.roll_vel = self.base_rot_vel[0]
        self.pitch_vel = self.base_rot_vel[1]
        self.yaw_vel = self.base_rot_vel[2]

        rot_speed = np.array(
            [[np.cos(-self.yaw), -np.sin(-self.yaw), 0],
                [np.sin(-self.yaw), np.cos(-self.yaw), 0],
                [		0,			 0, 1]]
        )

        self.vx, self.vy, self.vz = np.dot(rot_speed, (self.body_vxyz[0],self.body_vxyz[1],self.body_vxyz[2]))
        
        # Policy shouldn't know yaw
        self.body = [self.vx, self.vy, self.vz, self.roll, self.pitch, self.roll_vel, self.pitch_vel, self.yaw_vel, self.body_xyz[2] - self.z_offset]

        self.ob_dict['prev_right_heel1']    = self.ob_dict['right_heel1'] 
        self.ob_dict['prev_right_heel2']    = self.ob_dict['right_heel2'] 
        self.ob_dict['prev_right_toe1']     = self.ob_dict['right_toe1']     
        self.ob_dict['prev_right_toe2']     = self.ob_dict['right_toe2']     
        self.ob_dict['prev_left_heel1']     = self.ob_dict['left_heel1'] 
        self.ob_dict['prev_left_heel2']     = self.ob_dict['left_heel2'] 
        self.ob_dict['prev_left_toe1']        = self.ob_dict['left_toe1']     
        self.ob_dict['prev_left_toe2']        = self.ob_dict['left_toe2']     

        self.ob_dict['right_heel1'] = len(p.getContactPoints(self.Id, -1, self.feet_dict['right_heel1'], -1))>0
        self.ob_dict['right_heel2'] = len(p.getContactPoints(self.Id, -1, self.feet_dict['right_heel2'], -1))>0
        self.ob_dict['right_toe1']    = len(p.getContactPoints(self.Id, -1, self.feet_dict['right_toe1'], -1))>0
        self.ob_dict['right_toe2']    = len(p.getContactPoints(self.Id, -1, self.feet_dict['right_toe2'], -1))>0
        self.ob_dict['left_heel1']    = len(p.getContactPoints(self.Id, -1, self.feet_dict['left_heel1'], -1))>0
        self.ob_dict['left_heel2']    = len(p.getContactPoints(self.Id, -1, self.feet_dict['left_heel2'], -1))>0
        self.ob_dict['left_toe1']     = len(p.getContactPoints(self.Id, -1, self.feet_dict['left_toe1'], -1))>0
        self.ob_dict['left_toe2']     = len(p.getContactPoints(self.Id, -1, self.feet_dict['left_toe2'], -1))>0

        for w in self.shin_dict:
            self.ob_dict[w] = len(p.getContactPoints(self.Id, -1, self.shin_dict[w], -1))>0

        # Update feet that have left the ground.
        self.ob_dict['prev_right_foot_left_ground'] = self.ob_dict['right_foot_left_ground']
        self.ob_dict['prev_left_foot_left_ground'] = self.ob_dict['left_foot_left_ground']
        self.ob_dict['right_foot_left_ground'] = not self.ob_dict['right_heel1'] and not self.ob_dict['right_heel2'] and not self.ob_dict['right_toe2'] and not self.ob_dict['right_toe1']
        self.ob_dict['left_foot_left_ground'] = not self.ob_dict['left_heel1'] and not self.ob_dict['left_heel2'] and not self.ob_dict['left_toe2'] and not self.ob_dict['left_toe1']
        
        self.ob_dict['right_full_foot_on_ground'] = self.ob_dict['right_heel1'] and self.ob_dict['right_heel2'] and self.ob_dict['right_toe2'] and self.ob_dict['right_toe1']        
        self.ob_dict['left_full_foot_on_ground'] = self.ob_dict['left_heel1'] and self.ob_dict['left_heel2'] and self.ob_dict['left_toe2'] and self.ob_dict['left_toe1']        
        
        self.ob_dict['right_foot_on_ground'] = not self.ob_dict['right_foot_left_ground']
        self.ob_dict['left_foot_on_ground'] = not self.ob_dict['left_foot_left_ground']
        
        right_contacts = [self.ob_dict['right_heel1'],self.ob_dict['right_heel2'],self.ob_dict['right_toe2'],self.ob_dict['right_toe1']]
        left_contacts = [self.ob_dict['left_heel1'],self.ob_dict['left_heel2'],self.ob_dict['left_toe2'],self.ob_dict['left_toe1']]
        prev_right_contacts = [self.ob_dict['prev_right_heel1'],self.ob_dict['prev_right_heel2'],self.ob_dict['prev_right_toe2'],self.ob_dict['prev_right_toe1']]
        prev_left_contacts = [self.ob_dict['prev_left_heel1'],self.ob_dict['prev_left_heel2'],self.ob_dict['prev_left_toe2'],self.ob_dict['prev_left_toe1']]

        self.foot_pos = {}
        self.foot_pos['left'] = p.getLinkState(self.Id, self.feet_dict['left_heel1'])[0]
        self.foot_pos['right'] = p.getLinkState(self.Id, self.feet_dict['right_heel1'])[0]

        self.local_foot_pos = {}
        self.local_foot_pos['left'] = self.global_to_local_2d(self.body_xyz, self.yaw, self.foot_pos['left'])
        self.local_foot_pos['right'] = self.global_to_local_2d(self.body_xyz, self.yaw, self.foot_pos['right'])
        self.x_min = min(self.body_xyz[0], self.foot_pos['left'][0], self.foot_pos['right'][0])
        self.x_max = max(self.body_xyz[0], self.foot_pos['left'][0], self.foot_pos['right'][0])
        # Was previously stopped, now need to select a swing foot
        # if self.prev_time_of_step == 0 and self.time_of_step != 0:
        #     print("setting swing foot")
        #     self.ob_dict['swing_foot'] = self.ob_dict['cur_swing_foot'] = np.random.randint(2)
        #     self.ob_dict['right_foot_swing'] = self.ob_dict['swing_foot']
        #     self.ob_dict['left_foot_swing'] = not self.ob_dict['right_foot_swing']

        if self.ob_dict['right_foot_swing']:
            swing = 'right'
            stance = 'left'
        else: 
            swing = 'left'
            stance = 'right'

        if self.time_of_step == 0:
            self.ob_dict['right_foot_swing'] = 0
            self.ob_dict['left_foot_swing'] = 0
            if self.box_info is not None:
                for foot in ['right', 'left']:
                    for i in range(self.box_num-2, self.box_num+2):
                        if i < 0 or i > (len(self.box_info[0]) - 1): continue
                        x, y = self.foot_pos[foot][0], self.foot_pos[foot][1]
                        length, width = self.box_info[2][i][0], self.box_info[2][i][1]
                        box_yaw = self.box_info[1][i][5] 
                        diag = np.sqrt(length**2 + width**2)
                        x1 = self.box_info[1][i][0] + diag*np.cos(box_yaw+np.arctan2( width,    length))
                        y1 = self.box_info[1][i][1] + diag*np.sin(box_yaw+np.arctan2( width,    length))
                        x2 = self.box_info[1][i][0] + diag*np.cos(box_yaw+np.arctan2(-width, -length))
                        y2 = self.box_info[1][i][1] + diag*np.sin(box_yaw+np.arctan2(-width, -length))
                        if x1 > x2:
                            x_temp = x1
                            x1 = x2
                            x2 = x_temp
                        if y1 > y2:
                            y_temp = y1
                            y1 = y2
                            y2 = y_temp
                        if x1 < x < x2 and y1 < y < y2:
                            self.desired_box[foot] = i
                            break
        else:

            if self.ob_dict['prev_' + swing + '_foot_left_ground'] and not self.ob_dict[swing + '_foot_left_ground']:
                self.swing_on_ground_time = self.steps

        self.swing_stance = [self.ob_dict['right_foot_swing'], self.ob_dict['left_foot_swing']]

        self.contacts = right_contacts + left_contacts + prev_right_contacts + prev_left_contacts + self.swing_stance
        # self.contacts = right_contacts + left_contacts + prev_right_contacts + prev_left_contacts + [self.initial_swing_foot]

        # Find current box number (under com)
        if self.box_info is not None:
            if self.box_num == None:
                self.box_num = 0
            i = self.box_num
            total_i = 0
            while True:
                x, y = self.body_xyz[0], self.body_xyz[1]
                
                # length, width = self.box_info[2][i][0], self.box_info[2][i][1]
                # box_yaw = self.box_info[1][i][5] 
                # diag = np.sqrt(length**2 + width**2)
                # x1 = self.box_info[1][i][0] + diag*np.cos(box_yaw+np.arctan2( width,    length))
                # y1 = self.box_info[1][i][1] + diag*np.sin(box_yaw+np.arctan2( width,    length))
                # x2 = self.box_info[1][i][0] + diag*np.cos(box_yaw+np.arctan2(-width, -length))
                # y2 = self.box_info[1][i][1] + diag*np.sin(box_yaw+np.arctan2(-width, -length))
                
                x1, y1 = self.box_info[1][i][0] - self.box_info[2][i][0], self.box_info[1][i][1] - self.box_info[2][i][1]
                x2, y2 = self.box_info[1][i][0] + self.box_info[2][i][0], self.box_info[1][i][1] + self.box_info[2][i][1]
                
                if x1 > x2:
                    x_temp = x1
                    x1 = x2
                    x2 = x_temp
                elif y1 > y2:
                    y_temp = y1
                    y1 = y2
                    y2 = y_temp
                if x1 < x < x2 and y1 < y < y2:
                    self.box_num = i
                    break
                i += 1
                total_i += 1
                if i > (len(self.box_info[0]) - 1):
                    i = 0
                if total_i > (len(self.box_info[0]) - 1):
                    # ("com not over box, box number frozen"), box number will remain the last box number
                    break
            
            for foot in ['left','right']:
                # foot_pos = p.getLinkState(self.Id, self.feet_dict[foot +'_heel1'])[0]
                for i in range(self.box_num-1, self.box_num+2):
                    if i < 0 or i > (len(self.box_info[0]) - 1): continue
                    x, y = self.foot_pos[foot][0], self.foot_pos[foot][1]
                    x1, y1 = self.box_info[1][i][0] - self.box_info[2][i][0], self.box_info[1][i][1] - self.box_info[2][i][1]
                    x2, y2 = self.box_info[1][i][0] + self.box_info[2][i][0], self.box_info[1][i][1] + self.box_info[2][i][1]
                    if x1 > x2:
                        x_temp = x1
                        x1 = x2
                        x2 = x_temp
                    elif y1 > y2:
                        y_temp = y1
                        y1 = y2
                        y2 = y_temp
                    if x1 < x < x2 and y1 < y < y2:
                        self.foot_on_box[foot] = i
                        break

            # Need to play with this
            self.boxes = []
            self.null_box = [0]*self.box_dim
            for box in self.box_nums:
                if (self.box_num + box < 1) or (self.box_num + box > len(self.box_info[0]) - 1):
                    self.boxes.extend(self.null_box)
                else:
                    # if self.args.nicks:
                    #     length, width = self.box_info[2][self.box_num+box][0], self.box_info[2][self.box_num+box][1]
                    #     box_yaw = self.box_info[1][self.box_num+box][5] 
                        
                    #     diag = np.sqrt(length**2 + width**2)
                    #     self.world_box_x1 = self.box_info[1][self.box_num+box][0] + diag*np.cos(box_yaw+np.arctan2( width,    length))
                    #     self.world_box_y1 = self.box_info[1][self.box_num+box][1] + diag*np.sin(box_yaw+np.arctan2( width,    length))
                    #     self.world_box_x2 = self.box_info[1][self.box_num+box][0] + diag*np.cos(box_yaw+np.arctan2( width, -length))
                    #     self.world_box_y2 = self.box_info[1][self.box_num+box][1] + diag*np.sin(box_yaw+np.arctan2( width, -length))
                    #     self.world_box_x3 = self.box_info[1][self.box_num+box][0] + diag*np.cos(box_yaw+np.arctan2(-width,    length))
                    #     self.world_box_y3 = self.box_info[1][self.box_num+box][1] + diag*np.sin(box_yaw+np.arctan2(-width,    length))
                    #     self.world_box_x4 = self.box_info[1][self.box_num+box][0] + diag*np.cos(box_yaw+np.arctan2(-width, -length))
                    #     self.world_box_y4 = self.box_info[1][self.box_num+box][1] + diag*np.sin(box_yaw+np.arctan2(-width, -length))
                    #     # d1 = self.dist_to_segment([self.body_xyz[0], self.body_xyz[1]], [self.world_box_x1, self.world_box_y1], [self.world_box_x3, self.world_box_y3])
                    #     # d2 = self.dist_to_segment([self.body_xyz[0], self.body_xyz[1]], [self.world_box_x2, self.world_box_y2], [self.world_box_x4, self.world_box_y4])
                    #     # if d1 < d2:
                    #     x, y = self.project_to_segment([self.body_xyz[0], self.body_xyz[1]], [self.world_box_x1, self.world_box_y1], [self.world_box_x3, self.world_box_y3])
                    #     delta_y1 = np.sqrt((x - self.world_box_x1)**2 + (y - self.world_box_y1)**2)
                    #     delta_y2 = np.sqrt((x - self.world_box_x3)**2 + (y - self.world_box_y3)**2)
                    #     # else:
                    #     #     # x, y = self.project_to_segment([self.body_xyz[0], self.body_xyz[1]], [self.world_box_x1, self.world_box_y1], [self.world_box_x3, self.world_box_y3])
                    #     #     x, y = self.project_to_segment([self.body_xyz[0], self.body_xyz[1]], [self.world_box_x2, self.world_box_y2], [self.world_box_x4, self.world_box_y4])
                    #     #     delta_y1 = np.sqrt((x - self.world_box_x2)**2 + (y - self.world_box_y2)**2)
                    #     #     delta_y2 = np.sqrt((x - self.world_box_x4)**2 + (y - self.world_box_y4)**2)

                    #     length = self.box_info[2][self.box_num+box][0]
                    #     z    = self.body_xyz[2] - (self.box_info[1][self.box_num+box][2] + self.box_info[2][self.box_num+box][2])
                    #     # Closest point to horizontal segment, delta y along closest edge, length, delta yaw
                    #     self.boxes.extend([x,y,z, delta_y1, delta_y2, length, self.yaw - box_yaw])
                    # else:

                    length, width = self.box_info[2][self.box_num+box][0], self.box_info[2][self.box_num+box][1]
                    # box_yaw = np.arctan2(length, width) + self.box_info[1][self.box_num+box][5]
                    box_yaw = self.box_info[1][self.box_num+box][5] 
                    
                    diag = np.sqrt(length**2 + width**2)
                    self.world_box_x1 = self.box_info[1][self.box_num+box][0] + diag*np.cos(box_yaw+np.arctan2( width,    length))
                    self.world_box_y1 = self.box_info[1][self.box_num+box][1] + diag*np.sin(box_yaw+np.arctan2( width,    length))
                    self.world_box_x2 = self.box_info[1][self.box_num+box][0] + diag*np.cos(box_yaw+np.arctan2( width, -length))
                    self.world_box_y2 = self.box_info[1][self.box_num+box][1] + diag*np.sin(box_yaw+np.arctan2( width, -length))
                    self.world_box_x3 = self.box_info[1][self.box_num+box][0] + diag*np.cos(box_yaw+np.arctan2(-width,    length))
                    self.world_box_y3 = self.box_info[1][self.box_num+box][1] + diag*np.sin(box_yaw+np.arctan2(-width,    length))
                    self.world_box_x4 = self.box_info[1][self.box_num+box][0] + diag*np.cos(box_yaw+np.arctan2(-width, -length))
                    self.world_box_y4 = self.box_info[1][self.box_num+box][1] + diag*np.sin(box_yaw+np.arctan2(-width, -length))

                    box_x1, box_y1 = self.global_to_local_2d(self.body_xyz, self.yaw, [self.world_box_x1, self.world_box_y1])
                    box_x2, box_y2 = self.global_to_local_2d(self.body_xyz, self.yaw, [self.world_box_x4, self.world_box_y4])

                    z    = self.body_xyz[2] - (self.box_info[1][self.box_num+box][2] + self.box_info[2][self.box_num+box][2])
                    # z    = self.body_xyz[2] - (self.box_info[1][self.box_num+box][2]*2)
                    self.boxes.extend([box_x1,box_y1,box_x2,box_y2,z])
        else:
            self.boxes = [0.0]*self.box_dim*self.num_boxes
            
        if self.box_num is None or self.box_info is None:
            self.z_min = 0
        else:
            zs = []
            zs.append(self.box_info[1][self.box_num][2] + self.box_info[2][self.box_num][2])
            if self.box_num - 1 > 0:
                zs.append(self.box_info[1][self.box_num - 1][2] + self.box_info[2][self.box_num - 1][2])
            if self.box_num + 1 < len(self.box_info[0]) - 1:
                zs.append(self.box_info[1][self.box_num + 1][2] + self.box_info[2][self.box_num + 1][2])
            self.z_min = min(zs)
            if self.z_min < 0.1:
                zs.remove(self.z_min)
                self.z_min = min(zs)
    
        if self.multi:
            self.artifact_detection()

    def artifact_detection(self):
        '''
        Look at upcoming terrain. Identify: Jumps, gaps, stairs, base, zero, and xyyaw position of region of attraction. Put in the map if render.
        '''
        # self.switch = False
        x,y,z = self.box_info[1][self.box_num][0], self.box_info[1][self.box_num][1], self.box_info[1][self.box_num][2] + self.box_info[2][self.box_num][2]
        self.artifact = 'flat'
        if self.box_num < 3:
            x,y,z = self.box_info[1][self.box_num][0], self.box_info[1][self.box_num][1], self.box_info[1][self.box_num][2] + self.box_info[2][self.box_num][2]
        if (self.box_num > len(self.box_info[0]) - 5):
            self.artifact = 'zero'
            x,y,z = self.box_info[1][self.box_num][0], self.box_info[1][self.box_num][1], self.box_info[1][self.box_num][2] + self.box_info[2][self.box_num][2]
        else:
            prev_z = self.box_info[1][self.box_num - 1][2]
            for box in self.box_nums:
                if (self.box_num + box > len(self.box_info[0]) - 1):
                    continue
                if self.box_info[1][self.box_num+box][2] < 0.2:
                    self.artifact = 'gaps'
                    x,y,z = self.box_info[1][self.box_num+box-2][0], self.box_info[1][self.box_num+box-2][1], self.box_info[1][self.box_num+box-2][2] + self.box_info[2][self.box_num+box-2][2]
                    break
                elif abs(self.box_info[1][self.box_num+box][2] - prev_z) > 0.10:
                    self.artifact = 'jumps'
                    x,y,z = self.box_info[1][self.box_num+box-2][0], self.box_info[1][self.box_num+box-2][1], self.box_info[1][self.box_num+box-2][2] + self.box_info[2][self.box_num+box-2][2]
                    break
                elif abs(self.box_info[1][self.box_num+box][2] - prev_z) > 0.05:
                    self.artifact = 'stairs'
                    x,y,z = self.box_info[1][self.box_num+box-2][0], self.box_info[1][self.box_num+box-2][1], self.box_info[1][self.box_num+box-2][2] + self.box_info[2][self.box_num+box-2][2]
                    break
                prev_z = self.box_info[1][self.box_num + box][2]
            if self.artifact == 'flat':
                x,y,z = self.box_info[1][self.box_num][0], self.box_info[1][self.box_num][1], self.box_info[1][self.box_num][2] + self.box_info[2][self.box_num][2]
                self.switch = True

        self.prev_artifact = self.artifact


    def get_expert(self):
        # if self.time_of_step == 0:
        if (np.abs([self.speed, self.y_speed, self.yaw_speed]) < 0.1).all():
            self.target_joints = copy.copy(self.still_pos)
            self.cur_time = 0
            self.ob_dict['right_foot_swing'] = 0
            self.ob_dict['left_foot_swing'] = 0
            # print("speed    < 0.1")
            # if self.obstacle_type in ['high_jumps','hard_high_jumps','one_leg_hop']:
            # # if self.order[self.box_num] in ['high_jumps','hard_high_jumps','one_leg_hop']:
            #     # if self.obstacle_type == 'high_jumps' and self.high_jump_step:
            #     if self.obstacle_type in ['high_jumps', 'hard_high_jumps']:
            #         if (self.num_artifacts == 1 and self.high_jump_step) or (self.num_artifacts == 2 and self.second_high_jump_step):
            #             self.target_pos = [self.box_info[1][-1][0], 0, self.z_offset + 0.95]
            #     # if self.obstacle_type == 'hard_high_jumps' and self.high_jump_step:
            #         # if (self.num_artifacts == 1 and self.high_jump_step) or (self.num_artifacts == 1 and self.second_high_jump_step):
            #         #     self.target_pos = [self.box_info[1][-1][0], 0, self.z_offset + 0.95]
            self.stage = '1'
            self.phase = 0
            # else:
            if self.obstacle_type == 'None':
                self.target_pos = [self.body_xyz[0], self.body_xyz[1], self.z_offset + 0.95]
            else:
                self.target_pos = [self.box_info[1][self.box_num][0], self.box_info[1][self.box_num][1], self.z_offset + 0.95]
            
        else:
            # print("speed    > 0.5")
            if self.obstacle_type in ['high_jumps','hard_high_jumps']:
            # if self.order[self.box_num] in ['high_jumps','hard_high_jumps']:
                # self.get_jumping_expert()
                if (self.args.e2e and self.args.flat_e2e):
                    self.get_walking_expert()
                else:
                    self.get_jumping_expert2()
                    # self.get_walking_expert()
            # elif self.obstacle_type in ['one_leg_hop']:
            # elif self.order[self.box_num] in ['one_leg_hop']:
                # self.get_one_leg_jumping_expert()
                # self.get_walking_expert()
            else:
                self.get_walking_expert()

    def get_jumping_expert2(self):
        '''
        upright, lower, jump, pull in legs, land (upright)
        vertical velocity
        '''

        dy = 0.0
        self.target_orn = [0,0,0,1]

        # height = self.box_info[1][self.box_num][2] + self.box_info[2][self.box_num][2]
        # if self.box_num + 1 < len(self.box_info[1]):
        #     next_height = self.box_info[1][self.box_num + 1][2] + self.box_info[2][self.box_num + 1][2]
        # else:
        #     next_height = self.box_info[1][self.box_num][2] + self.box_info[2][self.box_num][2]

        # zero pose
        # if self.stage == '1' and (self.phase > 50 ) and not self.ob_dict['left_foot_left_ground'] and not self.ob_dict['right_foot_left_ground'] and self.vx < 0.1:
        # if self.stage == '1':
        #     print(not (self.ob_dict['left_foot_left_ground'] and self.ob_dict['right_foot_left_ground']) , (np.array([self.vx, self.vy, self.vz ])< 0.1).all() , (self.body_xyz[2] > self.foot_pos['left'][2] + 0.8) and (self.body_xyz[2] > self.foot_pos['right'][2] + 0.8))

        if self.stage == '1' and not (self.ob_dict['left_foot_left_ground'] and self.ob_dict['right_foot_left_ground']) and (np.array([self.vx, self.vy, self.vz ])< 0.1).all() and (self.body_xyz[2] > self.foot_pos['left'][2] + 0.8) and (self.body_xyz[2] > self.foot_pos['right'][2] + 0.8):
            self.stage = '2'
            self.phase = 0

        # Lower
        # if self.stage == '2' and (self.phase > 50 or self.body_xyz[2] < self.z_offset + 0.7):
        # print(self.body_xyz[2], self.foot_pos['left'][2])
        # if self.stage == '2' and (self.body_xyz[2] < self.z_offset + 0.7):
        if self.stage == '2' and (((self.body_xyz[2] < self.foot_pos['left'][2] + 0.6) and (self.body_xyz[2] < self.foot_pos['right'][2] + 0.6)) or (self.phase > 50)):
            self.stage = '3'
            self.phase = 0
        
        # Jump
        # if self.stage == '3'    and d['left_foot_left_ground'] and d['right_foot_left_ground'] and d['z'] > d['z_min'] + 0.95:
        # if self.stage == '3' and (self.phase > 25 or (next_height > height and self.body_xyz[2] > next_height + 1.05)):
        if self.stage == '3' and (self.phase > 25 ):
            self.stage = '4'
            self.phase = 0
            
        # Lift legs
        if self.stage == '4' and (self.phase > 25 or (not self.ob_dict['left_foot_left_ground'] and not self.ob_dict['right_foot_left_ground'])):
            self.stage = '1'
            self.phase = 0

        # if self.stage in ['2','3','4']:
        self.phase += 1
        
        if self.stage in ['1','2']:
        # if (not self.ob_dict['left_foot_left_ground'] or not self.ob_dict['right_foot_left_ground']):
            self.speed = 0.15
        else:
        #     if self.obstacle_type == 'hard_high_jumps':
        #         self.speed = 1.2 + 0.03 * self.difficulty
        #         # self.speed = 1.2
            # else:
            self.speed = 1.2

        dx = self.speed * self.timeStep

        if self.stage == '1':
            if self.target_joints[self.motor_names.index("left_hip_y")] < 0.0:
                self.target_joints[self.motor_names.index("left_hip_y")]     += 0.03
            if self.target_joints[self.motor_names.index("left_knee")]    < 0.0:
                self.target_joints[self.motor_names.index("left_knee")]        += 0.06
            if self.target_joints[self.motor_names.index("left_ankle_y")] > 0.5:
                self.target_joints[self.motor_names.index("left_ankle_y")] -= 0.03
            # self.target_joints = copy.copy(self.still_pos)
            if self.body_xyz[2] > (self.z_offset + 0.95):
                # dz = -0.01
                dz = -0.02
            else:
                dz = 0

        # lower
        if self.stage == '2':
            if self.target_joints[self.motor_names.index("left_hip_y")] > -1.0:
                self.target_joints[self.motor_names.index("left_hip_y")]     -= 0.03
            if self.target_joints[self.motor_names.index("left_knee")]    > -2.0:
                self.target_joints[self.motor_names.index("left_knee")]        -= 0.06
            if self.target_joints[self.motor_names.index("left_ankle_y")] > -0.5:
                self.target_joints[self.motor_names.index("left_ankle_y")] -= 0.03
            dz = -0.005

        # Jump
        if self.stage == '3':
            if self.target_joints[self.motor_names.index("left_hip_y")] < 0.0:
                self.target_joints[self.motor_names.index("left_hip_y")]     += 0.08
            if self.target_joints[self.motor_names.index("left_knee")]    < 0.0:
                self.target_joints[self.motor_names.index("left_knee")]        += 0.15
            if self.target_joints[self.motor_names.index("left_ankle_y")] < 0.5:
                self.target_joints[self.motor_names.index("left_ankle_y")] += 0.08
            # dz = 0.01
            dz = 0.0225
            # dx = 0.01

        # pull in legs
        if self.stage == '4':
            if self.target_joints[self.motor_names.index("left_hip_y")] > -1.0:
                self.target_joints[self.motor_names.index("left_hip_y")]     -= 0.3
            if self.target_joints[self.motor_names.index("left_knee")]    > -2.0:
                self.target_joints[self.motor_names.index("left_knee")]        -= 0.6
            if self.target_joints[self.motor_names.index("left_ankle_y")] > -0.5:
                self.target_joints[self.motor_names.index("left_ankle_y")] -= 0.08
            dz = 0.01

        self.ob_dict['right_foot_swing'] = True
        self.ob_dict['left_foot_swing']    = True

        self.target_joints[self.motor_names.index("right_hip_y")]     = self.target_joints[self.motor_names.index("left_hip_y")]     
        self.target_joints[self.motor_names.index("right_knee")]        = self.target_joints[self.motor_names.index("left_knee")]        
        self.target_joints[self.motor_names.index("right_ankle_y")] = self.target_joints[self.motor_names.index("left_ankle_y")] 

        self.target_pos = [self.target_pos[0]+dx,self.target_pos[1]+dy,self.target_pos[2]+dz]


    def get_jumping_expert(self):
        '''
        upright, lower, jump, pull in legs, land (upright)
        vertical velocity
        '''
        # if abs(d['vz']) < 0.1:
        #     self.stage == '1'
        #     self.stage == '2'
        #     self.stage == '3'
        #     self.stage == '4'
        # if self.stage == '1':
        dy = 0.0
        # x_pos = min(d['body_xyz'][0], d['foot_pos']['left'][0], d['foot_pos']['right'][0])

        self.target_orn = [0,0,0,1]

        # print(self.stage, self.phase)
        # if (self.stage == '4' and d['vz'] < 0.0) or (not self.stage == '1' and not d['left_foot_left_ground'] and not d['right_foot_left_ground'] and d['z'] > d['z_min'] + 0.95):
        #     self.stage = '1'

        height = self.box_info[1][self.box_num][2] + self.box_info[2][self.box_num][2]
        if self.box_num + 1 < len(self.box_info[1]):
            next_height = self.box_info[1][self.box_num + 1][2] + self.box_info[2][self.box_num + 1][2]
        else:
            next_height = self.box_info[1][self.box_num][2] + self.box_info[2][self.box_num][2]

        # zero pose
        # if self.stage == '1' and (self.phase > 50 ) and not self.ob_dict['left_foot_left_ground'] and not self.ob_dict['right_foot_left_ground'] and self.vx < 0.1:
        if self.stage == '1' and (self.phase > 50 ) and not self.ob_dict['left_foot_left_ground'] and not self.ob_dict['right_foot_left_ground']:
            self.stage = '2'
            self.phase = 0

        # Lower
        if self.stage == '2' and (self.phase > 50 or self.body_xyz[2] < self.z_offset + 0.7):
            self.stage = '3'
            self.phase = 0
        
        # Jump
        # if self.stage == '3'    and d['left_foot_left_ground'] and d['right_foot_left_ground'] and d['z'] > d['z_min'] + 0.95:
        # if self.stage == '3' and (self.phase > 25 or (next_height > height and self.body_xyz[2] > next_height + 1.05)):
        if self.stage == '3' and (self.phase > 25 ):
            # print(self.stage, self.first_jump)
            # if self.first_jump:
            self.stage = '4'
            # else:
            #     self.stage = '1'
            self.phase = 0
            
        # Lift legs
        if self.stage == '4' and (self.phase > 25 or (not self.ob_dict['left_foot_left_ground'] and not self.ob_dict['right_foot_left_ground'])):
        # if self.stage == '4' and (self.phase > 25 or next_height == height) :
            self.stage = '1'
            self.phase = 0

        # if self.stage in ['2','3','4']:
        self.phase += 1
        
        # if self.stage in ['1','2']:
        if (not self.ob_dict['left_foot_left_ground'] or not self.ob_dict['right_foot_left_ground']):
            self.speed = 0.15
        else:
            if self.obstacle_type == 'hard_high_jumps':
                self.speed = 1.2 + 0.03 * self.difficulty
                # self.speed = 1.2
            else:
                self.speed = 1.2

        dx = self.speed * self.timeStep

        # if not self.ob_dict['left_foot_left_ground'] or not self.ob_dict['right_foot_left_ground']:
        #     dx = 0.001
        # else:
        #     if self.obstacle_type == 'hard_high_jumps':
        #         dx = 0.0018*self.difficulty
        #     else:
        #         dx = 0.01

        # if self.stage in ['1','2']:
        #     dx = 0.001
        # else:
        #     if self.obstacle_type == 'hard_high_jumps':
        #         dx = 0.0018*self.difficulty
        #     else:
        #         dx = 0.01

        # if self.stage in ['1','2']:
        #     dx = 0.001
        # else:
        #     if self.obstacle_type == 'hard_high_jumps':
        #         dx = 0.0018*self.difficulty
        #     else:
        #         dx = 0.01
        
        # print(self.stage, dx)

        # if not self.ob_dict['left_foot_left_ground'] or not self.ob_dict['right_foot_left_ground']:
        # else:
        #         dx = 0.05
        #     else:
        #         if not self.first_jump:
        #             dx = 0.005
        #         else:
        #             dx = 0.005

        if self.stage == '1':
            self.target_joints = copy.copy(self.still_pos)
            # # print(self.target_joints)
            # self.target_joints[self.motor_names.index("left_hip_y")]     = 0.0
            # self.target_joints[self.motor_names.index("left_knee")]        = 0.0
            # self.target_joints[self.motor_names.index("left_ankle_y")] = 0.0
            # dz = 0
            # print(d['z']    , d['z_min'] + 0.95)
            # print(d['body_xyz'][2], d['z_min'], 0.95)
            if self.body_xyz[2] > (self.z_offset + 0.95):
                # dz = -0.01
                dz = -0.02
            else:
                dz = 0

        # lower
        if self.stage == '2':
            # self.target_joints[self.motor_names.index("left_hip_y")]     = -1.0
            # self.target_joints[self.motor_names.index("left_knee")]        = -2.0
            # self.target_joints[self.motor_names.index("left_ankle_y")] = -0.5
            # print(self.target_joints[self.motor_names.index("left_hip_y")], self.target_joints[self.motor_names.index("left_knee")], self.target_joints[self.motor_names.index("left_ankle_y")])
            # if self.target_joints[self.motor_names.index("left_hip_y")] > -1.0:
            #     self.target_joints[self.motor_names.index("left_hip_y")]     -= 0.1
            # if self.target_joints[self.motor_names.index("left_knee")]    > -2.0:
            #     self.target_joints[self.motor_names.index("left_knee")]        -= 0.2
            # if self.target_joints[self.motor_names.index("left_ankle_y")] < -0.05:
            #     self.target_joints[self.motor_names.index("left_ankle_y")] -= 0.05

            # if next_height > height:
            if self.target_joints[self.motor_names.index("left_hip_y")] > -1.0:
                self.target_joints[self.motor_names.index("left_hip_y")]     -= 0.03
            if self.target_joints[self.motor_names.index("left_knee")]    > -2.0:
                self.target_joints[self.motor_names.index("left_knee")]        -= 0.06
            if self.target_joints[self.motor_names.index("left_ankle_y")] > -0.5:
                self.target_joints[self.motor_names.index("left_ankle_y")] -= 0.03
                # print(self.target_joints[self.motor_names.index("left_ankle_y")])
            dz = -0.005
            # else:
            #     if self.target_joints[self.motor_names.index("left_hip_y")] > -0.5:
            #         self.target_joints[self.motor_names.index("left_hip_y")]     -= 0.03
            #     if self.target_joints[self.motor_names.index("left_knee")]    > -1.0:
            #         self.target_joints[self.motor_names.index("left_knee")]        -= 0.06
            #     if self.target_joints[self.motor_names.index("left_ankle_y")] > -0.25:
            #         self.target_joints[self.motor_names.index("left_ankle_y")] -= 0.03
            #     dz = -0.0025

            # dz = -0.0025
            # dz = -0.0065
            # dx = 0.001

        # Jump
        if self.stage == '3':
            # self.target_joints[self.motor_names.index("left_hip_y")]     = 0.0
            # self.target_joints[self.motor_names.index("left_knee")]        = 0.0
            # self.target_joints[self.motor_names.index("left_ankle_y")] = 0.5

            if self.target_joints[self.motor_names.index("left_hip_y")] < 0.0:
                self.target_joints[self.motor_names.index("left_hip_y")]     += 0.08
            if self.target_joints[self.motor_names.index("left_knee")]    < 0.0:
                self.target_joints[self.motor_names.index("left_knee")]        += 0.15
            if self.target_joints[self.motor_names.index("left_ankle_y")] < 0.5:
                self.target_joints[self.motor_names.index("left_ankle_y")] += 0.08
            # dz = 0.01
            dz = 0.0225
            # dx = 0.01

        # pull in legs
        if self.stage == '4':
            # if self.obstacle_type != 'hard_high_jumps':
                # self.first_jump = False
            # self.target_joints[self.motor_names.index("left_hip_y")]     = -1.0
            # self.target_joints[self.motor_names.index("left_knee")]        = -2.0
            # self.target_joints[self.motor_names.index("left_ankle_y")] = -0.5
            if self.target_joints[self.motor_names.index("left_hip_y")] > -1.0:
                self.target_joints[self.motor_names.index("left_hip_y")]     -= 0.3
            if self.target_joints[self.motor_names.index("left_knee")]    > -2.0:
                self.target_joints[self.motor_names.index("left_knee")]        -= 0.6
            if self.target_joints[self.motor_names.index("left_ankle_y")] > -0.5:
                self.target_joints[self.motor_names.index("left_ankle_y")] -= 0.3
            dz = 0.01
            # dx = 0.01

        # self.stage'] = self.stage

        self.ob_dict['right_foot_swing'] = True
        self.ob_dict['left_foot_swing']    = True

        self.target_joints[self.motor_names.index("right_hip_y")]     = self.target_joints[self.motor_names.index("left_hip_y")]     
        self.target_joints[self.motor_names.index("right_knee")]        = self.target_joints[self.motor_names.index("left_knee")]        
        self.target_joints[self.motor_names.index("right_ankle_y")] = self.target_joints[self.motor_names.index("left_ankle_y")] 
        
        # self.target_joint_vel = (self.target_joints - self.prev_target_joints)/self.timestep
        # self.prev_target_joints = self.target_joints

        self.target_pos = [self.target_pos[0]+dx,self.target_pos[1]+dy,self.target_pos[2]+dz]

        # return self.target_pos, self.target_orn, self.target_joints, self.target_joint_vel

    def get_walking_expert(self):
        # print(self.speed, self.time_of_step)
        # Was previously stopped, now need to select a swing foot
        # print(self.prev_time_of_step == 0 , self.time_of_step != 0)
        if self.prev_time_of_step == 0 and self.time_of_step != 0:
            if self.obstacle_type == 'one_leg_hop':
                # print(self.ob_first_step)
                self.ob_dict['swing_foot'] = self.ob_first_step
            else:
                self.ob_dict['swing_foot'] = self.ob_dict['cur_swing_foot'] = np.random.randint(2)
            self.ob_dict['right_foot_swing'] = self.ob_dict['swing_foot']
            self.ob_dict['left_foot_swing'] = not self.ob_dict['right_foot_swing']

        if self.ob_dict['cur_swing_foot']:
            swing = 'right'
            stance = 'left'
        else:
            swing = 'left'
            stance = 'right'
        self.touchdown_pen = 0
        
        if self.time_of_step > 0.0 and ((self.args.expert or self.comparison in ['no_link','no_link_no_exp']) and (self.steps-self.pause_time) % self.time_of_step == 0) or (not (self.args.expert or self.comparison in ['no_link','no_link_no_exp']) and self.ob_dict['prev_' + swing + '_foot_left_ground'] and not self.ob_dict[swing + '_foot_left_ground']):
        
            self.touchdown_pen = abs(self.cur_time - self.time_of_step)
            # print(self.time_of_step, self.cur_time)
            self.touchdown_dists[swing] = (self.local_foot_pos[swing][0])
            # self.touchdown_box[swing] = self.foot_on_box[swing]
            
            if len(self.touchdown_box[swing]) > 0:
                max_box = max(self.touchdown_box[swing])
                if (max_box - self.foot_on_box[swing]) != 2:
                    self.single_step = True
                else:
                    self.single_step = False

            if self.foot_on_box[swing] not in self.touchdown_box[stance]:
                if len(self.touchdown_box[swing]) > 0 and abs(max_box - self.foot_on_box[swing]) > 1:
                    self.touchdown_box[swing].add(self.foot_on_box[swing])
                elif len(self.touchdown_box[swing]) < 1:
                    self.touchdown_box[swing].add(self.foot_on_box[swing])
                self.double_feet = False
            else:
                self.double_feet = True

            self.cur_time = 0
            self.ob_dict['cur_swing_foot'] = not self.ob_dict['cur_swing_foot']
            self.step_count += 1
            
            if self.box_info is not None:
                if self.ob_dict['cur_swing_foot']:
                    swing = 'right'
                    stance = 'left'
                else:
                    swing = 'left'
                    stance = 'right'

                if self.foot_on_box[stance] + 1 < len(self.box_info[1]):
                    self.desired_box[swing] = self.foot_on_box[stance] + 1
        
        self.ob_dict['swing_foot'] = self.ob_dict['cur_swing_foot']
        if self.ob_dict['swing_foot']:
            self.ob_dict['right_foot_swing'] = True
        else:
            self.ob_dict['right_foot_swing'] = False
        self.ob_dict['left_foot_swing'] = not self.ob_dict['right_foot_swing']
    
        # if self.args.expert and self.box_info is not None:
        #     self.box_info[2][self.box_num][2] = 0.01
        #     self.box_info[2][self.box_num+1][2] = 0.0

        step_type = 'flat'
        step_fact = 0
        swing_fact = 2.5
        if self.steps - self.pause_time < self.time_of_step:
            stance_hip = self.hip_y_total_angle/4
            swing_hip    = -self.hip_y_total_angle/4
            if self.cur_time < 2*self.time_of_step/4:
                swing_knee = -0.4
            else:
                swing_knee = -0.2
            stance_knee = -0.2
        else:
            # if step_type == 'flat':
            stance_hip = 2*self.hip_y_total_angle/5 
            swing_hip    = -3*self.hip_y_total_angle/5 
            if self.cur_time < swing_fact*self.time_of_step/4:
                swing_knee = -1.3
            else:
                swing_knee = -0.2
            stance_knee = -0.2
                # print(stance_hip, swing_hip, swing_knee)
            # elif step_type == 'up':
            #     stance_hip = 2*self.hip_y_total_angle/5 
            #     swing_hip    = -3*self.hip_y_total_angle/5 - step_fact*8
            #     if self.cur_time < 2*self.time_of_step/4:
            #         swing_knee = -1.3 - step_fact*2*7.5
            #     else:
            #         swing_knee = -0.2 - step_fact*2*7.5
            #     stance_knee = -0.2
            # elif step_type == 'down':
            #     stance_hip = 2*self.hip_y_total_angle/5 
            #     swing_hip    = -3*self.hip_y_total_angle/5 
            #     if self.cur_time < 2*self.time_of_step/4:
            #         swing_knee = -1.3
            #         stance_knee = -0.2
            #     else:
            #         swing_knee = -0.2
            #         stance_knee = -0.2 - step_fact*2*7.5
        
        # delta = 0.015*(1.0*abs(self.speed)) + 0.1*step_fact
        # knee_delta = 0.04*(1.0* abs(self.speed))+ 0.1*step_fact
        delta = 1.2*(0.015*(1.0*abs(self.speed)) + 0.1*step_fact)
        knee_delta = 1.2*(0.04*(1.0* abs(self.speed))+ 0.1*step_fact)

        # print(self.speed)

        stance_hip_x = 0.05
        take_off_ankle_y = 0.3
        if self.speed < 0:
            if swing == 'right':
                # Swing
                self.target_joints[2] = min(self.target_joints[2] + delta, -swing_hip)
                if self.cur_time < swing_fact*self.time_of_step/4:
                    self.target_joints[3] = max(self.target_joints[3] - knee_delta, swing_knee)
                else:
                    self.target_joints[3] = min(self.target_joints[3] + knee_delta, swing_knee)
                # Stance
                self.target_joints[8] = max(self.target_joints[8] - delta, -stance_hip)
                self.target_joints[9] = min(self.target_joints[9] + knee_delta, stance_knee)
            else:
                # Swing
                self.target_joints[8] = min(self.target_joints[8] + delta, -swing_hip)
                if self.cur_time < swing_fact*self.time_of_step/4:
                    self.target_joints[9] = max(self.target_joints[9] - knee_delta, swing_knee)
                else:
                    self.target_joints[9] = min(self.target_joints[9] + knee_delta, swing_knee)
                # Stance
                self.target_joints[2] = max(self.target_joints[2] - delta, -stance_hip)
                self.target_joints[3] = min(self.target_joints[3] + knee_delta, stance_knee)
        elif self.speed == 0 and (self.yaw_speed or self.y_speed):
            y_dt = 40*0.02/self.time_of_step
            if swing == 'right':
                if self.cur_time < swing_fact*self.time_of_step/4:
                    self.target_joints[2] = max(self.target_joints[2] - y_dt, -0.3)
                    self.target_joints[3] = max(self.target_joints[3] - y_dt, -0.7)
                else:
                    self.target_joints[2] = min(self.target_joints[2] + y_dt, -0.1)
                    self.target_joints[3] = min(self.target_joints[3] + y_dt, -0.2)
            else:
                if self.cur_time < swing_fact*self.time_of_step/4:
                    self.target_joints[8] = max(self.target_joints[8] - y_dt, -0.3)
                    self.target_joints[9] = max(self.target_joints[9] - y_dt, -0.6)
                else:
                    self.target_joints[8] = min(self.target_joints[8] + y_dt, -0.1)
                    self.target_joints[9] = min(self.target_joints[9] + y_dt, -0.2)

        else:
            if swing == 'right':
                # Swing
                self.target_joints[2] = max(self.target_joints[2] - delta, swing_hip)
                if self.cur_time < swing_fact*self.time_of_step/4:
                    self.target_joints[3] = max(self.target_joints[3] - knee_delta, swing_knee)
                else:
                    self.target_joints[3] = min(self.target_joints[3] + knee_delta, swing_knee)
                # Stance
                self.target_joints[8] = min(self.target_joints[8] + delta, stance_hip)
                self.target_joints[9] = min(self.target_joints[9] + knee_delta, stance_knee)
            else:
                # Swing
                self.target_joints[8] = max(self.target_joints[8] - delta, swing_hip)
                if self.cur_time < swing_fact*self.time_of_step/4:
                    self.target_joints[9] = max(self.target_joints[9] - knee_delta, swing_knee)
                else:
                    self.target_joints[9] = min(self.target_joints[9] + knee_delta, swing_knee)
                # Stance
                self.target_joints[2] = min(self.target_joints[2] + delta, stance_hip)
                self.target_joints[3] = min(self.target_joints[3] + knee_delta, stance_knee)

        # right z
        if self.yaw_speed == 0:
            self.target_joints[0] = self.still_pos[0]
            self.target_joints[6] = self.still_pos[6]
        else:
            z_dt = 40*0.02/self.time_of_step
            if swing == 'right':
                if self.yaw_speed < 0:
                    self.target_joints[0] = max(self.target_joints[0] - z_dt, -self.hip_z_total_angle)
                    self.target_joints[6] = max(self.target_joints[6] - z_dt, -self.hip_z_total_angle)
                else:
                    self.target_joints[0] = min(self.target_joints[0] + z_dt, self.hip_z_total_angle)
                    self.target_joints[6] = min(self.target_joints[6] + z_dt, self.hip_z_total_angle)
            else:
                if self.yaw_speed < 0:
                    self.target_joints[0] = min(self.target_joints[0] + z_dt, self.hip_z_total_angle)
                    self.target_joints[6] = min(self.target_joints[6] + z_dt, self.hip_z_total_angle)
                else:
                    self.target_joints[0] = max(self.target_joints[0] - z_dt, -self.hip_z_total_angle)
                    self.target_joints[6] = max(self.target_joints[6] - z_dt, -self.hip_z_total_angle)
                
        if self.y_speed == 0:
            self.target_joints[1] = self.still_pos[1]
            self.target_joints[7] = self.still_pos[7]
        else:
            x_dt = 120*0.002/self.time_of_step
            if swing == 'right':
                if self.y_speed < 0:                        
                    self.target_joints[1] = max(self.target_joints[1] - x_dt, -self.hip_x_total_angle*4)
                    self.target_joints[7] = max(self.target_joints[7] - x_dt, -self.hip_x_total_angle*4)
                else:                        
                    self.target_joints[1] = min(self.target_joints[1] + x_dt, self.hip_x_total_angle)
                    self.target_joints[7] = min(self.target_joints[7] + x_dt, self.hip_x_total_angle)
            else:
                if self.y_speed < 0:                        
                    self.target_joints[1] = min(self.target_joints[1] + x_dt, self.hip_x_total_angle)
                    self.target_joints[7] = min(self.target_joints[7] + x_dt, self.hip_x_total_angle)
                else:                        
                    self.target_joints[1] = max(self.target_joints[1] - x_dt, -self.hip_x_total_angle*4)
                    self.target_joints[7] = max(self.target_joints[7] - x_dt, -self.hip_x_total_angle*4)
                    
        # Ankles
        self.target_joints[4] = self.target_joints[1]

        self.target_joints[10] = self.target_joints[7]

        if swing == 'right' and self.joints[2] < 0:
            self.target_joints[5] = self.still_pos[5] + self.target_joints[2]/2
        else:
            self.target_joints[5] = self.still_pos[5] - self.target_joints[2]/2

        if swing == 'left' and self.joints[8] < 0:
            self.target_joints[11] = self.still_pos[11] + self.target_joints[8]/2
        else:
            self.target_joints[11] = self.still_pos[11] - self.target_joints[8]/2

        self.cur_time += 1
        
        if self.obstacle_type in ['one_leg_hop', 'hard_steps']:
            if self.obstacle_type == 'one_leg_hop':
                # dx = 0.006
                dx = self.speed*self.timeStep
            else:
                # dx = 0.008
                dx = self.speed*self.timeStep

            # dx = 0.005
            dy = 0
            # print(self.box_info[0])
            # print(self.body_xyz[2] - 0.95 , self.box_info[1][-1][2] + self.box_info[2][-1][2])
            # if self.body_xyz[2] - 0.95 < self.box_info[1][-1][2] + self.box_info[2][-1][2]:
                # dz = 0.0055
            # else:
                # dz = 0.0
            target_z =    self.z_offset + 0.95

        else:
            dx = self.speed*self.timeStep
            # dx = 0.008
            dy = dz = 0
            target_z =    self.target_pos[2] + dz
        if self.obstacle_type != 'None':
            self.target_pos = [self.target_pos[0]+dx*np.cos(self.box_info[1][self.box_num][5]),self.target_pos[1]+dx*np.sin(self.box_info[1][self.box_num][5]),target_z]
        else:
            self.target_pos = [self.target_pos[0]+dx,self.target_pos[1],target_z]

        # print(self.speed, self.target_joints[2], self.target_joints[8])


    def apply_forces(self, actions=None):

        # if self.obstacle_type in ['high_jumps', 'hard_high_jumps', 'one_leg_hop']:
        # if self.order[self.box_num] in ['high_jumps', 'hard_high_jumps']:
        #     x_force = 1.0*self.Kp * (self.target_pos[0] - self.body_xyz[0])
        #     # z_force = 1.5*(self.Kp * ((self.target_pos[2])-self.body_xyz[2]) + self.Kp * self.vz * -1)
        #     z_force = 1.5*(self.Kp * ((self.target_pos[2])-self.body_xyz[2]) + 0.1*self.Kp * self.vz * -1)
        #     y_force = 1.0*self.Kp*(0 - self.body_xyz[1]) + self.Kp*(0.0 - self.vy)
        #     # z_force = 1.5*(self.Kp * ((self.target_pos[2])-self.body_xyz[2]))
        #     # z_force = 0.5*(self.Kp * ((self.target_pos[2])-self.body_xyz[2]))
        # else:
            # if self.order[self.box_num] in ['one_leg_hop']:
        z_force = 1.5*(self.Kp * ((0.95+self.z_offset)-self.body_xyz[2]) + 0.1*self.Kp * self.vz * -1)
        # else:
            # z_force = 1.5*(self.Kp * ((0.95+self.z_offset)-self.body_xyz[2]) + self.Kp * self.vz * -1)
        # if self.obstacle_type == 'flat':
        #     # if self.speed < 0.3:
        #     # x_force = 0.1*self.Kp*(self.box_info[1][self.box_num][0] - self.body_xyz[0]) + self.Kp*((self.speed - self.vx)*np.cos(self.yaw))
        #     # y_force = 0.1*self.Kp*(self.box_info[1][self.box_num][1] - self.body_xyz[1]) + self.Kp*((self.speed - self.vx)*np.sin(self.yaw))
        #     # if self.body_xyz[0] < 2:
        #     x_force = 1.0*self.Kp * (self.speed - self.vx)
        #     y_force = 1.0*self.Kp*(0 - self.body_xyz[1]) + self.Kp*(0.0 - self.vy)
        #         # x_force = self.Kp*((self.speed - self.vx)*np.cos(self.yaw))
        #         # y_force = self.Kp*((self.speed - self.vx)*np.sin(self.yaw))
        #     # else:
        #     #     x_force = 0
        #     #     y_force = 0
        # else:
        # x_force = 1.0*self.Kp * (self.target_pos[0] - self.body_xyz[0])
        # y_force = 1.0*self.Kp * (self.target_pos[1] - self.body_xyz[1])
        # if self.order[self.box_num] in ['high_jumps', 'hard_high_jumps'] and self.stage in ['1','2']:
        #     x_force = 1.0*self.Kp * (0 - self.vx)
        # else:
        # if self.obstacle_type in ['high_jumps','hard_high_jumps']:
        #     x_force = 2.0*self.Kp * (self.speed - self.vx)
        # else:
        # x_error = np.exp(-2.5*max(0,self.speed - self.vx)**2)
        # x_force = 1.0*self.Kp * max(self.speed - self.vx, 0)
        x_force = 1.0*self.Kp * (self.speed - self.vx)
        if self.obstacle_type in ['flat', 'hard_steps'] and self.steps > 200:
            y_force = self.Kp*(0.0 - self.vy)
        else:
            y_force = 1.0*self.Kp*(0 - self.body_xyz[1]) + self.Kp*(0.0 - self.vy)
        # print(x_force)
        p.applyExternalForce(self.Id,-1,[x_force,y_force,z_force],[0,0,0],p.LINK_FRAME)
        # p.applyExternalForce(self.Id,-1,[x_force,y_force,z_force],[0,0,0],p.WORLD_FRAME)

        if self.speed < 0.3:
            # yaw_force = 0.1*self.Kp*(self.yaw_speed - self.yaw_vel)
            if self.args.expert:
                yaw_force = 0.0
            else:
                yaw_force = 1.0*self.Kp*(self.target_yaw - self.yaw) - 0.1*self.Kp*self.yaw_vel
            roll_force = -1.0*self.Kp * self.roll        - 0.1*self.Kp*self.roll_vel
            pitch_force = -2.0*self.Kp * self.pitch    - 0.1*self.Kp*self.pitch_vel
        else:
            # yaw_force = 1.0*self.Kp*(0.0 - self.yaw) - 0.1*self.Kp*self.yaw_vel
            # self.box_info[1][self.box_num][5]
            if self.args.expert:
                yaw_force = 0.0
            else:
                # if self.obstacle_type == 'flat':
                #     # yaw_force = 1.0*self.Kp*(self.box_info[1][self.box_num][5] - self.yaw)- 0.1*self.Kp*self.yaw_vel
                #     yaw_force = 0.05*self.Kp*(self.box_info[1][self.box_num][5] - self.yaw)
                # else:
                yaw_force = 1.0*self.Kp*(self.target_yaw - self.yaw) - 0.1*self.Kp*self.yaw_vel
                
            roll_force = -1.0*self.Kp * self.roll        - 0.1*self.Kp*self.roll_vel
            pitch_force = -2.0*self.Kp * self.pitch    - 0.1*self.Kp*self.pitch_vel
        # p.applyExternalTorque(self.Id,-1,[roll_force,pitch_force,yaw_force],p.LINK_FRAME)
        p.applyExternalTorque(self.Id,-1,[roll_force,pitch_force,yaw_force],p.WORLD_FRAME)
    
        # if self.obstacle_type == 'jumps':
        #     if self.box_num + 1 < len(self.box_info[0]):
        #         if self.box_info[self.box]
        return (self.Kp/self.initial_Kp)*(80*(self.target_joints - np.array(self.joints)) + 0.5*(np.zeros(self.ac_size) - np.array(self.joint_vel)))
        # return (self.Kp/self.initial_Kp)*(100*(self.target_joints - np.array(self.joints)) + 0.0*(np.zeros(self.ac_size) - np.array(self.joint_vel)))


    def set_position(self, pos=[0,0,0], orn=[0,0,0,1], joints=None, velocities=None, joint_vel=None, robot_id=None):
        if robot_id is None:
            robot_id = self.Id
        pos = [pos[0], pos[1], pos[2]]
        p.resetBasePositionAndOrientation(robot_id, pos, orn)
        if joints is not None:
            if joint_vel is not None:
                for j, jv, m in zip(joints, joint_vel, self.motors):
                    p.resetJointState(robot_id, m, targetValue=j, targetVelocity=jv)
            else:
                for j, m in zip(joints, self.motors):
                    p.resetJointState(robot_id, m, targetValue=j)
        if velocities is not None:
            p.resetBaseVelocity(robot_id, velocities[0], velocities[1])

    def save_sim_data(self, PATH=None, last_steps=False, tag='', evaluate=False, force=False):
        if tag != '':
            tail = '_' + tag
        else:
            tail = tag
        if self.sim_data and (self.rank == 0 or force) or last_steps:
            if PATH is not None:
                path = PATH
            else:
                path = self.PATH
            try:
                if last_steps:
                    # print("saving to ", path + 'sim_data.npy')
                    np.save(path + 'sim_data.npy', np.array(self.sim_data)[-300:,:])         
                else:
                    if self.eval or evaluate:
                        # print('saving to ', path + 'sim_data_eval' + tail + '.npy')
                        np.save(path + 'sim_data_eval' + tail + '.npy', np.array(self.sim_data))         
                    else:
                        np.save(path + 'sim_data' + tail + '.npy', np.array(self.sim_data))         
                try:
                    if self.box_info is not None:
                        if self.eval or evaluate:
                            np.save(path + 'box_info_eval' + tail + '.npy', np.array(self.box_info))
                        else:
                            np.save(path + 'box_info' + tail + '.npy', np.array(self.box_info))
                except Exception as e:
                    print("trying to save box info", e)
                if tag == '':
                    self.sim_data = []
            except Exception as e:
                print("Save sim data error:")
                print(e)

    # def save_sim_data(self, PATH=None, last_steps=False, tail='', evaluate=False):
    #     if tail != '':
    #         np.save(self.PATH + 'box_info_eval' + tail + '.npy', np.array(self.box_info))
    #         np.save(self.PATH + 'sim_data_eval' + tail + '.npy', np.array(self.sim_data))         

    #     if self.sim_data and self.rank == 0 or last_steps:
    #         if PATH is not None:
    #             path = PATH
    #         else:
    #             path = self.PATH
    #         try:
    #             if last_steps:
    #                 # print("saving to ", path + 'sim_data.npy')
    #                 np.save(path + 'sim_data.npy', np.array(self.sim_data)[-300:,:])         
    #             else:
    #                 if self.eval or evaluate:
    #                     # print('saving to ', path + 'sim_data_eval' + tail + '.npy')
    #                     np.save(path + 'sim_data_eval' + tail + '.npy', np.array(self.sim_data))         
    #                 else:
    #                     np.save(path + 'sim_data' + tail + '.npy', np.array(self.sim_data))         
    #             try:
    #                 if self.box_info is not None:
    #                     if self.eval or evaluate:
    #                         np.save(path + 'box_info_eval' + tail + '.npy', np.array(self.box_info))
    #                     else:
    #                         np.save(path + 'box_info' + tail + '.npy', np.array(self.box_info))
    #             except Exception as e:
    #                 print("trying to save box info", e)
    #             self.sim_data = []
    #         except Exception as e:
    #             print("Save sim data error:")
    #             print(e)
    def get_terrain(self, box_tense="current"):
        if box_tense == "current":
            num = 0
        elif box_tense == "previous":
            num = -1
        elif box_tense == "next":   
            num = 1
        if self.order[self.box_num + num] == "zero":
            return "flat"
        else:
            return self.order[self.box_num + num]
    
    def set_current_pol(self, current_pol):
        self.current_pol = current_pol
        # print(self.current_pol)

    def record_sim_data(self):
        if len(self.sim_data) > 100000: return
        pos, orn = p.getBasePositionAndOrientation(self.Id)
        data = [pos, orn]
        joints = p.getJointStates(self.Id, self.motors)
        data.append([i[0] for i in joints])
        data.append(self.current_pol)
        self.sim_data.append(data)

    def to_grid(self, y, x):
        x = int(x/self.grid_size +(-self.min_x+1)/self.grid_size)
        y = int(y/self.grid_size + (-self.min_y+1)/self.grid_size)
        return (y, x) 

    def project_to_segment(self, p, v, w):
        l2 = (v[0] - w[0])**2 + (v[1] - w[1])**2
        if l2 == 0: 
            return v[0], v[1]
        t = ((p[0] - v[0]) * (w[0] - v[0]) + (p[1] - v[1]) * (w[1] - v[1])) / l2
        t = max(0, min(1, t))
        return [v[0] + t * (w[0] - v[0]), v[1] + t * (w[1] - v[1])] 

    def dist_to_segment(self, p, v, w):
        p_on_line = self.project_to_segment(p, v, w)
        return np.sqrt((p_on_line[0] - p[0])**2 + (p_on_line[1] - p[1])**2)

    def global_to_local_2d(self, robot_pos, robot_yaw, global_point):
        x1 = global_point[0] - robot_pos[0]
        y1 = global_point[1] - robot_pos[1]
        new_x_point = np.cos(-robot_yaw) * x1 - np.sin(-robot_yaw) * y1
        new_y_point = np.cos(-robot_yaw) * y1 + np.sin(-robot_yaw) * x1
        return new_x_point, new_y_point
    
    def transform_rot(self, yaw, pos, point):
        # rot_mat = np.array(
        # 	[[np.cos(-yaw), -np.sin(-yaw), 0],
        # 	 [np.sin(-yaw), np.cos(-yaw), 0],
        # 	 [		0,			 0, 1]] )
        rot_mat = np.array(
            [[np.cos(-yaw), np.sin(-yaw), 0],
                [-np.sin(-yaw), np.cos(-yaw), 0],
                [		0,			 0, 1]] )
        x,y,z = np.dot(rot_mat,[point[0],point[1],point[2]])
        return pos[0] + x, pos[1] + y, pos[2] + z
    
    def get_z_offset(self):
        # if self.world_map is not None:
        x,y,yaw = np.nan_to_num(self.body_xyz[0]/self.grid_size), np.nan_to_num(self.body_xyz[1]/self.grid_size), np.nan_to_num(self.yaw)
        # # Calculating z offset from box numbers now
        try:
            x_pix = int(x+(-self.min_x+1)/self.grid_size)
            y_pix = int(y+(-self.min_y+1)/self.grid_size)
            ordered_pix = np.sort(self.world_map[x_pix-10:x_pix+10, y_pix-10:y_pix+10], axis=None)
            self.z_offset = max(ordered_pix[-1], 0.5)
        except Exception as e:
            # print("get_z_offset exception", e)
            self.z_offset = 0.0

    def get_hm(self, im_size=[60,40,1]):
        x,y,yaw = self.body_xyz[0]/self.grid_size, self.body_xyz[1]/self.grid_size, self.yaw
        if self.world_map is not None:

            # t1 = time.time()
            hm_pts = self.transform_rot_and_add(1*yaw, [x+(-self.min_x+1)/self.grid_size, y+(-self.min_y+1)/self.grid_size], self.ij)
            hm_pts[:,0] = np.clip(hm_pts[:,0], 0, self.world_map.shape[0]-1)
            hm_pts[:,1] = np.clip(hm_pts[:,1], 0, self.world_map.shape[1]-1)
            self.hm[self.hm_ij[:,0],self.hm_ij[:,1]] = self.world_map[hm_pts[:,0],hm_pts[:,1]]
            # Clip to -1, 1 meters
            # print(self.z_offset)
            
            # Constant height
            # hm = np.clip(self.hm - self.z_offset, -1, 1).reshape(im_size)
            # hm = (hm+1)/2

            # White is lowest
            # hm = np.clip(self.body_xyz[2] - self.hm, 0, 2).reshape(im_size)
            # hm = (hm)/2

            # Black is lowest
            hm = np.clip(self.hm-self.body_xyz[2], -2, 0).reshape(im_size)
            hm = (hm+2)/2
        
        else:
            print("No world map")
            hm = np.zeros(im_size)
        return hm

    def get_rgbd(self, cam_pos='lower'):
        camTargetPos = [0.,0.,0.]
        cameraUp = [0,0,1]
        cameraPos = [1,1,1]
        yaw = 40
        pitch = 10.0

        roll=0
        upAxisIndex = 2
        camDistance = 4
        # new ===================
        pixelWidth = self.im_size[0]
        pixelHeight = self.im_size[1]
        pos = self.body_xyz
        roll, pitch, yaw = self.roll, self.pitch, self.yaw
        # pos, orn = p.getBasePositionAndOrientation(self.Id)
        # roll, pitch, yaw = p.getEulerFromQuaternion(orn)
        if cam_pos == 'lower':
            dx = 0.6
            dy = 0
            dz = -1
            # cam_dx = 0.2
            cam_dx = 0.1
            cam_dy = 0
            cam_dz = 0.2
            fov = 60

        elif cam_pos == 'upper':
            dx = 3.0
            dy = 0
            dz = -1
            cam_dx = 0.0
            cam_dy = 0
            cam_dz = 0.4
            fov = 80

        lightDirection = [pos[0]+cam_dx,pos[1],pos[2]]
        shadow = 1
        
        lightColor = [1,1,1]#optional argument

        p_x, p_y, p_z = self.transform_rot(yaw, pos, [dx,dy,dz])
        cam_x, cam_y, cam_z = self.transform_rot(yaw, pos, [cam_dx, cam_dy, cam_dz])
        nearPlane = 0.25
        farPlane = 2
        aspect = 1
        
        projectionMatrix = p.computeProjectionMatrixFOV(fov, aspect, nearPlane, farPlane)
        viewMatrix = p.computeViewMatrix([cam_x,cam_y,cam_z], [p_x, p_y, p_z], cameraUp)
        try:
            image = p.getCameraImage(pixelWidth, pixelHeight, viewMatrix, projectionMatrix=projectionMatrix, lightDirection=lightDirection,lightColor=lightColor, shadow=shadow, renderer = p.ER_TINY_RENDERER)
            
            depth = copy.deepcopy(image[3]).reshape([pixelHeight, pixelWidth, 1])
            if self.vis_type == 'depth':
                # return np.nan_to_num(depth)
                # print(depth[:100,:100,:])
                # cv2.imshow("depth", depth)
                # cv2.waitKey(1)
                return depth

            rgb = np.array(copy.deepcopy(image[2])).reshape([pixelHeight, pixelWidth, 4])[:,:,:3]/255.0
            # print(rgb.shape, depth.shape)
            # if down_stairs, block off 75% of the image. It can see too far, so if any changes far in the image, the policy is affected
            # if self.down_stairs:
            #         if random.random() < 0.2:
            #                 rgb[:int(48*0.3),:,:] = 0
            #                 depth[:int(48*0.3),:] = 0
            # rgb[:,:,:] = 0
            # depth[:,:,:] = 0
            # if cam_pos == 'upper':
            # re_im = rgb*255.0
            # cv2.imshow('robot_vision2',re_im.astype(dtype=np.uint8)[...,::-1])
            # cv2.imshow('robot_vision2',re_im.astype(dtype=np.uint8)[...,::-1])
            # cv2.waitKey(1)
            return np.nan_to_num(np.concatenate((rgb,depth), axis=2))

        except Exception as e:
                print("get_rgb exception", e)
                return np.zeros([pixelWidth, pixelHeight, 4])


    def get_im(self):
        self.get_z_offset()
        if self.vis_type == 'hm':
            self.im = self.get_hm()
        elif self.vis_type == 'state':
            self.im = np.array(self.boxes).reshape(self.im_size)
        elif self.vis_type in ['rgbd','depth']:
            if self.steps % self.args.camera_rate == 0:
                self.im = self.get_rgbd()
        if self.display_im:
            self.display()                
        return self.im

    def display(self):
        cv2.imshow("image", self.im)
        cv2.waitKey(1)
    
    def get_world_map(self, box_info=None):
        gz = False
        if gz:
            self.min_x = np.min(np.array(box_info[1])[:,0] - np.array(box_info[2])[:,0]/2)
            self.min_y = np.min(np.array(box_info[1])[:,1] - np.array(box_info[2])[:,1]/2)
            self.max_x = np.max(np.array(box_info[1])[:,0] + np.array(box_info[2])[:,0]/2)
            self.max_y = np.max(np.array(box_info[1])[:,1] + np.array(box_info[2])[:,1]/2)
        else:
            self.min_x = np.min(np.array(box_info[1])[:,0] - np.array(box_info[2])[:,0])
            self.min_y = np.min(np.array(box_info[1])[:,1] - np.array(box_info[2])[:,1])
            self.max_x = np.max(np.array(box_info[1])[:,0] + np.array(box_info[2])[:,0])
            self.max_y = np.max(np.array(box_info[1])[:,1] + np.array(box_info[2])[:,1])
        world_shape = [int((self.max_x - self.min_x + 2)/self.grid_size), int((self.max_y - self.min_y + 2)/self.grid_size)]

        hm = np.ones([world_shape[0], world_shape[1]]).astype(np.float32)*-1
        if box_info is not None:
            positions = box_info[1]
            sizes = box_info[2]
            for pos, size in zip(positions, sizes):
                x, y, z = pos[0]/self.grid_size, pos[1]/self.grid_size, pos[2]
                if gz:
                    x_size, y_size, z_size = (size[0]/2)/self.grid_size, (size[1]/2)/self.grid_size, size[2]/2
                else:
                    x_size, y_size, z_size = (size[0])/self.grid_size, (size[1])/self.grid_size, size[2]
                # print(x_size, y_size)
                ij = np.array([[i,j,1] for i in range(int(0-x_size), int(0+x_size)) for j in range(int(0-y_size), int(0+y_size))])

                hm_pts = self.transform_rot_and_add(1*pos[5], [x+(-self.min_x+1)/self.grid_size, y+(-self.min_y+1)/self.grid_size], ij)
                for pt in hm_pts:
                    try:
                        # Not sure why this doesn't work for larger z heights? Should be box z pos + box z size (half extent). It's because I wasn't copying a list properly..
                        # print("**** fix world map ****")
                        # if hm[pt[0], pt[1]] < (z + z_size):
                        #     hm[pt[0], pt[1]] = (z + z_size)
                        if hm[pt[0], pt[1]] < (z * 2):
                            hm[pt[0], pt[1]] = (z * 2)
                    except Exception as e:
                        print(e)
                        print(pt)
        
                        
        self.dx_forward, self.dx_back, self.dy = 36, 24, 20                
        self.ij = np.array([[i,j,1] for i in range(-self.dx_back, self.dx_forward) for j in range(-self.dy, self.dy)])
        self.hm_ij = np.array([[i,j] for i in range(0, self.dx_back + self.dx_forward) for j in range(0, self.dy + self.dy)])
        self.hm = np.ones([self.dx_back+self.dx_forward, 2*self.dy], dtype=np.float32)*-1
        # print("in world map", self.hm.shape)
        return hm

    def transform_rot_and_add(self, yaw, pos, points):
        rot_mat = np.array(
                        [[np.cos(yaw), -np.sin(yaw), pos[0]],
                        [np.sin(yaw), np.cos(yaw), pos[1]],
                        [0, 0, 1]])
        return np.dot(rot_mat,points.T).T.astype(np.int32)

    # def display(self):
        
    #     # dx_forward, dx_back, dy = 40, 20, 20
    #     x1, y1, x2, y2, x3, y3, x4, y4 = self.dx_forward, self.dy, -self.dx_back, self.dy, -self.dx_back, -self.dy, self.dx_forward, -self.dy
        
    #     dx_forward_col, dx_back_col, dy_col = 30, 12, 12
    #     x1_col, y1_col, x2_col, y2_col, x3_col, y3_col, x4_col, y4_col = dx_forward_col, dy_col, -dx_back_col, dy_col, -dx_back_col, -dy_col, dx_forward_col, -dy_col
    #     if self.world_map is not None:
    #         norm_wm = (self.world_map - np.min(self.world_map))/(np.max(self.world_map) - np.min(self.world_map)).astype(int)
    #     else:
    #         norm_wm = np.zeros([100,100])

    #     wm_col = cv2.cvtColor(norm_wm,cv2.COLOR_GRAY2RGB)


    #     if True:
    #         x,y,yaw = self.body_xyz[0]/self.grid_size, self.body_xyz[1]/self.grid_size, self.yaw
    #         rect_pts = np.array([[y1,x1,1],[y2,x2,1],[y3,x3,1],[y4,x4,1]])
    #         pts = self.transform_rot_and_add(-1*yaw, [y+ (-self.min_y+1)/self.grid_size, x+ (-self.min_x+1)/self.grid_size], rect_pts)                
    #         pts = pts[:, :2]
    #         cv2.polylines(wm_col, [pts], True, (0,255,0), 1)
    #         cv2.circle(wm_col,(int(y + (-self.min_y+1)/self.grid_size), int(x +(-self.min_x+1)/self.grid_size)), 3, (0,0,255), -1)
            
    #         if True:
    #             # self.steps > 0:
    #             try:    
    #                 # cv2.circle(wm_col, self.to_grid(self.world_box_y1, self.world_box_x1), 3, (255,0,0), -1)
    #                 cv2.circle(wm_col, self.to_grid(self.world_box_y2, self.world_box_x2), 3, (255,0,0), -1)
    #                 # cv2.circle(wm_col, self.to_grid(self.world_box_y3, self.world_box_x3), 3, (255,0,0), -1)
    #                 cv2.circle(wm_col, self.to_grid(self.world_box_y4, self.world_box_x4), 3, (255,0,0), -1)
            
    #                 # for key in self.foot_on_box:
    #                 for key in ['right', 'left']:
    #                     x1 = x4 =    self.box_info[2][self.desired_box[key]][0]/self.grid_size
    #                     x2 = x3 = -self.box_info[2][self.desired_box[key]][0]/self.grid_size
    #                     y1 = y2 =    self.box_info[2][self.desired_box[key]][1]/self.grid_size
    #                     y3 = y4 = -self.box_info[2][self.desired_box[key]][1]/self.grid_size
    #                     x,y,yaw =    self.box_info[1][self.desired_box[key]][0]/self.grid_size, self.box_info[1][self.desired_box[key]][1]/self.grid_size, self.box_info[1][self.desired_box[key]][5]
    #                     rect_pts = np.array([[y1,x1,1],[y2,x2,1],[y3,x3,1],[y4,x4,1]])
    #                     pts = self.transform_rot_and_add(-1*yaw, [y+ (-self.min_y+1)/self.grid_size, x+ (-self.min_x+1)/self.grid_size], rect_pts)                
    #                     pts = pts[:, :2]
    #                     if key == 'right':
    #                         cv2.polylines(wm_col, [pts], True, (0,0,255), 2)
    #                     elif key == 'left':
    #                         cv2.polylines(wm_col, [pts], True, (255,0,0), 2)
    #                     else:
    #                         cv2.polylines(wm_col, [pts], True, (0,255,0), 1)
    #                     if key != 'com':
    #                         # # Circles:
    #                         # box_x, box_y = self.box_info[1][self.desired_box[key]][0], self.box_info[1][self.desired_box[key]][1]
    #                         # if key == 'left':
    #                         #     box_yaw = self.box_info[1][self.desired_box[key]][5] + np.pi/2
    #                         # else:
    #                         #     box_yaw = self.box_info[1][self.desired_box[key]][5] + np.pi/2 + np.pi
    #                         # # print(box_yaw)
    #                         # dims = np.array([0.1*np.cos(box_yaw)+box_x, 0.1*np.sin(box_yaw)+box_y])/self.grid_size
    #                         # if key == 'left':
    #                         #     cv2.circle(wm_col,(int(dims[1] + (-self.min_y+1)/self.grid_size), int(dims[0] +(-self.min_x+1)/self.grid_size)), 4, (255,0,0), -1)
    #                         # else:
    #                         #     cv2.circle(wm_col,(int(dims[1] + (-self.min_y+1)/self.grid_size), int(dims[0] +(-self.min_x+1)/self.grid_size)), 4, (0,0,255), -1)
                            
    #                         # Lines

    #                         box_x, box_y = self.box_info[1][self.desired_box[key]][0], self.box_info[1][self.desired_box[key]][1]
    #                         yaw1 = self.box_info[1][self.desired_box[key]][5] + np.pi/2
    #                         yaw2 = self.box_info[1][self.desired_box[key]][5] + np.pi/2 + np.pi
    #                         l = self.box_info[2][self.desired_box[key]][1]
    #                         x1,y1 = [l*np.cos(yaw1)+box_x, l*np.sin(yaw1)+box_y]
    #                         x2,y2 = [l*np.cos(yaw2)+box_x, l*np.sin(yaw2)+box_y]
    #                         if key == 'right':
    #                             wm_col = cv2.line(wm_col, self.to_grid(y1,x1), self.to_grid(y2,x2), (0,0,255), 3) 
    #                         else:
    #                             wm_col = cv2.line(wm_col, self.to_grid(y1,x1), self.to_grid (y2,x2), (255,0,0), 3) 

    #                     # x1, y1 = self.box_info[1][self.desired_box['left']][0], self.box_info[1][self.desired_box['left']][1]
    #                     # box_yaw = self.box_info[1][self.desired_box['left']][5] + np.pi/2
    #                     # x2, y2 = x1 + np.cos(box_yaw), y1 + np.sin(box_yaw)

    #             except Exception as e:
    #                 print("exception when trying to display")
    #                 print(e)     
            
    #     norm_hm = np.array(self.hm*255.0, dtype=np.uint8)
    #     norm_hm = cv2.rotate(norm_hm, cv2.ROTATE_180)
    #     # px,py = self.hm.shape[0], self.hm.shape[1]

    #     if self.rank == 0:
    #         # print(wm_col.shape)
    #         # print(wm_col[:10,:10])
    #         cv2.imshow('world_map', wm_col)
    #         cv2.waitKey(1)
    #         cv2.imshow('local_map', norm_hm)
    #         cv2.waitKey(1)

    def add_disturbance(self, max_dist=None, forward_only=False):
        if forward_only:
            force_x = random.random()*max_dist/2
        else:
            force_x = (random.random() - 0.5)*max_dist
        force_y = (random.random() - 0.5)*max_dist
        force_z = (random.random() - 0.5)*(max_dist/4)
        # print("applying forces", force_x, force_y, force_z)
        p.applyExternalForce(self.Id,-1,[force_x,force_y,force_z],[0,0,0],p.LINK_FRAME)
        force_roll = (random.random() - 0.5)*max_dist/2
        force_pitch = (random.random() - 0.5)*max_dist/2
        force_yaw = (random.random() - 0.5)*(max_dist/2)
        p.applyExternalTorque(self.Id,-1,[force_roll,force_pitch,force_yaw],p.LINK_FRAME)

    def log_stuff(self, logger, writer, iters_so_far, log=True):
        if self.early_stop:
            ready_to_stop = MPI.COMM_WORLD.allgather(self.ready_to_stop)
            self.all_ready_to_stop = sum(ready_to_stop) == comm.Get_size()
            if self.rank == 0:
                print("early stop", self.all_ready_to_stop, ready_to_stop, sum(ready_to_stop), len(ready_to_stop), comm.Get_size())

        difficulty = MPI.COMM_WORLD.allgather(self.difficulty)
        dist_difficulty = MPI.COMM_WORLD.allgather(self.dist_difficulty)
        self.min_difficulty = np.min(difficulty)
        disturbances = MPI.COMM_WORLD.allgather(self.max_disturbance)
        Kp = MPI.COMM_WORLD.allgather(self.local_Kp) 
        if self.cur:
            if not self.args.yu:
                if not self.args.cur_local:
                    self.Kp = np.max(Kp)
                else:
                    self.Kp = self.local_Kp
            if self.local_Kp < 5:
                self.prev_local_Kp = self.local_Kp = self.Kp = 0
                self.cur = False
        
        self.iters_so_far = iters_so_far

        if self.args.cur_decay == 'continuous_adaptive':
            adapts = MPI.COMM_WORLD.allgather(np.mean(self.adapts))
            if self.rank == 0:
                print(adapts)
                writer.add_scalar("Adapts", np.mean(adapts), self.iters_so_far)
                logger.record_tabular("Adapts", np.mean(adapts))
            self.adapts = [0.9]

        if self.rank == 0 and log:
            if not self.dqn:
                print("gains", Kp, self.Kp, self.prev_local_Kp, self.local_Kp)
                print("dist_difficulty", dist_difficulty, self.dist_difficulty)

            print("disturbances", disturbances, self.max_disturbance)
            print("difficulty", difficulty, self.difficulty)
            # if self.args.yu:
            # else:
                # print("gains", Kp, self.Kp, self.local_Kp)
            writer.add_scalar("breakdown/goal", np.mean(self.reward_breakdown['goal']), self.iters_so_far)                
            writer.add_scalar("breakdown/pos", np.mean(self.reward_breakdown['pos']), self.iters_so_far)                
            writer.add_scalar("breakdown/vel", np.mean(self.reward_breakdown['vel']), self.iters_so_far)     
            writer.add_scalar("breakdown/tip", np.mean(self.reward_breakdown['tip']), self.iters_so_far)     
            writer.add_scalar("breakdown/com", np.mean(self.reward_breakdown['com']), self.iters_so_far)     
            writer.add_scalar("breakdown/neg", np.mean(self.reward_breakdown['neg']), self.iters_so_far)     
            writer.add_scalar("breakdown/sym", np.mean(self.reward_breakdown['sym']), self.iters_so_far)     
            writer.add_scalar("breakdown/act", np.mean(self.reward_breakdown['act']), self.iters_so_far)     
            writer.add_scalar("difficulty", self.min_difficulty, self.iters_so_far)     
            writer.add_scalar("dist_difficulty", self.dist_difficulty, self.iters_so_far)     
            writer.add_scalar("height", self.height_coeff, self.iters_so_far)     

            # (np.array(self.ep_rewards) > self.cur_len).all()


            writer.add_scalar("Best reward", self.best_reward, self.iters_so_far)
            logger.record_tabular("Best reward", self.best_reward)
            # writer.add_scalar("Success", sum(np.array(self.ep_rewards) > self.cur_len), self.iters_so_far)
            # logger.record_tabular("Success", sum(np.array(self.ep_rewards) > self.cur_len))

            # writer.add_scalar("Success", np.mean(np.array(self.success)), self.iters_so_far)
            # logger.record_tabular("Success", np.mean(np.array(self.success)))

            # writer.add_scalar("Kp", np.mean(Kp), self.iters_so_far)
            # logger.record_tabular("Kp", np.mean(Kp))
            # logger.record_tabular("max yaw", self.max_yaw)
            # logger.record_tabular("pos error", np.mean(self.pos_error))
            # # if self.obstacle_type == 'path':
            # logger.record_tabular("difficulty", self.min_difficulty)
            # logger.record_tabular("dist_difficulty", self.dist_difficulty)
            # # elif self.obstacle_type == 'stairs':
            # logger.record_tabular("height_coeff", self.height_coeff)
            
            # writer.add_scalar("cur_num", self.cur_num, self.iters_so_far)     
            # logger.record_tabular("cur_num", self.cur_num)
            
            # writer.add_scalar("touchdown/left", np.mean(self.touchdown_pens[0]), self.iters_so_far)     
            # writer.add_scalar("touchdown/right", np.mean(self.touchdown_pens[1]), self.iters_so_far)     

            # writer.add_scalar("touchdown_dists/left", np.mean(self.touchdown_dists['left']), self.iters_so_far)     
            # writer.add_scalar("touchdown_dists/right", np.mean(self.touchdown_dists['right']), self.iters_so_far)     

            # logger.record_tabular("touchdown_left", np.mean(self.touchdown_pens[0]))
            # logger.record_tabular("touchdown_right", np.mean(self.touchdown_pens[1]))
            
            # logger.record_tabular("touchdown_dists_left", np.mean(self.touchdown_dists['left']))
            # logger.record_tabular("touchdown_dists_right", np.mean(self.touchdown_dists['right']))
            
            writer.add_scalar("max_disturbance", self.max_disturbance, self.iters_so_far)     
            logger.record_tabular("max_disturbance", self.max_disturbance)