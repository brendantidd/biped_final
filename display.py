'''
Script used to display robot stuff while training (loads in save robot state)
'''
import pybullet as p
import numpy as np
import argparse
from pathlib import Path
import time
import os
import cv2
home = str(Path.home())

# Hack for loading defaults, but still accepting run specific defaults
import defaults
from dotmap import DotMap
args1, unknown1 = defaults.get_defaults() 
parser = argparse.ArgumentParser()
# Arguments that are specific for this run (including run specific defaults, ignore unknown arguments)
parser.add_argument('--tag', default="")
parser.add_argument('--folder', default='b')
parser.add_argument('--obstacle_type', default='high_jumps')
parser.add_argument('--robot', default='biped', help='biped or hexapod')
parser.add_argument('--render', default=True, action='store_false')
parser.add_argument('--record_step', default=False, action='store_true')
parser.add_argument('--mac', default=False, action='store_true')
parser.add_argument('--debug', default=True, action='store_false')
parser.add_argument('--terrain_count', default=1, type=int)
parser.add_argument('--jump_height', default=0.8, type=float)
parser.add_argument('--sleep', default=0.01, type=float)
parser.add_argument('--joint_gain', default=80, type=float)
args2, unknown2 = parser.parse_known_args()
args2 = vars(args2)
# Replace any arguments from defaults with run specific defaults
for key in args2:
    args1[key] = args2[key]
# Look for any changes to defaults (unknowns) and replace values in args1
for n, unknown in enumerate(unknown2):
    if "--" in unknown and n < len(unknown2)-1 and "--" not in unknown2[n+1]:
        arg_type = type(args1[unknown[2:]])
        args1[unknown[2:]] = arg_type(unknown2[n+1])
args = DotMap(args1)
# Check for dodgy arguments
unknowns = []
for unknown in unknown1 + unknown2:
    if "--" in unknown and unknown[2:] not in args:
        unknowns.append(unknown)
if len(unknowns) > 0:
    print("Dodgy argument")
    print(unknowns)
    exit()

if args.mac:
    MODEL_PATH = '/Users/Volumes/tid010/results/biped_final/latest/' + args.exp + '/'
else:
    if args.hpc:
        hpc = 'hpc-home/'
        # MODEL_PATH = home + '/Dropbox/robot/mybot_ws/results_hpc/' + args.exp + '/'
    else:
        hpc = ''
    MODEL_PATH = home + '/' + hpc + 'results/biped_final/latest/' + args.exp + '/'

# MODEL_PATH = home + '/results/biped_model/latest/multi/roa_not_new3/failure1/'


if args.robot == 'biped':
    # from assets.env_pb_biped import Env
    from assets.env_pb_biped_setup1 import Env
elif args.robot == 'hexapod':
    from assets.env_pb_hex import Env

# env = Env(PATH=None, args=args)
env = Env(render=args.render, PATH=None, record_step=args.record_step, args=args, display_im=False, obstacle_type=args.obstacle_type, obstacle_types=[args.obstacle_type], vis=False, doa=False, disturbances=True, initial_disturbance=False, final_disturbance=False, multi=False, dqn=True, difficulty=False, cur=False)


def data_gen(path, box_path = None, im_path = None):
    print(path)
    # env.reset()
    while True:
        try:
            try:
                box_info = np.load(box_path, allow_pickle=True)
                sim_data = np.load(path, allow_pickle=True)
            except Exception as e:
                print("no box info", e)
                box_info = None
            try:
                im_data = np.load(im_path, allow_pickle=True)
            except Exception as e:
                print("im data", e)
                im_data = [None for i in range(sim_data.shape[0])]
            
            env.reset(box_info=box_info)
            if len(sim_data) == 0:
                yield None
            count = 0
            # print("Max torque", len(sim_data[5][:]))
            # print(np.max(sim_data[4,:], axis=0))
            # print(np.max(sim_data[5,:], axis=0))
            for seg, im in zip(sim_data, im_data):
                yield seg, count, im
                count += 1
        except Exception as e:
            print("exception")
            print(e)

if args.tag is not "":
    args.tag = "_" + args.tag

data = data_gen(path = MODEL_PATH + 'sim_data' + args.tag + '.npy', box_path = MODEL_PATH + 'box_info' + args.tag + '.npy', im_path = MODEL_PATH + 'im_data' + args.tag + '.npy')

prev_x = 0
x_pos = 0.0
# all_segs = []
while True:
    seg, count, im = data.__next__()
    # print(p.getEulerFromQuaternion(seg[1])[1])
    # print(env.steps, "vel", (seg[0][0] - prev_x) / env.timeStep )
    # , " pitch ", env.pitch, " contact " , sum([env.ob_dict[w] for w in env.shin_dict]) > 0, " length " , env.steps >= 1000, " height ", env.body_xyz[2] < (0.3 + env.z_min))
    # print(env.body_xyz[2])
    prev_x = seg[0][0]
    # print(seg[4])
    # print(seg[5])
    # print()
    # print(np.max(seg[4], axis=1))
    # print(np.max(seg[5], axis=1))
    
    if seg is not None:
        # if ( not env.ob_dict['prev_left_foot_left_ground'] and not env.ob_dict['prev_right_foot_left_ground'] ) and ( env.ob_dict['left_foot_left_ground'] and env.ob_dict['right_foot_left_ground'] ):
            # print(env.steps,  env.body_xyz[0] - x_pos)
            # x_pos = env.x_min
        # if ( env.ob_dict['prev_left_foot_left_ground'] and env.ob_dict['prev_right_foot_left_ground'] ) and ( not env.ob_dict['left_foot_left_ground'] and not env.ob_dict['right_foot_left_ground'] ):
            # print(env.steps, env.x_min - x_pos)
        env.step(actions=np.zeros(env.ac_size), set_position=seg)
        # print(env.body_xyz[2] , env.foot_pos['left'][2] + 0.6 , env.body_xyz[2],  env.foot_pos['right'][2] + 0.6)
        # print(((env.body_xyz[2] > env.foot_pos['left'][2] + 0.85) and (env.body_xyz[2] > env.foot_pos['right'][2] + 0.85)), ((env.body_xyz[2] < env.foot_pos['left'][2] + 0.7) and (env.body_xyz[2] < env.foot_pos['right'][2] + 0.7)))
        # print(env.box_num, (env.box_info[1][env.box_num ][2] + env.box_info[2][env.box_num ][2]), env.body_xyz[2] - (env.box_info[1][env.box_num][2] + env.box_info[2][env.box_num][2]))
        # print(((env.body_xyz[2] < env.foot_pos['left'][2] + 0.7) , (env.body_xyz[2] < env.foot_pos['right'][2] + 0.7)))
        # print(env.body_xyz[2] , (env.get_box_pos("z", env.box_num, "after") + 0.7))
        # print(env.steps, env.vx)
        if args.obstacle_type == "high_jumps":
            print(env.jump_count, env.stage, env.phase, env.jump_size)
        # print((env.body_xyz[2] - env.foot_pos['left'][2] ) , (env.body_xyz[2] - env.foot_pos['right'][2] ))
        # print(env.stage, env.vx, env.body_vxyz[0])
        # time.sleep(0.1)
        if im is not None:
            im = im.astype('float32')
            im = cv2.resize(im,dsize=(int(im.shape[1] * 6), int(im.shape[0] * 6)))
            cv2.imshow("frame", im)
            cv2.waitKey(1)

