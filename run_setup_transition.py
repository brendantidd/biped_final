import os     
import sys
import psutil
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' 
import tensorflow as tf
tf.get_logger().setLevel('DEBUG')
import numpy as np
import argparse
import tensorboardX
from collections import deque     
from scripts.utils import *
from baselines.common.mpi_util import sync_from_root
import random
from mpi4py import MPI
import time
from pathlib import Path
home = str(Path.home())
from baselines import logger
import matplotlib.pyplot as plt
plt.switch_backend('agg')
import pybullet as p
import cv2
import json
import git
np.set_printoptions(precision=3, suppress=True)
import gc
from six.moves import shlex_quote

def all_gather(data, name, rank, num_workers):
    all_d = MPI.COMM_WORLD.allgather(data)
    temp_d = np.concatenate(all_d)
    length = temp_d.shape[0]//num_workers
    start = rank*length
    end = start + length
    if name == 'done' and rank == 0:
        print("data shape", data.shape, [len(ad) for ad in all_d])
    if len(data.shape) == 1:
        return temp_d[start:end]
    else:
        return temp_d[start:end, ::]

def run(args):

    PATH = home + '/results/biped_model/latest/' + args.folder + '/' + args.exp + '/'

    comm = MPI.COMM_WORLD
    rank = comm.Get_rank()
    num_workers = comm.Get_size()
    myseed = int(args.seed + 10000 * rank)
    np.random.seed(myseed)
    random.seed(myseed)
    tf.set_random_seed(myseed)
    
    logger.configure(dir=PATH)
    if rank == 0:
        writer = tensorboardX.SummaryWriter(log_dir=PATH)
        repo = git.Repo(search_parent_directories=True)
        sha = repo.head.object.hexsha
        with open(PATH + 'commandline_args.txt', 'w') as f:
            f.write('Hash:')
            f.write(str(sha) + "\n")
            json.dump(args.__dict__, f, indent=2)
        print('Save git commit and diff to {}/git.txt'.format(PATH))
        cmds = ["echo `git rev-parse HEAD` >> {}".format(
                        shlex_quote(os.path.join(PATH, 'git.txt'))),
                        "git diff >> {}".format(
                        shlex_quote(os.path.join(PATH, 'git.txt')))]
        os.system("\n".join(cmds))
    else: 
        writer = None 

    gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction= 0.1)
    sess = tf.InteractiveSession(config=tf.ConfigProto(inter_op_parallelism_threads=1,
                                                                                    intra_op_parallelism_threads=1,                         
                                                                                    gpu_options=gpu_options), graph=None)
 
    horizon = 2048

    from assets.env_pb_biped_setup1 import Env
    from models import ppo
    # from models.dqn_adv import DQN
    if args.use_classifier:
        from models.classifier import Classifier
        classifier = Classifier(args, im_size=[48,48,1], dense_size=128, sess=sess) 
        classifier.load('./weights/classifier/')

    obstacle_types = [args.obstacle_type, args.obstacle_type + '_setup', 'flat']

    env = Env(render=args.render, PATH=PATH, args=args, display_im=args.display_im, obstacle_type=args.obstacle_type, obstacle_types=obstacle_types, vis=args.vis, doa=args.doa, disturbances=False, multi=args.multi, dqn=True, difficulty=args.difficulty)
 
    pol = {name:ppo.Model(name, env=env, ob_size=env.ob_size, ac_size=env.ac_size, im_size=env.im_size, args=args, PATH=PATH, horizon=horizon, writer=writer, max_timesteps=args.max_ts, vis=args.vis, const_std=args.const_std) for name in obstacle_types if 'setup' not in name}    
    
    from models import ppo_setup_transition
    pol[args.obstacle_type + '_setup'] = ppo_setup_transition.Model(args.obstacle_type + '_setup', env=env, ob_size=env.ob_size, ac_size=env.ac_size, im_size=env.im_size, args=args, PATH=PATH, horizon=horizon, writer=writer, max_timesteps=args.max_ts, vis=args.vis, const_std=args.const_std)

    if args.hpc:
        WEIGHTS_PATH = home + '/hpc-home/results/biped_model/weights/' + args.folder 
    else:
        # WEIGHTS_PATH = home + '/results/biped_model/weights/' + args.folder 
        WEIGHTS_PATH = './weights'
    
    for name in obstacle_types:
        if 'setup' in name:
            if not args.blank:
                if args.train_select and not args.train_setup:
                    pol[name].load_pol(base_name=name, WEIGHTS_PATH='./weights/' + name + '/', vf=False)
                else:
                    pol[name].load_pol(base_name='flat', WEIGHTS_PATH='./weights/' + 'flat' + '/', vf=False)
        else:
            pol[name].load_pol(base_name=name, WEIGHTS_PATH='./weights/' + name + '/')
        
    initialize_uninitialized()
    sync_from_root(sess, pol[args.obstacle_type + '_setup'].vars, comm=comm)

    # Throw an error if the graph grows (shouldn't change once everything is initialised)
    tf.get_default_graph().finalize()

    env.collect_data = True    
    prev_done = True
    prev_setup_done = True
    prev_select_done = True
    ob = env.reset()
    im = env.get_im()
    eval_success = []
    eval_distance = []
    eval_lengths = []
    if args.eval_first:
        eval_next = True
        t_eval = time.time()
        evaluate = True
        stochastic = False
    else:
        eval_next = False
        evaluate = False
        stochastic = True
        if rank == 0:
            pol[args.obstacle_type + '_setup'].writer.add_scalar("Eval_success", 0, 0)
            pol[args.obstacle_type + '_setup'].writer.add_scalar("Eval_distance", 0, 0)
            pol[args.obstacle_type + '_setup'].writer.add_scalar("Eval_lengths", 0, 0)
    
    ep_ret = 0
    ep_len = 0
    ep_rets = []
    ep_lens = []
    setup_ep_ret = 0
    setup_ep_len = 0
    setup_ep_rets = []
    setup_ep_lens = []
    select_ep_ret = 0
    select_ep_len = 0
    select_ep_rets = []
    select_ep_lens = []
    ep_steps = 0
    ep_count = 0
    # timesteps_so_far = 0

    vpred_data = []
    vpred_orig_data = []
    probs_data = []
    stable_vpred = []
    terrain_data = []
    current_pol_data = []
    baseline_data = []
    select_raw_data = []
    success = []

    if env.collect_data:
        next_pol = desired_pol = prev_pol = current_pol = obstacle_types.index(args.obstacle_type)
    else:
        next_pol = desired_pol = prev_pol = current_pol = obstacle_types.index('flat')

    if args.render and args.debug:
        replace_Id2 = p.addUserDebugText(obstacle_types[current_pol],[env.body_xyz[0], env.body_xyz[1], env.body_xyz[2]+0.5],[0,0,1],textSize=3)

    pols = [i for i in range(len(obstacle_types))]                         
    box_cross_steps = None

    # max_setup_length = 90
    max_setup_length = 75
    _, exp_vpred, _, _ = pol[args.obstacle_type].step(ob, im, stochastic=False)    
    setup_done = False
    select_done = False
    prev_avg_success = avg_success = 0
    train_steps = 0
    eps = 1.0
    switched = False
    initial_setup = 150
    if args.train_select and args.train_setup:
        ready_to_train_select = False
    elif args.train_select:
        ready_to_train_select = True

    obs = []
    imgs = []
    switch = None
    prev_prox = pol[args.obstacle_type + '_setup'].train_model.get_prox(ob, im)
    total_steps = 0
    eval_step = 0
    select_desired_pol = current_pol
    terminate_setup = False
    max_disturbance = 500
    while True:

        # ===================================================================================
        # Evaluate stuff ====================================================================
        # ===================================================================================
        total_steps += 1
        if eval_next and total_steps % 1000 == 0:
            all_success = MPI.COMM_WORLD.allgather(eval_success)
            all_success = [h for d in all_success for h in d]
            all_distance = MPI.COMM_WORLD.allgather(eval_distance)
            all_distance = [h for d in all_distance for h in d]
            all_lengths = MPI.COMM_WORLD.allgather(eval_lengths)
            all_lengths = [h for d in all_lengths for h in d]
            if len(all_success) > 100:
                eval_next = False
                if rank == 0:
                    pol[args.obstacle_type + '_setup'].writer.add_scalar("Eval_success", np.mean(all_success), pol[args.obstacle_type + '_setup'].iters_so_far)
                    pol[args.obstacle_type + '_setup'].writer.add_scalar("Eval_distance", np.mean(all_distance), pol[args.obstacle_type + '_setup'].iters_so_far)
                    pol[args.obstacle_type + '_setup'].writer.add_scalar("Eval_lengths", np.mean(all_lengths), pol[args.obstacle_type + '_setup'].iters_so_far)
                    print("resuming training", len(all_success), "evaluation took ", time.time() - t_eval, "eval success", np.mean(all_success), "lengths", np.mean(all_lengths))
        # ===================================================================================

        if (pol[args.obstacle_type + '_setup'].timesteps_so_far > args.max_ts):
            break 

        # if avg_success > prev_avg_success:
        #     prev_avg_success = avg_success
        # # eps = max(1 - 2*prev_avg_success, 0.01)
        # eps = max(1 - prev_avg_success, 0.01)
        if not args.no_disturbance and random.random() < 0.02 and (not (args.test_pol or evaluate)) and obstacle_types[current_pol] != args.obstacle_type:
            env.add_disturbance(max_disturbance)
            # print("adding disturbance", max_disturbance)

        # ===================================================================================
        # Step env ==========================================================================
        # ===================================================================================
        if obstacle_types[current_pol] == args.obstacle_type + '_setup':
            if args.test_pol:
                act, setup_vpred, _, nlogp, select_raw, nlogp_select = pol[args.obstacle_type + '_setup'].step(ob, im, stochastic=False)
            else:
                act, setup_vpred, _, nlogp, select_raw, nlogp_select = pol[args.obstacle_type + '_setup'].step(ob, im, stochastic=stochastic)
            if args.categorical:
                eps = max(1.0 * (1 - (pol[args.obstacle_type + '_setup'].iters_so_far)/50), 0.005)*60
                if setup_ep_len > eps:
                    select_raw = select_raw
                else:
                    select_raw = 0
                terminate_setup = select_raw
                nlogp_select = pol[args.obstacle_type + '_setup'].train_model.get_neglogp_select(ob, im, np.array(select_raw))
            else:
                # print(select_raw)
                # terminate_setup = select_raw > 2
                terminate_setup = select_raw[0] > 3
        else:
            act, _, _, _ = pol[obstacle_types[current_pol]].step(ob, im, stochastic=False)

        torques = act
        
        next_ob, rew, done, _ = env.step(torques)
        next_im = env.get_im()
        if (args.obstacle_type == 'flat' and env.box_num == (len(env.box_info[1])-1)) or (args.obstacle_type != 'flat' and (env.box_info[1][-1][0] - env.box_info[2][-1][0]) < env.x_min):
            done = True

        obs.append(ob)
        imgs.append(im)

        # if not setup_done and ((args.train_select and terminate_setup) or done or (prev_pol == obstacle_types.index(args.obstacle_type + '_setup') and current_pol != obstacle_types.index(args.obstacle_type + '_setup'))):
        if not setup_done and ((args.train_select and terminate_setup) or done or (current_pol == obstacle_types.index(args.obstacle_type))):
        # if not setup_done and ((args.train_select and terminate_setup) or done or (prev_pol == obstacle_types.index(args.obstacle_type + '_setup') and current_pol != obstacle_types.index(args.obstacle_type + '_setup'))):
            setup_done = True
            last_next_ob = next_ob
            last_next_im = next_im
            switch = env.steps
            # print()
            # print("switching at ", env.steps)
        # ===================================================================================

        terrain_data.append(env.z_offset)

        if args.render and args.debug:
            replace_Id2 = p.addUserDebugText(obstacle_types[current_pol],[env.body_xyz[0], env.body_xyz[1], env.body_xyz[2]+0.5],[0,0,1],textSize=3,replaceItemUniqueId=replace_Id2)
        
        # ===================================================================================
        # Setup stuff =======================================================================
        # ===================================================================================


        if obstacle_types[current_pol] == args.obstacle_type + '_setup':
            prox = pol[args.obstacle_type + '_setup'].train_model.get_prox(next_ob, next_im)
            setup_rew = prox - prev_prox
            # if rank == 0: print(setup_rew, prox, prev_prox)
            prev_prox = prox

            setup_ep_ret += setup_rew
            setup_ep_len += 1

        # ===================================================================================


        # ===================================================================================
        # Training stuff ====================================================================
        # ===================================================================================
        if not args.test_pol and not evaluate:
            if not env.collect_data and args.train_setup and obstacle_types[current_pol] == args.obstacle_type + '_setup':
                pol[args.obstacle_type + '_setup'].add_to_buffer([ob, im, act, select_raw, setup_rew, prev_setup_done, setup_vpred, nlogp, nlogp_select])
        train_steps += 1
        if not args.test_pol and not evaluate: 
            if len(pol[args.obstacle_type + '_setup'].data['rew']) > 1440 and train_steps % 100 == 0:
            # if len(dqn.data['rew']) > 2048 and train_steps % 100 == 0:
                setup_data_lengths = MPI.COMM_WORLD.allgather(len(pol[args.obstacle_type + '_setup'].data['rew']))
                if (np.array(setup_data_lengths) > 1440).all():
                    if rank == 0:
                        print("setup", setup_data_lengths)
                    successes = MPI.COMM_WORLD.allgather(success)
                    successes = np.array([d for h in successes for d in h], dtype=np.float32)
                    avg_success = np.mean(successes)
                    things = {'Avg_success':np.mean(successes)}
                    things['eps'] = eps
                    if rank == 0:
                        process = psutil.Process(os.getpid())
                        things["RAM"] = num_workers*process.memory_info().rss/(1024.0 ** 3)

                    if args.debug and rank == 0: print(rank, "training setup, total steps", len(pol[args.obstacle_type + '_setup'].data['rew']), train_steps)
                    
                    if setup_done:
                        _, setup_next_vpred, _, _, _, _ = pol[args.obstacle_type + '_setup'].step(last_next_ob, last_next_im, stochastic=True)
                    else:
                        _, setup_next_vpred, _, _, _, _ = pol[args.obstacle_type + '_setup'].step(next_ob, next_im, stochastic=True)
                    
                    # Calculate advantage with local data
                    pol[args.obstacle_type + '_setup'].finalise_buffer({"ep_rets":ep_rets, "ep_lens":ep_lens}, last_value=setup_next_vpred, last_done=setup_done)    
                    
                     # Lots of smaller calls to allgather
                    for d in pol[args.obstacle_type + '_setup'].training_input:
                        all_d = MPI.COMM_WORLD.allgather(pol[args.obstacle_type     + '_setup'].data[d])
                        temp_d = np.concatenate(all_d)
                        length = temp_d.shape[0]//num_workers
                        start = rank*length
                        end = start + length
                        if len(pol[args.obstacle_type + '_setup'].data[d].shape) == 1:
                            pol[args.obstacle_type + '_setup'].data[d] = temp_d[start:end]
                        else:
                            pol[args.obstacle_type + '_setup'].data[d] = temp_d[start:end, ::]
                        temp_d = None
                        all_d = None

                    # Add additional things that should be logged and added to tensorboard
                    pol[args.obstacle_type + '_setup'].log_stuff(things)

                    pol[args.obstacle_type + '_setup'].run_train(setup_ep_rets, setup_ep_lens)
                    
                    success = []
                    ep_rets = []
                    ep_lens = []
                    setup_ep_rets = []
                    setup_ep_lens = []
                    select_ep_rets = []
                    select_ep_lens = []

        # ===================================================================================
        env.set_current_pol(current_pol)
        current_pol_data.append(current_pol)
        baseline_data.append(desired_pol)
        select_raw_data.append(terminate_setup)

        # ===================================================================================
        # Terrain oracle ====================================================================
        # ===================================================================================
        
        if not box_cross_steps and env.order[env.box_num] == args.obstacle_type:
            box_cross_steps = env.steps

        if not box_cross_steps:
            desired_pol = obstacle_types.index('flat') 
        elif box_cross_steps and (box_cross_steps + max_setup_length > env.steps):
            desired_pol = obstacle_types.index(args.obstacle_type + '_setup') 
        elif box_cross_steps and (box_cross_steps + max_setup_length <= env.steps):
            if (env.order[env.box_num] == args.obstacle_type):
                desired_pol = obstacle_types.index(args.obstacle_type) 
            elif (env.order[env.box_num-1] == args.obstacle_type and env.order[env.box_num] != args.obstacle_type and env.body_xyz[0] < env.box_info[1][env.box_num][0]):
                desired_pol = obstacle_types.index(args.obstacle_type) 
            else:
                desired_pol = obstacle_types.index('flat') 

        # ===================================================================================
        # Select stuff ======================================================================
        # ===================================================================================
        if env.collect_data == True:
            if (env.order[env.box_num] == args.obstacle_type) or (env.order[env.box_num-1] == args.obstacle_type and env.order[env.box_num] != args.obstacle_type and env.body_xyz[0] < env.box_info[1][env.box_num][0]):
                next_pol = obstacle_types.index(args.obstacle_type)
            else:
                next_pol = obstacle_types.index('flat')
        else:
            if not box_cross_steps:
                next_pol = obstacle_types.index('flat') 
            elif box_cross_steps and (box_cross_steps + max_setup_length > env.steps) and not setup_done:
                next_pol = obstacle_types.index(args.obstacle_type + '_setup') 
            elif box_cross_steps and (box_cross_steps + max_setup_length <= env.steps or setup_done):
                if (env.order[env.box_num] == args.obstacle_type):
                    next_pol = obstacle_types.index(args.obstacle_type) 
                elif (env.order[env.box_num-1] == args.obstacle_type and env.order[env.box_num] != args.obstacle_type and env.body_xyz[0] < env.box_info[1][env.box_num][0]):
                    next_pol = obstacle_types.index(args.obstacle_type) 
                else:
                    next_pol = obstacle_types.index('flat') 

        prev_pol = current_pol
        current_pol = next_pol

        if obstacle_types[current_pol] == args.obstacle_type + '_setup':
            prev_setup_done = setup_done
        prev_done = done
        ob = next_ob
        im = next_im
        ep_ret += rew
        ep_len += 1
        ep_steps += 1
        
        if not env.collect_data and args.eval and not eval_next and pol[args.obstacle_type + '_setup'].iters_so_far % 10 == 0 and pol[args.obstacle_type + '_setup'].iters_so_far != 0 and eval_step != pol[args.obstacle_type + '_setup'].iters_so_far: 
            eval_step = pol[args.obstacle_type + '_setup'].iters_so_far
            eval_next = True
            eval_success = []
            eval_distance = []
            t_eval = time.time()
        
        # ====================================================================================================================
        # Done
        # ====================================================================================================================
        if done:        
            if (args.obstacle_type == 'flat' and env.box_num == (len(env.box_info[1])-1)) or (args.obstacle_type != 'flat' and (env.box_info[1][-1][0] - env.box_info[2][-1][0]) < env.x_min):
            # if ((env.box_info[1][-1][0] - env.box_info[2][-1][0]) < env.body_xyz[0]) and env.steps >= env.max_steps - 2:
                label = True    
                if rank == 0:
                    env.save_sim_data(tag=str(rank)+'_success')
            else:
                label = False
            success.append(label)
         
            if evaluate:
                eval_success.append(label)
                eval_distance.append(env.x_min/(env.box_info[1][-1][0] - env.box_info[2][-1][0]))

            if eval_next:
                evaluate = True
                stochastic = False
            else:
                evaluate = False
                stochastic = True

            pol[args.obstacle_type + '_setup'].add_to_success_buffer(obs, imgs, label, switch)

            obs = []
            imgs = []
            switch = None
            if env.collect_data and pol[args.obstacle_type + '_setup'].prox_data_pointer['pos'] > 20000:
            # if env.collect_data and pol[args.obstacle_type + '_setup'].prox_data_pointer['pos'] > 2500:
                env.collect_data = False
                print("collected data", pol[args.obstacle_type + '_setup'].prox_data_pointer['pos'], pol[args.obstacle_type + '_setup'].prox_data_pointer['neg'])

         
            # if args.plot or (rank == 0 and ep_count % 100 == 0) or label:
            if args.plot or (rank == 0 and (ep_count % 100 == 0 or (ep_count % 10 == 0 and label and not evaluate))):
                num_axes = 4
                fig, axes = plt.subplots(num_axes,    figsize=(10, 3*num_axes))
                axes[0].plot([_ for _ in range(len(vpred_data))], vpred_data, alpha=1.0)    
                
                colours = [np.array([0, 102, 204])/255.0, np.array([0, 102, 204])/255.0, np.array([255,165,0])/255.0, np.array([51, 204, 51])/255.0, np.array([100, 204, 0])/255.0, np.array([200, 100, 51])/255.0]
                
                print(np.array(current_pol_data).shape, len(current_pol_data))
                axes[0].plot([_ for _ in range(len(current_pol_data))], current_pol_data, c=np.array([0, 102, 204])/255.0, alpha=1.0)    
                axes[0].set_ylim([-0.9,len(obstacle_types) + 0.1])
                for s, n in enumerate(obstacle_types):
                    axes[0].text(0,s*0.5+0.5,str(s) + ". " + n)
                axes[0].set_title("Current policy", loc='left')

                axes[1].plot([_ for _ in range(len(baseline_data))], baseline_data, c=np.array([0, 102, 204])/255.0, alpha=1.0)    
                axes[1].set_ylim([-0.9,len(obstacle_types) + 0.1])
                for s, n in enumerate(obstacle_types):
                    axes[1].text(0,s*0.5+0.5,str(s) + ". " + n)
                axes[1].set_title("baseline data", loc='left')

                axes[2].plot([_ for _ in range(len(select_raw_data))], select_raw_data, c=np.array([0, 102, 204])/255.0, alpha=1.0)    
                # axes[2].set_ylim([-0.9, 1.1])
                axes[2].set_title("select data", loc='left')

                axes[3].plot([_ for _ in range(len(terrain_data))], terrain_data, c=np.array([0, 102, 204])/255.0, alpha=1.0)    
                axes[3].set_title("terrain height", loc='left')
                fig.tight_layout(pad=4.0)
                if label:
                    print(PATH + str(rank) + '_success.png')
                    plt.savefig(PATH + str(rank) + '_success.png', bbox_inches='tight')
                else:
                    print(PATH + 'ep.png')
                    plt.savefig(PATH + 'ep.png', bbox_inches='tight')
                plt.close()


            vpred_data = []
            vpred_orig_data = []
            probs_data = []
            terrain_data = []
            current_pol_data = []
            baseline_data = []
            select_raw_data = []
            switch_data = []
            
            ob = env.reset()
            im = env.get_im()

            ep_rets.append(ep_ret)    
            ep_lens.append(ep_len)         
            ep_ret = 0
            ep_len = 0
            setup_ep_rets.append(setup_ep_ret)    
            setup_ep_lens.append(setup_ep_len)         
            setup_ep_ret = 0
            setup_ep_len = 0
            select_ep_rets.append(select_ep_ret)    
            select_ep_lens.append(select_ep_len)         
            select_ep_ret = 0
            select_ep_len = 0
            ep_count += 1    

            box_cross_steps = None
            if env.collect_data:
                next_pol = desired_pol = prev_pol = current_pol = obstacle_types.index(args.obstacle_type)
            else:
                next_pol = desired_pol = prev_pol = current_pol = obstacle_types.index('flat')
            _, exp_vpred, _, _ = pol[args.obstacle_type].step(ob, im, stochastic=False)    
            setup_done = False
            select_done = False
            switched = False
            select_raw = 0
            terminate_setup = False
                 
            obs = []
            imgs = []
            switch = None
            prev_prox = pol[args.obstacle_type + '_setup'].train_model.get_prox(ob, im)
            

if __name__ == '__main__':
    import defaults
    from dotmap import DotMap
    args1, unknown1 = defaults.get_defaults() 
    parser = argparse.ArgumentParser()
    # Arguments that are specific for this run (including run specific defaults, ignore unknown arguments)
    parser.add_argument('--folder', default='dqn5')
    parser.add_argument('--difficulty', default=10, type=int)
    parser.add_argument('--terrain_count', default=2, type=int)
    parser.add_argument('--single_pol', default=True, action='store_false')
    parser.add_argument('--obstacle_type', default='high_jumps')
    parser.add_argument('--adv', default=True, action='store_false')
    parser.add_argument('--dqn', default=True, action='store_false')
    parser.add_argument('--baseline_reward', default=True, action='store_false')
    parser.add_argument('--advantage2', default=False, action='store_true')
    parser.add_argument('--use_classifier', default=False, action='store_true')
    parser.add_argument('--old_rew', default=True, action='store_false')
    parser.add_argument('--eval', default=True, action='store_false')
    parser.add_argument('--eval_first', default=False, action='store_true')
    parser.add_argument('--same_value', default=False, action='store_true')
    parser.add_argument('--train_setup', default=True, action='store_false')
    # parser.add_argument('--train_select', default=False, action='store_true')
    parser.add_argument('--train_prox', default=True, action='store_false')
    parser.add_argument('--train_select', default=True, action='store_false')
    parser.add_argument('--success_explore', default=False, action='store_true')
    parser.add_argument('--blank', default=False, action='store_true')
    parser.add_argument('--categorical', default=False, action='store_true')
    # parser.add_argument('--share_vis', default=False, action='store_true')
    parser.add_argument('--share_vis', default=True, action='store_false')
    parser.add_argument('--no_disturbance', default=False, action='store_true')
    parser.add_argument('--mem_test', default='small')
    parser.add_argument('--rew', default='adv')
    parser.add_argument('--setup_length', default=0, type=int)
    # parser.add_argument('--max_ts', default=1.5e7, type=int)
    parser.add_argument('--max_ts', default=2e7, type=int)
    args2, unknown2 = parser.parse_known_args()
    args2 = vars(args2)
    # Replace any arguments from defaults with run specific defaults
    for key in args2:
        args1[key] = args2[key]
    # Look for any changes to defaults (unknowns) and replace values in args1
    for n, unknown in enumerate(unknown2):
        if "--" in unknown and n < len(unknown2)-1 and "--" not in unknown2[n+1]:
            arg_type = type(args1[unknown[2:]])
            args1[unknown[2:]] = arg_type(unknown2[n+1])
    args = DotMap(args1)
    # Check for dodgy arguments
    unknowns = []
    # print(unknown1)
    # print(unknown2)
    # print(args.same_value)
    for unknown in unknown1 + unknown2:
        if "--" in unknown and unknown[2:] not in args:
            unknowns.append(unknown)
    if len(unknowns) > 0:
        print("Dodgy argument")
        print(unknowns)
        exit()
    # exit()
    os.environ["CUDA_VISIBLE_DEVICES"]="-1"
    run(args)