import os, inspect
from cv2 import data
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
os.environ["TF_CPP_MIN_LOG_LEVEL"] = "3" 
import tensorflow as tf
tf.get_logger().setLevel("DEBUG")
import numpy as np
import argparse
import tensorboardX
from collections import deque   
from scripts.utils import *
from scripts.mpi_utils import sync_from_root
import random
from mpi4py import MPI
import time
from pathlib import Path
home = str(Path.home())
from scripts import logger
import json
import git
from six.moves import shlex_quote
import pybullet as p
from plots import Plots
np.set_printoptions(precision=3, suppress=True)

def run(args):

    PATH = home + "/results/biped_final/latest/" + args.folder + "/" + args.exp + "/"

    comm = MPI.COMM_WORLD
    rank = comm.Get_rank()
    myseed = args.seed + 10000 * rank
    np.random.seed(myseed)
    random.seed(myseed)
    tf.set_random_seed(myseed)
    
    logger.configure(dir=PATH)
    if rank == 0:
        writer = tensorboardX.SummaryWriter(log_dir=PATH)
        repo = git.Repo(search_parent_directories=True)
        sha = repo.head.object.hexsha
        with open(PATH + "commandline_args.txt", "w") as f:
            f.write("Hash:")
            f.write(str(sha) + "\n")
            json.dump(args.__dict__, f, indent=2)
        print('Save git commit and diff to {}/git.txt'.format(PATH))
        cmds = ["echo `git rev-parse HEAD` >> {}".format(
                        shlex_quote(os.path.join(PATH, 'git.txt'))),
                        "git diff >> {}".format(
                        shlex_quote(os.path.join(PATH, 'git.txt')))]
        os.system("\n".join(cmds))
    else: 
        writer = None 

    gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction= 0.1)
    sess = tf.InteractiveSession(config=tf.ConfigProto(inter_op_parallelism_threads=1,
                                            intra_op_parallelism_threads=1,             
                                            gpu_options=gpu_options), graph=None)
    
    if args.robot == "titan":
        horizon = 180
    else:
        horizon = 2048

    if args.old_env or args.run_state in ["run_setup","train_setup","run_switch","train_switch","train_admis","run_admis","multi","train_q"]:
        if args.robot == "titan":
            from assets.env_titan import Env
        else:
            from assets.env_pb_biped_old import Env
    else:
        if args.robot == "titan":
            from assets.env_titan import Env
        else:
            from assets.env_pb_biped import Env

    env = Env(PATH=PATH, args=args)

    if not args.run_state in ["run_single", "train_single"]:
        plots = Plots(PATH, plot_freq=args.plot_freq)
    all_base_obstacle_types = ["high_jumps","jumps","gaps","steps","stairs","flat"]
    all_obstacle_types = all_base_obstacle_types.copy()
    for ob in all_base_obstacle_types:
        if ob is not "flat":
            all_obstacle_types.append(ob + "_setup")
    terrains = all_base_obstacle_types.copy()
    terrains.remove(args.obstacle_type)
    terrains.remove("flat")

    from models.ppo import Model
    if args.run_state in ["run_setup","train_setup","run_switch","train_switch","train_admis","run_admis","test_admis", "train_q"]:
        env.obstacle_types = ["high_jumps"]
        if args.run_state in ["train_setup","train_switch"]:
            obstacle_types = [args.obstacle_type, "flat"]  
            if "setup" in args.run_state:
                pol = Model(args.obstacle_type + "_setup", env=env, ob_size=env.ob_size, ac_size=env.ac_size, im_size=env.im_size, args=args, PATH=PATH, horizon=horizon, writer=writer, max_timesteps=args.max_ts, vis=args.vis, const_std=args.const_std)
            elif "switch" in args.run_state:
                pol = Model(args.obstacle_type + "_switch", env=env, ob_size=env.ob_size, ac_size=env.ac_size, im_size=env.im_size, args=args, PATH=PATH, horizon=horizon, writer=writer, max_timesteps=args.max_ts, vis=args.vis, const_std=args.const_std)
            all_pols = {name:Model(name, env=env, ob_size=env.ob_size, ac_size=env.ac_size, im_size=env.im_size, args=args, PATH=PATH, horizon=horizon, writer=writer, max_timesteps=args.max_ts, vis=args.vis, const_std=args.const_std) for name in obstacle_types}    
        else:
            from models import q 
            if "train_q" in args.run_state:
                args.max_ts = 1.0e7
                pol = q.Model(args.obstacle_type + "_q", env=env, ob_size=env.ob_size, ac_size=1, im_size=env.im_size, vae_im_size=env.im_size, args=args, PATH=PATH, horizon=horizon, writer=writer, max_timesteps=args.max_ts, vis=args.vis, const_std=args.const_std)
            else:
                q_pol = q.Model(args.obstacle_type + "_q", env=env, ob_size=env.ob_size, ac_size=1, im_size=env.im_size, vae_im_size=env.im_size, args=args, PATH=PATH, horizon=horizon, writer=writer, max_timesteps=args.max_ts, vis=args.vis, const_std=args.const_std)
            if "admis" in args.run_state:
                args.max_ts = 1.5e7
                if "roi" in args.tests:
                    # shape needs to be divisible by 4
                    # admis_im_size = [28,28,1]
                    admis_im_size = [32,32,1]
                else:
                    admis_im_size = env.im_size
               
                from models import ppo_ae5
                pol = ppo_ae5.Model(args.obstacle_type + "_admis", env=env, ob_size=env.ob_size, ac_size=1, im_size=env.im_size, vae_im_size=admis_im_size, args=args, PATH=PATH, horizon=horizon, writer=writer, max_timesteps=args.max_ts, vis=args.vis, const_std=args.const_std)
            
            if args.robot == "titan":
                obstacle_types = ["gap_behaviour"]  
            else:
                obstacle_types = [args.obstacle_type, args.obstacle_type + "_setup", "flat"]     
            all_pols = {name:Model(name, env=env, ob_size=env.ob_size, ac_size=env.ac_size, im_size=env.im_size, args=args, PATH=PATH, horizon=horizon, writer=writer, max_timesteps=args.max_ts, vis=args.vis, const_std=args.const_std) for name in obstacle_types}    

    elif args.run_state in ["multi"]:
        env.args.obstacle_type = "mix"
        env.obstacle_types = ["flat","jumps","gaps","stairs","steps","high_jumps"]
        obstacle_types = []
        for ob in env.obstacle_types:
            obstacle_types.append(ob)
            if ob is not "flat":
                obstacle_types.append(ob + "_setup")
        all_pols = {name:Model(name, env=env, ob_size=env.ob_size, ac_size=env.ac_size, im_size=env.im_size, args=args, PATH=PATH, horizon=horizon, writer=writer, max_timesteps=args.max_ts, vis=args.vis, const_std=args.const_std) for name in obstacle_types}    
        # pol = Model("gan", env=env, ob_size=env.ob_size, ac_size=env.ac_size, im_size=env.im_size, args=args, PATH=PATH, horizon=horizon, writer=writer, max_timesteps=args.max_ts, vis=args.vis, const_std=args.const_std)
    else:
        # env.args.difficulty = 1
        # env.args.num_artifacts = 1
        env.obstacle_types = [args.obstacle_type]
        pol = Model(args.obstacle_type, env=env, ob_size=env.ob_size, ac_size=env.ac_size, im_size=env.im_size, args=args, PATH=PATH, horizon=horizon, writer=writer, max_timesteps=args.max_ts, vis=args.vis, const_std=args.const_std)
    
    initialize_uninitialized()
    
    if not args.run_state in ["run_single","run_setup","run_switch","run_admis","multi"]:
        sync_from_root(sess, pol.vars, comm=comm)
        pol.set_training_params(max_timesteps=args.max_ts, learning_rate=args.lr, horizon=horizon)

    if args.run_state in ["run_setup","train_setup","run_switch","train_switch","run_admis","train_admis","multi", "train_q"]:
        for ob in obstacle_types:
            all_pols[ob].load_pol(ob, "./weights/" + ob + "/")
            # if args.run_state in ["multi"]:
                # all_q_pols[ob].load_pol(ob, "./weights/" + ob + "/")
        if args.run_state in ["train_admis"]:
            q_pol.load("./weights/" + args.obstacle_type + "_q/")

    # if args.test_pol:
    if "run_single" in args.run_state:
        if args.hpc:
            pol.load(home + "/hpc-home/results/biped_final/latest/" + args.folder + "/" + args.exp + "/")      
        else:   
            pol.load_pol(args.obstacle_type, "./weights/" + args.obstacle_type + "/")

    # Throw an error if the graph grows (shouldn"t change once everything is initialised)
    tf.get_default_graph().finalize()
    
    prev_done = True

    def pause_training():
        state = {"boxes":env.box_info, 
                 "robot": env.get_robot_state(), 
                 "obstacle_type":args.obstacle_type, 
                 "steps":env.steps,
                 "disturbances": env.args.disturbances,
                 "order": env.order,
                 "box_num": env.box_num,
                 "prev_speed": env.prev_speed,                    
                 "speed": env.speed,                    
                 "original_speed": env.original_speed,                    
                 "prev_time_of_step": env.prev_time_of_step,
                 "time_of_step": env.time_of_step,
                 "step_count": env.step_count
                 }
        env.args.disturbances = False
        return state

    def resume_training(state):
        env.args.obstacle_type = state["obstacle_type"]
        env.args.disturbances = state["disturbances"]
        env.order = state["order"]
        env.box_num = state["box_num"]
        ob = env.reset(box_info=state["boxes"], set_position=state["robot"])
        env.steps = state["steps"]        
        env.prev_speed = state["prev_speed"]
        env.speed = state["speed"]
        env.original_speed = state["original_speed"]
        env.prev_time_of_step = state["prev_time_of_step"]
        env.time_of_step = state["time_of_step"]
        env.step_count = state["step_count"]
        im = env.get_im()
        return ob, im

    def evaluate():
        state = pause_training()
        env.obstacle_types = [env.args.obstacle_type]
    
        # if args.run_state in ["run_setup","train_setup","run_switch","train_switch","multi","run_admis","train_admis"]:
        eval_current_base_pol = eval_next_base_pol = eval_current_pol = "flat"
        eval_ob_type = random.sample(terrains,1)[0]

        # for num, ob_type in enumerate([state["obstacle_type"], eval_ob_type, state["obstacle_type"]]):
        for num, ob_type in enumerate([state["obstacle_type"], eval_ob_type, state["obstacle_type"]]):
            env.args.obstacle_type = ob_type
            env.set_current_pol(eval_current_base_pol)
            eval_ob = env.reset()
            eval_im = env.get_im() 
            env.set_images(eval_im, eval_im)
            admis_count = []
            scaled_recon_losses = []
            recon_losses = []
            plots = Plots(PATH, plot_freq=args.plot_freq)
            if args.collect_data or num == 0:
                eval_pol = False
            else:
                eval_pol = True
            eval_select_output = [0]
            while True:
                if "setup" in eval_current_pol:
                    if args.run_state in ["train_setup"]:
                        eval_act, _, _, _, eval_select_output, _ = pol.step(eval_ob, eval_im, stochastic=False)
                    else:
                        eval_act, _, _, _, eval_select_output, _ = all_pols[eval_current_pol].step(eval_ob, eval_im, stochastic=False)
                    # eval_terminate_setup = eval_select_output[0] > 2
                    eval_terminate_setup = eval_select_output[0] > 3
                else:
                    eval_act, _, _, _ = all_pols[eval_current_pol].step(eval_ob, eval_im, stochastic=False)
                if args.run_state in ["train_q"]:
                    eval_adm, _, qs = pol.step(eval_ob, eval_im, stochastic=False)
                else:
                    eval_adm, _, qs = q_pol.step(eval_ob, eval_im, stochastic=False)
                    # eval_adm, _, eval_recon_im, eval_q_err_loss, eval_q_loss, qs, 
                    eval_thres, _, _, eval_adm, eval_recon_im, eval_q_pred, eval_q_pred_loss, eval_recon_loss = pol.step(eval_ob, eval_im, stochastic=False, eval_pol=eval_pol, qs=qs)

                # if args.discrete:
                eval_admissible = eval_adm[0] == 1
                # else:
                #     eval_admissible = eval_adm[0] > 0.5

                if args.run_state not in ["train_q"]:
                    if ob_type == state["obstacle_type"] and eval_admissible:
                        pol.train_model.recon_error_buffer.append(eval_recon_loss)
                        pol.train_model.q_error_buffer.append(eval_q_pred_loss)
    
                    recon_losses.append(eval_recon_loss)

                admis_count.append(eval_admissible)
                # scaled_recon_losses.append(eval_scaled_recon_loss)
                torques = eval_act
                eval_next_ob, _, eval_done, _ = env.step(torques)
                eval_next_im = env.get_im()
                if args.run_state not in ["train_q"]:
                    # print(eval_next_im.shape, eval_recon_im.shape); exit()
                    env.set_images(eval_next_im, eval_recon_im[0])

                if eval_admissible:
                    if "flat" in eval_current_pol:
                        eval_current_pol = state["obstacle_type"] + "_setup"
                    elif "setup" in eval_current_pol and eval_terminate_setup:
                        eval_current_pol = state["obstacle_type"]
                else:
                    # if num != 1:
                    #     ood_data_im.append(eval_next_im)
                    eval_current_pol = "flat"
                env.set_current_pol(eval_current_base_pol)
                if args.run_state in ["train_q"]:
                    plots.update({"current_pol":obstacle_types.index(eval_current_pol),
                            "terrain":{"z":env.box_info[1][env.box_num][2] + env.box_info[2][env.box_num][2],"robot_z":env.body_xyz[2]},"qvalue":{"flat":qs[0],"terrain":qs[1]}})
                else:
                    plots.update({"current_pol":obstacle_types.index(eval_current_pol),
                            "terrain":{"z":env.box_info[1][env.box_num][2] + env.box_info[2][env.box_num][2],"robot_z":env.body_xyz[2]},"qvalue":{"flat":qs[0],"terrain":qs[1],"flat pred":eval_q_pred[0],"terrain pred":eval_q_pred[1]},
                            "recon_loss":{"recon_loss":eval_recon_loss,"recon_thres":eval_thres[0]}, 
                            "q_loss_pred":{"eval_q_pred_loss":eval_q_pred_loss,"eval_q_pred_thres":eval_thres[1]}})

                if eval_done: 
                    if ob_type != state["obstacle_type"]:
                        env.save_sim_data(tag="eval_" + ob_type, force=True)
                    plots.plot(ep_count=0, tag="_eval_" + str(0) + "_" + ob_type + "_")
                    break
                eval_ob = eval_next_ob
                eval_im = eval_next_im
            if num == 0:
                if args.run_state not in ["train_q"]:
                    pol.train_model.update_err_thres()
                    if rank == 0:
                        print("updated err thres: ", pol.train_model.recon_thres, "q thres: ", pol.train_model.q_thres)
            
            all_eval_success = MPI.COMM_WORLD.allgather(env.get_success())
            all_eval_outputs = MPI.COMM_WORLD.allgather(np.mean(admis_count))
            all_terrains = MPI.COMM_WORLD.allgather(ob_type)
            all_recon_losses = MPI.COMM_WORLD.allgather(np.mean(recon_losses))
            if rank == 0:
                print("eval outputs")
                print(np.array(all_eval_outputs))
                print("eval terrain")
                print(all_terrains)
                print("eval recon losses")
                print(np.array(all_recon_losses))
                if num == 2:
                    writer.add_scalar("Eval", np.mean(all_eval_outputs), pol.iters_so_far)        
                    writer.add_scalar("Eval success", np.mean(all_eval_success), pol.iters_so_far)        
                elif num == 1:
                    writer.add_scalar("Eval out of class", np.mean(all_eval_outputs), pol.iters_so_far)        
        
        ob, im = resume_training(state)
        return ob, im

    if args.run_state in ["run_setup","train_setup","run_switch","train_switch","multi","run_admis","train_admis","train_q"]:
        current_base_pol = next_base_pol = current_pol = "flat"
    env.set_current_pol(current_pol)
    ob = env.reset()
    im = env.get_im()
    # im = 1.0 - im 

    evaluate()
    
    if args.run_state in ["run_admis", "train_admis"]:  
        env.set_images(im, im)
    ep_ret = 0
    ep_len = 0
    ep_rets = []
    ep_lens = []
    ep_steps = 0
    ep_count = 0

    if "run" in args.run_state:
        stochastic = False
    else:
        stochastic = True
    select_output = adm = [0]   

    # prev_artifact = artifact = None
    box_cross_steps = None
    t1 = time.time()
    while True: 
        if not args.run_state in ["run_single","run_setup","run_switch","run_admis","multi"]:
            if pol.timesteps_so_far > pol.max_timesteps:
                break 
        # print(current_pol)
        if args.run_state in ["run_setup","train_setup","run_switch","train_switch","multi","run_admis","train_admis","train_q"]:
            if "setup" in current_pol:
                if args.run_state in ["train_setup"]:
                    act, vpred, _, nlogp, select_output, nlogp_select = pol.step(ob, im, stochastic=True)
                else:
                    act, vpred, _, nlogp, select_output, _ = all_pols[current_pol].step(ob, im, stochastic=False)
                terminate_setup = select_output[0] > 3
                # terminate_setup = select_output[0] > 2
                # print(select_output[0])
            else:
                act, vpred, _, nlogp = all_pols[current_pol].step(ob, im, stochastic=False)
        
        if args.run_state in ["run_admis", "train_admis", "train_q"]:
            # else:
            #     adm, vpred, _, nlogp = pol.step(ob, im, stochastic=stochastic)
            # admissible = adm[0] > 3
            if args.run_state in ["train_q"]:
                adm, vpred, qs = pol.step(ob, im, stochastic=True)
            else:
                adm, vpred, qs = q_pol.step(ob, im, stochastic=False)

            if args.run_state in ["run_admis", "train_admis"]:
                # adm, vpred, recon_im, q_err, q, qs, 
                # adm, recon_im, q_pred, q_pred_loss, recon_loss = pol.step(ob, im, stochastic=stochastic, qs=qs)
                act_thres, vpred, nlogp, adm, recon_im, q_pred, q_pred_loss, recon_loss = pol.step(ob, im, stochastic=stochastic, qs=qs)
                pol.train_model.recon_error_buffer.append(recon_loss)
                pol.train_model.q_error_buffer.append(q_pred_loss)

                # print("set admiss criteria")
                # if ep_count == 0:
                #     admissible = True
                # else:
                #     if args.discrete:
                #         admissible = adm[0] == 1
            admissible = adm[0] == 1

            # if args.run_state in ["run_admis", "train_admis"] and admissible:
            #     pol.train_model.recon_error_buffer.append(recon_loss)
                # pol.train_model.q_error_buffer.append(q_pred_loss)

        elif args.run_state in ["run_single","train_single"]:
            act, vpred, _, nlogp = pol.step(ob, im, stochastic=stochastic)

        # print(act, current_pol)
        torques = act

        next_ob, rew, done, _ = env.step(torques)
        next_im = env.get_im()
        # next_im = 1.0 - next_im 

        if args.run_state in ["run_admis", "train_admis"]:
            env.set_images(next_im, recon_im)

        # ========================================================================================================
        # Terrain detection stuff: TODO clean up!!
        # ========================================================================================================
        if args.run_state in ["train_admis","train_q"]:
            if admissible:
                if "flat" in current_pol:
                    current_pol = args.obstacle_type + "_setup"
                elif "setup" in current_pol and terminate_setup:
                    current_pol = args.obstacle_type
            else:
                current_pol = "flat"
                
        elif args.run_state in ["run_setup","train_setup","run_switch","train_switch","run_admis","train_admis","multi","train_q"]:
            # if artifact is None and env.order[env.box_num] != "flat":
                # prev_artifact = artifact = env.order[env.box_num]
                # prev_artifact = artifact = next_pol = env.get_next_pol()
            if args.run_state in ["run_admis","train_admis","train_q"]:
                if admissible:
                    next_base_pol = args.obstacle_type
                else:
                    next_base_pol = "flat"
            else:
                next_base_pol = env.get_terrain()

            # if (current_pol == "high_jumps" and env.order[env.box_num] == "flat") or (current_pol != "high_jumps" and env.order[env.box_num] != "flat"):
            #     artifact = env.order[env.box_num]

            # if artifact is not None:
            #     admis_current_pol = artifact
            # else:
            #     admis_current_pol = "flat"
            # current_base_pol = admis_current_pol
            if next_base_pol != current_base_pol and "setup" not in current_pol:
            # if current_base_pol not in current_pol and "setup" not in current_pol:
                if (current_pol == "high_jumps" and env.x_min > (env.box_info[1][env.box_num][0] - env.box_info[2][env.box_num][0]) and (not env.ob_dict["left_foot_left_ground"] and not env.ob_dict["right_foot_left_ground"]) and env.vx > 0) or (current_pol != "high_jumps"):
                    if "flat" in next_base_pol:
                        current_pol = current_base_pol = next_base_pol
                    else:
                        current_pol = current_base_pol = next_base_pol + "_setup"
                    terminate_setup = False
                    box_cross_steps = env.steps
            elif "setup" in current_pol and (terminate_setup or (box_cross_steps and env.steps - box_cross_steps > 75)):
                # print("current_pol ", current_pol, " timeout", env.steps - box_cross_steps > 75, "terminate", terminate_setup)
                current_pol = current_base_pol = next_base_pol
                box_cross_steps = None

        # ========================================================================================================
        # This is necessary to set the desired speed, new policies will fix this
        if args.old_env and args.multi:
            if 'high_jumps' in current_pol:
                env.obstacle_type = 'high_jumps'
            else:
                env.obstacle_type = 'mix'
                env.original_speed = 1.0
        # ========================================================================================================

            # if args.no_setup and "setup" in current_pol:
            #     current_pol = current_base_pol
            # if args.no_flat_setup and current_pol == "flat_setup":
            #     current_pol = "flat"

            # next_pol = env.get_next_pol()
            # # if current_base_pol not in current_pol and "setup" not in current_pol:

            # if next_pol not in current_pol:
            #     current_pol = next_pol + "_setup"
            # elif "setup" in current_pol and terminate_setup:nd "setup" in current_pol:
            #     current_pol = current_base_pol
            # if args.no_flat_setup and cu  rrent_pol == "flat_setup":
            #     current_pol = 
            #     current_pol = next_pol
            # print(current_pol)
        # ========================================================================================================

        if not args.run_state in ["run_single","run_setup","run_switch","run_admis","multi"]:
            if args.run_state in ["train_setup"]:
                pol.add_to_buffer([ob, im, act, select_output, setup_rew, prev_setup_done, setup_vpred, nlogp, nlogp_select])
            elif args.run_state in ["train_admis","train_q"]:
                # if "rew" in args.tests:
                #     rew = rew
                # else:
                if env.get_terrain("current") == args.obstacle_type or (env.get_terrain("previous") == args.obstacle_type and env.get_terrain("current") == "flat" and env.x_min > (env.box_info[1][env.box_num][0] - env.box_info[2][env.box_num][0]) and (not env.ob_dict["right_foot_left_ground"] and not env.ob_dict["left_foot_left_ground"])):
                    rew = 1.0 - 0.4*(np.clip((admissible - 1.0)**2, 0, 1)) 
                else:
                    rew = 1.0 - 0.4*(np.clip((admissible - 0.0)**2, 0, 1))
                if args.run_state in ["train_q"]:
                    pol.add_to_buffer([ob, im, adm, rew, prev_done, vpred])
                else:

                    # rew = admissible - 0.02*sum(act_thres**2)
                    # if admissible:
                    #     rew = 1.0 - 0.01*np.sum((act_thres - np.clip(np.array([recon_loss[0] + 5.0, q_pred_loss + 10.0]), 0, 10))**2)
                    # else:
                    #     rew = 1.0 - 0.01*np.sum((act_thres)**2)
                    rew = 1
                    # print(rew)
                    # rew = 1.0 - np.clip(0.01*np.sum((act_thres - np.array([recon_loss, q_pred_loss]))**2), 0, 1)[0]
                    # rew = 1.0 - np.clip(0.01*np.sum((act_thres - np.array([10, 10]))**2), 0, 1)
                    # rew = 1.0 - 0.01*np.sum((act_thres - np.array([10, 10]))**2)
                    # print(rew, act_thres)
                    # print(np.sum((act_thres - np.array([recon_loss, q_pred_loss]))**2).shape, rew.shape)
                    if admissible:
                        pol.add_to_buffer([ob, im, act_thres, rew, prev_done, vpred, nlogp, im, qs])
                    else:
                        pol.add_to_buffer([ob, im, act_thres, rew, prev_done, vpred, nlogp, np.zeros(env.im_size), [0.0,0.0]])
            else:
                pol.add_to_buffer([ob, im, act, rew, prev_done, vpred, nlogp])
        prev_done = done
        ob = next_ob
        im = next_im
        ep_ret += rew
        ep_len += 1
        ep_steps += 1
        if not args.run_state in ["run_single","run_setup","run_switch","run_admis","multi"] and ep_steps % horizon == 0:
            if args.run_state in ["run_admis", "train_admis"]:
                _, vpred, _, _, _, _, _,_ = pol.step(next_ob, next_im, stochastic=True, qs=qs)
            elif args.run_state in ["train_q"]:
                _, vpred, _  = pol.step(next_ob, next_im, stochastic=True)
            else:
                _, vpred, _, _ = pol.step(next_ob, next_im, stochastic=True)
            # if rank == 0: print("episode time", time.time() - t1)
            pol.run_train(data={"ep_rets":ep_rets, "ep_lens":ep_lens}, last_value=vpred, last_done=done)
            t1 = time.time()
            ep_rets = []
            ep_lens = []
            
            # pol.train_model.update_err_thres()
            # if rank == 0:
            #     print("updated err thres: ", pol.train_model.recon_thres, "q thres: ", pol.train_model.q_thres)

            if pol.iters_so_far % args.plot_freq == 0:
                ob, im = evaluate()

        # plots.update({"current_pol": obstacle_types.index(current_pol), "terrain": terrain})
        if rank == 0 and not args.run_state in ["run_single", "train_single"]:
            if args.run_state in ["train_q"]:
                plots.update({"current_pol":obstacle_types.index(current_pol),
                            "terrain":{"z":env.box_info[1][env.box_num][2] + env.box_info[2][env.box_num][2],"robot_z":env.body_xyz[2]}, 
                            "qvalue":{"flat":qs[0],"terrain":qs[1]}})
            else:
                plots.update({"current_pol":obstacle_types.index(current_pol),
                            "terrain":{"z":env.box_info[1][env.box_num][2] + env.box_info[2][env.box_num][2],"robot_z":env.body_xyz[2]}, 
                            "qvalue":{"flat":qs[0],"terrain":qs[1],"flat pred":q_pred[0],"terrain pred":q_pred[1]}, "recon_loss":{"recon_loss":recon_loss, "thres":act_thres[0]},
                            "q_pred_loss":{"q_pred_loss": q_pred_loss, "thres":act_thres[1]}})
                            # {"recon_thres":pol.train_model.recon_thres,"q_thres":pol.train_model.q_thres})
                            # {"recon_thres":act_thres[0],"q_thres":act_thres[1]})

        env.set_current_pol(current_pol)

        if done:
            if not args.run_state in ["run_single", "train_single"] and rank == 0:
                plots.plot(ep_count)

            if args.run_state in ["run_setup","train_setup","run_switch","train_switch","multi","run_admis","train_admis","train_q"]:
                current_base_pol = next_base_pol = current_pol = "flat"
            env.set_current_pol(current_pol)
            ob = env.reset()
            im = env.get_im()
            # im = 1.0 - im 

            ep_rets.append(ep_ret)  
            ep_lens.append(ep_len)     
            ep_ret = 0
            ep_len = 0
            ep_count += 1        
            # prev_artifact = artifact = None
            box_cross_steps = None
            select_output = adm = [0]

if __name__ == "__main__":
    # Hack for loading defaults, but still accepting run specific defaults
    import defaults
    from dotmap import DotMap
    args1, unknown1 = defaults.get_defaults() 
    parser = argparse.ArgumentParser()
    # Arguments that are specific for this run (including run specific defaults, ignore unknown arguments)
    parser.add_argument("--folder", default="b")
    parser.add_argument("--difficulty", default=10, type=int)
    parser.add_argument("--latent_dim", default=16, type=int)
    parser.add_argument("--hid_size", default=256, type=int)
    parser.add_argument("--init_value", default=0.5, type=float)
    parser.add_argument("--enc", default=False, action="store_true")
    parser.add_argument("--batch_norm", default=True, action="store_false")
    parser.add_argument("--ae_type", default="ae")
    parser.add_argument("--conv", default=False, action="store_true")
    parser.add_argument("--obstacle_type", default="gaps", help="Obstacle types are: flat, gaps, jumps, stairs, steps, high_jumps, one_leg_hop, hard_steps, hard_high_jumps, or mix (for a combination)")
    parser.add_argument("--sim_type", default="pb", help="pb or gz (pybullet or gazebo)")
    parser.add_argument("--robot", default="biped", help="biped or titan, todo: hexapod")
    parser.add_argument("--alg", default="ppo", help="ppo, eventually sac as well") 
    parser.add_argument("--setup", default=False, action="store_true")
    parser.add_argument("--no_flat_setup", default=True, action="store_false")
    parser.add_argument("--advantage2", default=False, action="store_true")
    parser.add_argument("--collect_data", default=False, action="store_true")
    parser.add_argument("--multi", default=False, action="store_true")
    parser.add_argument("--admis_type", default="RL")
    parser.add_argument("--gan_type", default="vae_gan")
    parser.add_argument("--share_vis", default=True, action="store_false")
    parser.add_argument("--train_vae", default=True, action="store_false")
    # parser.add_argument("--discrete", default=False, action="store_true")
    parser.add_argument("--discrete", default=True, action="store_false")
    parser.add_argument("--debug", default=True, action="store_false")
    parser.add_argument("--run_state", default="train_single", help="train_single, run_single, train_setup, run_setup, train_switch, run_switch, train_admis, run_admis, multi")
    parser.add_argument("--lam", default=1.0, type=float)
    parser.add_argument("--recon_scale", default=80.0, type=float)
    parser.add_argument("--old_env", default=False, action="store_true")
    parser.add_argument("--num_artifacts", default=1, type=int)
    parser.add_argument('--stacked', default=5, type=int)
    parser.add_argument("--tests", default="orig")
    parser.add_argument("--ent_coef", default=0.0, type=float)
    parser.add_argument("--std_from_state", default=False, action="store_true")
    # parser.add_argument("--std_from_state", default=True, action="store_false")
    parser.add_argument("--plot_freq", default=20, type=int)
    args2, unknown2 = parser.parse_known_args()
    args2 = vars(args2)
    # Replace any arguments from defaults with run specific defaults
    for key in args2:
        args1[key] = args2[key]
    # Look for any changes to defaults (unknowns) and replace values in args1
    for n, unknown in enumerate(unknown2):
        if "--" in unknown and n < len(unknown2)-1 and "--" not in unknown2[n+1]:
            arg_type = type(args1[unknown[2:]])
            args1[unknown[2:]] = arg_type(unknown2[n+1])
    args = DotMap(args1)
    # Check for dodgy arguments
    unknowns = []
    for unknown in unknown1 + unknown2:
        if "--" in unknown and unknown[2:] not in args:
            unknowns.append(unknown)
    if len(unknowns) > 0:
        print("Dodgy argument")
        print(unknowns)
        exit()

    os.environ["CUDA_VISIBLE_DEVICES"]="-1"
    run(args)
