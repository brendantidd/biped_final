from models.contrast import Model

from tensorflow import keras
import numpy as np
import cv2
import tensorboardX
import time
np.set_printoptions(precision=3, suppress=True)
import random

def multivariate_pdf(vector, mean, cov):
    quadratic_form = np.dot(np.dot(vector-mean,np.linalg.inv(cov)),np.transpose(vector-mean))
    return np.exp(-.5 * quadratic_form)/ (2*np.pi * np.linalg.det(cov))

def plot_sample(data, n=30, figsize=15):
    # display a n*n 2D manifold of digits
    digit_size = 28
    # scale = 1.0
    scale = int(np.sqrt(len(data)))
    n = int(np.sqrt(len(data)))
    
    figure = np.zeros((digit_size * n, digit_size * n))
    # linearly spaced coordinates corresponding to the 2D plot
    # of digit classes in the latent space
    grid_x = np.linspace(-scale, scale, n)
    grid_y = np.linspace(-scale, scale, n)[::-1]

    for i, yi in enumerate(grid_y):
        for j, xi in enumerate(grid_x):
            z_sample = np.array([[xi, yi]])
            # x_decoded = vae.decoder.predict(z_sample)
            # x_decoded = vae.decode_from_ph(z_sample)
            x_decoded = data[i + n*j, ::]
            digit = x_decoded.reshape(digit_size, digit_size)
            figure[
                i * digit_size : (i + 1) * digit_size,
                j * digit_size : (j + 1) * digit_size,
            ] = digit

    plt.figure(figsize=(figsize, figsize))
    start_range = digit_size // 2
    end_range = n * digit_size + start_range
    pixel_range = np.arange(start_range, end_range, digit_size)
    sample_range_x = np.round(grid_x, 1)
    sample_range_y = np.round(grid_y, 1)
    plt.xticks(pixel_range, sample_range_x)
    plt.yticks(pixel_range, sample_range_y)
    plt.xlabel("z[0]")
    plt.ylabel("z[1]")
    plt.imshow(figure, cmap="Greys_r")
    plt.show()

def plot_sample2(vae, data, n=30, figsize=15):
    # display a n*n 2D manifold of digits
    digit_size = 28
    # scale = 1.0
    scale = int(np.sqrt(len(data)))
    n = int(np.sqrt(len(data)))
    
    figure = np.zeros((digit_size * n, digit_size * n * 2))
    # linearly spaced coordinates corresponding to the 2D plot
    # of digit classes in the latent space
    grid_x = np.linspace(-scale, scale, n)
    grid_y = np.linspace(-scale, scale, n)[::-1]

    for i, yi in enumerate(grid_y):
        for j, xi in enumerate(grid_x):
            z_sample = np.array([[xi, yi]])
            # x_decoded = vae.decoder.predict(z_sample)
            # x_decoded = vae.decode_from_ph(z_sample)
            x_decoded = data[i + n*j, ::]
            digit = x_decoded.reshape(digit_size, digit_size)
            figure[
                i * digit_size : (i + 1) * digit_size,
                j * digit_size : (j + 1) * digit_size,
            ] = digit
    data = vae.train_model.decode(data)
    for i, yi in enumerate(grid_y):
        for j, xi in enumerate(grid_x):
            z_sample = np.array([[xi, yi]])
            # x_decoded = vae.decoder.predict(z_sample)
            # x_decoded = vae.decode_from_ph(z_sample)
            x_decoded = data[i + n*j, ::]
            digit = x_decoded.reshape(digit_size, digit_size)
            figure[
                i * digit_size : (i + 1) * digit_size,
                j * digit_size + digit_size*n: (j + 1) * digit_size + digit_size*n,
            ] = digit

    plt.figure(figsize=(figsize, figsize * 2))
    start_range = digit_size // 2
    end_range = n * digit_size + start_range
    pixel_range = np.arange(start_range, end_range, digit_size)
    sample_range_x = np.round(grid_x, 1)
    sample_range_y = np.round(grid_y, 1)
    plt.xticks(pixel_range, sample_range_x)
    plt.yticks(pixel_range, sample_range_y)
    plt.xlabel("z[0]")
    plt.ylabel("z[1]")
    plt.imshow(figure, cmap="Greys_r")
    plt.show()

def plot_latent_space2(vae):
    figsize=15
    digit_size = 28
    scale = 3.0
    n = 10
    figure = np.zeros((digit_size * n, digit_size * n*2))
    grid_x = np.linspace(-scale, scale, n)
    grid_y = np.linspace(-scale, scale, n)[::-1]
    # sample_z = np.array([(i,j) for i in range(-3,3) for j in range(-3,3)])
    sample_z = np.zeros([n*n, args.latent_dim])
    # sample_z = np.array([(i,j) for i in grid_x for j in grid_y])
    sample_zs = np.array([(i,j) for i in grid_x for j in grid_y])
    sample_z[0:n*n,0:2] = np.array([(i,j) for i in grid_x for j in grid_y])
    samples = vae.decode_from_z(sample_z)
    enc_samples = vae.train_model.decode(samples)
    for count, (zs, x_decoded) in enumerate(zip(sample_z, samples)):
        i = count%n
        j = count//n
        # print(col, row, x_decoded.shape)
        digit = x_decoded.reshape(digit_size, digit_size)
        figure[
                i * digit_size : (i + 1) * digit_size,
                j * digit_size : (j + 1) * digit_size,
            ] = digit
    for count, (zs, x_decoded) in enumerate(zip(sample_z, enc_samples)):
        i = count%n
        j = count//n
        # print(col, row, x_decoded.shape)
        digit = x_decoded.reshape(digit_size, digit_size)
        figure[
                i * digit_size : (i + 1) * digit_size,
                j * digit_size + digit_size*n: (j + 1) * digit_size + digit_size*n,
            ] = digit
            
    plt.figure(figsize=(figsize, figsize*2))
    # start_range = digit_size // 2
    # end_range = n * digit_size + start_range
    # pixel_range = np.arange(start_range, end_range, digit_size)
    # sample_range_x = np.round(grid_x, 1)
    # sample_range_y = np.round(grid_y, 1)
    # plt.xticks(pixel_range, sample_range_x)
    # plt.yticks(pixel_range, sample_range_y)
    # plt.xlabel("z[0]")
    # plt.ylabel("z[1]")
    plt.imshow(figure, cmap="Greys_r")
    plt.show()
    return figure

def plot_latent_space(vae, n=30, figsize=15):
    # display a n*n 2D manifold of digits
    digit_size = 28
    # scale = 1.0
    # scale = 6.0
    scale = 3.0
    # scale = 10.0
    figure = np.zeros((digit_size * n, digit_size * n))
    # linearly spaced coordinates corresponding to the 2D plot
    # of digit classes in the latent space
    grid_x = np.linspace(-scale, scale, n)
    grid_y = np.linspace(-scale, scale, n)[::-1]

    for i, yi in enumerate(grid_y):
        for j, xi in enumerate(grid_x):
            z_sample = np.array([0]*args.latent_dim).reshape([-1,args.latent_dim])
            z_sample[0][0] = xi
            z_sample[0][1] = yi
            # x_decoded = vae.decoder.predict(z_sample)
            x_decoded = vae.decode_from_z(z_sample)
            
            digit = x_decoded[0].reshape(digit_size, digit_size)
            figure[
                i * digit_size : (i + 1) * digit_size,
                j * digit_size : (j + 1) * digit_size,
            ] = digit

    plt.figure(figsize=(figsize, figsize))
    start_range = digit_size // 2
    end_range = n * digit_size + start_range
    pixel_range = np.arange(start_range, end_range, digit_size)
    sample_range_x = np.round(grid_x, 1)
    sample_range_y = np.round(grid_y, 1)
    plt.xticks(pixel_range, sample_range_x)
    plt.yticks(pixel_range, sample_range_y)
    plt.xlabel("z[0]")
    plt.ylabel("z[1]")
    plt.imshow(figure, cmap="Greys_r")
    plt.show()
    return figure

def plot_mse(query, mse, n=30, figsize=15):
    # display a n*n 2D manifold of digits
    digit_size = 28
    # scale = 1.0
    # scale = 2.0
    # scale = 10.0
    scale = int(np.sqrt(len(mse)))

    # print(scale, len(mse))
    n = int(np.sqrt(len(mse)))
    figure = np.zeros((digit_size * n, digit_size * n,3))
    # linearly spaced coordinates corresponding to the 2D plot
    # of digit classes in the latent space
    grid_x = np.linspace(-scale, scale, n)
    grid_y = np.linspace(-scale, scale, n)[::-1]

    # for i, yi in enumerate(grid_y):
    #     for j, xi in enumerate(grid_x):
    min_mse = min(mse)
    max_mse = max(mse)
    for i in range(scale):
        for j in range(scale):
            # z_sample = np.array([[xi, yi]])
            # x_decoded = vae.decoder.predict(z_sample)
            # x_decoded = vae.decode_from_ph(z_sample)
            # digit = x_decoded[0].reshape(digit_size, digit_size)
            # print(i,j, i*scale + j, len(query))
            # digit = query[int(i*scale+j)].reshape(digit_size, digit_size)
            digit = query[int(j*scale+i)].reshape(digit_size, digit_size)
            digit = cv2.cvtColor(digit, cv2.COLOR_GRAY2RGB)
            digit[:,:,0] = (mse[int(j*scale+i)] - min_mse) / (max_mse - min_mse)

            # digit 
            # cv2.
            # mse = np.mean((digit - query)**2)
            # mse.
            # v2.putText(digit, str(mse), (0,0), font, 1, (0, 255, 0), 2, cv2.LINE_AA)
            # print(mse)
            # *(digit - query)
            figure[
                i * digit_size : (i + 1) * digit_size,
                j * digit_size : (j + 1) * digit_size, :
            ] = digit


    plt.figure(figsize=(figsize, figsize))
    # start_range = digit_size // 2
    # end_range = n * digit_size + start_range
    # pixel_range = np.arange(start_range, end_range, digit_size)
    # sample_range_x = np.round(grid_x, 1)
    # sample_range_y = np.round(grid_y, 1)
    # plt.xticks(pixel_range, sample_range_x)
    # plt.yticks(pixel_range, sample_range_y)
    # plt.xlabel("z[0]")
    # plt.ylabel("z[1]")
    plt.imshow(figure, cmap="Greys_r")
    plt.show()
    return figure

def plot_figure(figure):
    plt.imshow(figure, cmap="Greys_r")
    plt.show()

def plot_label_clusters(vae, data, labels):
    # display a 2D plot of the digit classes in the latent space
    # z_mean, _, _ = vae.encoder.predict(data)
    z_mean = vae.train_model.encode(data[:1000,::].reshape(-1,28,28,1)) 
    if args.latent_dim > 2:
        fig = plt.figure()
        ax = Axes3D(fig)
        ax.scatter(z_mean[:, 0], z_mean[:, 1], z_mean[:, 2], c=labels[:1000])
        # plt.xlim([-5,5])
        # plt.ylim([-5,5])
        # plt.zlim([-5,5])
        # ax.set_zlim(-5,5)
        # plt.colorbar()
        plt.xlabel("z[0]")
        plt.ylabel("z[1]")
        # plt.zlabel("z[2]")
        plt.show()
    else:
        # z_mean = vae.encode(data.reshape(-1,28,28,1)) 
        plt.figure(figsize=(12, 10))
        plt.scatter(z_mean[:, 0], z_mean[:, 1], c=labels)
        # plt.xlim([-5,5])
        # plt.ylim([-5,5])
        plt.colorbar()
        plt.xlabel("z[0]")
        plt.ylabel("z[1]")
        plt.show()

def plot_errors(x_target_err, x_not_target_err, width):
    # z_mean = vae.encode(data.reshape(-1,28,28,1)) 
    plt.figure(figsize=(12, 10))
    xs = []
    ys = []
    # print(x_target_err)
    # print(x_not_target_err)

    for x in x_target_err:
        xs.append(x)
        ys.append(x_target_err[x])
    plt.bar(xs, ys, width=width, color=[0.4,0.2,0.8,0.5] )
    xs = []
    ys = []
    for x in x_not_target_err:
        xs.append(x)
        ys.append(x_not_target_err[x])
    plt.bar(xs, ys, width=width, color=[0.8,0.2,0.4,0.5])

    # plt.xlim([-5,5])
    # plt.ylim([-5,5])
    # plt.colorbar()
    # plt.xlabel("z[0]")
    # plt.ylabel("z[1]")
    plt.show()

def image_ultererer(imgs):
    h, w = 28, 28
    center = (w // 2, h // 2)
    rhos = np.random.uniform(-1.5,1.5,size=[imgs.shape[0], 3])
    new_imgs = []
    for img, rho in zip(imgs, rhos):
        M = cv2.getRotationMatrix2D(center, rho[2]*180/np.pi, 1.0)
        new_img = cv2.warpAffine(img, M, (w, h))
        # M = np.float32([[np.cos(rho[2]), -np.sin(rho[2]), center[0] + 0], [np.sin(rho[2]), np.cos(rho[2]), center[1] + 0]])
        M = np.float32([[1, 0, rho[0]*2], [0, 1, rho[1]*2]])
        new_img = cv2.warpAffine(new_img, M, (w, h))
        new_imgs.append(new_img)
    new_imgs = np.array(new_imgs).astype("float32").reshape([-1,28,28,1])
    # print(imgs.shape,  new_imgs.shape)
    # display_images(imgs, new_imgs, lag=200); exit()
    return np.array(new_imgs), rhos

if __name__=="__main__":
    # As much of this code is from https://github.com/keras-team/keras-io/blob/master/examples/generative/vae.py as possible
    # (adapted to our openai's mpiadam, and tf1.14)
    import matplotlib.pyplot as plt
    # matplotlib.use('Agg')
    import matplotlib
    # matplotlib.use('TkAgg')
    matplotlib.use('QT4Agg')
    from mpl_toolkits.mplot3d import Axes3D
    from scripts.utils import *
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("--train", default=False, action="store_true")
    parser.add_argument("--train2", default=False, action="store_true")
    parser.add_argument("--batch_norm", default=True, action="store_false")
    # parser.add_argument("--mse", default=False, action="store_true")
    parser.add_argument("--mse", default=True, action="store_false")
    parser.add_argument("--discrete", default=True, action="store_false")
    parser.add_argument("--enc", default=False, action="store_true")
    parser.add_argument("--conv", default=False, action="store_true")
    parser.add_argument("--ae_type", default="ae")
    parser.add_argument("--robot", default="titan")
    parser.add_argument("--const_std", default=False, action="store_true")
    parser.add_argument("--ensemble", default=False, action="store_true")
    parser.add_argument("--std_from_state", default=False, action="store_true")
    parser.add_argument("--train_on_test", default=False, action="store_true")
    parser.add_argument("--lam", default=0.0, type=float)
    parser.add_argument("--ent_coef", default=0.0, type=float)
    parser.add_argument("--init_value", default=0.5, type=float)
    parser.add_argument("--latent_dim", default=16, type=int)
    parser.add_argument("--epochs", default=10, type=int)
    parser.add_argument("--hid_size", default=128, type=int)
    parser.add_argument("--target", default=0, type=int)
    parser.add_argument("--exp", default="test")
    parser.add_argument("--tests", default="test")
    parser.add_argument("--folder", default="gan")
    args = parser.parse_args()

    myseed = 42
    np.random.seed(myseed)
    tf.set_random_seed(myseed)
    random.seed(myseed)

    target = args.target
    train = args.train

    gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction= 0.5)
    sess = tf.InteractiveSession(config=tf.ConfigProto(inter_op_parallelism_threads=1,
                                            intra_op_parallelism_threads=1,             
                                            gpu_options=gpu_options), graph=None)

    (x_train, y_train), (x_test, y_test) = keras.datasets.mnist.load_data()
    # x_train, x_test = x_train[:30000,::], x_test[:2000,::]
    # x_train = x_train[:30000,::]
    # y_train = y_train[:30000]
    # print(np.where(y_train == 0)[:100])
    # print(y_train[:100])
    if args.target == 11:
        x_train = x_train
        y_train = y_train
        x_test_target = x_test
        y_test_target = y_test
    else:
        x_train = x_train[ np.where(y_train == target),::].reshape(-1,28,28)
        y_train = y_train[ np.where(y_train == target)]
        x_test_target = x_test[ np.where(y_test == target),::].reshape(-1,28,28)
        y_test_target = y_test[ np.where(y_test == target)]
        
    if args.train_on_test:
        x_train = np.concatenate([x_train, x_test_target], axis=0)
        y_train = np.concatenate([y_train, y_test_target], axis=0)

    # mnist_digits = np.expand_dims(mnist_digits, -1).astype("float32") / 255
    x_train = np.expand_dims(x_train, -1).astype("float32") / 255
    # print(mnist_digits.shape)
    print(y_train[:100])
    PATH = "/home/brendan/results/biped_final/latest/" + args.folder + "/" + args.exp + "/" + str(target) + "/"
    writer = tensorboardX.SummaryWriter(log_dir=PATH)
    # vae = Model(name="admis", env=None, ob_size=0, ac_size=1, im_size=list(x_train.shape[1:]), PATH=PATH)
    
    if args.ensemble:
        vaes = [Model(name="admis" + str(i), env=None, ob_size=0, ac_size=1, im_size=list(x_train.shape[1:]), vae_im_size=list(x_train.shape[1:]), PATH=PATH,  args=args) for i in range(10)]
    else:
        vae = Model(name="admis", env=None, ob_size=0, ac_size=1, im_size=list(x_train.shape[1:]), vae_im_size=list(x_train.shape[1:]), PATH=PATH,  args=args)

    initialize_uninitialized()
    print(" training with ", x_train.shape)

    # batch_size = 64
    batch_size = 32
    pos_x = x_test[np.where(y_test == args.target)[0][:500],::]
    pos_labels = np.array([0 for _ in range(500)])
    neg_x = x_test[np.where(y_test != args.target)[0][:500],::]
    neg_labels = np.array([1 for _ in range(500)])
    # print(pos_x.shape, neg_x.shape, pos_labels.shape, neg_labels.shape)
    y_test = np.concatenate([pos_labels, neg_labels], axis=0)
    x_test = np.concatenate([pos_x, neg_x], axis=0)
    t1 = time.time()
    if train:

        for epoch in range(args.epochs):
            lrnow = 0.0002
            dis_loss = [] 
            err_loss = [] 
            gan_loss = [] 
            gen_loss = [] 
            # batch_idxs = min(len(x_train), config.train_size) // batch_size
            batch_idxs = len(x_train) // batch_size
            for idx in range(0, int(batch_idxs)):
                # print(idx)
                batch_images = x_train[idx*batch_size:(idx+1)*batch_size]
                batch_images1, batch_rho1 = image_ultererer(batch_images)
                batch_images2, batch_rho2 = image_ultererer(batch_images)
                # batch_labels = data_y[idx*batch_size:(idx+1)*batch_size]
                # outs = vae.train_ae(epoch, lrnow, cliprange=0.2, vae_imgs=batch_images, imgs=batch_images)
                outs = vae.train_ae(lrnow, img1=batch_images1, img2=batch_images2, rho1=batch_rho1, rho2=batch_rho2)
                # # std.append(outs[1])
                # print(len(outs))
                # print(len(outs))
                if "ae" in args.ae_type:
                    dis_loss.append(0.0)
                    gan_loss.append(0.0)
                    err_loss.append(outs[1])
                    # err_loss.append(outs[1])
                    gen_loss.append(outs[0])
                else:
                    dis_loss.append(outs[2])
                    gan_loss.append(outs[1])
                    err_loss.append(0.0)
                    # err_loss.append(outs[1])
                    gen_loss.append(outs[0])
                    # dis_loss.append(outs[3])
                    # gan_loss.append(outs[0])
                    # gen_loss.append(outs[1])
                    # err_loss.append(outs[2])
                # gen_loss.append(outs[2])
            # print()
            # zeds = plot_latent_space2(vae)
            print("gan epoch", epoch, "dis loss", np.mean(dis_loss),"err loss", np.mean(err_loss), "gen loss", np.mean(gen_loss),  "gan loss", np.mean(gan_loss), "time: ", np.array(time.time() - t1))
            t1 = time.time()
        vae.save()
        plot_sample2(vae, x_train[:100,::])
        zeds = plot_latent_space2(vae)
        plot_label_clusters(vae, x_test[:1000,::], y_test[:1000])

    else:

        if args.ensemble:
            for vae in vaes:
                vae.load(PATH)
        else:
            vae.load(PATH)
        # plot_label_clusters(vae, x_train, y_train)
        
        # plot_label_clusters(vae, x_test, y_test)       
        # zeds = plot_latent_space(vae, n=30)
        # print("original shape", x_test.shape)
        # x_test, y_test = x_test[:2000,::], y_test[:2000]
        # # x_test, y_test = x_train[:2000,::], y_train[:2000]
        # x_test = np.expand_dims(x_test, -1).astype("float32") / 255

        # plot_sample(x_test)
        # gen_x = vae.decode(x_test)
        # plot_sample(gen_x)

        # exit()
        """
        ## Display how the latent space clusters different digit classes
        """

        # (x_train, y_train), _ = keras.datasets.mnist.load_data()
        # _, (x_test, y_test) = keras.datasets.mnist.load_data()



        # print(gen_x.shape)

        # vae z sample as an array of 28x28 x10x10
        # mse of a query image and every image in that array
        # add colour to the image the greater the mse
        query = []
        label = []

        # for i in 
            # all_i = x_test[np.where(y_test==num)]
            # all_labels = y_test[np.where(y_test==num)]
        # for i in range(0,400):
        #     # num = np.random.randint(0,10)
        #     all_i = x_test[np.where(y_test==num)]
        #     all_labels = y_test[np.where(y_test==num)]
        #     # for j in range(3):
        #     query.append(all_i)
        #     label.append(all_labels)
        #     print(all_i.shape)
        # print(np.array(query).shape)
        # plot_sample(np.array(query))
        # test_size = 100
        test_size = 900
        num = 900
        # query = x_test[:num,::]
        # label = y_test[:num]
        (x_train, y_train), (x_test, y_test) = keras.datasets.mnist.load_data()
        x_train = np.expand_dims(x_train, -1).astype("float32") / 255
        x_test = np.expand_dims(x_test, -1).astype("float32") / 255

        max_ram_size = 5000
        # max_ram_size = 1000
        x_test, y_test = x_test[:max_ram_size,::], y_test[:max_ram_size]

        plot_sample(x_test[:400,::])
        gen_x = vae.train_model.decode(x_test[:400,::])
        plot_sample(gen_x)

        query = x_test
        label = y_test
        # query = x_test[:num,::]
        # label = y_test[:num]
        all_mse = vae.get_mse(x_test.reshape(-1,28,28,1)) * -1
        # all_mse = vae.get_mse(x_test[:test_size,::].reshape(-1,28,28,1)) * -1
     
        all_mse = np.array(all_mse)
        # print(all_mse.shape)
        label = np.array(label)
        
        print(np.array(all_mse))
        print(np.array(label))
        min_idx = np.argsort(all_mse)
        # print(min_idx)
        # min_idx = list(min_idx).sort()
        print(np.take_along_axis(all_mse, min_idx, axis=0)[:400])
        print(np.take_along_axis(label, min_idx, axis=0)[:400])
        # print(np.take_along_axis(all_mse, min_idx, axis=0).shape)
        # print(np.take_along_axis(label, min_idx, axis=0).shape)
        count = 0
        first_non_zero = None
        num_count = 0
        count = 0
        count_at_last_zero = 0
        false_positive_count_at_last_zero = 0
        non_target_count = 0
        total_non_zeros = 0
        total_count = 0
        error_bin_size = (max(all_mse) - min(all_mse))/100
        current_error_bin = None
        x_zeros = {} 
        x_not_zeros = {} 
        for i, (zeros, err) in enumerate(zip(np.take_along_axis(label, min_idx, axis=0),np.take_along_axis(all_mse, min_idx, axis=0) )):
            if current_error_bin is None:
                current_error_bin = err
                x_zeros[current_error_bin] = 0
                x_not_zeros[current_error_bin] = 0
            else:
                if err > current_error_bin:
                    current_error_bin += error_bin_size
                    x_zeros[current_error_bin] = 0
                    x_not_zeros[current_error_bin] = 0
            # print(error_bin_size, current_error_bin, err)

            # print(zeros)
            
            if first_non_zero is None and zeros != target:
                first_non_zero = i
            if first_non_zero is not None and zeros != target:
                non_target_count += 1

            if zeros != target:
                x_not_zeros[current_error_bin] += 1
                total_non_zeros += 1
            if zeros == target:
                # num_count += 1
                x_zeros[current_error_bin] += 1
                count += 1
                count_at_last_zero = total_count
                false_positive_count_at_last_zero = non_target_count
            total_count += 1
               
        # total_zeros = np.count_nonzero(label == 0)
        print(x_test.shape, count, first_non_zero)
        print("percentage correct", 100*first_non_zero/count, " of total: ", count, " correct: ", first_non_zero)
        print("percentage for all zeros", 100*count/count_at_last_zero, " of total: ", count, " last zero: ", count_at_last_zero)
        print("percentage of false positives:", 100*false_positive_count_at_last_zero/(total_non_zeros), " of total: ", (total_non_zeros), " last zero: ", false_positive_count_at_last_zero)
        print("total count: ", total_count, " zeros ", count , " non_zeros ", total_non_zeros)
        # print(all_mse[min_idx])
        # print(label[min_idx])
        # query = x_test[:num,::]
        # label = y_test[:num]
        plot_errors(x_zeros, x_not_zeros, error_bin_size)

        plot_mse(query[:400,::], all_mse[:400], n=30, figsize=15)

        
