import os, inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
os.environ["TF_CPP_MIN_LOG_LEVEL"] = "3" 
import tensorflow as tf
tf.get_logger().setLevel("DEBUG")
import numpy as np
import argparse
import tensorboardX
from collections import deque   
from scripts.utils import *
from scripts.mpi_utils import sync_from_root
import random
from mpi4py import MPI
import time
from pathlib import Path
home = str(Path.home())
from scripts import logger
import json
import git
from plots import Plots
np.set_printoptions(precision=3, suppress=True)

def run(args):

    PATH = home + "/results/biped_final/latest/" + args.folder + "/" + args.exp + "/"
    plots = Plots(PATH, plot_freq=args.plot_freq)

    comm = MPI.COMM_WORLD
    rank = comm.Get_rank()
    myseed = args.seed + 10000 * rank
    np.random.seed(myseed)
    random.seed(myseed)
    tf.set_random_seed(myseed)
    
    logger.configure(dir=PATH)
    if rank == 0:
        writer = tensorboardX.SummaryWriter(log_dir=PATH)
        repo = git.Repo(search_parent_directories=True)
        sha = repo.head.object.hexsha
        with open(PATH + "commandline_args.txt", "w") as f:
            f.write("Hash:")
            f.write(str(sha) + "\n")
            json.dump(args.__dict__, f, indent=2)
    else: 
            writer = None 

    # sess = tf.Session()
    gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction= 0.1)
    sess = tf.InteractiveSession(config=tf.ConfigProto(inter_op_parallelism_threads=1,
                                            intra_op_parallelism_threads=1,             
                                            gpu_options=gpu_options), graph=None)
    
    horizon = 2048

    if args.old_env:
        from assets.env_pb_biped_old import Env
    else:
        from assets.env_pb_biped import Env
    
    env = Env(PATH=PATH, args=args)

    from models import ppo_setup
    pol = ppo_setup.Model(args.obstacle_type + "_setup", env=env, ob_size=env.ob_size, ac_size=env.ac_size, im_size=env.im_size, args=args, PATH=PATH, horizon=horizon, writer=writer, max_timesteps=args.max_ts, vis=args.vis, const_std=args.const_std)

    from models.ppo import Model
    pols = {name:Model(name, env=env, ob_size=env.ob_size, ac_size=env.ac_size, im_size=env.im_size, args=args, PATH=PATH, horizon=horizon, writer=writer, max_timesteps=args.max_ts, vis=args.vis, const_std=args.const_std) for name in ["flat", args.obstacle_type]}
    
    # initialize()
    initialize_uninitialized()
    sync_from_root(sess, pol.vars, comm=comm)
    pol.set_training_params(max_timesteps=args.max_ts, learning_rate=args.lr, horizon=horizon)

    for name in ["flat", args.obstacle_type]:
        pols[name].load_pol(name, "./weights/" + name + "/")
    
    pol.load_pol(base_name='flat', WEIGHTS_PATH='./weights/' + 'flat' + '/', vf=False)

    if args.test_pol:
        if args.hpc:
            pol.load(home + "/hpc-home/results/biped_final/latest/" + args.folder + "/" + args.exp + "/")      
        else:
            # pol.load(home + "/results/biped_final/latest/" + args.folder + "/" + args.exp + "/")      
            # pol.load("./weights/" + args.obstacle_type + "/")      
            # pol.load("/home/brendan/Dropbox/robot/mybot_ws/src/biped_final/weights/" + args.obstacle_type + "/")      
            pol.load_pol(args.obstacle_type, "./weights/" + args.obstacle_type + "/")

    # Throw an error if the graph grows (shouldn"t change once everything is initialised)
    tf.get_default_graph().finalize()
    
    prev_done = True
    ob = env.reset(cur_params=None)
    im = env.get_im()
    ep_ret = 0
    ep_len = 0
    ep_rets = []
    ep_lens = []
    ep_steps = 0
    ep_count = 0
    prev_pol = current_pol = "flat"
    env.set_current_pol(current_pol)
    if args.test_pol:
        stochastic = False
    else:
        stochastic = True
    
    next_act, exp_vpred, _, _ = pols[args.obstacle_type].step(ob, im, stochastic=False)    
    box_cross_steps = None
    terminate_setup = False
    max_setup_length = 75
    setup_done = False

    while True: 

        if pol.timesteps_so_far > pol.max_timesteps:
            break 

        if current_pol == args.obstacle_type + '_setup':
            act, vpred, _, nlogp, setup, setup_nlogp = pol.step(ob, im, stochastic=stochastic)
            terminate_setup = setup > 3
        elif current_pol == args.obstacle_type:
            act = next_act
        else:
            act, _, _, _ = pols["flat"].step(ob, im, stochastic=False)       

        torques = act

        next_ob, orig_rew, done, _ = env.step(torques)
        next_im = env.get_im()
        if (args.obstacle_type == 'flat' and env.box_num == (len(env.box_info[1])-1)) or (args.obstacle_type != 'flat' and (env.box_info[1][-1][0] - env.box_info[2][-1][0]) < env.x_min):
            done = True

        if not setup_done and (terminate_setup or done or (prev_pol == args.obstacle_type + "_setup") and current_pol != args.obstacle_type + "_setup"):
            setup_done = True
            last_next_ob = next_ob
            last_next_im = next_im

        # Setup policy reward
        next_act, exp_next_vpred, _, _ = pols[args.obstacle_type].step(next_ob, next_im, stochastic=False)    
        adv = orig_rew + (1-done)*0.99*exp_next_vpred - exp_vpred
        scaled_adv = adv**2/15
        if not box_cross_steps:
            rew = (1 - min(scaled_adv, 1))*exp_vpred/100
        elif (env.steps - box_cross_steps < 50):
            rew = (1 - min(scaled_adv, 1))*exp_vpred/100 - 0.4*(np.clip((setup - 1.0)**2, 0, 1))
        else:
            rew = (1 - min(scaled_adv, 1))*exp_vpred/100 
        # print(current_pol, box_cross_steps, env.speed)

        if current_pol == args.obstacle_type + "_setup":
            ep_ret += rew
            ep_len += 1
        elif setup_done:
            if terminate_setup and ep_len > 50:
                pol.data['rew'][-1] += rew
                ep_ret += rew

       
        if not args.test_pol and current_pol == args.obstacle_type + "_setup":
            pol.add_to_buffer([ob, im, act, rew, prev_done, vpred, nlogp, setup, setup_nlogp])
        
        ep_steps += 1       
        if not args.test_pol and not setup_done and len(pol.data['rew']) > 1440 and ep_steps % 100 == 0:
            setup_data_lengths = MPI.COMM_WORLD.allgather(len(pol.data['rew']))
            if (np.array(setup_data_lengths) > 1440).all():
                if rank == 0:
                    print("setup lengths: ", setup_data_lengths)
                if setup_done:
                    _, vpred, _, _, _, _ = pol.step(last_next_ob, last_next_im, stochastic=True)
                else:
                    _, vpred, _, _, _, _ = pol.step(next_ob, next_im, stochastic=True)
                # pol.finalise_buffer({"ep_rets":ep_rets, "ep_lens":ep_lens}, vpred, terminate_setup)
                pol.finalise_buffer({"ep_rets":ep_rets, "ep_lens":ep_lens}, vpred, setup_done)

                # Lots of smaller calls to allgather
                for d in pol.training_input:
                    all_d = MPI.COMM_WORLD.allgather(pol.data[d])
                    temp_d = np.concatenate(all_d)
                    length = temp_d.shape[0]//comm.Get_size()
                    start = rank*length
                    end = start + length
                    if len(pol.data[d].shape) == 1:
                        pol.data[d] = temp_d[start:end]
                    else:
                        pol.data[d] = temp_d[start:end, ::]
                temp_d = None
                all_d = None
        
                pol.run_train(data={"ep_rets":ep_rets, "ep_lens":ep_lens}, last_value=vpred, last_done=done)
                ep_rets = []
                ep_lens = []
                # TODO:
                # evaluate()


        # ========================================================================================================
        # Terrain detection stuff: TODO clean up!!
        # ========================================================================================================
        # next_base_pol = env.get_terrain()
        # if next_base_pol == args.obstacle_type:
        #     if box_cross_steps is None and current_pol != args.obstacle_type and "setup" not in current_pol:
        #         current_pol = args.obstacle_type + "_setup"
        #         box_cross_steps = env.steps
        #         setup_next_ob = next_ob
        #         setup_next_im = next_im
        #     elif "setup" in current_pol and terminate_setup:
        #         current_pol = next_base_pol
        # elif current_pol == args.obstacle_type and env.x_min > (env.box_info[1][env.box_num][0] - env.box_info[2][env.box_num][0]) and (not env.ob_dict["left_foot_left_ground"] and not env.ob_dict["right_foot_left_ground"]):
        #     current_pol = next_base_pol

        if not box_cross_steps and env.order[env.box_num] == args.obstacle_type:
            box_cross_steps = env.steps

        if not box_cross_steps:
            next_pol = "flat"
        elif box_cross_steps and (box_cross_steps + max_setup_length > env.steps) and not setup_done:
            next_pol = args.obstacle_type + "_setup"
        elif box_cross_steps and (box_cross_steps + max_setup_length <= env.steps or setup_done):
            if (env.order[env.box_num] == args.obstacle_type):
                next_pol = args.obstacle_type
            elif 'flat' == current_pol or (env.order[env.box_num-1] == args.obstacle_type and env.order[env.box_num] != args.obstacle_type and (env.ob_dict['left_foot_on_ground'] and env.ob_dict['right_foot_on_ground'] and abs(env.vz) < 0.05 and env.vx > 0)):
                next_pol = "flat"
            else:
                next_pol = args.obstacle_type

        prev_pol = current_pol
        current_pol = next_pol

        exp_vpred = exp_next_vpred

        # if current_pol == args.obstacle_type + '_setup':
        #     prev_setup_done = setup_done

        # ========================================================================================================
        # This is necessary to set the desired speed, new policies will fix this
        # if args.old_env and args.multi:
        #     if 'high_jumps' in current_pol:
        #         env.obstacle_type = 'high_jumps'
        #     else:
        #         env.obstacle_type = 'mix'
        #         env.original_speed = 1.0
        # ========================================================================================================

        
        prev_done = done
        ob = next_ob
        im = next_im
        exp_vpred = exp_next_vpred
        env.set_current_pol(current_pol)  

        plots.update({"current_pol":["flat", args.obstacle_type + "_setup", args.obstacle_type].index(current_pol),
                    "terrain":{"z":env.box_info[1][env.box_num][2] + env.box_info[2][env.box_num][2],"robot_z":env.body_xyz[2]}})
        

        if done:
            if rank == 0:
                plots.plot(ep_count)
            ep_count += 1
            ob = env.reset()
            im = env.get_im()
            ep_rets.append(ep_ret)  
            ep_lens.append(ep_len)     
            ep_ret = 0
            ep_len = 0        
            prev_pol = current_pol = "flat"

            env.set_current_pol(current_pol)
            box_cross_steps = None
            terminate_setup = False
            next_act, exp_vpred, _, _ = pols[args.obstacle_type].step(ob, im, stochastic=False)
            setup_done = False

if __name__ == "__main__":
    # Hack for loading defaults, but still accepting run specific defaults
    import defaults
    from dotmap import DotMap
    args1, unknown1 = defaults.get_defaults() 
    parser = argparse.ArgumentParser()
    # Arguments that are specific for this run (including run specific defaults, ignore unknown arguments)
    parser.add_argument("--old_env", default=True, action="store_false")
    parser.add_argument("--ent_coef", default=0.0, type=float)
    parser.add_argument("--run_state", default="train_setup")
    parser.add_argument("--folder", default="setup")
    parser.add_argument("--plot_freq", default=10, type=int)
    parser.add_argument("--difficulty", default=10, type=int)
    parser.add_argument("--max_ts", default=25000000, type=int)
    parser.add_argument("--num_artifacts", default=1, type=int)
    parser.add_argument("--obstacle_type", default="high_jumps", help="Obstacle types are: flat, gaps, jumps, stairs, steps, high_jumps, one_leg_hop, hard_steps, hard_high_jumps, or mix (for a combination)")
    parser.add_argument("--sim_type", default="pb", help="pb or gz (pybullet or gazebo)")
    parser.add_argument("--robot", default="biped", help="biped or hexapod")
    parser.add_argument("--alg", default="ppo", help="ppo, eventually sac as well") 
    args2, unknown2 = parser.parse_known_args()
    args2 = vars(args2)
    # Replace any arguments from defaults with run specific defaults
    for key in args2:
        args1[key] = args2[key]
    # Look for any changes to defaults (unknowns) and replace values in args1
    for n, unknown in enumerate(unknown2):
        if "--" in unknown and n < len(unknown2)-1 and "--" not in unknown2[n+1]:
            arg_type = type(args1[unknown[2:]])
            args1[unknown[2:]] = arg_type(unknown2[n+1])
    args = DotMap(args1)
    # Check for dodgy arguments
    unknowns = []
    for unknown in unknown1 + unknown2:
        if "--" in unknown and unknown[2:] not in args:
            unknowns.append(unknown)
    if len(unknowns) > 0:
        print("Dodgy argument")
        print(unknowns)
        exit()

    os.environ["CUDA_VISIBLE_DEVICES"]="-1"
    run(args)
