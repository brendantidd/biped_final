from models.ppo_rand import Model
from tensorflow import keras
import numpy as np
import cv2
import tensorboardX


def multivariate_pdf(vector, mean, cov):
    quadratic_form = np.dot(np.dot(vector-mean,np.linalg.inv(cov)),np.transpose(vector-mean))
    return np.exp(-.5 * quadratic_form)/ (2*np.pi * np.linalg.det(cov))

def plot_sample(data, n=30, figsize=15):
    # display a n*n 2D manifold of digits
    digit_size = 28
    # scale = 1.0
    scale = int(np.sqrt(len(data)))
    n = int(np.sqrt(len(data)))
    
    figure = np.zeros((digit_size * n, digit_size * n))
    # linearly spaced coordinates corresponding to the 2D plot
    # of digit classes in the latent space
    grid_x = np.linspace(-scale, scale, n)
    grid_y = np.linspace(-scale, scale, n)[::-1]

    for i, yi in enumerate(grid_y):
        for j, xi in enumerate(grid_x):
            z_sample = np.array([[xi, yi]])
            # x_decoded = vae.decoder.predict(z_sample)
            # x_decoded = vae.decode_from_ph(z_sample)
            x_decoded = data[i + n*j, ::]
            digit = x_decoded.reshape(digit_size, digit_size)
            figure[
                i * digit_size : (i + 1) * digit_size,
                j * digit_size : (j + 1) * digit_size,
            ] = digit

    plt.figure(figsize=(figsize, figsize))
    start_range = digit_size // 2
    end_range = n * digit_size + start_range
    pixel_range = np.arange(start_range, end_range, digit_size)
    sample_range_x = np.round(grid_x, 1)
    sample_range_y = np.round(grid_y, 1)
    plt.xticks(pixel_range, sample_range_x)
    plt.yticks(pixel_range, sample_range_y)
    plt.xlabel("z[0]")
    plt.ylabel("z[1]")
    plt.imshow(figure, cmap="Greys_r")
    plt.show()

def plot_latent_space(vae, n=30, figsize=15):
    # display a n*n 2D manifold of digits
    digit_size = 28
    # scale = 1.0
    # scale = 6.0
    scale = 3.0
    # scale = 10.0
    figure = np.zeros((digit_size * n, digit_size * n))
    # linearly spaced coordinates corresponding to the 2D plot
    # of digit classes in the latent space
    grid_x = np.linspace(-scale, scale, n)
    grid_y = np.linspace(-scale, scale, n)[::-1]

    for i, yi in enumerate(grid_y):
        for j, xi in enumerate(grid_x):
            z_sample = np.array([0]*args.latent_dim).reshape([-1,args.latent_dim])
            z_sample[0][0] = xi
            z_sample[0][1] = yi
            # x_decoded = vae.decoder.predict(z_sample)
            x_decoded = vae.decode_from_ph(z_sample)
            digit = x_decoded[0].reshape(digit_size, digit_size)
            figure[
                i * digit_size : (i + 1) * digit_size,
                j * digit_size : (j + 1) * digit_size,
            ] = digit

    plt.figure(figsize=(figsize, figsize))
    start_range = digit_size // 2
    end_range = n * digit_size + start_range
    pixel_range = np.arange(start_range, end_range, digit_size)
    sample_range_x = np.round(grid_x, 1)
    sample_range_y = np.round(grid_y, 1)
    plt.xticks(pixel_range, sample_range_x)
    plt.yticks(pixel_range, sample_range_y)
    plt.xlabel("z[0]")
    plt.ylabel("z[1]")
    plt.imshow(figure, cmap="Greys_r")
    plt.show()
    return figure

def plot_mse(query, mse, n=30, figsize=15):
    # display a n*n 2D manifold of digits
    digit_size = 28
    # scale = 1.0
    # scale = 2.0
    # scale = 10.0
    scale = int(np.sqrt(len(mse)))

    # print(scale, len(mse))
    n = int(np.sqrt(len(mse)))
    figure = np.zeros((digit_size * n, digit_size * n,3))
    # linearly spaced coordinates corresponding to the 2D plot
    # of digit classes in the latent space
    grid_x = np.linspace(-scale, scale, n)
    grid_y = np.linspace(-scale, scale, n)[::-1]

    # for i, yi in enumerate(grid_y):
    #     for j, xi in enumerate(grid_x):
    min_mse = min(mse)
    max_mse = max(mse)
    for i in range(scale):
        for j in range(scale):
            # z_sample = np.array([[xi, yi]])
            # x_decoded = vae.decoder.predict(z_sample)
            # x_decoded = vae.decode_from_ph(z_sample)
            # digit = x_decoded[0].reshape(digit_size, digit_size)
            # print(i,j, i*scale + j, len(query))
            # digit = query[int(i*scale+j)].reshape(digit_size, digit_size)
            digit = query[int(j*scale+i)].reshape(digit_size, digit_size)
            digit = cv2.cvtColor(digit, cv2.COLOR_GRAY2RGB)
            digit[:,:,0] = (mse[int(j*scale+i)] - min_mse) / (max_mse - min_mse)

            # digit 
            # cv2.
            # mse = np.mean((digit - query)**2)
            # mse.
            # v2.putText(digit, str(mse), (0,0), font, 1, (0, 255, 0), 2, cv2.LINE_AA)
            # print(mse)
            # *(digit - query)
            figure[
                i * digit_size : (i + 1) * digit_size,
                j * digit_size : (j + 1) * digit_size, :
            ] = digit


    plt.figure(figsize=(figsize, figsize))
    # start_range = digit_size // 2
    # end_range = n * digit_size + start_range
    # pixel_range = np.arange(start_range, end_range, digit_size)
    # sample_range_x = np.round(grid_x, 1)
    # sample_range_y = np.round(grid_y, 1)
    # plt.xticks(pixel_range, sample_range_x)
    # plt.yticks(pixel_range, sample_range_y)
    # plt.xlabel("z[0]")
    # plt.ylabel("z[1]")
    plt.imshow(figure, cmap="Greys_r")
    plt.show()
    return figure

def plot_figure(figure):
    plt.imshow(figure, cmap="Greys_r")
    plt.show()

def plot_label_clusters(vae, data, labels):
    # display a 2D plot of the digit classes in the latent space
    # z_mean, _, _ = vae.encoder.predict(data)
    fig = plt.figure()
    ax = Axes3D(fig)
    z_mean = vae.encode(data.reshape(-1,28,28,1)) 
    ax.scatter(z_mean[:, 0], z_mean[:, 1], z_mean[:, 2], c=labels)
    # plt.xlim([-5,5])
    # plt.ylim([-5,5])
    # plt.zlim([-5,5])
    # ax.set_zlim(-5,5)
    # plt.colorbar()
    plt.xlabel("z[0]")
    plt.ylabel("z[1]")
    # plt.zlabel("z[2]")
    plt.show()

    # z_mean = vae.encode(data.reshape(-1,28,28,1)) 
    # plt.figure(figsize=(12, 10))
    # plt.scatter(z_mean[:, 0], z_mean[:, 1], c=labels)
    # plt.xlim([-5,5])
    # plt.ylim([-5,5])
    # plt.colorbar()
    # plt.xlabel("z[0]")
    # plt.ylabel("z[1]")
    # plt.show()

def plot_errors(x_target_err, x_not_target_err, width):
    # z_mean = vae.encode(data.reshape(-1,28,28,1)) 
    plt.figure(figsize=(12, 10))
    xs = []
    ys = []
    # print(x_target_err)
    # print(x_not_target_err)

    for x in x_target_err:
        xs.append(x)
        ys.append(x_target_err[x])
    plt.bar(xs, ys, width=width, color=[0.4,0.2,0.8,0.5] )
    xs = []
    ys = []
    for x in x_not_target_err:
        xs.append(x)
        ys.append(x_not_target_err[x])
    plt.bar(xs, ys, width=width, color=[0.8,0.2,0.4,0.5])

    # plt.xlim([-5,5])
    # plt.ylim([-5,5])
    # plt.colorbar()
    # plt.xlabel("z[0]")
    # plt.ylabel("z[1]")
    plt.show()


if __name__=="__main__":
    # As much of this code is from https://github.com/keras-team/keras-io/blob/master/examples/generative/vae.py as possible
    # (adapted to our openai's mpiadam, and tf1.14)
    import matplotlib.pyplot as plt
    # matplotlib.use('Agg')
    import matplotlib
    # matplotlib.use('TkAgg')
    matplotlib.use('QT4Agg')
    from mpl_toolkits.mplot3d import Axes3D
    from scripts.utils import *
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("--train", default=False, action="store_true")
    parser.add_argument("--mse", default=False, action="store_true")
    parser.add_argument("--enc", default=False, action="store_true")
    parser.add_argument("--conv", default=False, action="store_true")
    parser.add_argument("--train_on_test", default=False, action="store_true")
    parser.add_argument("--init_value", default=0.5, type=float)
    parser.add_argument("--latent_dim", default=128, type=int)
    parser.add_argument("--hid_size", default=128, type=int)
    parser.add_argument("--target", default=0, type=int)
    parser.add_argument("--exp", default="test")
    parser.add_argument("--tests", default="test")
    parser.add_argument("--folder", default="rand")
    args = parser.parse_args()

    myseed = 42
    np.random.seed(myseed)
    tf.set_random_seed(myseed)

    target = args.target
    train = args.train

    gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction= 0.5)
    sess = tf.InteractiveSession(config=tf.ConfigProto(inter_op_parallelism_threads=1,
                                            intra_op_parallelism_threads=1,             
                                            gpu_options=gpu_options), graph=None)

    (x_train, y_train), (x_test, y_test) = keras.datasets.mnist.load_data()
    # x_train, x_test = x_train[:30000,::], x_test[:2000,::]
    # x_train = x_train[:30000,::]
    # y_train = y_train[:30000]
    # print(np.where(y_train == 0)[:100])
    # print(y_train[:100])
    x_train = x_train[ np.where(y_train == target),::].reshape(-1,28,28)
    y_train = y_train[ np.where(y_train == target)]
    x_test_target = x_test[ np.where(y_test == target),::].reshape(-1,28,28)
    y_test_target = y_test[ np.where(y_test == target)]
    
    if args.train_on_test:
        x_train = np.concatenate([x_train, x_test_target], axis=0)
        y_train = np.concatenate([y_train, y_test_target], axis=0)

    # mnist_digits = np.expand_dims(mnist_digits, -1).astype("float32") / 255
    x_train = np.expand_dims(x_train, -1).astype("float32") / 255
    # print(mnist_digits.shape)
    print(y_train[:100])
    PATH = "/home/brendan/results/biped_final/latest/" + args.folder + "/" + args.exp + "/" + str(target) + "/"
    writer = tensorboardX.SummaryWriter(log_dir=PATH)
    # vae = Model(name="admis", env=None, ob_size=0, ac_size=1, im_size=list(x_train.shape[1:]), PATH=PATH)
    vae = Model(name="admis", env=None, ob_size=0, ac_size=1, vae_im_size=list(x_train.shape[1:]), PATH=PATH, latent_dim=args.latent_dim, args=args)

    initialize_uninitialized()
    print(" training with ", x_train.shape)

    if train:
        plot_sample(x_train)

        training_input = ["input"]
        data = {"input": x_train.reshape(-1,28,28,1)}
        batch_size = 32
        inds = [i for i in range(x_train.shape[0])]
        # for epoch in range(200):
        for epoch in range(10):
            lrnow = 0.0001
            # if enable_shuffle:
            np.random.shuffle(inds)
            # not_inds = [inds[-1]] + inds[:-1]
            loss = [] 
            std  = []
            for start in range(0, x_train.shape[0], batch_size):
                end = start + batch_size
                mbinds = inds[start:end]
                
                # not_mbinds = not_inds[start:end]
                # if len(mbinds) != batch_size:
                    # continue
                # all_imgs = np.concatenate((data["input"][mbinds], data["input"][not_mbinds]))
                # all_labels = np.array([1 for _ in range(len(mbinds))] + [0 for _ in range(len(mbinds))])
                # outs = vae.train(epoch, lrnow, im=all_imgs, labels=all_labels)

                # print(data["input"][mbinds].shape, data["input"][not_mbinds].shape)
                # print(all_imgs.shape, all_labels.shape)
                # print(x_train.shape[0], batch_size, x_train.shape[0]/batch_size)
                slices = (data[key][mbinds] for key in training_input)
                outs = vae.train_vae(epoch, lrnow, *slices)
                # std.append(outs[1])
                loss.append(outs[0])
            # print()
            print("epoch", epoch, "loss", np.mean(loss))
    
        vae.save()
    
        zeds = plot_latent_space(vae)
        plot_label_clusters(vae, x_test, y_test)
        # plot_label_clusters(vae, x_train, y_train)

    else:

        vae.load(PATH)

        # plot_label_clusters(vae, x_train, y_train)
        
        # plot_label_clusters(vae, x_test, y_test)       
        # zeds = plot_latent_space(vae, n=30)
        # print("original shape", x_test.shape)
        # x_test, y_test = x_test[:2000,::], y_test[:2000]
        # # x_test, y_test = x_train[:2000,::], y_train[:2000]
        # x_test = np.expand_dims(x_test, -1).astype("float32") / 255

        # plot_sample(x_test)
        # gen_x = vae.decode(x_test)
        # plot_sample(gen_x)

        # exit()
        """
        ## Display how the latent space clusters different digit classes
        """

        # (x_train, y_train), _ = keras.datasets.mnist.load_data()
        # _, (x_test, y_test) = keras.datasets.mnist.load_data()



        # print(gen_x.shape)

        # vae z sample as an array of 28x28 x10x10
        # mse of a query image and every image in that array
        # add colour to the image the greater the mse
        query = []
        label = []

        # for i in 
            # all_i = x_test[np.where(y_test==num)]
            # all_labels = y_test[np.where(y_test==num)]
        # for i in range(0,400):
        #     # num = np.random.randint(0,10)
        #     all_i = x_test[np.where(y_test==num)]
        #     all_labels = y_test[np.where(y_test==num)]
        #     # for j in range(3):
        #     query.append(all_i)
        #     label.append(all_labels)
        #     print(all_i.shape)
        # print(np.array(query).shape)
        # plot_sample(np.array(query))

        num = 900
        # query = x_test[:num,::]
        # label = y_test[:num]
        (x_train, y_train), (x_test, y_test) = keras.datasets.mnist.load_data()
        x_train = np.expand_dims(x_train, -1).astype("float32") / 255
        x_test = np.expand_dims(x_test, -1).astype("float32") / 255

        plot_sample(x_test[:400,::])
        gen_x = vae.decode(x_test[:400,::])
        plot_sample(gen_x)

        query = x_test
        label = y_test
        # query = x_test[:num,::]
        # label = y_test[:num]
        all_mse = vae.get_mse(x_test.reshape(-1,28,28,1)) * -1
        # all_mse = []
        # for q,l in zip(query, label):
            # for z in zeds:
            #     print(q.shape, z.shape)
            #     plot_figure(np.array([q, z]))
            # print(q.shape)
            # mse_s = []

            # n=10
            # # n=2
            # figsize=15
            # digit_size = 28
            # # scale = 1.0
            # # scale = 6.0
            # # scale = 5.0
            # scale = 3.0
            # # figure = np.zeros((digit_size * n, digit_size * n))
            # # linearly spaced coordinates corresponding to the 2D plot
            # # of digit classes in the latent space
            # grid_x = np.linspace(-scale, scale, n)
            # grid_y = np.linspace(-scale, scale, n)[::-1]

            # x_decoded = vae.decoder.predict(z_sample)
            # z_mean, z_log_var = vae.encode(q.reshape(-1,28,28,1))
            # print(z_mean, z_log_var)
            # probs = multivariate_pdf(z_mean, np.array([0,0]), np.array([[1,0],[0,1]]))
            # # print(probs)
            # all_mse.append(probs[0][0])

            # digit = vae.decode(q.reshape(-1,28,28,1))
            # mse = np.sum((digit - q)**2)
            # mse = vae.get_mse(q.reshape(-1,28,28,1)) * -1
            # print(l, mse)
            
            # all_mse.append(mse)
            
            # d = vae.decode(q.reshape(-1,28,28,1))[0]
            # plot_label_clusters(vae, np.array([q, d]), np.array([l, 0]))

            # exit()
            # for i, yi in enumerate(grid_y):
            #     for j, xi in enumerate(grid_x):
            #         z_sample = np.array([[xi, yi]])
            #         # x_decoded = vae.decoder.predict(z_sample)
            #         x_decoded = vae.decode_from_ph(z_sample)
            #         digit = x_decoded[0].reshape(digit_size, digit_size, 1)
            #         # print(digit.shape, q.shape)
            #         mse = np.sum((digit - q)**2)
            #         mse_s.append(mse)
            #         # print(mse)
            #         # # *(digit - query)
            #         # figure[
            #         #     i * digit_size : (i + 1) * digit_size,
            #         #     j * digit_size : (j + 1) * digit_size,
            #         # ] = digit

            # print(np.min(mse_s))
            # all_mse.append(np.min(mse_s))
            # cv2.imshow("frame", q)
            # cv2.waitKey(1)
        # print(np.array(all_mse).reshape([20,20]))
        # print(np.array(labels).reshape([20,200]))
        all_mse = np.array(all_mse)
        label = np.array(label)
        
        print(np.array(all_mse))
        print(np.array(label))
        min_idx = np.argsort(all_mse)
        # print(min_idx)
        # min_idx = list(min_idx).sort()
        print(np.take_along_axis(all_mse, min_idx, axis=0)[:400])
        print(np.take_along_axis(label, min_idx, axis=0)[:400])
        count = 0
        first_non_zero = None
        num_count = 0
        count = 0
        count_at_last_zero = 0
        total_count = 0
        error_bin_size = (max(all_mse) - min(all_mse))/100
        current_error_bin = None
        x_zeros = {} 
        x_not_zeros = {} 
        for i, (zeros, err) in enumerate(zip(np.take_along_axis(label, min_idx, axis=0),np.take_along_axis(all_mse, min_idx, axis=0) )):
            if current_error_bin is None:
                current_error_bin = err
                x_zeros[current_error_bin] = 0
                x_not_zeros[current_error_bin] = 0
            else:
                if err > current_error_bin:
                    current_error_bin += error_bin_size
                    x_zeros[current_error_bin] = 0
                    x_not_zeros[current_error_bin] = 0
            # print(error_bin_size, current_error_bin, err)

            # print(zeros)
            if first_non_zero is None and zeros != target:
                first_non_zero = i
            if zeros != target:
                x_not_zeros[current_error_bin] += 1
            if zeros == target:
                # num_count += 1
                x_zeros[current_error_bin] += 1
                count += 1
                count_at_last_zero = total_count
            total_count += 1
               
        # total_zeros = np.count_nonzero(label == 0)
        print(x_test.shape, count, first_non_zero)
        print("percentage correct", 100*first_non_zero/count, " of total: ", count, " correct: ", first_non_zero)
        print("percentage for all zeros", 100*count/count_at_last_zero, " of total: ", count, " last zero: ", count_at_last_zero)
        # print(all_mse[min_idx])
        # print(label[min_idx])
        # query = x_test[:num,::]
        # label = y_test[:num]
        plot_errors(x_zeros, x_not_zeros, error_bin_size)

        plot_mse(query[:400,::], all_mse[:400], n=30, figsize=15)
        # gen_x = vae.decode(x_test[:1000,::])
        # plot_sample(gen_x)
        # plot_label_clusters(vae, x_test, y_test)

        
