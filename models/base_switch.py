'''
Base class for all networks.
For now includes training parameters..
'''
# import scripts.utils as U
import os   
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' 
import tensorflow as tf
from baselines import logger
from mpi4py import MPI
from baselines.common.distributions import make_pdtype
import time
import numpy as np
from collections import deque
import scripts.utils as U
# from models.ppo import Model

class Base():
  epochs = 10
  batch_size = 32
  enable_shuffle = True
  episodes_so_far = 0
  timesteps_so_far = 0
  iters_so_far = 0
  best_reward = 0
  tstart = time.time()
  t1 = time.time()
  all_rewards = []
  lenbuffer = deque(maxlen=100) # rolling buffer for episode lengths
  rewbuffer = deque(maxlen=100) # rolling buffer for episode rewards
  comm = MPI.COMM_WORLD
  rank = comm.Get_rank()
  def __init__(self):
    vars_with_adam = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, scope=self.name+"/")
    self.vars = [v for v in vars_with_adam if 'Adam' not in v.name]
    # print("these are the variables for ", self.name)
    # print(self.vars)
    self.policy_saver = tf.train.Saver(var_list=self.vars)
    self.t1 = time.time()

  def save(self, best=False):
    if best:
      self.policy_saver.save(tf.get_default_session(), self.PATH + '/best/model.ckpt', write_meta_graph=False)
    else:
      self.policy_saver.save(tf.get_default_session(), self.PATH + 'model.ckpt', write_meta_graph=False)
    # if self.rank == 0:
    print("Saving weights for " + self.name)

  def load(self, WEIGHT_PATH):
    if self.rank == 0:
      print()
      print("*** Loaded weights for " + self.name + " module from *** ")
      print( WEIGHT_PATH + 'model.ckpt')
      print()
    self.policy_saver.restore(tf.get_default_session(), WEIGHT_PATH + 'model.ckpt')
    # print("Loaded weights for " + self.name + " module")

  # def load_base(self, WEIGHT_PATH):
  #   print("Loaded weights for " + self.name + " module from ", WEIGHT_PATH + 'model.ckpt')
  #   self.policy_saver.restore(tf.get_default_session(), WEIGHT_PATH + 'model.ckpt')
  #   # print("Loaded weights for " + self.name + " module")

  def load_base(self, base_name, WEIGHT_PATH):

    # base_pol = Model(base_name, ob_size=self.ob_size, ac_size=self.ac_size, im_size=self.im_size, args=self.args, PATH=self.PATH, vis=args.vis)
    # base_pi = Model(base_name, self.args, self.ob, self.im, self.ob_space, self.im_size, self.ac_space)
    temp_vars = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, scope=base_name + "/")
    base_vars = [v for v in temp_vars if 'Adam' not in v.name]
    base_saver = tf.train.Saver(var_list=base_vars)    
    base_saver.restore(tf.get_default_session(), WEIGHT_PATH + 'model.ckpt')
    target_func_vars = [v for v in self.vars if 'Adam' not in v.name and 'vf_rms' not in v.name]
    update_target_expr = []
    for var, var_target in zip(sorted(base_vars, key=lambda v:v.name), sorted(target_func_vars, key=lambda v: v.name)):
      update_target_expr.append(var_target.assign(var))
    update_target_expr = tf.group(*update_target_expr)
    update_target = U.function([],[], updates=[update_target_expr])
    update_target()
    if self.rank == 0:
      print("Loaded weights for subpolicy: ", self.name)  


  def evaluate(self, rews, lens):

    lrlocal = (lens, rews) # local values
    listoflrpairs = MPI.COMM_WORLD.allgather(lrlocal) # list of tuples
    lens, rews = map(flatten_lists, zip(*listoflrpairs))
    # self.lenbuffer.extend(lens)
    # self.rewbuffer.extend(rews)

    if self.rank == 0:
      logger.log("********** Iteration %i ************"%self.iters_so_far)
      for loss, name in zip(self.loss, self.loss_names):
        logger.record_tabular(name, loss)
        self.writer.add_scalar(name, loss, self.iters_so_far)
      logger.record_tabular("LearningRate", self.lr)
      logger.record_tabular("EpRewMean", np.mean(rews))
      logger.record_tabular("EpLenMean", np.mean(lens))
      logger.record_tabular("EpThisIter", len(lens))
      logger.record_tabular("TimeThisIter", time.time() - self.t1)
      logger.record_tabular("TimeStepsSoFar", self.timesteps_so_far)
      logger.record_tabular("EnvTotalSteps", self.env.total_steps)
      # logger.record_tabular("EnvSampleDelay", self.env.sample_delay)
      # self.writer.add_scalar("sample_delay", self.env.sample_delay, self.iters_so_far)
      # logger.record_tabular("Kp", self.env.Kp)
      # self.writer.add_scalar("Kp", self.env.Kp, self.iters_so_far)
      self.writer.add_scalar("time per step", time.time() - self.t1, self.iters_so_far)
      self.t1 = time.time()
      self.writer.add_scalar("rewards", np.mean(rews), self.iters_so_far)
      self.writer.add_scalar("lengths", np.mean(lens), self.iters_so_far)
      self.all_rewards.append(np.mean(rews))
      np.save(self.PATH + 'rewards.npy',self.all_rewards)
      # print(np.mean(self.env.reward_breakdown['pos']),np.mean(self.env.reward_breakdown['vel']))
      self.env.log_stuff(logger, self.writer, self.iters_so_far)
      try:
          if np.mean(rews) > self.best_reward :
            self.best_reward = np.mean(rews)
            self.save(best=True)
          self.save()
      except:
          print("Couldn't save training model")
      logger.dump_tabular()
    self.timesteps_so_far += sum(lens)     
    self.iters_so_far += 1


  def set_training_params(self, max_timesteps, learning_rate, horizon, vf_learning_rate=None):
    if vf_learning_rate == None:
      vf_learning_rate = learning_rate
    if self.rank == 0:
      print()
      print("setting training params", max_timesteps, learning_rate, vf_learning_rate, horizon)
      print()
    self.max_timesteps = max_timesteps
    self.learning_rate = learning_rate
    self.vf_learning_rate = vf_learning_rate
    self.horizon = horizon

  def log_stuff(self):
    pass

def flatten_lists(listoflists):
    return [el for list_ in listoflists for el in list_]