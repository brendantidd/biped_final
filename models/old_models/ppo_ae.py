'''
From OpenAI Baselines
'''
from models.base import Base
import os   
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' 
import tensorflow as tf
tf.get_logger().setLevel('DEBUG')
from scripts.distributions import make_pdtype
from scripts.mpi_running_mean_std import RunningMeanStd
from scripts.mpi_moments import mpi_moments
# from baselines.common import explained_variance, fmt_row, zipsame
import numpy as np
from gym import spaces
# import scripts.utils as U
import scripts.utils as TF_U
from scripts.mpi_adam_optimizer import MpiAdamOptimizer
from mpi4py import MPI
from scripts import logger
import time
import cv2
comm = MPI.COMM_WORLD
from collections import deque

def display_images(im1, im2=None):
    im1 = im1.reshape([-1] + list(im1.shape))
    if im2 is None:
        for im in im1:
            im = cv2.resize(im,dsize=(int(im.shape[1] * 6), int(im.shape[0] * 6)))
            cv2.imshow("frame", im)
            cv2.waitKey(1)
    else:
        im2 = im2.reshape([-1] + list(im2.shape))
        for i1, i2 in zip(im1, im2):
            i1 = cv2.resize(i1,dsize=(int(i1.shape[1] * 6), int(i1.shape[0] * 6)))
            i2 = cv2.resize(i2,dsize=(int(i2.shape[1] * 6), int(i2.shape[0] * 6)))
            cv2.imshow("frame", np.hstack([i1,i2]))
            cv2.waitKey(1)

def get_vars(scope):
    return [x for x in tf.global_variables() if scope in x.name]

# def neglogp(self, x):
#     return 0.5 * tf.reduce_sum(tf.square((x - self.mean) / self.std), axis=-1) \
#             + 0.5 * np.log(2.0 * np.pi) * tf.to_float(tf.shape(x)[-1]) \
#             + tf.reduce_sum(self.logstd, axis=-1)

def log_normal_pdf(sample, mean, logvar, raxis=1):
    log2pi = tf.math.log(2. * np.pi)
    return tf.reduce_sum(
            -.5 * ((sample - mean) ** 2. * tf.exp(-logvar) + logvar + log2pi),
            axis=raxis)

def reparameterize(mean, logvar):
    eps = tf.random.normal(shape=tf.shape(mean))
    return eps * tf.exp(logvar * .5) + mean

class Policy():
    def __init__(self, name, ob, im, vae_im, z_ph, ob_space, ob_size, im_size, vae_im_size, ac_size, env, LAM, ac_space, latent_dim, sess, vis,  args, hid_size, normalize=True, const_std=False):
        self.name = name
        self.ob = ob
        self.im = im
        self.vae_im = vae_im
        self.z_ph = z_ph
        self.ob_size = ob_size
        self.im_size = im_size
        self.vae_im_size = vae_im_size
        self.ac_size = ac_size
        self.env = env
        self.LAM = LAM
        self.sess = sess
        self.args = args
        self.pdtype = pdtype = make_pdtype(ac_space)
        self.vis = vis
        self.const_std = const_std
        self.latent_dim = latent_dim
        sequence_length = None
        if normalize:
            with tf.variable_scope("obfilter"):
                self.ob_rms = RunningMeanStd(shape=ob_space.shape)
            
        if "admis" in self.name:
            with tf.variable_scope('vae'):
                quarter = int(vae_im_size[0]/4)
                # mnist and titan
                # min_value = 0
                # max_value = 1
                
                # walking
                min_val = 0.25
                max_val = 2
                
                data_range = max_val - min_val
                # first_layer = 32
                # second_layer = 64
                first_layer = 16
                second_layer = 32
                # print(quarter)

                # Full connected
                with tf.variable_scope('encode'):
                    # x = tf.nn.relu(tf.layers.conv2d(self.vae_im, first_layer, 3, strides=2, padding="same", name="e_1"))
                    # x = tf.nn.relu(tf.layers.conv2d(x, second_layer, 3, strides=2, padding="same", name="e_2"))
                    # self.z_mean = tf.nn.relu(tf.layers.conv2d(x, second_layer, 3, strides=2, padding="same", name="e_2"))
                    
                    x = tf.layers.flatten(self.vae_im)
                    x = tf.nn.relu(tf.layers.dense(x, hid_size, name="e_1"))
                    x = tf.nn.relu(tf.layers.dense(x, hid_size, name="e_2"))
                    x = tf.nn.relu(tf.layers.dense(x, hid_size, name="e_3"))
                    x = tf.nn.relu(tf.layers.dense(x, hid_size, name="e_4"))
                    # x = tf.nn.relu(tf.layers.dense(x, 16, name="e_3"))
                    self.z_mean = tf.nn.tanh(tf.layers.dense(x, self.latent_dim, name="e_z_mean"))

                    # self.z_mean = tf.layers.dense(x, self.latent_dim, name="z_mean")
                    # self.z_log_var = tf.layers.dense(x, self.latent_dim, name="z_log_var")
                    # self.z = reparameterize(self.z_mean, self.z_log_var)

                with tf.variable_scope('decode'):
            
                    x = tf.nn.relu(tf.layers.dense(self.z_mean, hid_size, name="d_1"))
                    x = tf.nn.relu(tf.layers.dense(x, hid_size, name="d_2"))
                    x = tf.nn.relu(tf.layers.dense(x, hid_size, name="d_3"))
                    x = tf.nn.relu(tf.layers.dense(x, hid_size, name="d_4"))
                    # print(x)
                    # exit()
                    self.decoded_logits = tf.reshape(tf.layers.dense(x, np.prod(self.vae_im_size), name="d_final"), [-1] + self.vae_im_size)
                    self.decoded = ((tf.nn.tanh(self.decoded_logits) + 1 )/ 2) 

                    # print(self.decoded)
                    # exit()
                    # last_out = tf.nn.relu(tf.layers.dense(tf.stop_gradient(self.z_mean), quarter * quarter * second_layer, name="d1"))
                    # last_out = tf.nn.relu(tf.layers.dense(self.z_mean, quarter * quarter * second_layer, name="d1"))
                    # last_out = tf.nn.relu(tf.layers.dense(self.z, quarter * quarter * second_layer, name="d1"))
                    # last_out = tf.reshape(last_out, (-1, quarter, quarter, second_layer))

                    # last_out = tf.nn.relu(tf.layers.conv2d_transpose(last_out, second_layer, 3, strides=2, padding='same', name="d2"))
                    # last_out = tf.nn.relu(tf.layers.conv2d_transpose(self.z_mean, second_layer, 3, strides=2, padding='same', name="d2"))
                    # last_out = tf.nn.relu(tf.layers.conv2d_transpose(last_out, first_layer, 3, strides=2, padding='same', name="d3"))
                    # self.decoded_logits = tf.layers.conv2d_transpose(last_out, 1, 3, padding="same", name="d4")
                    # self.decoded = tf.nn.sigmoid(self.decoded_logits)
                    # self.decoded = ((tf.nn.tanh(self.decoded_logits) + 1 )/ 2) * data_range + min_val
                    # self.decoded = ((tf.nn.tanh(self.decoded_logits) + 1 )/ 2) 

                
                # Full conv:
                # with tf.variable_scope('encode'):
                #     x = tf.nn.relu(tf.layers.conv2d(self.vae_im, first_layer, 3, strides=2, padding="same", name="e_1"))
                #     self.z_mean = tf.nn.relu(tf.layers.conv2d(x, second_layer, 3, strides=2, padding="same", name="e_2"))

                # with tf.variable_scope('decode'):
            
                #     last_out = tf.nn.relu(tf.layers.conv2d_transpose(self.z_mean, second_layer, 3, strides=2, padding='same', name="d2"))
                #     last_out = tf.nn.relu(tf.layers.conv2d_transpose(last_out, first_layer, 3, strides=2, padding='same', name="d3"))
                #     self.decoded_logits = tf.layers.conv2d_transpose(last_out, 1, 3, padding="same", name="d4")
                #     self.decoded = ((tf.nn.tanh(self.decoded_logits) + 1 )/ 2) 

                # with tf.variable_scope('decode', reuse=True):
        
                #     last_out = tf.nn.relu(tf.layers.dense(self.z_ph, quarter * quarter * second_layer, name="d1"))
                #     last_out = tf.reshape(last_out, (-1, quarter, quarter, second_layer))
                #     last_out = tf.nn.relu(tf.layers.conv2d_transpose(last_out, second_layer, 3, strides=2, padding='same', name="d2"))
                #     last_out = tf.nn.relu(tf.layers.conv2d_transpose(last_out, first_layer, 3, strides=2, padding='same', name="d3"))
                #     self.decoded_logits_from_ph = tf.layers.conv2d_transpose(last_out, 1, 3, padding="same", name="d4")
                #     # self.decoded_from_ph = tf.nn.sigmoid(self.decoded_logits_from_ph)
                #     # self.decoded_from_ph = ((tf.nn.tanh(self.decoded_logits_from_ph) + 1 )/ 2) * data_range + min_val
                #     self.decoded_from_ph = ((tf.nn.tanh(self.decoded_logits_from_ph) + 1 )/ 2) 
                    # self.decoded_from_ph = (tf.nn.tanh(self.decoded_logits_from_ph) - min_val) / (max_val - min_val)

                # with tf.variable_scope('encode', reuse=True):
                #     # x = tf.nn.relu(tf.layers.conv2d(self.decoded_logits_from_ph, 32, 3, strides=2, padding="same", name="e1"))
                #     x = tf.nn.relu(tf.layers.conv2d(self.decoded_from_ph, first_layer, 3, strides=2, padding="same", name="e_1"))
                #     x = tf.nn.relu(tf.layers.conv2d(x, second_layer, 3, strides=2, padding="same", name="e_2"))
                #     x = tf.layers.flatten(x)
                #     x = tf.nn.relu(tf.layers.dense(x, 16, name="e_3"))
                #     self.z_mean_ph = tf.layers.dense(x, self.latent_dim, name="e_z_mean")
                    # self.z_log_var_ph = tf.layers.dense(x, self.latent_dim, name="z_log_var")
                    # self.z_reparam_ph = reparameterize(self.z_mean_ph, self.z_log_var_ph)

                # with tf.variable_scope('encode', reuse=True):
                #     # x = tf.nn.relu(tf.layers.conv2d(self.decoded_logits_from_ph, 32, 3, strides=2, padding="same", name="e1"))
                #     x = tf.nn.relu(tf.layers.conv2d(self.decoded, first_layer, 3, strides=2, padding="same", name="e_1"))
                #     x = tf.nn.relu(tf.layers.conv2d(x, second_layer, 3, strides=2, padding="same", name="e_2"))
                #     x = tf.layers.flatten(x)
                #     x = tf.nn.relu(tf.layers.dense(x, 16, name="e_3"))
                #     self.z_mean_decoded = tf.layers.dense(x, self.latent_dim, name="e_z_mean")
                    # self.z_log_var_decoded = tf.layers.dense(x, self.latent_dim, name="z_log_var")
                    # self.z_reparam_decoded = reparameterize(self.z_mean_decoded, self.z_log_var_decoded)

                # # if reuse:
                #     tf.get_variable_scope().reuse_variables()
                if "err" in self.args.tests:
                    with tf.variable_scope('error'):  
                        err_x = tf.nn.relu(tf.layers.conv2d(self.vae_im, first_layer, 3, strides=2, padding="same", name="err_1"))
                        err_x = tf.nn.relu(tf.layers.conv2d(err_x, second_layer, 3, strides=2, padding="same", name="err_2"))
                        err_x = tf.layers.flatten(err_x)
                        err_x = tf.nn.relu(tf.layers.dense(err_x, 16, name="err_3"))
                        last_out = tf.nn.tanh(tf.layers.dense(err_x, hid_size, name="err_fc1", kernel_initializer=TF_U.normc_initializer(1.0)))
                        last_out = tf.nn.tanh(tf.layers.dense(last_out, hid_size, name="err_fc2", kernel_initializer=TF_U.normc_initializer(1.0)))
                        self.error_pred = tf.layers.dense(last_out, 1, name='err_final', kernel_initializer=TF_U.normc_initializer(1.0))[:,0]
                        # last_out = tf.nn.tanh(tf.layers.dense(self.z, hid_size, name="fc1", kernel_initializer=TF_U.normc_initializer(1.0)))
                        # last_out = tf.nn.tanh(tf.layers.dense(self.z_mean, hid_size, name="fc1", kernel_initializer=TF_U.normc_initializer(1.0)))
                        # last_out = tf.nn.tanh(tf.layers.dense(last_out, hid_size, name="fc2", kernel_initializer=TF_U.normc_initializer(1.0)))
                        # self.error_pred = tf.layers.dense(last_out, 1, name='final', kernel_initializer=TF_U.normc_initializer(1.0))[:,0]

            with tf.variable_scope('Discriminator'):
                last_out = tf.nn.relu(tf.layers.dense(self.z_mean, hid_size, name='dc_fc1', kernel_initializer=TF_U.normc_initializer(1.0)))
                last_out = tf.nn.relu(tf.layers.dense(last_out, hid_size, name='dc_fc2', kernel_initializer=TF_U.normc_initializer(1.0)))
                self.d_fake = tf.layers.dense(last_out, 1, name='dc_final', kernel_initializer=TF_U.normc_initializer(1.0)) 
            
            self.z_real = tf.placeholder(dtype=tf.float32, shape=[None, self.latent_dim], name='z_real')
            # self.z_real = 
            
            with tf.variable_scope('Discriminator', reuse=True):
                last_out = tf.nn.relu(tf.layers.dense(self.z_real, hid_size, name='dc_fc1', kernel_initializer=TF_U.normc_initializer(1.0)))
                last_out = tf.nn.relu(tf.layers.dense(last_out, hid_size, name='dc_fc2', kernel_initializer=TF_U.normc_initializer(1.0)))
                self.d_real = tf.layers.dense(last_out, 1, name='dc_final', kernel_initializer=TF_U.normc_initializer(1.0)) 
            

            # self.cross_ent = tf.square(self.im - self.decoded_logits)
#             self.cross_ent = tf.square(self.vae_im - self.decoded)
            
            # self.recon = tf.reduce_mean(tf.square(self.vae_im - self.decoded))
            self.recon = tf.reduce_mean(tf.reduce_sum(tf.square(self.vae_im - self.decoded), axis=[1, 2, 3]))
            # self.recon = tf.reduce_mean(tf.image.ssim(self.vae_im, self.decoded, max_val=1.0, filter_size=11,filter_sigma=1.5, k1=0.01, k2=0.03))

            # self.recon = tf.reduce_mean(tf.square(self.vae_im - self.decoded))

            dc_loss_real = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(labels=tf.ones_like(self.d_real), logits=self.d_real))
            
            dc_loss_fake = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(labels=tf.zeros_like(self.d_fake), logits=self.d_fake))
            self.dc_loss = dc_loss_fake + dc_loss_real
            
            self.generator_loss = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(labels=tf.ones_like(self.d_fake), logits=self.d_fake))
            
            # self.recon = -tf.reduce_mean(self.cross_ent, axis=[1, 2, 3])
            # self.mse = -tf.reduce_mean(tf.square(self.im - self.decoded))
#             self.recon_mse = -tf.reduce_mean(self.recon)
            # self.cross_ent = tf.square(self.im - self.decoded)
            # self.logpx_z = -tf.reduce_sum(tf.square(self.im - self.decoded), axis=[1, 2, 3]) + -tf.reduce_sum(tf.square(1-self.im - 1-self.decoded), axis=[1, 2, 3])

            # self.mse = self.logpx_z
            # self.logpz = log_normal_pdf(self.z, 0., 0.)
            # self.logqz_x = log_normal_pdf(self.z, self.z_mean, self.z_log_var)

        # with tf.variable_scope("vis"):  
        # x = tf.nn.relu(TF_U.conv2d(im, 16, "vis_l1", [8, 8], [4, 4], pad="VALID"))
        # x = tf.nn.relu(TF_U.conv2d(x, 32, "vis_l2", [4, 4], [2, 2], pad="VALID"))
        # x = TF_U.flattenallbut0(x)
        # x = tf.nn.tanh(tf.layers.dense(x, 64, name='vis_lin', kernel_initializer=TF_U.normc_initializer(1.0)))
        # x = self.z_mean
        # x = tf.stop_gradient(self.z)
        
        # if "decoded" in self.args.tests:
        #     x = self.z_mean_decoded
        # else:
        #     x = self.z_mean

        # if "decoded" in self.args.tests:
        #     x = self.z_reparam_decoded
        # else:
        #     x = self.z
        x = tf.nn.relu(tf.layers.conv2d(self.im, first_layer, 3, strides=2, padding="same", name="q_vis1"))
        # x = tf.nn.relu(tf.layers.conv2d(self.decoded, first_layer, 3, strides=2, padding="same", name="q_vis1"))
        x = tf.nn.relu(tf.layers.conv2d(x, second_layer, 3, strides=2, padding="same", name="q_vis2"))
        x = tf.layers.flatten(x)
        x = tf.nn.relu(tf.layers.dense(x, 16, name="q_vis3"))

        if normalize:
            obz = tf.clip_by_value((ob - tf.stop_gradient(self.ob_rms.mean)) / tf.stop_gradient(self.ob_rms.std), -5.0, 5.0)
        else:
            obz = ob
        
        with tf.variable_scope('vf'):
            last_out = obz
            last_out = tf.concat(axis=1,values=[last_out, x])

            if self.args.discrete:
                last_out = tf.nn.tanh(tf.layers.dense(last_out, hid_size, name="q_fc1", kernel_initializer=TF_U.normc_initializer(1.0)))
                last_out = tf.nn.tanh(tf.layers.dense(last_out, hid_size, name="q_fc2", kernel_initializer=TF_U.normc_initializer(1.0)))
                
                # if "lam" in self.args.tests:
                #     self.qs1 = tf.layers.dense(last_out, 1, name='final1', kernel_initializer=TF_U.normc_initializer(1.0))
                #     self.qs2 = tf.layers.dense(last_out, 1, name='final2', kernel_initializer=TF_U.normc_initializer(1.0))
                #     # self.qs = tf.concat([self.qs1, self.qs2/(tf.math.maximum(tf.stop_gradient(self.mse)*self.args.lam,1.0))],axis=1)
                #     # self.qs = tf.concat(values=[self.qs1, tf.math.divide(self.qs2, (tf.math.maximum(self.LAM*self.args.lam,1.0)))],axis=1)
                #     # self.qs3 = tf.math.divide(self.qs2, (tf.math.maximum(self.LAM*self.args.lam,1.0)))
                #     # self.qs3 = tf.reshape((tf.math.maximum(self.LAM*self.args.lam,1.0)),[-1,1])
                #     self.qs3 = tf.reshape(self.LAM,[-1,1])
                #     # self.qs = tf.concat(values=[self.qs1, self.qs2/self.qs3], axis=1)
                #     # self.qs = tf.concat(values=[self.qs1, self.qs2], axis=1)
                #     # if "subt" in self.args.tests:
                #     self.qs = tf.concat(values=[self.qs1, tf.math.subtract(self.qs2, self.qs3)], axis=1)
                #     # else:
                #         # self.qs = tf.concat(values=[self.qs1, tf.math.multiply(self.qs2, self.qs3)], axis=1)
                    
                #     # print(self.qs2.shape, self.qs.shape, self.qs3.shape)
                #     # exit()
                # else:
                self.qs = tf.layers.dense(last_out, 2, name='q_final', kernel_initializer=TF_U.normc_initializer(1.0))

                # self.qs = tf.concat(values= [self.qs1, self.qs2/tf.stop_gradient(self.mse)*self.args.lam],axis=1)
                # self.qs = tf.concat(values= [self.qs1, self.qs2],axis=1)
                # self.qs =  tf.convert_to_tensor([self.qs[:,0], self.qs[:,1]]).reshape([-1,2])
                # self.qs =  tf.convert_to_tensor([[self.qs[:,0]],[self.qs[:,1]]])
                
                # self.qs =  tf.reshape(tf.convert_to_tensor([self.qs[:,0],self.qs[:,1] / (self.mse*self.args.lam)]), [-1,2])
                # self.qs =  tf.reshape(tf.convert_to_tensor([self.qs[:,0],self.qs[:,1] / (tf.math.maximum(self.mse*self.args.lam,1.0))]), [-1,2])
                
                # self.qs[:,1] = self.qs[:,1])

                # print(self.qs.shape)
                # exit()

                self.vpred = tf.reduce_max(self.qs, name='q_vpred')
            elif self.args.dual_value:
                last_out1 = tf.nn.tanh(tf.layers.dense(last_out, hid_size, name="fc1_1", kernel_initializer=TF_U.normc_initializer(1.0)))
                last_out1 = tf.nn.tanh(tf.layers.dense(last_out1, hid_size, name="fc2_1", kernel_initializer=TF_U.normc_initializer(1.0)))
                self.vpred1 = tf.layers.dense(last_out1, 1, name='final_1', kernel_initializer=TF_U.normc_initializer(1.0))[:,0]
                last_out2 = tf.nn.tanh(tf.layers.dense(last_out, hid_size, name="fc1_2", kernel_initializer=TF_U.normc_initializer(1.0)))
                last_out2 = tf.nn.tanh(tf.layers.dense(last_out2, hid_size, name="fc2_2", kernel_initializer=TF_U.normc_initializer(1.0)))
                self.vpred2 = tf.layers.dense(last_out2, 1, name='final_2', kernel_initializer=TF_U.normc_initializer(1.0))[:,0]
                self.vpred = tf.minimum(self.vpred1, self.vpred2)
            else:
                last_out = tf.nn.tanh(tf.layers.dense(last_out, hid_size, name="fc1", kernel_initializer=TF_U.normc_initializer(1.0)))
                last_out = tf.nn.tanh(tf.layers.dense(last_out, hid_size, name="fc2", kernel_initializer=TF_U.normc_initializer(1.0)))
                # print(last_out)
                self.vpred = tf.layers.dense(last_out, 1, name='final', kernel_initializer=TF_U.normc_initializer(1.0))[:,0]

        with tf.variable_scope('pol'):
            last_out = obz
            last_out = tf.concat(axis=1,values=[last_out, x])
            last_out = tf.nn.tanh(tf.layers.dense(last_out, hid_size, name='fc1', kernel_initializer=TF_U.normc_initializer(1.0)))
            last_out = tf.nn.tanh(tf.layers.dense(last_out, hid_size, name='fc2', kernel_initializer=TF_U.normc_initializer(1.0)))
            self.mean = tf.layers.dense(last_out, pdtype.param_shape()[0]//2, name='final', kernel_initializer=TF_U.normc_initializer(0.01))
            if self.args.const_std:
                logstd = tf.constant([np.log(0.75)]*int(pdtype.param_shape()[0]//2))
            else:      
                if self.args.std_from_state:
                    logstd = tf.layers.dense(last_out, pdtype.param_shape()[0]//2, name='logstd', kernel_initializer=TF_U.normc_initializer(0.01))
                else:
                    init = tf.constant_initializer([np.log(1.0)]*int(pdtype.param_shape()[0]//2))   
                    logstd = tf.get_variable(name="logstd", shape=[1, pdtype.param_shape()[0]//2],initializer=init)

        pdparam = tf.concat([self.mean, self.mean * 0.0 + logstd], axis=1)
        self.pd = pdtype.pdfromflat(pdparam)

        self.state_in = []
        self.state_out = []
        self.stochastic = tf.placeholder(dtype=tf.bool, shape=())
        # print("SCALING THE ACTION")
        # self.action = TF_U.switch(self.stochastic, self.pd.sample(), self.pd.mode()) 
        # self.action = TF_U.switch(self.stochastic, self.pd.sample(), self.pd.mode()) - tf.clip_by_value(self.mse, 0, 1)
        # self.action = TF_U.switch(self.stochastic, self.pd.sample(), self.pd.mode()) / self.mse
        
        # self.max_lam = 0.01
        # self.max_lam = 0.05
        self.max_lam = 0.1
        # if "lam" in self.args.tests:
        #     self.action = TF_U.switch(self.stochastic, self.pd.sample(), self.pd.mode()) - self.LAM*self.mse
        # else:
        self.action = TF_U.switch(self.stochastic, self.pd.sample(), self.pd.mode()) 
        # self.action = TF_U.switch(self.stochastic, self.pd.sample(), self.pd.mode()) - 0.1
        # self.action = TF_U.switch(self.stochastic, self.pd.sample(), self.pd.mode())

        # self.action = self.pd.sample()
        self.neglogp = self.pd.neglogp(self.action)
        self.state = tf.constant([])
        self.initial_state = None

        self.steps = 0
        self.eps_start = 0.5
        # self.eps_start = 1.0
        self.eps_end = 0.001
        self.eps = self.eps_start
        # self.eps_decay = 100000
        # self.eps_decay = 500000
        # self.eps_decay = 100000
        self.eps_decay = 50000

        self.lam_steps = 0
        self.lam_decay = 100000
        # self.eps_decay = eps_decay
        self.scaled_recon_loss = 0.0
        self.recon_loss = 0.0

        # if "const" in self.args.tests:
        self.lam = self.args.lam
        # self.recon_error_buffer = deque(maxlen=self.args.)
        self.recon_error_buffer = []
        self.recon_thres = 0.0
        self.latent_thres = 0.0
        # else:
            # self.lam = 0.0

        if "setup" in self.name:
            with tf.variable_scope('select'):
                select_hid_size = hid_size
                # if not self.args.share_vis:
                # x = tf.nn.relu(TF_U.conv2d(im, 16, "vis_l1", [8, 8], [4, 4], pad="VALID"))
                # x = tf.nn.relu(TF_U.conv2d(x, 32, "vis_l2", [4, 4], [2, 2], pad="VALID"))
                # x = TF_U.flattenallbut0(x)
                # x = tf.nn.tanh(tf.layers.dense(x, 64, name='vis_lin', kernel_initializer=TF_U.normc_initializer(1.0)))
                # last_out = tf.concat(axis=1,values=[obz, x])
                # last_out = tf.nn.tanh(tf.layers.dense(last_out, select_hid_size, name='fc1', kernel_initializer=TF_U.normc_initializer(1.0)))
                # last_out_select = tf.nn.tanh(tf.layers.dense(last_out, select_hid_size, name='fc2', kernel_initializer=TF_U.normc_initializer(1.0)))
                # else:
                last_out_select = tf.nn.tanh(tf.layers.dense(last_out, select_hid_size, name='fc2', kernel_initializer=TF_U.normc_initializer(1.0)))

                high = np.inf*np.ones(1)
                low = -high
                select_ac_space = spaces.Box(low, high, dtype=np.float32)
                self.select_pdtype = select_pdtype = make_pdtype(select_ac_space)
                # last_out = tf.nn.tanh(tf.layers.dense(tf.stop_gradient(last_out), hid_size, name='fc', kernel_initializerTF_=U.normc_initializer(1.0)))
                self.select_mean = tf.layers.dense(last_out_select, 1, name="select_final", kernel_initializer=TF_U.normc_initializer(0.01))
                # self.select_pdtype = select_pdtype = CategoricalPdType(2)
                
                select_init = tf.constant_initializer([np.log(1.0)]*int(select_pdtype.param_shape()[0]//2))
                select_logstd = tf.get_variable(name="select_logstd", shape=[1, select_pdtype.param_shape()[0]//2],initializer=select_init)
                select_pdparam = tf.concat([self.select_mean, self.select_mean * 0.0 + select_logstd], axis=1)
                self.select_pd = pdtype.pdfromflat(select_pdparam)

                stochastic_act = self.select_pd.sample()
                determinstic_act = self.select_pd.mode()
            
                self.select = TF_U.switch(self.stochastic, stochastic_act, determinstic_act)
                self.neglogp_select = self.select_pd.neglogp(self.select)
        

    def decode_from_ph(self, z):
        decoded = self.sess.run(self.decoded_from_ph,feed_dict={self.z_ph:z})
        return decoded

    def encode(self, im):
        z_mean, z_log_var = self.sess.run([self.z_mean, self.z_log_var], feed_dict={self.im:im})
        return z_mean, z_log_var
    
    def decode(self, im):
        decoded = self.sess.run(self.decoded, feed_dict={self.im:im})
        return decoded


    def update_eps(self):
        self.eps =  max(self.eps_start - (float(self.steps) / (self.eps_decay)), self.eps_end)
        self.steps += 1

    # def update_lam(self):
        # self.eps =  max(self.eps_start - (float(self.steps) / (self.eps_decay)), self.eps_end)
        # if (self.lam_decay > self.lam_steps):
        #     self.lam =  min((float(self.lam_decay) / (self.lam_decay - self.lam_steps)), self.args.lam) - 1.0
        # else:
        #     self.lam =  self.args.lam - 1.0
        # self.lam_steps += 1
        # self.lam = self.lam_steps

    def step(self, ob, im, stochastic=False):

        if self.args.train_vae:
            # Just care about the region in front of the robot
            # print(im.shape[0], self.im_size[0])
            # orig_im_size = im.shape[0]
            im_copy = im.copy()
            vae_im = im[:self.vae_im_size[0],int((self.im_size[0]-self.vae_im_size[0])/2):int(self.im_size[0]-(self.im_size[0]-self.vae_im_size[0])/2),:]
            # if not stochastic:
            #     self.lam = self.max_lam
            if self.args.discrete:
                if "err" in self.args.tests:
                    self.recon_loss, self.recon_loss_pred = self.sess.run([self.recon, self.error_pred], feed_dict={ self.vae_im: vae_im.reshape([1]+self.vae_im_size)})
                else:
                    self.recon_loss = self.sess.run([self.recon], feed_dict={ self.vae_im: vae_im.reshape([1]
                    +self.vae_im_size)})
                    self.recon_loss_pred = [0]
                # print(mse, self.recon_loss, self.recon_loss_pred)
                # if "const" in self.args.tests:
                #     # self.recon_loss = mse*self.args.lam
                #     self.scaled_recon_loss = [self.recon_loss[0]*self.args.lam]
                # else:
                    # print(self.lam)
                    # if self.steps > 200000:
                        # self.update_lam()
                    # print(self.steps, self.lam)
                # self.scaled_recon_loss = [self.recon_loss[0]*self.lam]
                # self.scaled_recon_loss = [50.0]
                # self.scaled_recon_loss = [min(self.args.lam*self.recon_loss[0], 10)]
                # print(self.recon_loss, self.recon_loss_pred[0])
                if "err" in self.args.tests:
                    self.scaled_recon_loss = self.args.lam * min(abs(self.recon_loss[0] - self.recon_loss_pred[0]), 50)
                else:
                    # self.scaled_recon_loss = min(self.args.lam * abs(self.recon_loss[0]), 50)
                    self.scaled_recon_loss = self.args.lam * (self.recon_loss[0]*self.recon_loss[0])
                # self.scaled_recon_loss = [self.recon_loss[0]]
                # print(self.scaled_recon_loss)
                # print(self.scaled_recon_loss)
                    # self.recon_loss = [mse[0] - self.lam]
                    # self.recon_loss = np.exp(-self.args.lam*mse)
                # print("recon_loss", self.recon_loss)
                q, v, state, neglogp, recon_im = self.sess.run([self.qs, self.vpred, self.state, self.neglogp, self.decoded], 
                                    feed_dict={self.ob: ob.reshape([1,self.ob_size]), 
                                                self.im: im.reshape([1]+self.im_size), 
                                                self.vae_im: vae_im.reshape([1]+self.vae_im_size), 
                                                self.stochastic:stochastic})
                                                # self.LAM:self.scaled_recon_loss})
                # output_im = self.decode(im.reshape([-1] + self.im_size))
                # # def plot_sample(data, n=30, figsize=15):
                # # print(output_im[0].shape)
                # cv2.imshow('frame', output_im[0])
                # cv2.waitKey(1)
                # print(q, v)
                # new_qs = [q[0][0], q[0][1] - self.scaled_recon_loss]
                new_qs = q[0]
                # im1 = tf.image.convert_image_dtype(im1, tf.float32)
                # im2 = tf.image.convert_image_dtype(im2, tf.float32)
                im_copy[:self.vae_im_size[0],int((self.im_size[1]-self.vae_im_size[1])/2):int(self.im_size[1]-(self.im_size[1]-self.vae_im_size[1])/2),:] = recon_im[0,::]
                # ssim = tf.image.ssim(im, im_copy, max_val=1.0, filter_size=11,filter_sigma=1.5, k1=0.01, k2=0.03)
                # print(ssim, self.recon_mse_loss)
                # print(self.recon_mse_loss)
                self.update_eps()
                self.recon_error_buffer.append(self.recon_loss[0])
                if np.random.random() > self.eps or not stochastic: 

                    # if "lam" in self.args.tests:
                    #     # new_qs = [q[0], q[1] - recon_loss]
                    #     # print(q, recon_loss)
                    #     # new_qs = [[q[0][0], q[0][1] / tf.stop_gradient(recon_loss)]]
                    #     # if "min" in self.args.tests:
                    #     #     new_qs = [[q[0][0], q[0][1] - (recon_loss*10*self.args.lam)]]
                    #     # elif "div" in self.args.tests:
                    #     #     new_qs = [[q[0][0], q[0][1] / (max(recon_loss*self.args.lam,1.0))]]
                    #         # new_qs = [[q[0][0], q[0][1] / (max(recon_loss/10,1.0))]]
                    #     # new_qs = [[q[0][0], q[0][1] / (recon_loss/10)]]
                    #     # act = np.argmax(new_qs)
                    #     act = np.argmax(qs)
                    #     # print(new_qs, act)
                    # else:
                    # print(q, self.scaled_recon_loss)
                    # This is where we apply the thresholding.
                    if not stochastic:
                        # print(self.recon_loss)
                        act_from_q = np.argmax(new_qs)
                        if act_from_q == 1 and self.recon_loss[0] < self.recon_thres:
                            act = 1
                        else:
                            act = 0
                    else:
                        act = np.argmax(new_qs)
                    # print(new_qs)
                    # if new_qs[0] + 20 < new_qs[1]:
                    #     act = 1
                    # else:
                    #     act = 0
                    # act = np.argmax(new_qs)
                    # act = np.argmax(q)
                else:
                    
                    act = np.random.randint(2)

                    # if "rand" in self.args.tests:
                    #     act = np.random.randint(2)
                    # else:
                    #     if self.env.get_terrain("current") == self.args.obstacle_type or (self.env.get_terrain("previous") == self.args.obstacle_type and self.env.get_terrain("current") == "flat" and self.env.x_min > (self.env.box_info[1][self.env.box_num][0] - self.env.box_info[2][self.env.box_num][0]) and (not self.env.ob_dict["right_foot_left_ground"] and not self.env.ob_dict["left_foot_left_ground"])):
                    #         act = 1
                    #     else: 
                    #         act = 0
                # return [act], v, state, neglogp[0], recon_im, recon_loss*self.max_lam
                
                # print(im_copy.shape, recon_im.shape, orig_im_size, self.im_size[0])
                # print(self.im_size, self.vae_im_size)

                # display_images(im, im_copy)
                
                return [act], v, state, neglogp[0], im_copy, self.scaled_recon_loss, self.recon_loss[0], np.clip(new_qs,0.0, None), self.recon_loss_pred
            else:
                print("NOT USING DISCRETE")
            #     a, v, state, neglogp, recon_im, recon_loss = self.sess.run([self.action, self.vpred, self.state, self.neglogp, self.decoded, self.mse], 
            #                         feed_dict={self.ob: ob.reshape([1,self.ob_size]), 
            #                                     self.im: im.reshape([1]+self.im_size), 
            #                                     self.stochastic:stochastic, 
            #                                     self.LAM:self.lam})
            #     # output_im = self.decode(im.reshape([-1] + self.im_size))
            #     # # def plot_sample(data, n=30, figsize=15):
            #     # # print(output_im[0].shape)
            #     # cv2.imshow('frame', output_im[0])
            #     # cv2.waitKey(1)
            #     if "lam" in self.args.tests:
            #         a = [a[0] - 0.005*recon_loss]
            #     return a[0], v[0], state, neglogp[0], recon_im, recon_loss*self.max_lam
        else:
            if "setup" in self.name:
                a, v, state, neglogp, select_raw, neglogp_select = self.sess.run([self.action, self.vpred, self.state, self.neglogp, self.select, self.neglogp_select], feed_dict={self.ob: ob.reshape([1,self.ob_size]), self.im: im.reshape([1]+self.im_size), self.stochastic:stochastic})
                return a[0], v[0], state, neglogp[0], select_raw[0], neglogp_select[0]
            else:
                a, v, state, neglogp = self.sess.run([self.action, self.vpred, self.state, self.neglogp], 
                                    feed_dict={self.ob: ob.reshape([1,self.ob_size]), 
                                                self.im: im.reshape([1]+self.im_size), 
                                                self.stochastic:stochastic})
                return a[0], v[0], state, neglogp[0]

class Model(Base):
    def __init__(self, name, env, ac_size, ob_size, im_size=[48,48,1], vae_im_size=[48,48,1], args=None, PATH=None, writer=None, hid_size=256, vis=False, normalize=True, ent_coef=0.0, vf_coef=0.5, max_grad_norm=0.5, mpi_rank_weight=1, max_timesteps=int(1e6), lr=3e-4, horizon=2048, batch_size=32, const_std=False):
        self.max_timesteps = max_timesteps
        self.horizon = horizon
        self.batch_size = batch_size
        self.learning_rate = lr
        self.env = env
        self.ob_size = ob_size
        self.im_size = im_size
        self.vae_im_size = vae_im_size
        self.ac_size = ac_size

        self.sess = sess = TF_U.get_session()
        self.name = name
        high = np.inf*np.ones(ac_size)
        low = -high
        ac_space = spaces.Box(low, high, dtype=np.float32)
        high = np.inf*np.ones(ob_size)
        low = -high
        ob_space = spaces.Box(low, high, dtype=np.float32)
        self.args = args   
        im_size = im_size
        self.PATH = PATH
        self.hid_size = hid_size
        self.writer = writer
        # self.latent_dim = latent_dim = 16
        self.latent_dim = latent_dim = 32
        # self.latent_dim = latent_dim = 2
        # self.latent_dim = latent_dim = 64
        ob = TF_U.get_placeholder(name="ob_" + name, dtype=tf.float32, shape=[None] + list(ob_space.shape))
        im = TF_U.get_placeholder(name="im_" + name, dtype=tf.float32, shape=[None] + im_size)
        vae_im = TF_U.get_placeholder(name="vae_im_" + name, dtype=tf.float32, shape=[None] + vae_im_size)
        z_ph = TF_U.get_placeholder(name="z_ph_" + name, dtype=tf.float32, shape=[None, self.latent_dim])
        self.LAM = TF_U.get_placeholder(name="lam_" + self.name, dtype=tf.float32, shape=[None])
        
        self.args = args
        if ('vel' in name or 'hex' in name) and not args.vis:
            self.vis = False
        else:
            self.vis = vis
        with tf.variable_scope(name, reuse=tf.AUTO_REUSE):
            # Need some more params when using recurrent
            # act_model = Policy(ob, im, ob_space, im_size, ac_space, sess, self.vis, normalize=normalize, hid_size=hid_size)
            train_model = Policy(name, ob, im, vae_im, z_ph, ob_space, ob_size, im_size, vae_im_size, ac_size, env, self.LAM, ac_space, self.latent_dim, sess, self.vis, args=args, normalize=normalize, hid_size=hid_size, const_std=const_std)
        Base.__init__(self)

        # CREATE THE PLACEHOLDERS
        self.A = A = train_model.pdtype.sample_placeholder([None])
        self.ADV = ADV = tf.placeholder(tf.float32, [None])
        self.R = R = tf.placeholder(tf.float32, [None])
        # Keep track of old actor
        self.OLDNEGLOGPAC = OLDNEGLOGPAC = tf.placeholder(tf.float32, [None])
        # Keep track of old critic
        self.OLDVPRED = OLDVPRED = tf.placeholder(tf.float32, [None])
        self.LR = LR = tf.placeholder(tf.float32, [])
        # Cliprange
        self.CLIPRANGE = CLIPRANGE = tf.placeholder(tf.float32, [])

        neglogpac = train_model.pd.neglogp(A)

        # Calculate the entropy
        # Entropy is used to improve exploration by limiting the premature convergence to suboptimal policy.
        entropy = tf.reduce_mean(train_model.pd.entropy())


        if self.args.discrete:
            # self.ACTION = tf.placeholder(tf.float32, [None, 2])
            self.ACTION = tf.placeholder(tf.int32, [None,1])
            one_hot = tf.one_hot(tf.squeeze(self.ACTION), 2)
            # current_q_t = tf.reduce_sum(train_model.vpred * one_hot, 1)
            current_q_t = tf.reduce_sum(train_model.qs * one_hot, 1)
            vf_loss = 0.5*tf.reduce_mean(tf.square(current_q_t - self.R))
        # Get the predicted value
        elif self.args.dual_value:
            vf_loss1 = 0.5*tf.reduce_mean(tf.square(train_model.vpred1 - R))
            vf_loss2 = 0.5*tf.reduce_mean(tf.square(train_model.vpred2 - R))
            vf_loss = vf_loss1 + vf_loss2
        else:
            vf_loss = 0.5*tf.reduce_mean(tf.square(train_model.vpred - R))
        # print(vf_loss, current_q_t, R)
        # Calculate ratio (pi current policy / pi old policy)
        ratio = tf.exp(OLDNEGLOGPAC - neglogpac)

        # Defining Loss = - J is equivalent to max J
        pg_losses = -ADV * ratio
        pg_losses2 = -ADV * tf.clip_by_value(ratio, 1.0 - CLIPRANGE, 1.0 + CLIPRANGE)

        # Final PG loss
        pg_loss = tf.reduce_mean(tf.maximum(pg_losses, pg_losses2))
        approxkl = .5 * tf.reduce_mean(tf.square(neglogpac - OLDNEGLOGPAC))
        clipfrac = tf.reduce_mean(tf.to_float(tf.greater(tf.abs(ratio - 1.0), CLIPRANGE)))

        if "admis" in self.name:
            # self.kl_loss = 0.01*-tf.reduce_mean((train_model.logpz - train_model.logqz_x))
            # self.vae_loss = -tf.reduce_mean(train_model.logpx_z) 
            # self.recon_loss = train_model.mse*self.args.lam
            # self.vae_loss = self.recon_loss + self.kl_loss
            # self.vae_loss = self.recon_loss
            
            # self.pred_loss = 0.5*tf.reduce_mean(tf.square(train_model.error_pred - train_model.recon))
            # self.pred_loss = 0.5*tf.reduce_mean(tf.square(self.LAM - train_model.recon))
            # self.pred_loss = 0.5*tf.reduce_mean(tf.square(train_model.error_pred - self.LAM))
            # self.pred_loss = 0.5*tf.reduce_mean(tf.square(train_model.vpred - self.LAM))
            # self.pred_loss = 0.5*tf.reduce_mean(tf.square(train_model.error_pred - train_model.logpx_z))
            # print(train_model.error_pred, train_model.vpred, self.LAM)
            # exit()
            # self.vae_loss = train_model.recon + self.pred_loss + self.kl_loss
#             if "err" in self.args.tests:
#                 self.vae_loss = train_model.recon_mse + self.pred_loss 
#             else:
#                 self.vae_loss = train_model.recon_mse 
                
                
            if "err" in self.args.tests:
                self.pred_loss = 0.5*tf.reduce_mean(tf.square(train_model.error_pred - self.LAM))
                self.vae_loss = train_model.recon + self.pred_loss 
            else:
                self.vae_loss = train_model.recon

            # self.vae_loss = self.pred_loss

            # self.total_vae_loss = self.vae_loss
            # loss = pg_loss - entropy * self.args.ent_coef + vf_loss + self.total_vae_loss
            if self.args.discrete:
                if "decoded" in args.tests:
                    loss = vf_loss + self.vae_loss
                else:
                    loss = vf_loss 
            else:
                loss = pg_loss - entropy * self.args.ent_coef + vf_loss 
            
            # self.vae_loss = self.pred_loss
            # loss = self.pred_loss
                    
            std = tf.reduce_mean(train_model.pd.std)
            mean_ratio = tf.reduce_mean(ratio)
            mean_adv = tf.reduce_mean(self.ADV) 
            # if self.args.train_vae:
            #     self.loss_names = ['policy_loss', 'value_loss', 'vae_loss', 'policy_entropy', 'approxkl', 
            #     'clipfrac', 'std', 'ratio', 'adv', 'cliprange', 'learning_rate']
            #     self.stats_list = [pg_loss, vf_loss, vae_loss, entropy*self.args.ent_coef, approxkl, clipfrac, std, mean_ratio, mean_adv, CLIPRANGE, LR]
            all_variables = tf.trainable_variables()
            ae_var = [var for var in all_variables if 'ae' in var.name]
            dc_var = [var for var in all_variables if 'dc_' in var.name]
            en_var = [var for var in all_variables if 'e_' in var.name]
            q_var = [var for var in all_variables if 'q_' in var.name]

            self._train_op_vae = self.get_train_ops(self.vae_loss, name + "/vae", params=ae_var)
            self._train_op_dis = self.get_train_ops(train_model.dc_loss, name + "/dis", params=dc_var)
            self._train_op_gen = self.get_train_ops(train_model.generator_loss, name + "/gen", params=en_var)
            self._train_op = self.get_train_ops(loss, name, params=q_var)

            # self.loss_names = ['policy_loss', 'value_loss', 'policy_entropy', 'approxkl','clipfrac', 'std', 'ratio', 'adv', 'cliprange', 'learning_rate', 'vae_total_loss', 'vae_kL_loss', 'vae_mse']
            self.loss_names = ['policy_loss', 'value_loss', 'policy_entropy', 'approxkl','clipfrac', 'std', 'ratio', 'adv', 'cliprange', 'learning_rate', 'vae_total_loss', 'vae_recon', 'vae_dis_loss', 'vae_gen_loss']
            # self.stats_list = [pg_loss, vf_loss, entropy*self.args.ent_coef, approxkl, clipfrac, std, mean_ratio, mean_adv, CLIPRANGE, LR, self.total_vae_loss, self.vae_loss, self.recon_loss]
            if "decoded" in self.args.tests:
                # self.stats_list = [pg_loss, vf_loss, entropy*self.args.ent_coef, approxkl, clipfrac, std, mean_ratio, mean_adv, CLIPRANGE, LR, self.vae_loss, self.kl_loss]
                self.stats_list = [pg_loss, vf_loss, entropy*self.args.ent_coef, approxkl, clipfrac, std, mean_ratio, mean_adv, CLIPRANGE, LR, self.vae_loss]
            else:                
                self.stats_list = [pg_loss, vf_loss, entropy*self.args.ent_coef, approxkl, clipfrac, std, mean_ratio, mean_adv, CLIPRANGE, LR]
                # self.vae_stats_list = [self.vae_loss, self.kl_loss, train_model.recon_mse]
                self.vae_stats_list = [self.vae_loss, train_model.recon, train_model.dc_loss, train_model.generator_loss]

        else:
            loss = pg_loss - entropy * self.args.ent_coef + vf_loss
                
            std = tf.reduce_mean(train_model.pd.std)
            mean_ratio = tf.reduce_mean(ratio)
            mean_adv = tf.reduce_mean(self.ADV)
        
            self.loss_names = ['policy_loss', 'value_loss', 'policy_entropy', 'approxkl', 
            'clipfrac', 'std', 'ratio', 'adv', 'cliprange', 'learning_rate']
            self.stats_list = [pg_loss, vf_loss, entropy*self.args.ent_coef, approxkl, clipfrac, std, mean_ratio, mean_adv, CLIPRANGE, LR]
        
            self._train_op = self.get_train_ops(loss, self.name)

        # UPDATE THE PARAMETERS USING LOSS
        
        # params = tf.trainable_variables(name)
        # # 2. Build our trainer
        # with tf.variable_scope(name, reuse=tf.AUTO_REUSE):
        #     self.trainer = MpiAdamOptimizer(comm, learning_rate=LR, mpi_rank_weight=mpi_rank_weight, epsilon=1e-5)

        # # 3. Calculate the gradients
        # with tf.variable_scope(name, reuse=tf.AUTO_REUSE):
        #     grads_and_var = self.trainer.compute_gradients(loss, params)
        #     grads, var = zip(*grads_and_var)
        
        # grads, _grad_norm = tf.clip_by_global_norm(grads, max_grad_norm)
        
        # grads_and_var = list(zip(grads, var))
        # # zip aggregate each gradient with parameters associated
        # # For instance zip(ABCD, xyza) => Ax, By, Cz, Da

        # self.grads = grads
        # self.var = var
        # with tf.variable_scope(name, reuse=tf.AUTO_REUSE):
        #     self._train_op = self.trainer.apply_gradients(grads_and_var)

        self.step = train_model.step

        self.train_model = train_model
        self.init_buffer()

    def get_train_ops(self, loss, name, params=None, mpi_rank_weight=1, max_grad_norm=0.5): 
        if params is None:
            params = tf.trainable_variables(name)
        # print(name)
        # print(params)
        # 2. Build our trainer
        with tf.variable_scope(name, reuse=tf.AUTO_REUSE):
            self.trainer = MpiAdamOptimizer(comm, learning_rate=self.LR, mpi_rank_weight=mpi_rank_weight, epsilon=1e-5)
        # 3. Calculate the gradients
        with tf.variable_scope(name, reuse=tf.AUTO_REUSE):
            grads_and_var = self.trainer.compute_gradients(loss, params)
            grads, var = zip(*grads_and_var)
        grads, _grad_norm = tf.clip_by_global_norm(grads, max_grad_norm)
        
        grads_and_var = list(zip(grads, var))
        # zip aggregate each gradient with parameters associated
        # For instance zip(ABCD, xyza) => Ax, By, Cz, Da

        self.grads = grads
        self.var = var
        with tf.variable_scope(name, reuse=tf.AUTO_REUSE):
            train_op = self.trainer.apply_gradients(grads_and_var)
        return train_op

    def train_vae(self, epoch, lr, cliprange, imgs, lam=None):
        # print(lam)
        td_map = {
        self.LR : lr,
        self.train_model.vae_im:imgs,
        # self.train_model.z_real_dist = np.random.randn(batch_size, z_dim) * 5.
        # self.train_model.z_real: np.random.randn(imgs.shape[0], self.latent_dim)
        self.train_model.z_real: np.random.randn(imgs.shape[0], self.latent_dim) * 5.
        # self.train_model.ob:ob,
        }
        if lam is not None:
            td_map[self.train_model.LAM] = lam
        # vae_out = self.sess.run(self.vae_stats_list + [self._train_op_vae],td_map)[:-1]
        vae_out = self.sess.run(self._train_op_vae,td_map)
        if "adv" in self.args.tests:
            dis_out = self.sess.run(self._train_op_dis,td_map)
            gen_out = self.sess.run(self._train_op_gen,td_map)
        # print(dis_out, gen_out)
        return  self.sess.run(self.vae_stats_list, td_map)


    def train(self, epoch, lr, cliprange, obs, imgs, returns, masks, actions, values, neglogpacs, lam=None,  exp_actions=None, states=None):
        # Here we calculate advantage A(s,a) = R + yV(s') - V(s)
        # Returns = R + yV(s')
        # print(lam.shape)
        advs = returns - values
        # Normalize the advantages
        advs = (advs - advs.mean()) / (advs.std() + 1e-8)
        td_map = {
        self.train_model.ob : obs,
        self.A : actions,
        self.ADV : advs,
        self.R : returns,
        self.LR : lr,
        self.CLIPRANGE : cliprange,
        self.OLDNEGLOGPAC : neglogpacs,
        self.OLDVPRED : values
        # self.train_model.LAM : self.train_model.lam
        }
        td_map[self.train_model.im] = imgs
        td_map[self.train_model.vae_im] = imgs
        
        if "lam" in self.args.tests:
            td_map[self.train_model.LAM] = lam
            
        if self.args.discrete:
            td_map[self.ACTION] = actions
        if states is not None:
            td_map[self.train_model.S] = states
            td_map[self.train_model.M] = masks
            
        return self.sess.run(self.stats_list + [self._train_op],td_map)[:-1]

    def run_train(self, data, last_value, last_done):
        self.finalise_buffer(data, last_value, last_done)
        self.train_model.ob_rms.update(self.data['ob'])
        if self.args.const_lr:
            self.cur_lrmult =  1.0
        else:
            self.cur_lrmult =  max(1.0 - float(self.timesteps_so_far) / self.max_timesteps, 0)
       
        # self.train_model.lam =  min(float(self.timesteps_so_far) * self.train_model.max_lam / (self.max_timesteps), self.train_model.max_lam)
        # if "const" not in self.args.tests and self.iters_so_far > 100:
            # self.train_model.lam =  min(float((self.iters_so_far - 100) / 10 ) , self.args.lam)
        # self.train_model.lam =  0.0
       
        lrnow = self.learning_rate*self.cur_lrmult
        self.lr = lrnow
        cliprangenow = 0.2
        self.n = self.data['done'].shape[0]
        inds = np.arange(self.n)
        # print(self.epochs)
        t1 = time.time()
        if "decoded" not in self.args.tests:
            if "pre" in self.args.tests:
                if self.iters_so_far < 10:    
                    vae_epochs = 10
                    # vae_epochs = 5
                    # vae_epochs = 1
                else:
                    vae_epochs = 1
            else:
                vae_epochs = 1
            # vae_epochs = 0
            vae_loss = [0,0,0]
            if self.args.train_vae:
                if "roi" in self.args.tests:
                    vae_images = self.data["im"][:,:self.vae_im_size[0],int((self.im_size[0]-self.vae_im_size[1])/2):int(self.im_size[1]-(self.im_size[1]-self.vae_im_size[1])/2),:]
                else:
                    vae_images = self.data["im"]
                if "pos" in self.args.tests:
                    if self.iters_so_far == 0:
                        self.vae_images = vae_images
                        self.vae_images_idx = 0
                    else:
                        image_idx = np.where(self.data["ac"] == 1)[0]
                        # print(self.data["ac"][:10])
                        # print(image_idx[:10])

                        # print(self.data["im"].shape, len(image_idx))
                        # vae_images = self.data["im"][image_idx,::]
                        if len(image_idx) + self.vae_images_idx >= self.vae_images.shape[0]:
                            initial = self.vae_images.shape[0] - self.vae_images_idx
                            final =  len(image_idx) + self.vae_images_idx - self.vae_images.shape[0]
                            # print("idx ",self.vae_images_idx," initial ", initial," final ",  final, " len ", len(image_idx))
                            # print()
                            self.vae_images[self.vae_images_idx:,::] = vae_images[image_idx[:initial]]
                            self.vae_images[:final,::] = vae_images[image_idx[initial:]]
                            self.vae_images_idx = final
                        else:
                            # print("idx ", self.vae_images_idx, " final ",  len(image_idx) + self.vae_images_idx)
                            self.vae_images[self.vae_images_idx:len(image_idx)+self.vae_images_idx,::] = vae_images[image_idx]
                            self.vae_images_idx += len(image_idx) 

                        # print(self.data["im"].shape, vae_images.shape)
                        # Image crop
                        # new_w = 32
                
                        # new_w = 48
                        # if "roi" in self.args.tests:
                        #     old_w = self.im_size[0]
                        #     new_w = self.vae_im_size[0]
                            # for im in self.vae_images:
                            #     z_im = np.zeros_like(im)
                            #     # print(im.shape, int((48-32)/2), int(48 - (48-32)/2))
                                
                            #     z_im[:new_w, int((old_w-new_w)/2):int(old_w-(old_w-new_w)/2),0] = im[:new_w,int((old_w-new_w)/2):int(old_w-(old_w-new_w)/2),:]

                            #     im = cv2.resize(im,dsize=(int(im.shape[1] * 6), int(im.shape[0] * 6)))
                            #     z_im = cv2.resize(z_im,dsize=(int(z_im.shape[1] * 6), int(z_im.shape[0] * 6)))
                            #     cv2.imshow("frame", np.hstack([im,z_im]))
                            #     cv2.waitKey(1)
                            # vae_images = vae_images[:,:self.vae_im_size[0],int((self.im_size[0]-self.vae_im_size[0])/2):int(self.im_size[0]-(self.im_size[0]-self.vae_im_size[0])/2),:]
                else:
                    self.vae_images = vae_images
                vae_inds = [i for i in range(self.vae_images.shape[0])]
                for epoch in range(vae_epochs):
                    if self.enable_shuffle:
                        np.random.shuffle(vae_inds)
                        # np.random.shuffle(inds)
                        # np.random.shuffle(self.n)
                    self.loss = [] 
                    # for start in range(0, vae_images.shape[0]//self.batch_size, self.batch_size):
                    for start in range(0, self.n, self.batch_size):
                        end = start + self.batch_size
                        # print(epoch, start, end)
                        
                        mbinds = vae_inds[start:end]
                        if "err" in self.args.tests:
                            vae_loss = self.train_vae(epoch, lrnow, cliprangenow, self.vae_images[mbinds], self.data["lam"][mbinds])
                        else:
                            vae_loss = self.train_vae(epoch, lrnow, cliprangenow, self.vae_images[mbinds])
                            
                            # slices = (vae_images[mbinds])
                            
                            # mbinds = inds[start:end]
                            # slices = (self.data[key][mbinds] for key in ["im"])
                            # vae_loss = self.train_vae(epoch, lrnow, cliprangenow, *slices)
                            
                            # print(outs)
                            # self.loss.append(outs[:len(self.loss_names)])
                    # print(vae_loss)
                    # sync_from_root(sess, variables)
                # else:
                #     for epoch in range(vae_epochs):
                #         if self.enable_shuffle:
                #             np.random.shuffle(inds)
                #         self.loss = [] 
                #         for start in range(0, self.n, self.batch_size):
                #             end = start + self.batch_size
                            
                #             mbinds = inds[start:end]
                #             slices = (self.data[key][mbinds] for key in ["im", "lam"])
                #             vae_loss = self.train_vae(epoch, lrnow, cliprangenow, *slices)
        else:
            self.epochs = 5               
       
        # print("mse", vae_loss[-1])
        # self.train_model.recon_error_buffer.append(vae_loss[-1])
        # self.train_model.recon_thres = np.percentile(self.train_model.recon_error_buffer, 90)
        # all_maxes = MPI.COMM_WORLD.allgather(max(self.train_model.recon_error_buffer)) # list of tuples
        ninety = np.percentile(self.train_model.recon_error_buffer, 90)
        all_maxes = MPI.COMM_WORLD.allgather(ninety) # list of tuples
        # print(self.train_model.recon_error_buffer)
        # print(all_maxes)
        # print(max(all_maxes))
        self.train_model.recon_thres = max(all_maxes)
        self.train_model.recon_error_buffer = []
        # print(max(self.train_model.recon_error_buffer), self.train_model.recon_thres)


        
        for epoch in range(self.epochs):
            if self.enable_shuffle:
                np.random.shuffle(inds)
            # self.loss = [] 
            for start in range(0, self.n, self.batch_size):
                end = start + self.batch_size
                # print(epoch, start, end)
                mbinds = inds[start:end]
                
                # im_slices = (self.data[key][mbinds] for key in ["im"])
                # vae_loss = self.train_vae(epoch, lrnow, cliprangenow, *im_slices)
                
                slices = (self.data[key][mbinds] for key in self.training_input)
                # print([self.data[key][mbinds].shape for key in self.training_input])
                # print( self.train(epoch, lrnow, cliprangenow, vf_only, *slices))
                

                outs = self.train(epoch, lrnow, cliprangenow, *slices)
                self.loss = outs[:len(self.loss_names)]
        

        # print("train", time.time()- t1)
        # self.loss = np.mean(self.loss, axis=0)
        if "decoded" not in self.args.tests:
            self.loss = self.loss + vae_loss
        # self.log_stuff(things={"Eps":self.train_model.eps, "vae_lam":self.train_model.lam, "vae_scaled_recon":self.train_model.scaled_recon_loss, "vae_recon_loss": self.train_model.recon_loss,"vae_recon_thres": self.train_model.recon_thres, "vae_recon_loss_pred":self.train_model.recon_loss_pred[0]})
        self.log_stuff(things={"Eps":self.train_model.eps, "vae_lam":self.train_model.lam, "vae_scaled_recon":self.train_model.scaled_recon_loss, "vae_recon_thres": self.train_model.recon_thres, "vae_recon_loss_pred":self.train_model.recon_loss_pred[0]})
        self.evaluate(data['ep_rets'], data['ep_lens'])

        if "setup" in self.name:
            self.re_init_buffer()
        else:
            self.init_buffer()
        
    # def step(self, ob, im, stochastic=False, multi=False): 
    #     if not multi:
    #         actions, values, self.states, neglogpacs =  self.train_model.step(ob[None], im[None], stochastic=stochastic)
    #         return actions[0], values, self.states, neglogpacs
    #     else:
    #         actions, values, self.states, neglogpacs =  self.train_model.step(ob, im, stochastic=stochastic)
    #         return actions, values, self.states, neglogpacs        

    def get_advantage(self, last_value, last_done):    
        gamma = 0.99; lam = 0.95
        advs = np.zeros_like(self.data['rew'])
        lastgaelam = 0
        for t in reversed(range(len(self.data['rew']))):
            if t == len(self.data['rew']) - 1:
                nextnonterminal = 1.0 - last_done
                nextvalues = last_value
            else:
                nextnonterminal = 1.0 - self.data['done'][t+1]
                nextvalues = self.data['value'][t+1]
            delta = self.data['rew'][t] + gamma * nextvalues * nextnonterminal - self.data['value'][t]
            advs[t] = lastgaelam = delta + gamma * lam * nextnonterminal * lastgaelam
        self.data['return'] = advs + self.data['value']
    
    def init_buffer(self):
        if "lam" in self.args.tests:
            self.data_input = ['ob', 'im', 'ac', 'rew', 'done', 'value', 'neglogpac', "lam"]
            self.training_input = ['ob', 'im', 'return', 'done', 'ac', 'value', 'neglogpac', "lam"]
        else:
            if "setup" in self.name:
                self.data_input = ['ob', 'im', 'ac', 'actions', 'rew', 'done', 'value', 'neglogpac', 'neglogpac_select']   
                self.training_input = ['ob', 'im', 'return', 'done', 'ac', 'value', 'neglogpac', 'actions', 'neglogpac_select']
            else:
                self.data_input = ['ob', 'im', 'ac', 'rew', 'done', 'value', 'neglogpac']
                self.training_input = ['ob', 'im', 'return', 'done', 'ac', 'value', 'neglogpac']
        self.data = {t:[] for t in self.data_input}

    def re_init_buffer(self):
        # self.old_last_data = {t:self.data[t][-1] for t in self.data_input}
        # print("before reinit")
        ob, im, ac, actions, rew, done = self.data['ob'][-1], self.data['im'][-1], self.data['ac'][-1], self.data['actions'][-1], self.data['rew'][-1], self.data['done'][-1]
        value, neglogpac, neglogpac_select = self.train_model.get_vpred_and_nlogps(ob, im, ac, actions)
        self.data = {t:[] for t in self.data_input}
        self.data['ob'].append(ob)
        self.data['im'].append(im)
        self.data['ac'].append(ac)
        self.data['actions'].append(actions)
        self.data['rew'].append(rew)
        self.data['done'].append(done)
        self.data['value'].append(value)
        self.data['neglogpac'].append(neglogpac)
        self.data['neglogpac_select'].append(neglogpac_select)        
        # print("after reinit", actions, neglogpac_select)

    def log_stuff(self, things):
        if self.rank == 0:
            for thing in things:
                self.writer.add_scalar(thing, things[thing], self.iters_so_far)
                logger.record_tabular(thing, things[thing])
        
    def add_to_buffer(self, data):
        ''' data needs to be a list of lists, same length as self.data'''
        for d,key in zip(data, self.data):
            if key == "im":
                im = d[:self.im_size[0],int((d.shape[0]-self.im_size[0])/2):int(d.shape[0]-(d.shape[0]-self.im_size[0])/2),:]
                # print("adding to buff", d.shape, im.shape)
                self.data[key].append(im)
            else:
                self.data[key].append(d)

    def finalise_buffer(self, data, last_value, last_done):
        ''' data must be dict'''
        for key in self.data_input:
            if key == 'done':
                self.data[key] = np.asarray(self.data[key], dtype=np.bool)
            else:
                self.data[key] = np.asarray(self.data[key])
        self.n = next(iter(self.data.values())).shape[0]
        for key in data:
            self.data[key] = data[key]
        self.get_advantage(last_value, last_done)
        # if self.rank == 0:
        # self.log_stuff()
