'''
From OpenAI Baselines
'''
from models.base import Base
import os   
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' 
import tensorflow as tf
tf.get_logger().setLevel('DEBUG')
from scripts.distributions import make_pdtype
from scripts.mpi_running_mean_std import RunningMeanStd
from scripts.mpi_moments import mpi_moments
# from baselines.common import explained_variance, fmt_row, zipsame
import numpy as np
from gym import spaces
# import scripts.utils as U
import scripts.utils as TF_U
from scripts.mpi_adam_optimizer import MpiAdamOptimizer
from mpi4py import MPI
from scripts import logger
import time
import cv2
comm = MPI.COMM_WORLD
from collections import deque

def display_images(im1, im2=None):
    im1 = im1.reshape([-1] + list(im1.shape))
    if im2 is None:
        for im in im1:
            im = cv2.resize(im,dsize=(int(im.shape[1] * 6), int(im.shape[0] * 6)))
            cv2.imshow("frame", im)
            cv2.waitKey(1)
    else:
        im2 = im2.reshape([-1] + list(im2.shape))
        for i1, i2 in zip(im1, im2):
            i1 = cv2.resize(i1,dsize=(int(i1.shape[1] * 6), int(i1.shape[0] * 6)))
            i2 = cv2.resize(i2,dsize=(int(i2.shape[1] * 6), int(i2.shape[0] * 6)))
            cv2.imshow("frame", np.hstack([i1,i2]))
            cv2.waitKey(1)

def get_vars(scope):
    return [x for x in tf.global_variables() if scope in x.name]

# def neglogp(self, x):
#     return 0.5 * tf.reduce_sum(tf.square((x - self.mean) / self.std), axis=-1) \
#             + 0.5 * np.log(2.0 * np.pi) * tf.to_float(tf.shape(x)[-1]) \
#             + tf.reduce_sum(self.logstd, axis=-1)

def log_normal_pdf(sample, mean, logvar, raxis=1):
    log2pi = tf.math.log(2. * np.pi)
    return tf.reduce_sum(
            -.5 * ((sample - mean) ** 2. * tf.exp(-logvar) + logvar + log2pi),
            axis=raxis)

def reparameterize(mean, logvar):
    eps = tf.random.normal(shape=tf.shape(mean))
    return eps * tf.exp(logvar * .5) + mean

class Policy():
    def __init__(self, name, ob, im, vae_im, ob_space, ob_size, im_size, vae_im_size, ac_size, env, ac_space, z_ph, latent_dim, sess, args, hid_size, normalize=True, const_std=False):
        self.name = name
        self.ob = ob
        self.im = im
        self.vae_im = vae_im
        self.ob_size = ob_size
        self.im_size = im_size
        self.vae_im_size = vae_im_size
        self.ac_size = ac_size
        self.env = env
        self.sess = sess
        self.args = args
        self.pdtype = pdtype = make_pdtype(ac_space)
        self.const_std = const_std
        self.z_ph = z_ph
        self.latent_dim = latent_dim
        sequence_length = None
        if normalize:
            with tf.variable_scope("obfilter"):
                self.ob_rms = RunningMeanStd(shape=ob_space.shape)
        
        quarter = int(self.vae_im_size[0]/4)
        # mnist and titan
        # min_value = 0
        # max_value = 1
        
        # walking
        min_val = 0.25
        max_val = 2
        
        data_range = max_val - min_val
        # first_layer = 32
        # second_layer = 64
        first_layer = 16
        second_layer = 32
        # print(quarter)

        # Full connected
        with tf.variable_scope('encode'):
            if self.args.conv:
                x = tf.nn.relu(tf.layers.conv2d(self.vae_im, first_layer, 3, strides=2, padding="same", name="e_1"))
                x = tf.nn.relu(tf.layers.conv2d(x, second_layer, 3, strides=2, padding="same", name="e_2"))
                x = tf.layers.flatten(x)
                self.z = tf.layers.dense(x, self.latent_dim, name="e_final", kernel_initializer=tf.keras.initializers.RandomUniform(minval=-0.5, maxval=0.5))
            else:
                x = tf.layers.flatten(self.vae_im)
                x = tf.nn.tanh(tf.layers.dense(x, hid_size, name="e_1", kernel_initializer=tf.keras.initializers.RandomUniform(minval=-self.args.init_value, maxval=self.args.init_value)))
                x = tf.nn.tanh(tf.layers.dense(x, hid_size, name="e_2", kernel_initializer=tf.keras.initializers.RandomUniform(minval=-self.args.init_value, maxval=self.args.init_value)))
                self.z = tf.layers.dense(x, self.latent_dim, name="e_final", kernel_initializer=tf.keras.initializers.RandomUniform(minval=-self.args.init_value, maxval=self.args.init_value))
            
            
        with tf.variable_scope('decode'):
            if self.args.enc:
                last_out = tf.nn.relu(tf.layers.dense(self.z, quarter * quarter * second_layer, name="d_1"))
            else:
                last_out = tf.nn.relu(tf.layers.dense(tf.stop_gradient(self.z), quarter * quarter * second_layer, name="d_1"))
            last_out = tf.reshape(last_out, (-1, quarter, quarter, second_layer))
            last_out = tf.nn.relu(tf.layers.conv2d_transpose(last_out, second_layer, 3, strides=2, padding='same', name="d_2"))
            last_out = tf.nn.relu(tf.layers.conv2d_transpose(last_out, first_layer, 3, strides=2, padding='same', name="d_3"))
            self.decoded_logits = tf.layers.conv2d_transpose(last_out, 1, 3, padding="same", name="d_4")
            self.decoded = ((tf.nn.tanh(self.decoded_logits) + 1 )/ 2) * data_range + min_val

        with tf.variable_scope('encode', reuse=True):
            if self.args.conv:
                x = tf.nn.relu(tf.layers.conv2d(self.decoded, first_layer, 3, strides=2, padding="same", name="e_1"))
                x = tf.nn.relu(tf.layers.conv2d(x, second_layer, 3, strides=2, padding="same", name="e_2"))
                x = tf.layers.flatten(x)
                self.z_recon = tf.layers.dense(x, self.latent_dim, name="e_final", kernel_initializer=tf.keras.initializers.RandomUniform(minval=-0.5, maxval=0.5))
            else:
                x = tf.layers.flatten(self.decoded)
                x = tf.nn.tanh(tf.layers.dense(x, hid_size, name="e_1", kernel_initializer=tf.keras.initializers.RandomUniform(minval=-self.args.init_value, maxval=self.args.init_value)))
                x = tf.nn.tanh(tf.layers.dense(x, hid_size, name="e_2", kernel_initializer=tf.keras.initializers.RandomUniform(minval=-self.args.init_value, maxval=self.args.init_value)))
                self.z_recon = tf.layers.dense(x, self.latent_dim, name="e_final", kernel_initializer=tf.keras.initializers.RandomUniform(minval=-self.args.init_value, maxval=self.args.init_value))

        with tf.variable_scope('decode', reuse=True):
            last_out = tf.nn.relu(tf.layers.dense(self.z_ph, quarter * quarter * second_layer, name="d_1"))
            last_out = tf.reshape(last_out, (-1, quarter, quarter, second_layer))
            last_out = tf.nn.relu(tf.layers.conv2d_transpose(last_out, second_layer, 3, strides=2, padding='same', name="d_2"))
            last_out = tf.nn.relu(tf.layers.conv2d_transpose(last_out, first_layer, 3, strides=2, padding='same', name="d_3"))
            self.decoded_logits_ph = tf.layers.conv2d_transpose(last_out, 1, 3, padding="same", name="d_4")
            self.decoded_from_ph = ((tf.nn.tanh(self.decoded_logits_ph) + 1 )/ 2) * data_range + min_val

        with tf.variable_scope('error'):
            x = tf.concat(axis=1,values=[last_out, x])
            x = tf.nn.relu(tf.layers.conv2d(self.im, first_layer, 3, strides=2, padding="same", name="err_1"))
            x = tf.nn.relu(tf.layers.conv2d(x, second_layer, 3, strides=2, padding="same", name="err_2"))
            x = tf.layers.flatten(x)
            x = tf.nn.relu(tf.layers.dense(x, 16, name="err_3"))
            self.error_logitsx = tf.layers.dense(x, 1, name="err_final")
            self.error = tf.nn.softmax(self.error_logits)

        if normalize:
            obz = tf.clip_by_value((ob - tf.stop_gradient(self.ob_rms.mean)) / tf.stop_gradient(self.ob_rms.std), -5.0, 5.0)
        else:
            obz = ob
        
        with tf.variable_scope('qf'):
            x = tf.nn.relu(tf.layers.conv2d(self.im, first_layer, 3, strides=2, padding="same", name="q_vis1"))
            x = tf.nn.relu(tf.layers.conv2d(x, second_layer, 3, strides=2, padding="same", name="q_vis2"))
            x = tf.layers.flatten(x)
            x = tf.nn.relu(tf.layers.dense(x, 16, name="q_vis3"))
            last_out = obz
            last_out = tf.concat(axis=1,values=[last_out, x])
            last_out = tf.nn.tanh(tf.layers.dense(last_out, hid_size, name="q_fc1", kernel_initializer=TF_U.normc_initializer(1.0)))
            last_out = tf.nn.tanh(tf.layers.dense(last_out, hid_size, name="q_fc2", kernel_initializer=TF_U.normc_initializer(1.0)))
            self.qs = tf.layers.dense(last_out, 2, name='q_final', kernel_initializer=TF_U.normc_initializer(1.0))
            self.vpred = tf.reduce_max(self.qs, name='q_vpred')


        self.recon = tf.reduce_sum(tf.square(self.decoded - self.vae_im), axis=[1,2,3])
        self.stochastic = tf.placeholder(dtype=tf.bool, shape=())
        self.state = tf.constant([])
        self.neglogp = tf.constant([])
        self.steps = 0
        self.eps_start = 0.5
        self.eps_end = 0.001
        self.eps = self.eps_start
        self.eps_decay = 50000
        self.recon_error_buffer = []
        self.recon_thres = 0.0

    def update_eps(self):
        self.eps =  max(self.eps_start - (float(self.steps) / (self.eps_decay)), self.eps_end)
        self.steps += 1

    def detect(self, im):
        detection = self.sess.run(self.detection,feed_dict={self.z_ph:z})
        return detection

    def decode_from_ph(self, z):
        decoded = self.sess.run(self.decoded_from_ph,feed_dict={self.z_ph:z})
        return decoded

    def encode(self, im):
        z = self.sess.run(self.z, feed_dict={self.vae_im:im})
        return z
    
    def decode(self, im):
        decoded = self.sess.run(self.decoded, feed_dict={self.vae_im:im})
        return decoded
    
    def update_eps(self):
        self.eps =  max(self.eps_start - (float(self.steps) / (self.eps_decay)), self.eps_end)
        self.steps += 1

    def step(self, ob, im, stochastic=False):
        im_copy = im.copy()
        vae_im = im[:self.vae_im_size[0],int((self.im_size[0]-self.vae_im_size[0])/2):int(self.im_size[0]-(self.im_size[0]-self.vae_im_size[0])/2),:]
        self.recon_loss = self.sess.run([self.recon], feed_dict={ self.vae_im: vae_im.reshape([1]+self.vae_im_size)})
        self.recon_loss_pred = [0]
        self.scaled_recon_loss = self.args.lam * (self.recon_loss[0]*self.recon_loss[0])
        q, v, state, neglogp, recon_im = self.sess.run([self.qs, self.vpred, self.state, self.neglogp, self.decoded], 
                                    feed_dict={self.ob: ob.reshape([1,self.ob_size]), 
                                                self.im: im.reshape([1]+self.im_size), 
                                                self.vae_im: vae_im.reshape([1]+self.vae_im_size), 
                                                self.stochastic:stochastic})
        
        im_copy[:self.vae_im_size[0],int((self.im_size[1]-self.vae_im_size[1])/2):int(self.im_size[1]-(self.im_size[1]-self.vae_im_size[1])/2),:] = recon_im[0,::]
        new_qs = q[0]
        self.update_eps()
        self.recon_error_buffer.append(self.recon_loss[0])
        if np.random.random() > self.eps or not stochastic: 
            # This is where we apply the thresholding.
            if not stochastic:
                act_from_q = np.argmax(new_qs)
                if act_from_q == 1 and self.recon_loss[0] < self.recon_thres:
                    act = 1
                else:
                    act = 0
            else:
                act = np.argmax(new_qs)
        else:
            
            act = np.random.randint(2)
        return [act], v, state, neglogp, im_copy, self.scaled_recon_loss, self.recon_loss[0], np.clip(new_qs,0.0, None), self.recon_loss_pred

class Model(Base):
    def __init__(self, name, env, ac_size, ob_size, im_size=[48,48,1], vae_im_size=[48,48,1], args=None, PATH=None, writer=None, hid_size=256, vis=False, normalize=True, ent_coef=0.0, vf_coef=0.5, max_grad_norm=0.5, mpi_rank_weight=1, max_timesteps=int(1e6), lr=3e-4, horizon=2048, batch_size=32, const_std=False, latent_dim=2):
        self.max_timesteps = max_timesteps
        self.horizon = horizon
        self.batch_size = batch_size
        self.learning_rate = lr
        self.env = env
        self.ob_size = ob_size
        self.im_size = im_size
        self.vae_im_size = vae_im_size
        self.ac_size = ac_size
        self.latent_dim = latent_dim

        self.sess = sess = TF_U.get_session()
        self.name = name
        high = np.inf*np.ones(ac_size)
        low = -high
        ac_space = spaces.Box(low, high, dtype=np.float32)
        high = np.inf*np.ones(ob_size)
        low = -high
        ob_space = spaces.Box(low, high, dtype=np.float32)
        self.args = args   
        im_size = im_size
        self.PATH = PATH
        self.hid_size = args.hid_size
        self.writer = writer

        self.ob = TF_U.get_placeholder(name="ob_" + self.name, dtype=tf.float32, shape=[None, self.ob_size])
        self.im = TF_U.get_placeholder(name="im_" + self.name, dtype=tf.float32, shape=[None] + self.im_size)
        self.vae_im = TF_U.get_placeholder(name="vae_im_" + self.name, dtype=tf.float32, shape=[None] + self.vae_im_size)
        self.z_ph = TF_U.get_placeholder(name="z_ph_" + self.name, dtype=tf.float32, shape=[None,self.latent_dim])
        self.detect_labels = TF_U.get_placeholder(name="det_" + self.name, dtype=tf.float32, shape=[None])
    
        with tf.variable_scope(name, reuse=tf.AUTO_REUSE):
            self.train_model = Policy(name, self.ob, self.im, self.vae_im, ob_space, ob_size, im_size, vae_im_size, ac_size, env, ac_space, self.z_ph, self.latent_dim, sess, args=self.args, normalize=normalize, hid_size=hid_size, const_std=const_std)

        Base.__init__(self)

        # CREATE THE PLACEHOLDERS
        self.LR = LR = tf.placeholder(tf.float32, [], name="LR")
        self.A = A = tf.placeholder(tf.float32, [None,1], name="A")
        # self.A = A = train_model.pdtype.sample_placeholder([None])
        self.ACTION = tf.placeholder(tf.int32, [None,1], name="ACTION")
        self.ADV = ADV = tf.placeholder(tf.float32, [None], name="ADV")
        self.R = R = tf.placeholder(tf.float32, [None], name="R")

        one_hot = tf.one_hot(tf.squeeze(self.ACTION), 2)
        current_q_t = tf.reduce_sum(self.train_model.qs * one_hot, 1)
        self.vf_loss = 0.5*tf.reduce_mean(tf.square(current_q_t - self.R))
        
        self.decoder_train_loss = 0.5*tf.reduce_mean(self.train_model.recon)
        self.decoder_train_loss2 = tf.reduce_sum(tf.square(self.train_model.decoded - self.vae_im), axis=[1,2,3])
        self.ssim_loss = tf.image.ssim(self.vae_im, self.train_model.decoded, max_val=1.0, filter_size=11,filter_sigma=1.5, k1=0.01, k2=0.03)

        self.loss_names = ['q_loss','learning_rate','vae_train_loss']
        self.stats_list = [self.vf_loss, LR]
        self.vae_stats_list = [self.decoder_train_loss]

        all_variables = tf.trainable_variables()
        if self.args.enc:
            dec_var = [var for var in all_variables if 'd_' in var.name or 'e_' in var.name]
        else:
            dec_var = [var for var in all_variables if 'd_' in var.name]
        q_var = [var for var in all_variables if 'q_' in var.name]
        self._vae_train_op = self.get_train_ops(self.decoder_train_loss, self.name + "_dec", params=dec_var)        
        self._train_op = self.get_train_ops(self.vf_loss, self.name + "_q", params=q_var)        

        self.encode = self.train_model.encode
        self.decode = self.train_model.decode
        self.decode_from_ph = self.train_model.decode_from_ph
        self.step = self.train_model.step
        self.init_buffer()
    
    def get_mse(self, im):
        if self.args.mse:
            mse = self.sess.run(self.decoder_train_loss2, feed_dict={self.vae_im:im})
        else:
            mse = []
            for i in im:
                mse.append(self.sess.run(self.ssim_loss, feed_dict={self.vae_im:i.reshape([-1] + self.vae_im_size)})[0]*-1)
            mse = np.array(mse)
        # mse = self.sess.run(self.detect_loss, feed_dict={self.im:im})
        return -mse

    def get_train_ops(self, loss, name, params=None, mpi_rank_weight=1, max_grad_norm=0.5): 
        if params is None:
            params = tf.trainable_variables(name)
        # 2. Build our trainer
        with tf.variable_scope(name, reuse=tf.AUTO_REUSE):
            self.trainer = MpiAdamOptimizer(comm, learning_rate=self.LR, mpi_rank_weight=mpi_rank_weight, epsilon=1e-5)
        # 3. Calculate the gradients
        with tf.variable_scope(name, reuse=tf.AUTO_REUSE):
            grads_and_var = self.trainer.compute_gradients(loss, params)
            grads, var = zip(*grads_and_var)
        grads, _grad_norm = tf.clip_by_global_norm(grads, max_grad_norm)
        
        grads_and_var = list(zip(grads, var))
        # zip aggregate each gradient with parameters associated
        # For instance zip(ABCD, xyza) => Ax, By, Cz, Da

        self.grads = grads
        self.var = var
        with tf.variable_scope(name, reuse=tf.AUTO_REUSE):
            train_op = self.trainer.apply_gradients(grads_and_var)
        return train_op

    def train_vae(self, epoch, lr, im):
        td_map = {
            self.train_model.vae_im : im,
            self.LR : lr
        }
        return self.sess.run(self.vae_stats_list + [self._vae_train_op],td_map)[:-1]


    def train(self, epoch, lr, cliprange, obs, imgs, returns, masks, actions, values, neglogpacs, lam=None,  exp_actions=None, states=None):
        # Here we calculate advantage A(s,a) = R + yV(s') - V(s)
        # Returns = R + yV(s')
        advs = returns - values
        # Normalize the advantages
        advs = (advs - advs.mean()) / (advs.std() + 1e-8)
        td_map = {
        self.train_model.ob : obs,
        self.A : actions,
        self.ACTION : actions,
        self.ADV : advs,
        self.R : returns,
        self.LR : lr,
        # self.CLIPRANGE : cliprange,
        # self.OLDNEGLOGPAC : neglogpacs,
        # self.OLDVPRED : values
        }
        td_map[self.train_model.im] = imgs
        # td_map[self.train_model.vae_im] = imgs
        
        return self.sess.run(self.stats_list + [self._train_op],td_map)[:-1]

    def run_train(self, data, last_value, last_done):
        self.finalise_buffer(data, last_value, last_done)
        self.train_model.ob_rms.update(self.data['ob'])
        if self.args.const_lr:
            self.cur_lrmult =  1.0
        else:
            self.cur_lrmult =  max(1.0 - float(self.timesteps_so_far) / self.max_timesteps, 0)
       
        lrnow = self.learning_rate*self.cur_lrmult
        self.lr = lrnow
        cliprangenow = 0.2
        self.n = self.data['done'].shape[0]
        inds = np.arange(self.n)

        t1 = time.time()
        if "decoded" not in self.args.tests:
            if "pre" in self.args.tests:
                if self.iters_so_far < 10:    
                    vae_epochs = 10
                    # vae_epochs = 5
                    # vae_epochs = 1
                else:
                    vae_epochs = 1
            else:
                vae_epochs = 1
            # vae_epochs = 0
            vae_loss = [0,0,0]
            if self.args.train_vae:
                if "roi" in self.args.tests:
                    vae_images = self.data["im"][:,:self.vae_im_size[0],int((self.im_size[0]-self.vae_im_size[1])/2):int(self.im_size[1]-(self.im_size[1]-self.vae_im_size[1])/2),:]
                else:
                    vae_images = self.data["im"]
                if "pos" in self.args.tests:
                    if self.iters_so_far == 0:
                        self.vae_images = vae_images
                        self.vae_images_idx = 0
                    else:
                        image_idx = np.where(self.data["ac"] == 1)[0]
                        if len(image_idx) + self.vae_images_idx >= self.vae_images.shape[0]:
                            initial = self.vae_images.shape[0] - self.vae_images_idx
                            final =  len(image_idx) + self.vae_images_idx - self.vae_images.shape[0]
                            self.vae_images[self.vae_images_idx:,::] = vae_images[image_idx[:initial]]
                            self.vae_images[:final,::] = vae_images[image_idx[initial:]]
                            self.vae_images_idx = final
                        else:
                            self.vae_images[self.vae_images_idx:len(image_idx)+self.vae_images_idx,::] = vae_images[image_idx]
                            self.vae_images_idx += len(image_idx) 
                else:
                    self.vae_images = vae_images
                vae_inds = [i for i in range(self.vae_images.shape[0])]
                for epoch in range(vae_epochs):
                    if self.enable_shuffle:
                        np.random.shuffle(vae_inds)
                    self.loss = [] 
                    # for start in range(0, vae_images.shape[0]//self.batch_size, self.batch_size):
                    for start in range(0, self.n, self.batch_size):
                        end = start + self.batch_size
                        
                        mbinds = vae_inds[start:end]
                        if "err" in self.args.tests:
                            vae_loss = self.train_vae(epoch, lrnow, self.vae_images[mbinds], self.data["lam"][mbinds])
                        else:
                            vae_loss = self.train_vae(epoch, lrnow, self.vae_images[mbinds])
        else:
            self.epochs = 5               
       
        ninety = np.percentile(self.train_model.recon_error_buffer, 90)
        all_maxes = MPI.COMM_WORLD.allgather(ninety) # list of tuples
        self.train_model.recon_thres = max(all_maxes)
        self.train_model.recon_error_buffer = []
        
        for epoch in range(self.epochs):
            if self.enable_shuffle:
                np.random.shuffle(inds)
            # self.loss = [] 
            for start in range(0, self.n, self.batch_size):
                end = start + self.batch_size
                # print(epoch, start, end)
                mbinds = inds[start:end]
                
                # im_slices = (self.data[key][mbinds] for key in ["im"])
                # vae_loss = self.train_vae(epoch, lrnow, cliprangenow, *im_slices)
                
                slices = (self.data[key][mbinds] for key in self.training_input)
                # print([self.data[key][mbinds].shape for key in self.training_input])
                # print( self.train(epoch, lrnow, cliprangenow, vf_only, *slices))
                

                outs = self.train(epoch, lrnow, cliprangenow, *slices)
                self.loss = outs[:len(self.loss_names)]
        

        # print("train", time.time()- t1)
        # self.loss = np.mean(self.loss, axis=0)
        if "decoded" not in self.args.tests:
            self.loss = self.loss + vae_loss
        # self.log_stuff(things={"Eps":self.train_model.eps, "vae_lam":self.train_model.lam, "vae_scaled_recon":self.train_model.scaled_recon_loss, "vae_recon_loss": self.train_model.recon_loss,"vae_recon_thres": self.train_model.recon_thres, "vae_recon_loss_pred":self.train_model.recon_loss_pred[0]})
        self.log_stuff(things={"Eps":self.train_model.eps,"vae_scaled_recon":self.train_model.scaled_recon_loss, "vae_recon_thres": self.train_model.recon_thres, "vae_recon_loss_pred":self.train_model.recon_loss_pred[0]})
        self.evaluate(data['ep_rets'], data['ep_lens'])

        if "setup" in self.name:
            self.re_init_buffer()
        else:
            self.init_buffer()
        
    # def step(self, ob, im, stochastic=False, multi=False): 
    #     if not multi:
    #         actions, values, self.states, neglogpacs =  self.train_model.step(ob[None], im[None], stochastic=stochastic)
    #         return actions[0], values, self.states, neglogpacs
    #     else:
    #         actions, values, self.states, neglogpacs =  self.train_model.step(ob, im, stochastic=stochastic)
    #         return actions, values, self.states, neglogpacs        

    def get_advantage(self, last_value, last_done):    
        gamma = 0.99; lam = 0.95
        advs = np.zeros_like(self.data['rew'])
        lastgaelam = 0
        for t in reversed(range(len(self.data['rew']))):
            if t == len(self.data['rew']) - 1:
                nextnonterminal = 1.0 - last_done
                nextvalues = last_value
            else:
                nextnonterminal = 1.0 - self.data['done'][t+1]
                nextvalues = self.data['value'][t+1]
            delta = self.data['rew'][t] + gamma * nextvalues * nextnonterminal - self.data['value'][t]
            advs[t] = lastgaelam = delta + gamma * lam * nextnonterminal * lastgaelam
        self.data['return'] = advs + self.data['value']
    
    def init_buffer(self):
        if "lam" in self.args.tests:
            self.data_input = ['ob', 'im', 'ac', 'rew', 'done', 'value', 'neglogpac', "lam"]
            self.training_input = ['ob', 'im', 'return', 'done', 'ac', 'value', 'neglogpac', "lam"]
        else:
            if "setup" in self.name:
                self.data_input = ['ob', 'im', 'ac', 'actions', 'rew', 'done', 'value', 'neglogpac', 'neglogpac_select']   
                self.training_input = ['ob', 'im', 'return', 'done', 'ac', 'value', 'neglogpac', 'actions', 'neglogpac_select']
            else:
                self.data_input = ['ob', 'im', 'ac', 'rew', 'done', 'value', 'neglogpac']
                self.training_input = ['ob', 'im', 'return', 'done', 'ac', 'value', 'neglogpac']
        self.data = {t:[] for t in self.data_input}

    def re_init_buffer(self):
        # self.old_last_data = {t:self.data[t][-1] for t in self.data_input}
        # print("before reinit")
        ob, im, ac, actions, rew, done = self.data['ob'][-1], self.data['im'][-1], self.data['ac'][-1], self.data['actions'][-1], self.data['rew'][-1], self.data['done'][-1]
        value, neglogpac, neglogpac_select = self.train_model.get_vpred_and_nlogps(ob, im, ac, actions)
        self.data = {t:[] for t in self.data_input}
        self.data['ob'].append(ob)
        self.data['im'].append(im)
        self.data['ac'].append(ac)
        self.data['actions'].append(actions)
        self.data['rew'].append(rew)
        self.data['done'].append(done)
        self.data['value'].append(value)
        self.data['neglogpac'].append(neglogpac)
        self.data['neglogpac_select'].append(neglogpac_select)        
        # print("after reinit", actions, neglogpac_select)

    def log_stuff(self, things):
        if self.rank == 0:
            for thing in things:
                self.writer.add_scalar(thing, things[thing], self.iters_so_far)
                logger.record_tabular(thing, things[thing])
        
    def add_to_buffer(self, data):
        ''' data needs to be a list of lists, same length as self.data'''
        for d,key in zip(data, self.data):
            if key == "im":
                im = d[:self.im_size[0],int((d.shape[0]-self.im_size[0])/2):int(d.shape[0]-(d.shape[0]-self.im_size[0])/2),:]
                # print("adding to buff", d.shape, im.shape)
                self.data[key].append(im)
            else:
                self.data[key].append(d)

    def finalise_buffer(self, data, last_value, last_done):
        ''' data must be dict'''
        for key in self.data_input:
            if key == 'done':
                self.data[key] = np.asarray(self.data[key], dtype=np.bool)
            else:
                self.data[key] = np.asarray(self.data[key])
        self.n = next(iter(self.data.values())).shape[0]
        for key in data:
            self.data[key] = data[key]
        self.get_advantage(last_value, last_done)
        # if self.rank == 0:
        # self.log_stuff()
