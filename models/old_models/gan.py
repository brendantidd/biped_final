'''
From OpenAI Baselines
'''
from models.base import Base
import os   
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' 
import tensorflow as tf
tf.get_logger().setLevel('DEBUG')
from scripts.distributions import make_pdtype
from scripts.mpi_running_mean_std import RunningMeanStd
from scripts.mpi_moments import mpi_moments
# from baselines.common import explained_variance, fmt_row, zipsame
import numpy as np
from gym import spaces
# import scripts.utils as U
import scripts.utils as TF_U
from scripts.mpi_adam_optimizer import MpiAdamOptimizer
from mpi4py import MPI
from scripts import logger
import time
from tensorflow import keras
from tensorflow.keras import layers
comm = MPI.COMM_WORLD

def get_vars(scope):
    return [x for x in tf.global_variables() if scope in x.name]

def log_normal_pdf(sample, mean, logvar, raxis=1):
    log2pi = tf.math.log(2. * np.pi)
    return tf.reduce_sum(
            -.5 * ((sample - mean) ** 2. * tf.exp(-logvar) + logvar + log2pi),
            axis=raxis)

def reparameterize(mean, logvar):
    eps = tf.random.normal(shape=tf.shape(mean))
    return eps * tf.exp(logvar * .5) + mean

def generator(x):
    with tf.variable_scope("generator"):
        last_out = tf.nn.relu(tf.layers.dense(x, 7 * 7 * 64, name="gen1"))
        last_out = tf.reshape(last_out, (-1, 7, 7, 64))
        last_out = tf.nn.relu(tf.layers.conv2d_transpose(last_out, 64, 3, strides=2, padding='same', name="gen2"))
        last_out = tf.nn.relu(tf.layers.conv2d_transpose(last_out, 32, 3, strides=2, padding='same', name="gen3"))
        gen_logits = tf.layers.conv2d_transpose(last_out, 1, 3, padding="same", name="gen4")
        # self.decoded_from_ph = tf.nn.sigmoid(self.decoded_logits_from_ph)
        generated = (tf.nn.tanh(gen_logits) + 1 )/ 2
    return generated

def discriminator(x, reuse=False):
    with tf.variable_scope("discriminator", reuse=reuse):
        # x = tf.nn.relu(tf.layers.conv2d(self.decoded_logits_from_ph, 32, 3, strides=2, padding="same", name="e1"))
        # x = tf.nn.relu(tf.layers.conv2d(self.decoded_from_ph, 32, 3, strides=2, padding="same", name="dis1"))
        x = tf.nn.relu(tf.layers.conv2d(x, 32, 3, strides=2, padding="same", name="dis1"))
        x = tf.nn.relu(tf.layers.conv2d(x, 64, 3, strides=2, padding="same", name="dis2"))
        x = tf.layers.flatten(x)
        x = tf.nn.relu(tf.layers.dense(x, 16, name="dis3"))
        logits = tf.layers.dense(x, 1, name="dis_output")
        output = tf.nn.sigmoid(logits)
    return output, logits

class Policy():
    def __init__(self, name, ob, im, z_ph, ob_space, ob_size, im_size, ac_space, latent_dim, sess, vis,  args, hid_size, normalize=True, const_std=False):
        self.name = name
        self.ob = ob
        self.im = im
        self.z_ph = z_ph
        self.ob_size = ob_size
        self.im_size = im_size
        self.latent_dim = latent_dim
        self.sess = sess
        self.args = args
        self.pdtype = pdtype = make_pdtype(ac_space)
        self.vis = vis
        self.const_std = const_std
        sequence_length = None
        if normalize:
            with tf.variable_scope("obfilter"):
                self.ob_rms = RunningMeanStd(shape=ob_space.shape)

        # x = tf.nn.relu(TF_U.conv2d(im, 16, "vis_l1", [8, 8], [4, 4], pad="VALID"))
        # x = tf.nn.relu(TF_U.conv2d(x, 32, "vis_l2", [4, 4], [2, 2], pad="VALID"))
        # # x = TF_U.flattenallbut0(x)
        # x = tf.layers.Flatten(x)

        # x = tf.nn.tanh(tf.layers.dense(x, 64, name='vis_lin', kernel_initializer=TF_U.normc_initializer(1.0)))
                
        # if normalize:
        #     obz = tf.clip_by_value((ob - tf.stop_gradient(self.ob_rms.mean)) / tf.stop_gradient(self.ob_rms.std), -5.0, 5.0)
        # else:
        #     obz = ob
        # with tf.variable_scope('vf'):
        #     last_out = obz
        #     last_out = tf.concat(axis=1,values=[last_out, x])
        with tf.variable_scope('encode'):
            x = tf.nn.relu(tf.layers.conv2d(im, 32, 3, strides=2, padding="same", name="e1"))
            x = tf.nn.relu(tf.layers.conv2d(x, 64, 3, strides=2, padding="same", name="e2"))
            x = tf.layers.flatten(x)
            x = tf.nn.relu(tf.layers.dense(x, 16, name="e3"))
            self.z_mean = tf.layers.dense(x, self.latent_dim, name="z_mean")
            self.z_log_var = tf.layers.dense(x, self.latent_dim, name="z_log_var")
            self.z = reparameterize(self.z_mean, self.z_log_var)

        with tf.variable_scope('decode'):
    
            last_out = tf.nn.relu(tf.layers.dense(self.z, 7 * 7 * 64, name="d1"))
            last_out = tf.reshape(last_out, (-1, 7, 7, 64))
            last_out = tf.nn.relu(tf.layers.conv2d_transpose(last_out, 64, 3, strides=2, padding='same', name="d2"))
            last_out = tf.nn.relu(tf.layers.conv2d_transpose(last_out, 32, 3, strides=2, padding='same', name="d3"))
            self.decoded_logits = tf.layers.conv2d_transpose(last_out, 1, 3, padding="same", name="d4")
            # self.decoded = tf.nn.sigmoid(self.decoded_logits)
            self.decoded = (tf.nn.tanh(self.decoded_logits) + 1 )/ 2

        with tf.variable_scope('decode', reuse=True):
   
            last_out = tf.nn.relu(tf.layers.dense(self.z_ph, 7 * 7 * 64, name="d1"))
            last_out = tf.reshape(last_out, (-1, 7, 7, 64))
            last_out = tf.nn.relu(tf.layers.conv2d_transpose(last_out, 64, 3, strides=2, padding='same', name="d2"))
            last_out = tf.nn.relu(tf.layers.conv2d_transpose(last_out, 32, 3, strides=2, padding='same', name="d3"))
            self.decoded_logits_from_ph = tf.layers.conv2d_transpose(last_out, 1, 3, padding="same", name="d4")
            # self.decoded_from_ph = tf.nn.sigmoid(self.decoded_logits_from_ph)
            self.decoded_from_ph = (tf.nn.tanh(self.decoded_logits_from_ph) + 1 )/ 2

        with tf.variable_scope('encode', reuse=True):
            # x = tf.nn.relu(tf.layers.conv2d(self.decoded_logits_from_ph, 32, 3, strides=2, padding="same", name="e1"))
            x = tf.nn.relu(tf.layers.conv2d(self.decoded_from_ph, 32, 3, strides=2, padding="same", name="e1"))
            x = tf.nn.relu(tf.layers.conv2d(x, 64, 3, strides=2, padding="same", name="e2"))
            x = tf.layers.flatten(x)
            x = tf.nn.relu(tf.layers.dense(x, 16, name="e3"))
            self.z_mean_ph = tf.layers.dense(x, self.latent_dim, name="z_mean")
            self.z_log_var_ph = tf.layers.dense(x, self.latent_dim, name="z_log_var")
            self.z_reparam_ph = reparameterize(self.z_mean_ph, self.z_log_var_ph)


        with tf.variable_scope("generator"):
   
            last_out = tf.nn.relu(tf.layers.dense(self.z_ph, 7 * 7 * 64, name="gen1"))
            last_out = tf.reshape(last_out, (-1, 7, 7, 64))
            last_out = tf.nn.relu(tf.layers.conv2d_transpose(last_out, 64, 3, strides=2, padding='same', name="gen2"))
            last_out = tf.nn.relu(tf.layers.conv2d_transpose(last_out, 32, 3, strides=2, padding='same', name="gen3"))
            self.gen_logits = tf.layers.conv2d_transpose(last_out, 1, 3, padding="same", name="gen4")
            # self.decoded_from_ph = tf.nn.sigmoid(self.decoded_logits_from_ph)
            self.generator = (tf.nn.tanh(self.gen_logits) + 1 )/ 2

        with tf.variable_scope("discriminator"):
            # x = tf.nn.relu(tf.layers.conv2d(self.decoded_logits_from_ph, 32, 3, strides=2, padding="same", name="e1"))
            # x = tf.nn.relu(tf.layers.conv2d(self.decoded_from_ph, 32, 3, strides=2, padding="same", name="dis1"))
            x = tf.nn.relu(tf.layers.conv2d(self.im, 32, 3, strides=2, padding="same", name="dis1"))
            x = tf.nn.relu(tf.layers.conv2d(x, 64, 3, strides=2, padding="same", name="dis2"))
            x = tf.layers.flatten(x)
            x = tf.nn.relu(tf.layers.dense(x, 16, name="dis3"))
            self.discriminator_real_logits = tf.layers.dense(x, 1, name="dis_output")
            self.discriminator_real = tf.nn.sigmoid(self.discriminator_real_logits)
        
        with tf.variable_scope("discriminator", reuse=True):
            # x = tf.nn.relu(tf.layers.conv2d(self.decoded_logits_from_ph, 32, 3, strides=2, padding="same", name="e1"))
            x = tf.nn.relu(tf.layers.conv2d(self.decoded_from_ph, 32, 3, strides=2, padding="same", name="dis1"))
            # x = tf.nn.relu(tf.layers.conv2d(self.im, 32, 3, strides=2, padding="same", name="dis1"))
            x = tf.nn.relu(tf.layers.conv2d(x, 64, 3, strides=2, padding="same", name="dis2"))
            x = tf.layers.flatten(x)
            x = tf.nn.relu(tf.layers.dense(x, 16, name="dis3"))
            self.discriminator_fake_logits = tf.layers.dense(x, 1, name="dis_output")
            self.discriminator_fake = tf.nn.sigmoid(self.discriminator_fake_logits)

    def decode_from_ph(self, z):
        decoded = self.sess.run(self.decoded_from_ph,feed_dict={self.z_ph:z})
        return decoded

    def encode(self, im):
        z_mean = self.sess.run(self.z_mean, feed_dict={self.im:im})
        return z_mean
    
    def decode(self, im):
        decoded = self.sess.run(self.decoded, feed_dict={self.im:im})
        return decoded

class Model(Base):
    def __init__(self, name, env=None, ac_size=2, ob_size=5, im_size=[48,48,4], args=None, PATH=None, writer=None, hid_size=256, vis=False, normalize=True, ent_coef=0.0, vf_coef=0.5, max_grad_norm=0.5, mpi_rank_weight=1, max_timesteps=int(1e6), lr=3e-4, horizon=2048, batch_size=32, const_std=False):
        self.max_timesteps = max_timesteps
        self.horizon = horizon
        self.batch_size = batch_size
        self.learning_rate = lr
        self.env = env
        self.ac_size = ac_size

        self.sess = sess = TF_U.get_session()
        self.name = name
        high = np.inf*np.ones(ac_size)
        low = -high
        ac_space = spaces.Box(low, high, dtype=np.float32)
        high = np.inf*np.ones(ob_size)
        low = -high
        ob_space = spaces.Box(low, high, dtype=np.float32)
        self.args = args   
        self.im_size = im_size
        self.PATH = PATH
        self.hid_size = hid_size
        self.writer = writer
        self.ob = TF_U.get_placeholder(name="sp_ob_" + name, dtype=tf.float32, shape=[None] + list(ob_space.shape))
        self.im = TF_U.get_placeholder(name="sp_im_" + name, dtype=tf.float32, shape=[None] + im_size)
        
        self.latent_dim = 2
        self.z_ph = TF_U.get_placeholder(name="z_ph_" + name, dtype=tf.float32, shape=[None, self.latent_dim])
                
        self.args = args
        if ('vel' in name or 'hex' in name) and not args.vis:
            self.vis = False
        else:
            self.vis = vis
        # with tf.variable_scope(name, reuse=tf.AUTO_REUSE):
            # train_model = Policy(name, ob, im, z_ph, ob_space, ob_size, im_size, ac_space, self.latent_dim, sess, self.vis, args=args, normalize=normalize, hid_size=hid_size, const_std=const_std)

        
        self.LR = LR = tf.placeholder(tf.float32, [])
  
        with tf.variable_scope(name, reuse=tf.AUTO_REUSE):

            self.dis_real, self.dis_real_logits = discriminator(self.im)
            self.generated = generator(self.z_ph)
            self.dis_fake, self.dis_fake_logits = discriminator(self.generated, reuse=True)

        Base.__init__(self)


        # discriminator_real_logits
        self.dis_loss_real = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(logits=self.dis_real_logits, labels=tf.ones_like(self.dis_real)))
        self.dis_loss_fake = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(logits=self.dis_fake_logits, labels=tf.zeros_like(self.dis_fake)))
        self.dis_loss = self.dis_loss_real + self.dis_loss_fake
        self.gen_loss = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(logits=self.dis_fake_logits, labels=tf.ones_like(self.dis_fake_logits)))
        
        self.loss_names = ["dis_loss", "gen_loss"]
        self.stats_list = [self.dis_loss, self.gen_loss]

        learning_rate = 0.01

        all_vars = tf.trainable_variables()
        dis_vars = [v for v in all_vars if 'discriminator' in v.name]
        gen_vars = [v for v in all_vars if 'generator' in v.name]
        # print(dis_vars, gen_vars)
        self._train_op_dis = tf.train.AdamOptimizer(learning_rate).minimize(
                                                                        self.dis_loss, var_list=dis_vars)

        self._train_op_gen = tf.train.AdamOptimizer(learning_rate * 2).minimize(
                                                                       self.gen_loss, var_list=gen_vars)

        # # loss1 = log_normal_pdf(train_model.z, train_model.z_mean, train_model.z_log_var)
        # # loss2 = self.decoded_from_ph tf.stop_gradient(train_model.z)

        # # loss1 = tf.log(train_model.discriminator)
        # loss1 = -tf.reduce_mean(tf.log(train_model.discriminator_real) + tf.log(1 - train_model.discriminator_gen))
        # loss2 = -tf.reduce_mean(tf.log(train_model.discriminator_gen))
        
        # self._train_ops_dis = self.get_train_ops(self.dis_loss, self.name + "/discriminator")
        # self._train_ops_gen = self.get_train_ops(self.gen_loss, self.name + "/generator")

        # vars_D = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope=self.name + '/discriminator')
        # vars_G = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope=self.name + '/generator')

        # # According to the GAN thesis formula, the loss should be maximized, but since the optimization function is used to minimize it, a negative sign is added to loss_D and loss_G to be optimized.
        # learning_rate = 0.01
        
        # print(vars_D)
        # print(vars_G)
        
        # train_D = tf.train.AdamOptimizer(learning_rate=learning_rate).minimize(-loss1, var_list=vars_D)
        # train_G = tf.train.AdamOptimizer(learning_rate=learning_rate).minimize(-loss2, var_list=vars_D + vars_G) 
        # print(train_D)
        # print(train_G)
        # exit()

        # # Loss1 ---------------------------------------------------------------
        # cross_ent = tf.square(train_model.im - train_model.decoded_logits)
        # logpx_z = -tf.reduce_sum(cross_ent, axis=[1, 2, 3])
        # # logpz = log_normal_pdf(train_model.z, 0., 0.)
        # logpz = log_normal_pdf(train_model.z, 6., 0.)
        # logqz_x = log_normal_pdf(train_model.z, train_model.z_mean, train_model.z_log_var)
        # loss1 = -tf.reduce_mean(logpx_z + logpz - logqz_x)

        # # Loss2 ---------------------------------------------------------------
        # logqz_x = log_normal_pdf(train_model.z, train_model.z_mean, train_model.z_log_var)
        # cross_ent2 = tf.square(train_model.z_ph - train_model.z_mean_ph)
        # logpx_z2 = -tf.reduce_sum(cross_ent2)
        # # logpz = log_normal_pdf(train_model.z, 0., 0.)
        # # logqz_x = log_normal_pdf(train_model.z, train_model.z_mean, train_model.z_log_var)
        # # loss = -tf.reduce_mean(logpx_z + logpz - logqz_x)
        # loss2 = -tf.reduce_mean(logpx_z2)

        # # # Loss3 ---------------------------------------------------------------
        # # # rand = tf.random.uniform(shape=tf.shape(train_model.im))
        # self.im2 = TF_U.get_placeholder(name="sp_im2_" + name, dtype=tf.float32, shape=[None] + im_size)
        # cross_ent3 = tf.square(self.im2 - train_model.decoded_logits)
        # logpx_z3 = -tf.reduce_sum(cross_ent3, axis=[1, 2, 3])
        # logpz3 = log_normal_pdf(train_model.z, 0., 0.)
        # logqz_x3 = log_normal_pdf(train_model.z, train_model.z_mean, train_model.z_log_var)
        # loss3 = -tf.reduce_mean(logpx_z3 + logpz3 - logqz_x3)
        # # -----------------------------------------------------------------------
        # loss = loss1 + loss2 + loss3
        # loss = loss1
        # loss = loss1 + loss3
        # loss = -tf.reduce_mean(logpx_z + logpz - logqz_x) 

        # reconstruction_loss = -tf.reduce_mean(logpx_z)
        # kl_loss = -tf.reduce_mean(logpz - logqz_x)
        # std = tf.reduce_mean(tf.exp(train_model.z_log_var))
        # mean_ratio = tf.reduce_mean(ratio)
        # mean_adv = tf.reduce_mean(self.ADV)

        # self.loss_names = ['loss', 'kl_loss', 
        # 'reconstruction_loss', 'learning_rate', 'std']
        # self.stats_list = [loss, kl_loss, reconstruction_loss, LR, std]

        # self._train_ops = self.get_train_ops(loss, self.name)
    
        # self.step = train_model.step
        # self.decode = train_model.decode
        # self.decode_from_ph = train_model.decode_from_ph
        # self.encode = train_model.encode
        # self.train_model = train_model
        self.init_buffer()

    def generate(self,z):
        return self.sess.run(self.generated,feed_dict={self.z_ph:z})



    def get_train_ops(self, loss, name, mpi_rank_weight=1, max_grad_norm=0.5): 
        params = tf.trainable_variables(name)
        # print(name)
        # print(params)
        # print(loss)
        # 2. Build our trainer
        with tf.variable_scope(name, reuse=tf.AUTO_REUSE):
            self.trainer = MpiAdamOptimizer(comm, learning_rate=self.LR, mpi_rank_weight=mpi_rank_weight, epsilon=1e-5)

        grads_and_vars = tf.train.AdamOptimizer.compute_gradients(loss, params)
        print(grads_and_vars)

        # 3. Calculate the gradients
        with tf.variable_scope(name, reuse=tf.AUTO_REUSE):
            grads_and_var = self.trainer.compute_gradients(loss, params)
            grads, var = zip(*grads_and_var)
        # print("grads and var")
        # print(grads_and_var)

        grads, _grad_norm = tf.clip_by_global_norm(grads, max_grad_norm)
        
        grads_and_var = list(zip(grads, var))
        # zip aggregate each gradient with parameters associated
        # For instance zip(ABCD, xyza) => Ax, By, Cz, Da

        self.grads = grads
        self.var = var
        with tf.variable_scope(name, reuse=tf.AUTO_REUSE):
            train_op = self.trainer.apply_gradients(grads_and_var)
        return train_op

    # def train(self, epoch, lr, imgs):
    #     td_map = {
    #     # self.train_model.ob : obs,
    #     self.train_model.im : imgs,
    #     self.LR : lr
    #     }
    #     # z = None
    #     # z = np.random.normal(size=[imgs.shape[0], self.latent_dim])
    #     # if z is not None:
    #     #     td_map[self.train_model.z_ph] = z
        
    #     rand_image = np.random.randint(0, 1, size=imgs.shape)
    #     td_map[self.im2] = rand_image

    #     return self.sess.run(self.stats_list + [self._train_op],td_map)[:-1]

    def train(self, epoch, lr, imgs):
        td_map = {
        # self.train_model.ob : obs,
        self.im : imgs,
        self.LR : lr
        }
        # z = None
        # z = np.random.normal(size=[imgs.shape[0], self.latent_dim])
        # if z is not None:
        #     td_map[self.train_model.z_ph] = z
        td_map[self.z_ph] = np.random.normal(size=[imgs.shape[0], self.latent_dim])
        
        loss_dis = self.sess.run(self.stats_list + [self._train_op_dis],td_map)[:-1]

        td_map = {
        # self.train_model.ob : obs,
        self.im : imgs,
        self.LR : lr
        }
        # z = None
        # z = np.random.normal(size=[imgs.shape[0], self.latent_dim])
        # if z is not None:
        td_map[self.z_ph] = np.random.normal(size=[imgs.shape[0], self.latent_dim])
        
        rand_image = np.random.randint(0, 1, size=imgs.shape)
        # td_map[self.im2] = rand_image
        return self.sess.run(self.stats_list + [self._train_op_gen],td_map)[:-1]


    def run_train(self, data, last_value, last_done):
        self.finalise_buffer(data, last_value, last_done)
        self.train_model.ob_rms.update(self.data['ob'])
        if self.args.const_lr:
            self.cur_lrmult =  1.0
        else:
            self.cur_lrmult =  max(1.0 - float(self.timesteps_so_far) / self.max_timesteps, 0)
        lrnow = self.learning_rate*self.cur_lrmult
        self.lr = lrnow
        cliprangenow = 0.2
        self.n = self.data['done'].shape[0]
        inds = np.arange(self.n)
        # print(self.epochs)
        t1 = time.time()
        for epoch in range(self.epochs):
            if self.enable_shuffle:
                np.random.shuffle(inds)
            self.loss = [] 
            for start in range(0, self.n, self.batch_size):
                end = start + self.batch_size
                # print(epoch, start, end)
                mbinds = inds[start:end]
                slices = (self.data[key][mbinds] for key in self.training_input)
                # print([self.data[key][mbinds].shape for key in self.training_input])
                # print( self.train(epoch, lrnow, cliprangenow, vf_only, *slices))
                outs = self.train(epoch, lrnow, *slices)
                self.loss.append(outs[:len(self.loss_names)])
        # print("train", time.time()- t1)
        self.loss = np.mean(self.loss, axis=0)
        self.evaluate(data['ep_rets'], data['ep_lens'])
        if "setup" in self.name:
            self.re_init_buffer()
        else:
            self.init_buffer()
        
    # def step(self, ob, im, stochastic=False, multi=False): 
    #     if not multi:
    #         actions, values, self.states, neglogpacs =  self.train_model.step(ob[None], im[None], stochastic=stochastic)
    #         return actions[0], values, self.states, neglogpacs
    #     else:
    #         actions, values, self.states, neglogpacs =  self.train_model.step(ob, im, stochastic=stochastic)
    #         return actions, values, self.states, neglogpacs        

    def get_advantage(self, last_value, last_done):    
        gamma = 0.99; lam = 0.95
        advs = np.zeros_like(self.data['rew'])
        lastgaelam = 0
        for t in reversed(range(len(self.data['rew']))):
            if t == len(self.data['rew']) - 1:
                nextnonterminal = 1.0 - last_done
                nextvalues = last_value
            else:
                nextnonterminal = 1.0 - self.data['done'][t+1]
                nextvalues = self.data['value'][t+1]
            delta = self.data['rew'][t] + gamma * nextvalues * nextnonterminal - self.data['value'][t]
            advs[t] = lastgaelam = delta + gamma * lam * nextnonterminal * lastgaelam
        self.data['return'] = advs + self.data['value']
    
    def init_buffer(self):
        if "setup" in self.name:
            self.data_input = ['ob', 'im', 'ac', 'actions', 'rew', 'done', 'value', 'neglogpac', 'neglogpac_select']   
            self.training_input = ['ob', 'im', 'return', 'done', 'ac', 'value', 'neglogpac', 'actions', 'neglogpac_select']
        else:
            self.data_input = ['ob', 'im', 'ac', 'rew', 'done', 'value', 'neglogpac']
            self.training_input = ['ob', 'im', 'return', 'done', 'ac', 'value', 'neglogpac']
        self.data = {t:[] for t in self.data_input}

    def re_init_buffer(self):
        # self.old_last_data = {t:self.data[t][-1] for t in self.data_input}
        # print("before reinit")
        ob, im, ac, actions, rew, done = self.data['ob'][-1], self.data['im'][-1], self.data['ac'][-1], self.data['actions'][-1], self.data['rew'][-1], self.data['done'][-1]
        value, neglogpac, neglogpac_select = self.train_model.get_vpred_and_nlogps(ob, im, ac, actions)
        self.data = {t:[] for t in self.data_input}
        self.data['ob'].append(ob)
        self.data['im'].append(im)
        self.data['ac'].append(ac)
        self.data['actions'].append(actions)
        self.data['rew'].append(rew)
        self.data['done'].append(done)
        self.data['value'].append(value)
        self.data['neglogpac'].append(neglogpac)
        self.data['neglogpac_select'].append(neglogpac_select)        
        # print("after reinit", actions, neglogpac_select)

    def log_stuff(self, things):
        if self.rank == 0:
            for thing in things:
                self.writer.add_scalar(thing, things[thing], self.iters_so_far)
                logger.record_tabular(thing, things[thing])
        
    def add_to_buffer(self, data):
        ''' data needs to be a list of lists, same length as self.data'''
        for d,key in zip(data, self.data):
            self.data[key].append(d)

    def finalise_buffer(self, data, last_value, last_done):
        ''' data must be dict'''
        for key in self.data_input:
            if key == 'done':
                self.data[key] = np.asarray(self.data[key], dtype=np.bool)
            else:
                self.data[key] = np.asarray(self.data[key])
        self.n = next(iter(self.data.values())).shape[0]
        for key in data:
            self.data[key] = data[key]
        self.get_advantage(last_value, last_done)
        # if self.rank == 0:
        # self.log_stuff()

