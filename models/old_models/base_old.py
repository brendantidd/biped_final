'''
Base class for all networks.
For now includes training parameters..
'''
# import scripts.utils as U
import os   
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' 
import tensorflow as tf
from baselines import logger
from mpi4py import MPI
from baselines.common.distributions import make_pdtype
import time
import numpy as np
from collections import deque
import scripts.utils as U
# from models.ppo import Model

class Base():
  epochs = 10
  batch_size = 32
  enable_shuffle = True
  episodes_so_far = 0
  timesteps_so_far = 0
  iters_so_far = 0
  best_reward = 0
  tstart = time.time()
  t1 = time.time()
  all_rewards = []
  lenbuffer = deque(maxlen=100) # rolling buffer for episode lengths
  rewbuffer = deque(maxlen=100) # rolling buffer for episode rewards
  comm = MPI.COMM_WORLD
  rank = comm.Get_rank()
  saved_early = False
  def __init__(self):
    vars_with_adam = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, scope=self.name+"/")
    self.vars = [v for v in vars_with_adam if 'Adam' not in v.name]
    # if self.rank == 0: print(self.vars)
    # print("these are the variables for ", self.name)
    # print(self.vars)
    self.policy_saver = tf.train.Saver(var_list=self.vars)
    self.t1 = time.time()

  def save(self, best=False, early=False):
    if best:
      self.policy_saver.save(tf.get_default_session(), self.PATH + '/best/model.ckpt', write_meta_graph=False)
    elif early:
      self.policy_saver.save(tf.get_default_session(), self.PATH + '/early/model.ckpt', write_meta_graph=False)
    else:
      self.policy_saver.save(tf.get_default_session(), self.PATH + 'model.ckpt', write_meta_graph=False)
    # if self.rank == 0:
    print("Saving weights for " + self.name)
    
    def load(self, WEIGHT_PATH):
        self.policy_saver.restore(tf.get_default_session(), WEIGHT_PATH + "model.ckpt")
        print("Loaded weights for " + self.name + " module, from " + WEIGHT_PATH)

  def load_dqn(self, base_name, WEIGHTS_PATH, reset_std=True, vis=True):

    temp_vars = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, scope="dqn/dqn/")
    
    # print(temp_vars)
    # exit()
    # base_vars = [v for v in temp_vars if 'Adam' not in v.name]
    base_vars = [v for v in temp_vars if 'Adam' not in v.name and 'dqn' in v.name]
    # print(WEIGHTS_PATH,  base_name)
    # print(base_vars)
    # target_func_vars = [v for v in self.vars if 'Adam' not in v.name and 'vf_rms' not in v.name]
    target_func_vars = [v for v in self.vars if 'Adam' not in v.name and 'dqn' in v.name]
    # target_func_vars += [v for v in self.vars if ('pol' in v.name and 'logstd' not in v.name)]
    # print(target_func_vars)
    # print(len(base_vars), len(target_func_vars))
    base_saver = tf.train.Saver(var_list=base_vars)    
    base_saver.restore(tf.get_default_session(), WEIGHTS_PATH + 'model.ckpt')
    update_target_expr = []
    for var, var_target in zip(sorted(base_vars, key=lambda v:v.name), sorted(target_func_vars, key=lambda v: v.name)):
      # print(var_target, var)
      update_target_expr.append(var_target.assign(var))
    update_target_expr = tf.group(*update_target_expr)
    update_target = U.function([],[], updates=[update_target_expr])
    update_target()


  def load_pol(self, base_name, WEIGHTS_PATH, reset_std=True, vis=True, vf=True):

    temp_vars = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, scope=base_name + "/")
    # print(temp_vars)
    # base_vars = [v for v in temp_vars if 'Adam' not in v.name]
    base_vars = [v for v in temp_vars if 'Adam' not in v.name and 'select' not in v.name  and 'logstd' not in v.name and ('obfilter' in v.name or 'vis' in v.name or 'pol' in v.name)]
    # print(base_vars)
    # base_vars = [v for v in temp_vars if 'Adam' not in v.name and 'obfilter' in v.name]
    # base_vars += [v for v in temp_vars if 'Adam' not in v.name and 'vis' in v.name]
    if vf:
      base_vars += [v for v in temp_vars if 'Adam' not in v.name and 'vf' in v.name]
    # base_vars += [v for v in temp_vars if 'Adam' not in v.name and 'pol' in v.name and 'logstd' not in v.name]
    # print(base_vars, base_name)
    # target_func_vars = [v for v in self.vars if 'Adam' not in v.name and 'vf_rms' not in v.name]
    target_func_vars = [v for v in self.vars if 'Adam' not in v.name and 'select' not in v.name  and 'logstd' not in v.name and ('obfilter' in v.name or 'vis' in v.name or 'pol' in v.name)]
    # target_func_vars += [v for v in self.vars if 'Adam' not in v.name and 'vis' in v.name]
    if vf:
      target_func_vars += [v for v in self.vars if 'Adam' not in v.name and 'vf' in v.name]
      # print("loading vf")
    # target_func_vars += [v for v in self.vars if 'Adam' not in v.name and 'pol' in v.name and 'logstd' not in v.name]
    # target_func_vars += [v for v in self.vars if ('pol' in v.name and 'logstd' not in v.name)]
    # print(len(base_vars), len(target_func_vars))
    # print(target_func_vars)
    base_saver = tf.train.Saver(var_list=base_vars)    
    base_saver.restore(tf.get_default_session(), WEIGHTS_PATH + 'model.ckpt')
    update_target_expr = []
    for var, var_target in zip(sorted(base_vars, key=lambda v:v.name), sorted(target_func_vars, key=lambda v: v.name)):
      print(var_target, var)
      update_target_expr.append(var_target.assign(var))
    update_target_expr = tf.group(*update_target_expr)
    update_target = U.function([],[], updates=[update_target_expr])
    update_target()

    if self.rank == 0:
      print("Loaded weights for subpolicy: ", self.name)  
    if reset_std:
      for v in target_func_vars:
        if "logstd" in v.name:
          assign_op = v.assign(np.log(np.ones([1,12])))
          # assign_op = v.assign(np.log(0.5*np.ones([1,self.ac_size])))
          assign_op.op.run()

  def load_base(self, base_name, WEIGHTS_PATH, reset_std=True):

    # base_pol = Model(base_name, ob_size=self.ob_size, ac_size=self.ac_size, im_size=self.im_size, args=self.args, PATH=self.PATH, vis=args.vis)
    # base_pi = Model(base_name, self.args, self.ob, self.im, self.ob_space, self.im_size, self.ac_space)
    temp_vars = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, scope=base_name + "/")
    base_vars = [v for v in temp_vars if 'Adam' not in v.name and 'logstd' not in v.name]
    base_saver = tf.train.Saver(var_list=base_vars)    
    base_saver.restore(tf.get_default_session(), WEIGHTS_PATH + 'model.ckpt')
    target_func_vars = [v for v in self.vars if 'Adam' not in v.name and 'vf_rms' not in v.name and 'logstd' not in v.name]
    # print(len(base_vars))
    # print(len(target_func_vars))
    update_target_expr = []
    for var, var_target in zip(sorted(base_vars, key=lambda v:v.name), sorted(target_func_vars, key=lambda v: v.name)):
      update_target_expr.append(var_target.assign(var))
    update_target_expr = tf.group(*update_target_expr)
    update_target = U.function([],[], updates=[update_target_expr])
    update_target()
    if self.rank == 0:
      print("Loaded weights for subpolicy: ", self.name)  
    if reset_std:
      logstd_vars = [v for v in self.vars if 'logstd' in v.name]
      # print(logstd_vars)
      for v in logstd_vars:
        if "logstd" in v.name:
          assign_op = v.assign(np.log(np.ones([1,self.ac_size])))
          # assign_op = v.assign(np.log(0.5*np.ones([1,self.ac_size])))
          assign_op.op.run()

  def evaluate(self, rews, lens, display_name="", extra=None):

    lrlocal = (lens, rews) # local values
    listoflrpairs = MPI.COMM_WORLD.allgather(lrlocal) # list of tuples
    lens, rews = map(flatten_lists, zip(*listoflrpairs))
    # self.lenbuffer.extend(lens)
    # self.rewbuffer.extend(rews)

    if self.rank == 0:
      logger.log(display_name + " ********** Iteration %i ************"%self.iters_so_far)
      if extra:
        for ex in extra:
          logger.record_tabular(ex, extra[ex])
          self.writer.add_scalar(ex, extra[ex], self.iters_so_far)
      for loss, name in zip(self.loss, self.loss_names):
        logger.record_tabular(name, loss)
        self.writer.add_scalar(display_name + name, loss, self.iters_so_far)
      logger.record_tabular(display_name + "LearningRate", self.lr)
      logger.record_tabular(display_name + "EpRewMean", np.mean(rews))
      logger.record_tabular(display_name + "EpLenMean", np.mean(lens))
      logger.record_tabular(display_name + "EpThisIter", len(lens))
      logger.record_tabular(display_name + "TimeThisIter", time.time() - self.t1)
      logger.record_tabular(display_name + "TimeStepsSoFar", self.timesteps_so_far)
      logger.record_tabular(display_name + "EnvTotalSteps", self.env.total_steps)
      # logger.record_tabular(display_name + "EnvSampleDelay", self.env.sample_delay)
      # self.writer.add_scalar(display_name + "sample_delay", self.env.sample_delay, self.iters_so_far)
      # logger.record_tabular(display_name + "Kp", self.env.Kp)
      # self.writer.add_scalar(display_name + "Kp", self.env.Kp, self.iters_so_far)
      self.writer.add_scalar(display_name + "time per step", time.time() - self.t1, self.iters_so_far)

      self.t1 = time.time()
      
      self.writer.add_scalar(display_name + "rewards", np.mean(rews), self.iters_so_far)
      self.writer.add_scalar(display_name + "lengths", np.mean(lens), self.iters_so_far)
      self.all_rewards.append(np.mean(rews))
      np.save(self.PATH + 'rewards.npy',self.all_rewards)
      # print(np.mean(self.env.reward_breakdown['pos']),np.mean(self.env.reward_breakdown['vel']))
    self.env.avg_reward = np.mean(rews)
    self.env.log_stuff(logger, self.writer, self.iters_so_far)
    if self.rank == 0:
      try:
          if not self.saved_early and self.env.all_ready_to_stop:
            self.writer.add_scalar(display_name + "Finished", 1, self.iters_so_far)
            self.save(early=True)
            self.saved_early = True
          else:
            self.writer.add_scalar(display_name + "Finished", 0, self.iters_so_far)

          if np.mean(rews) > self.best_reward and not self.env.cur:
            self.best_reward = np.mean(rews)
            self.save(best=True)
          self.save()
      except Exception as e:
          print("Couldn't save training model", e)
      logger.dump_tabular()
    self.timesteps_so_far += sum(lens)     
    self.iters_so_far += 1


  def set_training_params(self, max_timesteps, learning_rate, horizon, vf_learning_rate=None):
    if vf_learning_rate == None:
      vf_learning_rate = learning_rate
    if self.rank == 0:
      print()
      print("setting training params", max_timesteps, learning_rate, vf_learning_rate, horizon)
      print()
    self.max_timesteps = max_timesteps
    self.learning_rate = learning_rate
    self.vf_learning_rate = vf_learning_rate
    self.horizon = horizon

  def log_stuff(self):
    pass

def flatten_lists(listoflists):
    return [el for list_ in listoflists for el in list_]