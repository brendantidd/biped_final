'''
From OpenAI Baselines
'''
from models.base import Base
import os   
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' 
import tensorflow as tf
tf.get_logger().setLevel('DEBUG')
from scripts.distributions import make_pdtype
from scripts.mpi_running_mean_std import RunningMeanStd
from scripts.mpi_moments import mpi_moments
# from baselines.common import explained_variance, fmt_row, zipsame
import numpy as np
from gym import spaces
# import scripts.utils as U
import scripts.utils as TF_U
from scripts.mpi_adam_optimizer import MpiAdamOptimizer
from mpi4py import MPI
from scripts import logger
import time
import cv2
comm = MPI.COMM_WORLD
from collections import deque

def lrelu(x, leak=0.2, name="lrelu"):
  return tf.maximum(x, leak*x)

class batch_norm(object):
    def __init__(self, epsilon=1e-5, momentum = 0.9, name="batch_norm"):
        with tf.variable_scope(name):
            self.epsilon  = epsilon
            self.momentum = momentum
            self.name = name

    def __call__(self, x, train=True):
        return tf.contrib.layers.batch_norm(x,
                      decay=self.momentum, 
                      updates_collections=None,
                      epsilon=self.epsilon,
                      scale=True,
                      is_training=train,
                      scope=self.name)

def display_images(im1, im2=None, lag=1):
    im1 = im1.reshape([-1] + list(im1.shape))
    if im2 is None:
        for im in im1:
            im = cv2.resize(im,dsize=(int(im.shape[1] * 6), int(im.shape[0] * 6)))
            cv2.imshow("frame", im)
            cv2.waitKey(lag)
    else:
        im2 = im2.reshape([-1] + list(im2.shape))
        for i1, i2 in zip(im1, im2):
            i1 = cv2.resize(i1,dsize=(int(i1.shape[1] * 6), int(i1.shape[0] * 6)))
            i2 = cv2.resize(i2,dsize=(int(i2.shape[1] * 6), int(i2.shape[0] * 6)))
            cv2.imshow("frame", np.hstack([i1,i2]))
            cv2.waitKey(lag)

def get_vars(scope):
    return [x for x in tf.global_variables() if scope in x.name]

# def neglogp(self, x):
#     return 0.5 * tf.reduce_sum(tf.square((x - self.mean) / self.std), axis=-1) \
#             + 0.5 * np.log(2.0 * np.pi) * tf.to_float(tf.shape(x)[-1]) \
#             + tf.reduce_sum(self.logstd, axis=-1)

def log_normal_pdf(sample, mean, logvar, raxis=1):
    log2pi = tf.math.log(2. * np.pi)
    return tf.reduce_sum(
            -.5 * ((sample - mean) ** 2. * tf.exp(-logvar) + logvar + log2pi),
            axis=raxis)

def reparameterize(mean, logvar):
    eps = tf.random.normal(shape=tf.shape(mean))
    return eps * tf.exp(logvar * .5) + mean

class Policy():
    def __init__(self, name, ob, im, vae_im, z_ph, ob_space, ob_size, im_size, vae_im_size, ac_size, env, LAM, ac_space, latent_dim, sess, vis,  args, hid_size, normalize=True, const_std=False):
        self.name = name
        self.ob = ob
        self.im = im
        self.vae_im = vae_im
        self.z_ph = z_ph
        self.ob_size = ob_size
        self.im_size = im_size
        self.vae_im_size = vae_im_size
        self.ac_size = ac_size
        self.hid_size = hid_size
        self.env = env
        self.LAM = LAM
        self.sess = sess
        self.args = args
        self.pdtype = pdtype = make_pdtype(ac_space)
        self.vis = vis
        self.const_std = const_std
        self.latent_dim = latent_dim
        sequence_length = None
        if normalize:
            with tf.variable_scope("obfilter"):
                self.ob_rms = RunningMeanStd(shape=ob_space.shape)
        self.quarter = int(self.vae_im_size[0]/4)
        if self.args.robot == "titan":
            self.min_val = 0
            self.max_val = 1
        else:
            # walking
            self.min_val = 0.25
            self.max_val = 2
                
        self.data_range = self.max_val - self.min_val
        # first_layer = 32
        # second_layer = 64
        self.first_layer = 16
        self.second_layer = 32
        self.third_layer = 64
        self.y_dim = 1
        self.output_height, self.output_width = 28, 28
        # self.gf_dim = 64
        # self.dfc_dim = 1024
        # self.gfc_dim = 1024
        # self.df_dim = 64
        self.gf_dim = 16
        self.dfc_dim = 64
        self.gfc_dim = 64
        self.df_dim = 16
        self.c_dim = 1
        self.batch_size = 32
        self.s_h,  self.s_w = self.vae_im_size[0], self.vae_im_size[1]
        self.s_h2, self.s_h4 = int(self.s_h/2), int(self.s_h/4)
        self.s_w2, self.s_w4 = int(self.s_w/2), int(self.s_w/4) 

        # if "ae" not in args.ae_type:
        self.d_bn1 = batch_norm(name='d_bn1')
        self.d_bn2 = batch_norm(name='d_bn2')
        self.d_bn3 = batch_norm(name='d_bn3')

        self.g_bn0 = batch_norm(name='g_bn0')
        self.g_bn1 = batch_norm(name='g_bn1')
        self.g_bn2 = batch_norm(name='g_bn2')
        self.g_bn3 = batch_norm(name='g_bn3')

        x = tf.nn.relu(tf.layers.conv2d(self.im, self.first_layer, 3, strides=2, padding="same", name="q_vis1"))
        # x = tf.nn.relu(tf.layers.conv2d(self.decoded, self.first_layer, 3, strides=2, padding="same", name="q_vis1"))
        x = tf.nn.relu(tf.layers.conv2d(x, self.second_layer, 3, strides=2, padding="same", name="q_vis2"))
        x = tf.layers.flatten(x)
        self.q_vis = tf.nn.relu(tf.layers.dense(x, self.latent_dim, name="q_vis3"))
   

        if "admis" in self.name:
            if "q" in args.ae_type:
                self.z_mean = self.q_vis
            else:
                # with tf.variable_scope('encode'):
                #     x = tf.layers.flatten(self.vae_im)
                #     x = tf.nn.relu(tf.layers.dense(x, hid_size, name="e_1"))
                #     x = tf.nn.relu(tf.layers.dense(x, hid_size, name="e_2"))
                #     x = tf.nn.relu(tf.layers.dense(x, hid_size, name="e_3"))
                #     x = tf.nn.relu(tf.layers.dense(x, hid_size, name="e_4"))
                #     self.z_mean = tf.nn.tanh(tf.layers.dense(x, self.latent_dim, name="e_z_mean"))
                    # x = tf.nn.relu(tf.layers.conv2d(self.vae_im, self.first_layer, 3, strides=2, padding="same", name="e_vis1",kernel_initializer=tf.keras.initializers.RandomNormal()))
                    # x = tf.nn.relu(tf.layers.conv2d(x, self.second_layer, 3, strides=2, padding="same", name="e_vis2",kernel_initializer=tf.keras.initializers.RandomNormal()))
                    # x = tf.nn.relu(tf.layers.conv2d(x, self.second_layer, 3, strides=2, padding="same", name="e_vis3",kernel_initializer=tf.keras.initializers.RandomNormal()))
                    # x = tf.layers.flatten(x)
                    # # x = tf.nn.tanh(tf.layers.dense(x, 16, name="e_vis3",kernel_initializer=tf.keras.initializers.RandomNormal()))
                    # self.z_mean = tf.nn.tanh(tf.layers.dense(x, self.latent_dim, name="e_vis_final",kernel_initializer=tf.keras.initializers.RandomNormal()))
                self.z_mean,_,_,_ = self.encoder(self.vae_im)

            # if "ae" not in self.args.ae_type:
            #     self.decoded = self.generator2(self.z_mean)
            #     self.decoded_from_z = self.generator2(self.z_ph, reuse=True)
            # else:
            self.decoded = self.generator(self.z_mean)
            self.decoded_from_z = self.generator(self.z_ph, reuse=True)


            if "ae" not in self.args.ae_type:
                self.discrim, self.discrim_logits = self.discriminator(self.vae_im)
                self.discrim_gen, self.discrim_logits_gen = self.discriminator(self.decoded, reuse=True)

                # with tf.variable_scope('Discriminator'):
                #     x = tf.nn.relu(self.d_bn1(tf.layers.conv2d(self.vae_im, self.first_layer, 3, strides=2, padding="same", name="dis_vis1")))
                #     x = tf.nn.relu(self.d_bn2(tf.layers.conv2d(x, self.second_layer, 3, strides=2, padding="same", name="dis_vis2")))
                #     x = tf.layers.flatten(x)
                #     x = tf.nn.tanh(tf.layers.dense(x, 16, name="dis_lin1"))
                #     self.discrim_logits = tf.layers.dense(x, 1, name='dis_lin2')
                #     self.discrim = tf.nn.sigmoid(self.discrim_logits, name='dis_lin3')
                
                # with tf.variable_scope('Discriminator', reuse=True):
                #     x = tf.nn.relu(self.d_bn1(tf.layers.conv2d(self.decoded, self.first_layer, 3, strides=2, padding="same", name="dis_vis1")))
                #     x = tf.nn.relu(self.d_bn2(tf.layers.conv2d(x, self.second_layer, 3, strides=2, padding="same", name="dis_vis2")))
                #     x = tf.layers.flatten(x)
                #     x = tf.nn.tanh(tf.layers.dense(x, 16, name="dis_lin1"))
                #     self.discrim_logits_gen = tf.layers.dense(x, 1, name='dis_lin2')
                #     self.discrim_gen = tf.nn.sigmoid(self.discrim_logits_gen, name='dis_lin3')
                

            with tf.variable_scope('Error'):
                # err_x = tf.nn.relu(tf.layers.conv2d(self.vae_im, self.first_layer, 3, strides=2, padding="same", name="err_1"))
                err_x = tf.nn.relu(tf.layers.conv2d(self.im, self.first_layer, 3, strides=2, padding="same", name="err_1"))
                err_x = tf.nn.relu(tf.layers.conv2d(err_x, self.second_layer, 3, strides=2, padding="same", name="err_2"))
                err_x = tf.layers.flatten(err_x)
                err_x = tf.nn.relu(tf.layers.dense(err_x, 16, name="err_3"))
                last_out = tf.nn.tanh(tf.layers.dense(err_x, hid_size, name="err_fc1", kernel_initializer=TF_U.normc_initializer(1.0)))
                last_out = tf.nn.tanh(tf.layers.dense(last_out, hid_size, name="err_fc2", kernel_initializer=TF_U.normc_initializer(1.0)))
                self.err = tf.layers.dense(last_out, 1, name='err_final', kernel_initializer=TF_U.normc_initializer(1.0))[:,0]

            # if "sse" in self.args.ae_type:
                # self.recon = tf.reduce_sum(tf.square(self.vae_im - self.decoded), axis=0)
            self.recon = tf.reduce_sum(tf.square(self.vae_im - self.decoded), axis=[1, 2, 3])
                # self.recon = tf.reduce_mean(tf.reduce_sum(tf.square(self.vae_im - self.decoded), axis=[1, 2, 3]))
            # else:
                # self.recon = tf.reduce_mean(tf.square(self.vae_im - self.decoded), axis=0)          
                # self.recon = tf.reduce_mean(tf.square(self.vae_im - self.decoded), axis=[1, 2, 3])

        if normalize:
            obz = tf.clip_by_value((ob - tf.stop_gradient(self.ob_rms.mean)) / tf.stop_gradient(self.ob_rms.std), -5.0, 5.0)
        else:
            obz = ob
        
        with tf.variable_scope('vf'):
            last_out = obz
            last_out = tf.concat(axis=1,values=[last_out, self.q_vis])
            last_out = tf.nn.tanh(tf.layers.dense(last_out, hid_size, name="q_fc1", kernel_initializer=TF_U.normc_initializer(1.0)))
            last_out = tf.nn.tanh(tf.layers.dense(last_out, hid_size, name="q_fc2", kernel_initializer=TF_U.normc_initializer(1.0)))
            self.qs = tf.layers.dense(last_out, 2, name='q_final', kernel_initializer=TF_U.normc_initializer(1.0))
            self.vpred = tf.reduce_max(self.qs, name='q_vpred')

            self.qs_error = tf.square(self.qs[:,0] - self.qs[:,1])
            # print(self.qs.shape, tf.square(self.qs[:,0] - self.qs[:,1]).shape, self.qs_error.shape); exit()

        with tf.variable_scope('pol'):
            last_out = obz
            last_out = tf.concat(axis=1,values=[last_out, self.q_vis])
            last_out = tf.nn.tanh(tf.layers.dense(last_out, hid_size, name='fc1', kernel_initializer=TF_U.normc_initializer(1.0)))
            last_out = tf.nn.tanh(tf.layers.dense(last_out, hid_size, name='fc2', kernel_initializer=TF_U.normc_initializer(1.0)))
            self.mean = tf.layers.dense(last_out, pdtype.param_shape()[0]//2, name='final', kernel_initializer=TF_U.normc_initializer(0.01))
            if self.args.const_std:
                logstd = tf.constant([np.log(0.75)]*int(pdtype.param_shape()[0]//2))
            else:      
                if self.args.std_from_state:
                    logstd = tf.layers.dense(last_out, pdtype.param_shape()[0]//2, name='logstd', kernel_initializer=TF_U.normc_initializer(0.01))
                else:
                    init = tf.constant_initializer([np.log(1.0)]*int(pdtype.param_shape()[0]//2))   
                    logstd = tf.get_variable(name="logstd", shape=[1, pdtype.param_shape()[0]//2],initializer=init)

        pdparam = tf.concat([self.mean, self.mean * 0.0 + logstd], axis=1)
        self.pd = pdtype.pdfromflat(pdparam)

        self.state_in = []
        self.state_out = []
        self.stochastic = tf.placeholder(dtype=tf.bool, shape=())
       
        # self.max_lam = 0.01
        # self.max_lam = 0.05
        self.max_lam = 0.1
        # if "lam" in self.args.tests:
        #     self.action = TF_U.switch(self.stochastic, self.pd.sample(), self.pd.mode()) - self.LAM*self.mse
        # else:
        self.action = TF_U.switch(self.stochastic, self.pd.sample(), self.pd.mode()) 
        # self.action = TF_U.switch(self.stochastic, self.pd.sample(), self.pd.mode()) - 0.1
        # self.action = TF_U.switch(self.stochastic, self.pd.sample(), self.pd.mode())

        # self.action = self.pd.sample()
        self.neglogp = self.pd.neglogp(self.action)
        self.state = tf.constant([])
        self.initial_state = None

        self.steps = 0
        self.eps_start = 0.5
        # self.eps_start = 1.0
        self.eps_end = 0.001
        self.eps = self.eps_start
        # self.eps_decay = 100000
        # self.eps_decay = 500000
        # self.eps_decay = 100000
        self.eps_decay = 50000

        self.lam_steps = 0
        self.lam_decay = 100000
        # self.eps_decay = eps_decay
        self.scaled_recon_loss = 0.0
        self.recon_loss = 0.0

        # if "const" in self.args.tests:
        self.lam = self.args.lam
        # self.recon_error_buffer = deque(maxlen=self.args.)
        self.recon_error_buffer = []
        self.recon_thres = 0.0
        self.latent_thres = 0.0
        # else:
            # self.lam = 0.0

        if "setup" in self.name:
            with tf.variable_scope('select'):
                select_hid_size = hid_size
                last_out_select = tf.nn.tanh(tf.layers.dense(last_out, select_hid_size, name='fc2', kernel_initializer=TF_U.normc_initializer(1.0)))

                high = np.inf*np.ones(1)
                low = -high
                select_ac_space = spaces.Box(low, high, dtype=np.float32)
                self.select_pdtype = select_pdtype = make_pdtype(select_ac_space)
                self.select_mean = tf.layers.dense(last_out_select, 1, name="select_final", kernel_initializer=TF_U.normc_initializer(0.01))
                select_init = tf.constant_initializer([np.log(1.0)]*int(select_pdtype.param_shape()[0]//2))
                select_logstd = tf.get_variable(name="select_logstd", shape=[1, select_pdtype.param_shape()[0]//2],initializer=select_init)
                select_pdparam = tf.concat([self.select_mean, self.select_mean * 0.0 + select_logstd], axis=1)
                self.select_pd = pdtype.pdfromflat(select_pdparam)

                stochastic_act = self.select_pd.sample()
                determinstic_act = self.select_pd.mode()
            
                self.select = TF_U.switch(self.stochastic, stochastic_act, determinstic_act)
                self.neglogp_select = self.select_pd.neglogp(self.select)
        
    def generator(self, inputs, reuse=False):
        with tf.variable_scope("decode") as scope:
            if reuse:
                scope.reuse_variables()
            last_out = tf.nn.relu(tf.layers.dense(inputs, self.quarter * self.quarter * self.second_layer, name="g_1"))
            last_out = tf.reshape(last_out, (-1, self.quarter, self.quarter, self.second_layer))
            last_out = tf.nn.relu(tf.layers.conv2d_transpose(last_out, self.second_layer, 3, strides=2, padding='same', name="g_2"))
            last_out = tf.nn.relu(tf.layers.conv2d_transpose(last_out, self.first_layer, 3, strides=2, padding='same', name="g_3"))
            self.decoded_logits = tf.layers.conv2d_transpose(last_out, 1, 3, padding="same", name="g_4")
            return ((tf.nn.tanh(self.decoded_logits, name="g_final") + 1 )/ 2) * self.data_range + self.min_val
        

    # def discriminator(self, inputs, reuse=False):
    #     with tf.variable_scope("discriminator") as scope:
    #         if reuse:
    #             scope.reuse_variables()
    #         # h0 = lrelu(tf.layers.conv2d(inputs, 32, 3, strides=2, padding='same', name='d_h0_conv'))
    #         x = lrelu(tf.layers.conv2d(inputs, self.first_layer, 3, strides=2, padding="same", name="dis_vis1"))
    #         if self.args.batch_norm:
    #             x = lrelu(self.d_bn1(tf.layers.conv2d(x, self.second_layer, 3, strides=2, padding="same", name="dis_vis2")))
    #             x = tf.layers.flatten(x)
    #             x = lrelu(self.d_bn2(tf.layers.dense(x, self.hid_size, name="dis_lin1")))
    #         else:
    #             x = lrelu(tf.layers.conv2d(x, self.second_layer, 3, strides=2, padding="same", name="dis_vis2"))
    #             x = tf.layers.flatten(x)
    #             x = lrelu(tf.layers.dense(x, self.hid_size, name="dis_lin1"))
    #         discrim_logits = tf.layers.dense(x, 1, name='d_lin2')
    #         return tf.nn.sigmoid(discrim_logits), discrim_logits

    # def generator(self, inputs, reuse=False):

    #     with tf.variable_scope("generator") as scope:
    #         if reuse:
    #             scope.reuse_variables()
    #         if self.args.batch_norm:
    #             h0 = tf.nn.relu(self.g_bn0(tf.layers.dense(inputs, self.gfc_dim, name='g_h0_lin')))
    #             h1 = tf.nn.relu(self.g_bn1(tf.layers.dense(h0, self.gf_dim*2*self.s_h4*self.s_w4, name='g_h1_lin')))
    #             h1 = tf.reshape(h1, [-1, self.s_h4, self.s_w4, self.gf_dim * 2])
    #             h2 = tf.nn.relu(self.g_bn2(tf.layers.conv2d_transpose(h1, self.first_layer, 3, strides=2, padding='same',name='g_h2')))
    #             h3 = tf.nn.relu(self.g_bn3(tf.layers.conv2d_transpose(h2, self.second_layer, 3, strides=2, padding='same', name='g_h3')))
    #         else:
    #             h0 = tf.nn.relu(tf.layers.dense(inputs, self.gfc_dim, name='g_h0_lin'))
    #             h1 = tf.nn.relu(tf.layers.dense(h0, self.gf_dim*2*self.s_h4*self.s_w4, name='g_h1_lin'))
    #             h1 = tf.reshape(h1, [-1, self.s_h4, self.s_w4, self.gf_dim * 2])
    #             h2 = tf.nn.relu(tf.layers.conv2d_transpose(h1, self.first_layer, 3, strides=2, padding='same',name='g_h2'))
    #             h3 = tf.nn.relu(tf.layers.conv2d_transpose(h2, self.second_layer, 3, strides=2, padding='same',name='g_h3'))
    #         h4 = tf.layers.conv2d_transpose(h3, 1, 3, padding='same', name='g_h4')
    #         return ((tf.nn.tanh(h4) + 1 )/ 2) * self.data_range + self.min_val

    def discriminator(self, inputs, reuse=False):
        with tf.variable_scope("discriminator") as scope:
            if reuse:
                scope.reuse_variables()
            # h0 = lrelu(tf.layers.conv2d(inputs, 32, 3, strides=2, padding='same', name='d_h0_conv'))
            x = lrelu(tf.layers.conv2d(inputs, self.first_layer, 3, strides=2, padding="same", name="dis_vis1"))
            # if self.args.batch_norm:
            #     x = lrelu(self.d_bn1(tf.layers.conv2d(x, self.second_layer, 3, strides=2, padding="same", name="dis_vis2")))
            #     x = tf.layers.flatten(x)
            #     x = lrelu(self.d_bn2(tf.layers.dense(x, self.hid_size, name="dis_lin1")))
            # else:
            x = lrelu(tf.layers.conv2d(x, self.second_layer, 3, strides=2, padding="same", name="dis_vis2"))
            x = tf.layers.flatten(x)
            x = tf.nn.tanh(tf.layers.dense(x, self.hid_size, name="dis_lin1"))
            discrim_logits = tf.layers.dense(x, 1, name='d_lin2')
            return tf.nn.sigmoid(discrim_logits), discrim_logits


    # def discriminator(self, inputs, reuse=False):
    #     # df_dim = 64
    #     # dfc_dim = 1024
    #     with tf.variable_scope("discriminator") as scope:
    #         if reuse:
    #             scope.reuse_variables()
    #         h0 = lrelu(tf.layers.conv2d(inputs, 32, 3, strides=2, padding='same', name='dis_h0_conv'))
    #         if self.args.batch_norm:
    #             h1 = lrelu(self.d_bn1(tf.layers.conv2d(h0, self.df_dim, 3, strides=2, padding='same', name='dis_h1_conv')))
    #             h1 = tf.layers.flatten(h1)
    #             h2 = lrelu(self.d_bn2(tf.layers.dense(h1, self.dfc_dim, name='dis_h2_lin')))
    #         else:
    #             h1 = lrelu(tf.layers.conv2d(h0, self.df_dim, 3, strides=2, padding='same', name='dis_h1_conv'))
    #             h1 = tf.layers.flatten(h1)
    #             h2 = lrelu(tf.layers.dense(h1, self.dfc_dim, name='dis_h2_lin'))
    #         h3 = tf.layers.dense(h2, 1, name='dis_h3_lin')
    #         return tf.nn.sigmoid(h3), h3
    
    def encoder(self, inputs, reuse=False):
        df_dim = 128
        # self.first_layer = 16
        # df_dim = 64
        # df_dim = 32
        dfc_dim = 1024
        c_dim = 3
        with tf.variable_scope("encoder") as scope:
            if reuse:
                scope.reuse_variables()
            x = tf.nn.relu(tf.layers.conv2d(inputs, self.first_layer, 3, strides=2, padding="same", name="e_vis1"))
            # x = tf.nn.relu(tf.layers.conv2d(x, self.second_layer, 3, strides=2, padding="same", name="e_vis2"))
            x = tf.layers.flatten(x)
            # x = tf.nn.relu(tf.layers.dense(inputs, df_dim, name="e_vis1"))
            x = tf.nn.relu(tf.layers.dense(x, df_dim, name="e_vis2"))
            x = tf.nn.relu(tf.layers.dense(x, df_dim, name="e_vis3"))
            x = tf.nn.relu(tf.layers.dense(x, df_dim, name="e_vis4"))
            x = tf.nn.relu(tf.layers.dense(x, df_dim, name="e_vis5"))
            x = tf.nn.relu(tf.layers.dense(x, df_dim, name="e_vis6"))
            x = tf.nn.relu(tf.layers.dense(x, df_dim, name="e_vis7"))
            # x = tf.nn.relu(tf.layers.dense(x, df_dim, name="e_vis8"))
            # x = tf.nn.relu(tf.layers.dense(x, df_dim, name="e_vis9"))
            # x = tf.nn.relu(tf.layers.dense(x, df_dim, name="e_vis10"))
            # x = tf.nn.relu(tf.layers.dense(x, df_dim, name="e_vis6"))
            # x = tf.nn.relu(tf.layers.dense(x, df_dim, name="e_vis7"))
            # x = tf.nn.relu(tf.layers.dense(x, df_dim, name="e_vis9"))
            # x = tf.nn.relu(tf.layers.dense(x, df_dim, name="e_vis10"))
            # x = tf.nn.tanh(tf.layers.dense(x, df_dim, name="e_vis3"))
            # z_mean = tf.nn.tanh(tf.layers.dense(x, self.latent_dim, name="e_vis4"))
            
            z_mean = tf.nn.tanh(tf.layers.dense(x, self.latent_dim, name="e_vis_final"))
            # z_mean = tf.nn.relu(tf.layers.dense(x, self.latent_dim, name="e_vis_final"))
    
            # h0 = lrelu(tf.layers.conv2d(inputs, c_dim, 5, strides=2, padding='same', name='e_h0_conv', kernel_initializer=self.initializer()))
            # if self.args.batch_norm:
            #     h1 = lrelu(self.e_bn1(tf.layers.conv2d(h0, df_dim, 5, strides=2, padding='same', name='e_h1_conv',kernel_initializer=self.initializer())))
            #     h2 = lrelu(self.e_bn2(tf.layers.conv2d(h1, df_dim, 5, strides=2, padding='same', name='e_h2_conv',kernel_initializer=self.initializer())))
            #     h3 = tf.layers.flatten(h2)
            #     h3 = tf.nn.tanh(self.e_bn3(tf.layers.dense(h3, dfc_dim, name='e_h2_lin', kernel_initializer=self.initializer())))
            #     h3 = tf.nn.tanh(self.e_bn4(tf.layers.dense(h3, dfc_dim, name='e_h3_lin',kernel_initializer=self.initializer())))
            # else:
            # h1 = lrelu(tf.layers.conv2d(h0, df_dim, 5, strides=2, padding='same', name='e_h1_conv', kernel_initializer=self.initializer()))
            # h2 = lrelu(tf.layers.conv2d(h1, df_dim, 5, strides=2, padding='same', name='e_h2_conv', kernel_initializer=self.initializer()))
            
            # h0 = tf.nn.relu(tf.layers.conv2d(inputs, c_dim, 5, strides=2, padding='same', name='e_h0_conv', kernel_initializer=self.initializer()))
            # h1 = tf.nn.relu(tf.layers.conv2d(h0, self.df_dim, 5, strides=2, padding='same', name='e_h1_conv', kernel_initializer=self.initializer()))
            # h2 = tf.nn.relu(tf.layers.conv2d(h1, self.df_dim, 5, strides=2, padding='same', name='e_h2_conv', kernel_initializer=self.initializer()))
            # # h1 = lrelu(tf.layers.conv2d(h0, df_dim, 5, strides=2, padding='same', name='e_h1_conv', kernel_initializer=self.initializer()))
            # # h2 = lrelu(tf.layers.conv2d(h1, df_dim, 5, strides=2, padding='same', name='e_h2_conv', kernel_initializer=self.initializer()))

            # # z_mean = tf.layers.flatten(h2)
            # h3 = tf.layers.flatten(h2)
            # # h3 = tf.nn.tanh(tf.layers.dense(h2, dfc_dim, name='e_h2_lin', kernel_initializer=self.initializer()))
            # z_mean = tf.nn.tanh(tf.layers.dense(h3, self.latent_dim, name='e_mean', kernel_initializer=self.initializer()))
            # z_log_var = tf.layers.dense(h3, self.latent_dim, name='e_log_var')            

            # z = reparameterize(z_mean, z_log_var)
            # err = tf.nn.tanh(tf.layers.dense(tf.nn.tanh(z_mean), self.dfc_dim, name='e_err1'))
            # err = tf.layers.dense(err, 1, name='e_err2')
            return z_mean, 0, 0, 0

    # def discriminator(self, inputs, reuse=False):
    #     with tf.variable_scope("discriminator") as scope:
    #         if reuse:
    #             scope.reuse_variables()
    #         # h0 = lrelu(tf.layers.conv2d(inputs, 32, 3, strides=2, padding='same', name='d_h0_conv'))
    #         x = lrelu(tf.layers.conv2d(inputs, self.first_layer, 3, strides=2, padding="same", name="dis_vis1"))
    #         if self.args.batch_norm:
    #             x = lrelu(self.d_bn1(tf.layers.conv2d(x, self.second_layer, 3, strides=2, padding="same", name="dis_vis2")))
    #             x = tf.layers.flatten(x)
    #             x = lrelu(self.d_bn2(tf.layers.dense(x, self.hid_size, name="dis_lin1")))
    #         else:
    #             x = lrelu(tf.layers.conv2d(x, self.second_layer, 3, strides=2, padding="same", name="dis_vis2"))
    #             x = tf.layers.flatten(x)
    #             x = tf.nn.tanh(tf.layers.dense(x, self.hid_size, name="dis_lin1"))
    #         discrim_logits = tf.layers.dense(x, 1, name='d_lin2')
    #         return tf.nn.sigmoid(discrim_logits), discrim_logits

    # def generator1(self, inputs, reuse=False):
    #     with tf.variable_scope("decode") as scope:
    #         if reuse:
    #             scope.reuse_variables()
    #         last_out = tf.nn.relu(tf.layers.dense(inputs, self.quarter * self.quarter * self.second_layer, name="g_1"))
    #         last_out = tf.reshape(last_out, (-1, self.quarter, self.quarter, self.second_layer))
    #         last_out = tf.nn.relu(tf.layers.conv2d_transpose(last_out, self.second_layer, 3, strides=2, padding='same', name="g_2"))
    #         last_out = tf.nn.relu(tf.layers.conv2d_transpose(last_out, self.first_layer, 3, strides=2, padding='same', name="g_3"))
    #         self.decoded_logits = tf.layers.conv2d_transpose(last_out, 1, 3, padding="same", name="g_4")
    #         return ((tf.nn.tanh(self.decoded_logits, name="g_final") + 1 )/ 2) * self.data_range + self.min_val
        

    # def generator2(self, inputs, reuse=False):
    #     with tf.variable_scope("generator") as scope:
    #         if reuse:
    #             scope.reuse_variables()
    #         if self.args.batch_norm:
    #             h0 = tf.nn.relu(self.g_bn0(tf.layers.dense(inputs, self.gfc_dim, name='g_h0_lin')))
    #             h1 = tf.nn.relu(self.g_bn1(tf.layers.dense(h0, self.gf_dim*2*self.s_h4*self.s_w4, name='g_h1_lin')))
    #             h1 = tf.reshape(h1, [-1, self.s_h4, self.s_w4, self.gf_dim * 2])
    #             h2 = tf.nn.relu(self.g_bn2(tf.layers.conv2d_transpose(h1, self.first_layer, 3, strides=2, padding='same',name='g_h2')))
    #             h3 = tf.nn.relu(self.g_bn3(tf.layers.conv2d_transpose(h2, self.second_layer, 3, strides=2, padding='same', name='g_h3')))
    #         else:
    #             h0 = tf.nn.relu(tf.layers.dense(inputs, self.gfc_dim, name='g_h0_lin'))
    #             h1 = tf.nn.relu(tf.layers.dense(h0, self.gf_dim*2*self.s_h4*self.s_w4, name='g_h1_lin'))
    #             h1 = tf.reshape(h1, [-1, self.s_h4, self.s_w4, self.gf_dim * 2])
    #             h2 = tf.nn.relu(tf.layers.conv2d_transpose(h1, self.first_layer, 3, strides=2, padding='same',name='g_h2'))
    #             h3 = tf.nn.relu(tf.layers.conv2d_transpose(h2, self.second_layer, 3, strides=2, padding='same',name='g_h3'))
    #         h4 = tf.layers.conv2d_transpose(h3, 1, 3, padding='same', name='g_h4')
    #         return ((tf.nn.tanh(h4) + 1 )/ 2) * self.data_range + self.min_val
    
    # def encoder(self, inputs, reuse=False):
    #     df_dim = 64
    #     dfc_dim = 1024
    #     c_dim = 3
    #     with tf.variable_scope("encoder") as scope:
    #         if reuse:
    #             scope.reuse_variables()
    #         # h0 = lrelu(tf.layers.conv2d(inputs, c_dim, 5, strides=2, padding='same', name='e_h0_conv', kernel_initializer=self.initializer()))
    #         # if self.args.batch_norm:
    #         #     h1 = lrelu(self.e_bn1(tf.layers.conv2d(h0, df_dim, 5, strides=2, padding='same', name='e_h1_conv',kernel_initializer=self.initializer())))
    #         #     h2 = lrelu(self.e_bn2(tf.layers.conv2d(h1, df_dim, 5, strides=2, padding='same', name='e_h2_conv',kernel_initializer=self.initializer())))
    #         #     h3 = tf.layers.flatten(h2)
    #         #     h3 = tf.nn.tanh(self.e_bn3(tf.layers.dense(h3, dfc_dim, name='e_h2_lin', kernel_initializer=self.initializer())))
    #         #     h3 = tf.nn.tanh(self.e_bn4(tf.layers.dense(h3, dfc_dim, name='e_h3_lin',kernel_initializer=self.initializer())))
    #         # else:
    #         # h1 = lrelu(tf.layers.conv2d(h0, df_dim, 5, strides=2, padding='same', name='e_h1_conv', kernel_initializer=self.initializer()))
    #         # h2 = lrelu(tf.layers.conv2d(h1, df_dim, 5, strides=2, padding='same', name='e_h2_conv', kernel_initializer=self.initializer()))
            
    #         h0 = tf.nn.relu(tf.layers.conv2d(inputs, c_dim, 5, strides=2, padding='same', name='e_h0_conv', kernel_initializer=self.initializer()))
    #         h1 = tf.nn.relu(tf.layers.conv2d(h0, self.df_dim, 5, strides=2, padding='same', name='e_h1_conv', kernel_initializer=self.initializer()))
    #         h2 = tf.nn.relu(tf.layers.conv2d(h1, self.df_dim, 5, strides=2, padding='same', name='e_h2_conv', kernel_initializer=self.initializer()))
    #         # h1 = lrelu(tf.layers.conv2d(h0, df_dim, 5, strides=2, padding='same', name='e_h1_conv', kernel_initializer=self.initializer()))
    #         # h2 = lrelu(tf.layers.conv2d(h1, df_dim, 5, strides=2, padding='same', name='e_h2_conv', kernel_initializer=self.initializer()))

    #         # z_mean = tf.layers.flatten(h2)
    #         h3 = tf.layers.flatten(h2)
    #         # h3 = tf.nn.tanh(tf.layers.dense(h2, dfc_dim, name='e_h2_lin', kernel_initializer=self.initializer()))
    #         z_mean = tf.nn.tanh(tf.layers.dense(h3, self.latent_dim, name='e_mean', kernel_initializer=self.initializer()))
    #         z_log_var = tf.layers.dense(h3, self.latent_dim, name='e_log_var')            

    #         z = reparameterize(z_mean, z_log_var)
    #         err = tf.nn.tanh(tf.layers.dense(tf.nn.tanh(z_mean), self.dfc_dim, name='e_err1'))
    #         err = tf.layers.dense(err, 1, name='e_err2')
    #         return z_mean
    
    def initializer(self):
        return tf.keras.initializers.RandomNormal()

    def decode_from_z(self, z):
        decoded = self.sess.run(self.decoded_from_z,feed_dict={self.z_ph:z})
        return decoded

    def encode(self, im):
        # z_mean, z_log_var = self.sess.run([self.z_mean, self.z_log_var], feed_dict={self.im:im})
        z_mean = self.sess.run(self.z_mean, feed_dict={self.im:im, self.vae_im:im})
        return z_mean
    
    def decode(self, im):
        decoded = self.sess.run(self.decoded, feed_dict={self.im:im, self.vae_im:im})
        return decoded

    def update_eps(self):
        self.eps =  max(self.eps_start - (float(self.steps) / (self.eps_decay)), self.eps_end)
        self.steps += 1

    def step(self, ob, im, stochastic=False, eval_pol=False):

        if self.args.train_vae:
            im_copy = im.copy()
            vae_im = im[:self.vae_im_size[0],int((self.im_size[0]-self.vae_im_size[0])/2):int(self.im_size[0]-(self.im_size[0]-self.vae_im_size[0])/2),:]
            # if not stochastic:
            #     self.lam = self.max_lam
            # if "err" in self.args.tests:
            # print(im.shape, vae_im.shape); exit()
            im_mse = self.sess.run(self.recon, feed_dict={ self.vae_im: vae_im.reshape([1]+self.vae_im_size),self.im: im.reshape([1]+self.im_size)})
            # else:
            
            # self.recon_loss, self.err_pred = self.sess.run([self.recon, self.err], feed_dict={ self.vae_im: vae_im.reshape([1]+self.vae_im_size), self.im: im.reshape([1]+self.im_size)})
            # self.recon_loss_pred = (self.recon_loss[0] - self.err_pred[0])**2 

            
            # if "err" in self.args.tests:
                # self.scaled_recon_loss = self.args.lam * min(abs(self.recon_loss[0] - self.recon_loss_pred[0]), 50)
            # else:
                # self.scaled_recon_loss = min(self.args.lam * abs(self.recon_loss[0]), 50)
                # self.scaled_recon_loss = self.args.lam * (self.recon_loss[0]*self.recon_loss[0])

            q, v, state, neglogp, recon_im, self.err_pred, self.recon_loss = self.sess.run([self.qs, self.vpred, self.state, self.neglogp, self.decoded, self.err, self.qs_error], 
                                feed_dict={self.ob: ob.reshape([1,self.ob_size]), 
                                            self.im: im.reshape([1]+self.im_size), 
                                            self.vae_im: vae_im.reshape([1]+self.vae_im_size), 
                                            self.im: im.reshape([1]+self.im_size), 
                                            self.stochastic:stochastic})
            # self.recon_loss = [(qs[1] - qs[0])**2]
            # print(self.recon_loss)
            self.recon_loss = [self.recon_loss]
            self.recon_loss_pred = (self.recon_loss[0] - self.err_pred[0])**2             
            
            new_qs = q[0]
            im_copy[:self.vae_im_size[0],int((self.im_size[1]-self.vae_im_size[1])/2):int(self.im_size[1]-(self.im_size[1]-self.vae_im_size[1])/2),:] = recon_im[0,::]
            

            self.update_eps()
            # self.recon_error_buffer.append(self.recon_loss[0])
            if np.random.random() > self.eps or not stochastic: 
                # if not stochastic:
                if eval_pol:
                    # print(self.recon_loss)
                    act_from_q = np.argmax(new_qs)
                    if act_from_q == 1 and self.recon_loss_pred < self.recon_thres:
                        act = 1
                    else:
                        act = 0
                else:
                    act = np.argmax(new_qs)
            else:
                act = np.random.randint(2)
            return [act], v, state, neglogp[0], im_copy, self.err_pred[0], self.recon_loss[0], np.clip(new_qs,0.0, None), self.recon_loss_pred, im_mse

    def update_err_thres(self):
        if len(self.recon_error_buffer) == 0:
            self.recon_error_buffer.append(self.recon_thres)
        if "max" in self.args.ae_type:
            # thres = max(self.recon_error_buffer)
            thres = np.percentile(self.recon_error_buffer, 99)
        else:
            thres = np.percentile(self.recon_error_buffer, 95)
        all_maxes = MPI.COMM_WORLD.allgather(thres) 
        self.recon_thres = max(all_maxes)
        self.recon_error_buffer = []

class Model(Base):
    def __init__(self, name, env, ac_size, ob_size, im_size=[48,48,1], vae_im_size=[48,48,1], args=None, PATH=None, writer=None, hid_size=256, vis=False, normalize=True, ent_coef=0.0, vf_coef=0.5, max_grad_norm=0.5, mpi_rank_weight=1, max_timesteps=int(1e6), lr=3e-4, horizon=2048, batch_size=32, const_std=False):
        self.max_timesteps = max_timesteps
        self.horizon = horizon
        self.batch_size = batch_size
        self.learning_rate = lr
        self.env = env
        self.ob_size = ob_size
        self.im_size = im_size
        self.vae_im_size = vae_im_size
        self.ac_size = ac_size

        self.sess = sess = TF_U.get_session()
        self.name = name
        high = np.inf*np.ones(ac_size)
        low = -high
        ac_space = spaces.Box(low, high, dtype=np.float32)
        high = np.inf*np.ones(ob_size)
        low = -high
        ob_space = spaces.Box(low, high, dtype=np.float32)
        self.args = args   
        im_size = im_size
        self.PATH = PATH
        self.hid_size = hid_size
        self.writer = writer
        # self.latent_dim = latent_dim = 16
        self.latent_dim = self.args.latent_dim
        # self.latent_dim = latent_dim = 2
        # self.latent_dim = latent_dim = 64
        ob = TF_U.get_placeholder(name="ob_" + name, dtype=tf.float32, shape=[None] + list(ob_space.shape))
        im = TF_U.get_placeholder(name="im_" + name, dtype=tf.float32, shape=[None] + im_size)
        vae_im = TF_U.get_placeholder(name="vae_im_" + name, dtype=tf.float32, shape=[None] + vae_im_size)
        z_ph = TF_U.get_placeholder(name="z_ph_" + name, dtype=tf.float32, shape=[None, self.latent_dim])
        self.LAM = TF_U.get_placeholder(name="lam_" + self.name, dtype=tf.float32, shape=[None])
        
        self.args = args
        if ('vel' in name or 'hex' in name) and not args.vis:
            self.vis = False
        else:
            self.vis = vis
        with tf.variable_scope(name, reuse=tf.AUTO_REUSE):
            # Need some more params when using recurrent
            # act_model = Policy(ob, im, ob_space, im_size, ac_space, sess, self.vis, normalize=normalize, hid_size=hid_size)
            train_model = Policy(name, ob, im, vae_im, z_ph, ob_space, ob_size, im_size, vae_im_size, ac_size, env, self.LAM, ac_space, self.latent_dim, sess, self.vis, args=args, normalize=normalize, hid_size=hid_size, const_std=const_std)
        Base.__init__(self)

        # CREATE THE PLACEHOLDERS
        self.A = A = train_model.pdtype.sample_placeholder([None])
        self.ADV = ADV = tf.placeholder(tf.float32, [None])
        self.R = R = tf.placeholder(tf.float32, [None])
        # Keep track of old actor
        self.OLDNEGLOGPAC = OLDNEGLOGPAC = tf.placeholder(tf.float32, [None])
        # Keep track of old critic
        self.OLDVPRED = OLDVPRED = tf.placeholder(tf.float32, [None])
        self.LR = LR = tf.placeholder(tf.float32, [])
        # Cliprange
        self.CLIPRANGE = CLIPRANGE = tf.placeholder(tf.float32, [])
        self.ERR = ERR = tf.placeholder(tf.float32, [None], name="ERR")

        neglogpac = train_model.pd.neglogp(A)

        # Calculate the entropy
        # Entropy is used to improve exploration by limiting the premature convergence to suboptimal policy.
        entropy = tf.reduce_mean(train_model.pd.entropy())


        if self.args.discrete:
            # self.ACTION = tf.placeholder(tf.float32, [None, 2])
            self.ACTION = tf.placeholder(tf.int32, [None,1])
            one_hot = tf.one_hot(tf.squeeze(self.ACTION), 2)
            # current_q_t = tf.reduce_sum(train_model.vpred * one_hot, 1)
            current_q_t = tf.reduce_sum(train_model.qs * one_hot, 1)
            vf_loss = 0.5*tf.reduce_mean(tf.square(current_q_t - self.R))
        # Get the predicted value
        elif self.args.dual_value:
            vf_loss1 = 0.5*tf.reduce_mean(tf.square(train_model.vpred1 - R))
            vf_loss2 = 0.5*tf.reduce_mean(tf.square(train_model.vpred2 - R))
            vf_loss = vf_loss1 + vf_loss2
        else:
            vf_loss = 0.5*tf.reduce_mean(tf.square(train_model.vpred - R))
        # print(vf_loss, current_q_t, R)
        # Calculate ratio (pi current policy / pi old policy)
        ratio = tf.exp(OLDNEGLOGPAC - neglogpac)

        # Defining Loss = - J is equivalent to max J
        pg_losses = -ADV * ratio
        pg_losses2 = -ADV * tf.clip_by_value(ratio, 1.0 - CLIPRANGE, 1.0 + CLIPRANGE)

        # Final PG loss
        pg_loss = tf.reduce_mean(tf.maximum(pg_losses, pg_losses2))
        approxkl = .5 * tf.reduce_mean(tf.square(neglogpac - OLDNEGLOGPAC))
        clipfrac = tf.reduce_mean(tf.to_float(tf.greater(tf.abs(ratio - 1.0), CLIPRANGE)))

        if "admis" in self.name:
                
            # if "err" in self.args.tests:
            #     self.vae_loss = tf.reduce_mean(train_model.recon) + self.pred_loss 
            # else:

            self.gen_mse_loss = tf.reduce_mean(train_model.recon)
            # self.gen_mse_loss = train_model.recon
            # self.err_loss = tf.reduce_mean(tf.square(train_model.err - tf.stop_gradient(train_model.recon)))
            self.err_loss = 0.5*tf.reduce_mean(tf.square(train_model.err - self.ERR))


            std = tf.reduce_mean(train_model.pd.std)
            mean_ratio = tf.reduce_mean(ratio)
            mean_adv = tf.reduce_mean(self.ADV) 
            all_variables = tf.trainable_variables()

            if "ae" in args.ae_type:
                # self.g_loss = self.gen_mse_loss
                self.g_loss = self.gen_mse_loss 
                self.g_stats_list = [self.gen_mse_loss]
                self.e_stats_list = [self.err_loss]
                # self.vae_stats_list = [self.g_loss]
                self.loss_names = ['policy_loss', 'value_loss', 'policy_entropy', 'approxkl','clipfrac', 'std', 'ratio',    'adv', 'cliprange', 'learning_rate', 'vae_gen_loss', 'vae_err_loss']
            else:
                self.G = train_model.decoded
                self.D, self.D_logits = train_model.discrim, train_model.discrim_logits
                self.D_, self.D_logits_ = train_model.discrim_gen, train_model.discrim_logits_gen
            
                def sigmoid_cross_entropy_with_logits(x, y):
                    try:
                        return tf.nn.sigmoid_cross_entropy_with_logits(logits=x, labels=y)
                    except:
                        return tf.nn.sigmoid_cross_entropy_with_logits(logits=x, targets=y)

                self.d_loss_real = tf.reduce_mean(
                sigmoid_cross_entropy_with_logits(self.D_logits, tf.ones_like(self.D)))
                self.d_loss_fake = tf.reduce_mean(
                sigmoid_cross_entropy_with_logits(self.D_logits_, tf.zeros_like(self.D_)))
                
                self.d_loss = self.d_loss_real + self.d_loss_fake
                
                self.gan_loss = tf.reduce_mean(sigmoid_cross_entropy_with_logits(self.D_logits_, tf.ones_like(self.D_)))
                self.g_loss = self.gan_loss + self.gen_mse_loss
                        
                self.d_stats_list = [self.d_loss]
                self.g_stats_list = [self.gan_loss, self.gen_mse_loss]
                self.e_stats_list = [self.err_loss]

                dis_var = [var for var in all_variables if 'dis_' in var.name]
                # en_var = [var for var in all_variables if 'e_' in var.name]

                self._dis_train_op = self.get_train_ops(self.d_loss, name + "/dis", params=dis_var)

                # self._train_op_gen = self.get_train_ops(train_model.generator_loss, name + "/gen", params=en_var)
                # self.vae_stats_list = [self.g_loss, self.d_loss]
                self.loss_names = ['policy_loss', 'value_loss', 'policy_entropy', 'approxkl','clipfrac', 'std', 'ratio',  'adv', 'cliprange', 'learning_rate', 'vae_qs_error', 'vae_q_err_loss','vae_gan_loss', 'vae_gen_loss', 'vae_err_loss','vae_dis_loss']

            gen_var = [var for var in all_variables if 'g_' in var.name] 
            err_var = [var for var in all_variables if 'err_' in var.name]
            if "enc" in args.ae_type:
                gen_var += [var for var in all_variables if 'e_' in var.name]
            self._gen_train_op = self.get_train_ops(self.g_loss, name + "/gen", params=gen_var)            
            self._err_train_op = self.get_train_ops(self.err_loss, name + "/err", params=err_var)

            q_var = [var for var in all_variables if 'q_' in var.name]            
            loss = vf_loss 
            self._train_op = self.get_train_ops(loss, name, params=q_var)

            # self.stats_list = [pg_loss, vf_loss, entropy*self.args.ent_coef, approxkl, clipfrac, std, mean_ratio, mean_adv, CLIPRANGE, LR, self.total_vae_loss, self.vae_loss, self.recon_loss]
            self.stats_list = [pg_loss, vf_loss, entropy*self.args.ent_coef, approxkl, clipfrac, std, mean_ratio, mean_adv, CLIPRANGE, LR, train_model.qs_error]
            # self.e_stats = [self.err_loss]
            # self.vae_stats_list = [self.vae_loss, self.kl_loss, train_model.recon_mse]

        else:
            loss = pg_loss - entropy * self.args.ent_coef + vf_loss
                
            std = tf.reduce_mean(train_model.pd.std)
            mean_ratio = tf.reduce_mean(ratio)
            mean_adv = tf.reduce_mean(self.ADV)
        
            self.loss_names = ['policy_loss', 'value_loss', 'policy_entropy', 'approxkl', 
            'clipfrac', 'std', 'ratio', 'adv', 'cliprange', 'learning_rate']
            self.stats_list = [pg_loss, vf_loss, entropy*self.args.ent_coef, approxkl, clipfrac, std, mean_ratio, mean_adv, CLIPRANGE, LR]
        
            self._train_op = self.get_train_ops(loss, self.name)

        self.step = train_model.step

        self.decode_from_z = train_model.decode_from_z
        self.train_model = train_model
        self.init_buffer()

    def get_train_ops(self, loss, name, params=None, mpi_rank_weight=1, max_grad_norm=0.5): 
        if params is None:
            params = tf.trainable_variables(name)
        # print(name)
        # print(params)
        # 2. Build our trainer
        with tf.variable_scope(name, reuse=tf.AUTO_REUSE):
            self.trainer = MpiAdamOptimizer(comm, learning_rate=self.LR, mpi_rank_weight=mpi_rank_weight, epsilon=1e-5)
        # 3. Calculate the gradients
        with tf.variable_scope(name, reuse=tf.AUTO_REUSE):
            grads_and_var = self.trainer.compute_gradients(loss, params)
            grads, var = zip(*grads_and_var)
        grads, _grad_norm = tf.clip_by_global_norm(grads, max_grad_norm)
        
        grads_and_var = list(zip(grads, var))
        # zip aggregate each gradient with parameters associated
        # For instance zip(ABCD, xyza) => Ax, By, Cz, Da

        self.grads = grads
        self.var = var
        with tf.variable_scope(name, reuse=tf.AUTO_REUSE):
            train_op = self.trainer.apply_gradients(grads_and_var)
        return train_op

    def get_mse(self, im):
        
        if self.args.mse:
            # mse = 1 - self.sess.run(self.train_model.discrim_gen_enc, feed_dict={self.train_model.vae_im2:im}).reshape([im.shape[0],])
            # print(im.shape, mse.shape)
            # exit()"--folder adm --run_state train_admis --disturbances --ae_type ae_q_sse"
            # mse = self.sess.run(self.decoder_train_loss2, feed_dict={self.train_model.vae_im:im})
            # mse = self.sess.run(self.train_model.recon, feed_dict={self.train_model.vae_im:im, self.train_model.im:im})
            mse = self.sess.run(self.train_model.err, feed_dict={self.train_model.vae_im:im, self.train_model.im:im})
        else:
            mse = []
            for i in im:
                mse.append(self.sess.run(self.ssim_loss, feed_dict={self.train_model.vae_im:i.reshape([-1] + self.vae_im_size)})[0]*-1)
            mse = np.array(mse)
            # mse = self.sess.run(self.ssim_loss, feed_dict={self.train_model.vae_im2:im})[0]*-1
        return -mse

    def train_ae(self, epoch, lr, cliprange, vae_imgs, imgs, err=None, lam=None):
        # print(lam)
        # if self.rank == 0:
        #     print("im shape", imgs.shape, vae_imgs.shape, err.shape)
        #     print("error in")
        #     print(err)
        #     # for im in imgs:
        #     #     display_images(im, lag=1)
            
        td_map = {
        self.LR : lr,
        self.train_model.vae_im:vae_imgs,
        self.train_model.im:imgs
        # self.train_model.z_real_dist = np.random.randn(batch_size, z_dim) * 5.
        # self.train_model.z_real: np.random.randn(imgs.shape[0], self.latent_dim)
        # self.train_model.z_real: np.random.randn(imgs.shape[0], self.latent_dim) * 5.
        # self.train_model.ob:ob,
        }
        if lam is not None:
            td_map[self.train_model.LAM] = lam
        # vae_out = self.sess.run(self.vae_stats_list + [self._train_op_vae],td_map)[:-1]
        # errors = self.sess.run(self.train_model.err + [self._err_train_op],td_map)[:-1]

        if "ae" in self.args.ae_type:
            if "rand" in self.args.ae_type:
                g_stats = [0.0]
            else:
                g_stats = self.sess.run(self.g_stats_list + [self._gen_train_op],td_map)[:-1]
            
            # qs = self.sess.run(self.train_model.qs,td_map)
            # td_map[self.ERR] = recon
            # e_stats = self.sess.run(self.e_stats_list + [self._err_train_op],td_map)[:-1]
            
            # else:
                # e_stats = [0]
            # print(g_stats)
            # print(e_stats)
            # self._err_train_op = self.get_train_ops(self.err_loss, name + "/err", params=err_var)

            # g_mse = self.sess.run(self.gen_mse_loss,td_map)
            # error_loss = self.sess.run(self.err_loss,td_map)
            # errors = self.sess.run(self.train_model.err,td_map)
            # if self.rank == 0:
            #     print("mse ")
            #     print(g_mse, error_loss)
            #     print("error out")
            #     print(errors)
                # exit()
            return g_stats 
        else:
            d_stats = self.sess.run(self.d_stats_list + [self._dis_train_op],td_map)[:-1]
            # gan_stats = self.sess.run(self.gan_stats_list + [self._gan_train_op],td_map)[:-1]
            g_stats = self.sess.run(self.g_stats_list + [self._gen_train_op],td_map)[:-1]
            recon = self.sess.run(self.train_model.recon,td_map)
            td_map[self.ERR] = recon
            e_stats = self.sess.run(self.e_stats_list + [self._err_train_op],td_map)[:-1]
            return g_stats + e_stats + d_stats 

    def train(self, epoch, lr, cliprange, obs, imgs, returns, masks, actions, values, neglogpacs, lam=None,  exp_actions=None, states=None):
        # Here we calculate advantage A(s,a) = R + yV(s') - V(s)
        # Returns = R + yV(s')
        # print(lam.shape)
        advs = returns - values
        # Normalize the advantages
        advs = (advs - advs.mean()) / (advs.std() + 1e-8)
        td_map = {
        self.train_model.ob : obs,
        self.A : actions,
        self.ADV : advs,
        self.R : returns,
        self.LR : lr,
        self.CLIPRANGE : cliprange,
        self.OLDNEGLOGPAC : neglogpacs,
        self.OLDVPRED : values
        # self.train_model.LAM : self.train_model.lam
        }
        td_map[self.train_model.im] = imgs
        # td_map[self.train_model.vae_im] = imgs
        
        if "lam" in self.args.tests:
            td_map[self.train_model.LAM] = lam
            
        if self.args.discrete:
            td_map[self.ACTION] = actions
        if states is not None:
            td_map[self.train_model.S] = states
            td_map[self.train_model.M] = masks
            
        outputs = self.sess.run(self.stats_list + [self._train_op],td_map)[:-1]
        # qs = self.sess.run(self.train_model.qs,td_map)
        # print(len(outputs))
        # print(outputs[-1])
        td_map[self.ERR] = outputs[-1]
        e_stats = self.sess.run(self.e_stats_list + [self._err_train_op],td_map)[:-1]
        # print(e_stats)
        return outputs[-1] + e_stats

    def run_train(self, data, last_value, last_done):
        self.finalise_buffer(data, last_value, last_done)
        self.train_model.ob_rms.update(self.data['ob'])
        if self.args.const_lr:
            self.cur_lrmult =  1.0
        else:
            self.cur_lrmult =  max(1.0 - float(self.timesteps_so_far) / self.max_timesteps, 0)
       
        lrnow = self.learning_rate*self.cur_lrmult
        self.lr = lrnow
        cliprangenow = 0.2
        self.n = self.data['done'].shape[0]
        inds = np.arange(self.n)
        # print(self.epochs)
        t1 = time.time()
        if "decoded" not in self.args.tests:
            if "pre" in self.args.tests:
                if self.iters_so_far < 10:    
                    vae_epochs = 10
                    # vae_epochs = 5
                    # vae_epochs = 1
                else:
                    vae_epochs = 2
            else:
                vae_epochs = 2
                # vae_epochs = 1
            # vae_epochs = 0
            vae_loss = [0,0,0]
            if self.args.train_vae:
                if "roi" in self.args.tests:
                    vae_images = self.data["im"][:,:self.vae_im_size[0],int((self.im_size[0]-self.vae_im_size[1])/2):int(self.im_size[1]-(self.im_size[1]-self.vae_im_size[1])/2),:]
                else:
                    vae_images = self.data["im"]
                if "pos" in self.args.tests:
                    if self.iters_so_far == 0:
                        self.vae_images = vae_images
                        self.vae_images_idx = 0
                    else:
                        image_idx = np.where(self.data["ac"] == 1)[0]
                        # print(self.data["ac"][:10])
                        # print(image_idx[:10])

                        # print(self.data["im"].shape, len(image_idx))
                        # vae_images = self.data["im"][image_idx,::]
                        if len(image_idx) + self.vae_images_idx >= self.vae_images.shape[0]:
                            initial = self.vae_images.shape[0] - self.vae_images_idx
                            final =  len(image_idx) + self.vae_images_idx - self.vae_images.shape[0]
                            # print("idx ",self.vae_images_idx," initial ", initial," final ",  final, " len ", len(image_idx))
                            # print()
                            self.vae_images[self.vae_images_idx:,::] = vae_images[image_idx[:initial]]
                            self.vae_images[:final,::] = vae_images[image_idx[initial:]]
                            self.vae_images_idx = final
                        else:
                            # print("idx ", self.vae_images_idx, " final ",  len(image_idx) + self.vae_images_idx)
                            self.vae_images[self.vae_images_idx:len(image_idx)+self.vae_images_idx,::] = vae_images[image_idx]
                            self.vae_images_idx += len(image_idx) 
                else:
                    self.vae_images = vae_images

                vae_inds = [i for i in range(self.vae_images.shape[0])]
                for epoch in range(vae_epochs):
                    if self.enable_shuffle:
                        np.random.shuffle(vae_inds)
                        # np.random.shuffle(inds)
                        # np.random.shuffle(self.n)
                    self.loss = [] 
                    # for start in range(0, vae_images.shape[0]//self.batch_size, self.batch_size):
                    for start in range(0, self.n, self.batch_size):
                        end = start + self.batch_size
                        # print(epoch, start, end)
                        
                        mbinds = vae_inds[start:end]
                        # for im1, im2 in zip(self.vae_images[mbinds], self.data["im"][mbinds]):
                        #     display_images(im1, im2)
                        # continue
                        # if "err" in self.args.tests:
                        #     vae_loss = self.train_ae(epoch, lrnow, cliprangenow, self.vae_images[mbinds], self.data["im"][mbinds], self.data["lam"][mbinds])
                        # else:
                        vae_loss = self.train_ae(epoch, lrnow, cliprangenow, self.vae_images[mbinds], self.data["im"][mbinds], self.data["err"][mbinds])
                            # vae_loss = self.train_ae(epoch, lrnow, cliprangenow, self.vae_images[mbinds], self.data["im"][mbinds])
                            
        else:
            self.epochs = 5                     
        
        for epoch in range(self.epochs):
            if self.enable_shuffle:
                np.random.shuffle(inds)
            # self.loss = [] 
            for start in range(0, self.n, self.batch_size):
                end = start + self.batch_size
                # print(epoch, start, end)
                mbinds = inds[start:end]
                
                # im_slices = (self.data[key][mbinds] for key in ["im"])
                # vae_loss = self.train_vae(epoch, lrnow, cliprangenow, *im_slices)
                
                slices = (self.data[key][mbinds] for key in self.training_input)
                # print([self.data[key][mbinds].shape for key in self.training_input])
                # print( self.train(epoch, lrnow, cliprangenow, vf_only, *slices))
                

                outs = self.train(epoch, lrnow, cliprangenow, *slices)
                self.loss = outs[:len(self.loss_names)]
        

        # print("train", time.time()- t1)
        # self.loss = np.mean(self.loss, axis=0)
        if "decoded" not in self.args.tests:
            self.loss = self.loss + vae_loss
        # self.log_stuff(things={"Eps":self.train_model.eps, "vae_lam":self.train_model.lam, "vae_scaled_recon":self.train_model.scaled_recon_loss, "vae_recon_loss": self.train_model.recon_loss,"vae_recon_thres": self.train_model.recon_thres, "vae_recon_loss_pred":self.train_model.recon_loss_pred[0]})
        self.log_stuff(things={"Eps":self.train_model.eps, "vae_lam":self.train_model.lam, "vae_recon_thres": self.train_model.recon_thres, "vae_recon_loss_pred":self.train_model.recon_loss_pred})
        self.evaluate(data['ep_rets'], data['ep_lens'])

        if "setup" in self.name:
            self.re_init_buffer()
        else:
            self.init_buffer()
        
    # def step(self, ob, im, stochastic=False, multi=False): 
    #     if not multi:
    #         actions, values, self.states, neglogpacs =  self.train_model.step(ob[None], im[None], stochastic=stochastic)
    #         return actions[0], values, self.states, neglogpacs
    #     else:
    #         actions, values, self.states, neglogpacs =  self.train_model.step(ob, im, stochastic=stochastic)
    #         return actions, values, self.states, neglogpacs        

    def get_advantage(self, last_value, last_done):    
        gamma = 0.99; lam = 0.95
        advs = np.zeros_like(self.data['rew'])
        lastgaelam = 0
        for t in reversed(range(len(self.data['rew']))):
            if t == len(self.data['rew']) - 1:
                nextnonterminal = 1.0 - last_done
                nextvalues = last_value
            else:
                nextnonterminal = 1.0 - self.data['done'][t+1]
                nextvalues = self.data['value'][t+1]
            delta = self.data['rew'][t] + gamma * nextvalues * nextnonterminal - self.data['value'][t]
            advs[t] = lastgaelam = delta + gamma * lam * nextnonterminal * lastgaelam
        self.data['return'] = advs + self.data['value']
    
    def init_buffer(self):
        if "lam" in self.args.tests:
            self.data_input = ['ob', 'im', 'ac', 'rew', 'done', 'value', 'neglogpac', "lam"]
            self.training_input = ['ob', 'im', 'return', 'done', 'ac', 'value', 'neglogpac', "lam"]
        else:
            if "setup" in self.name:
                self.data_input = ['ob', 'im', 'ac', 'actions', 'rew', 'done', 'value', 'neglogpac', 'neglogpac_select']   
                self.training_input = ['ob', 'im', 'return', 'done', 'ac', 'value', 'neglogpac', 'actions', 'neglogpac_select']
            else:
                self.data_input = ['ob', 'im', 'ac', 'rew', 'done', 'value', 'neglogpac', 'err']
                self.training_input = ['ob', 'im', 'return', 'done', 'ac', 'value', 'neglogpac', 'err']
        self.data = {t:[] for t in self.data_input}

    def re_init_buffer(self):
        # self.old_last_data = {t:self.data[t][-1] for t in self.data_input}
        # print("before reinit")
        ob, im, ac, actions, rew, done = self.data['ob'][-1], self.data['im'][-1], self.data['ac'][-1], self.data['actions'][-1], self.data['rew'][-1], self.data['done'][-1]
        value, neglogpac, neglogpac_select = self.train_model.get_vpred_and_nlogps(ob, im, ac, actions)
        self.data = {t:[] for t in self.data_input}
        self.data['ob'].append(ob)
        self.data['im'].append(im)
        self.data['ac'].append(ac)
        self.data['actions'].append(actions)
        self.data['rew'].append(rew)
        self.data['done'].append(done)
        self.data['value'].append(value)
        self.data['neglogpac'].append(neglogpac)
        self.data['neglogpac_select'].append(neglogpac_select)        
        # print("after reinit", actions, neglogpac_select)

    def log_stuff(self, things):
        if self.rank == 0:
            for thing in things:
                self.writer.add_scalar(thing, things[thing], self.iters_so_far)
                logger.record_tabular(thing, things[thing])
        
    def add_to_buffer(self, data):
        ''' data needs to be a list of lists, same length as self.data'''
        for d,key in zip(data, self.data):
            if key == "im":
                im = d[:self.im_size[0],int((d.shape[0]-self.im_size[0])/2):int(d.shape[0]-(d.shape[0]-self.im_size[0])/2),:]
                # print("adding to buff", d.shape, im.shape)
                self.data[key].append(im)
            else:
                self.data[key].append(d)

    def finalise_buffer(self, data, last_value, last_done):
        ''' data must be dict'''
        for key in self.data_input:
            if key == 'done':
                self.data[key] = np.asarray(self.data[key], dtype=np.bool)
            else:
                self.data[key] = np.asarray(self.data[key])
        self.n = next(iter(self.data.values())).shape[0]
        for key in data:
            self.data[key] = data[key]
        self.get_advantage(last_value, last_done)
        # if self.rank == 0:
        # self.log_stuff()
