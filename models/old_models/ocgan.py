from tensorflow import keras
import tensorflow as tf


def get_encoder(input_shape=(28, 28, 1)):
    model = keras.Sequential([
        keras.layers.Conv2D(32, 3, padding='same', activation=tf.nn.tanh, input_shape=input_shape),
        keras.layers.Conv2D(32, 3, padding='same', activation=tf.nn.tanh),
        keras.layers.BatchNormalization(),
        keras.layers.MaxPool2D(),

        keras.layers.Conv2D(64, 3, padding='same', activation=tf.nn.tanh),
        keras.layers.Conv2D(64, 3, padding='same', activation=tf.nn.tanh),
        keras.layers.BatchNormalization(),
        keras.layers.MaxPool2D(),

        keras.layers.Conv2D(32, 3, padding='valid', activation=tf.nn.tanh),
        keras.layers.Conv2D(32, 3, padding='valid', activation=tf.nn.tanh),
        keras.layers.Flatten()
    ], name='encoder')
    return model


def get_decoder(input_shape=(288,)):
    model = keras.Sequential([
        keras.layers.Reshape((3, 3, 32), input_shape=input_shape),
        keras.layers.UpSampling2D(),
        keras.layers.Conv2DTranspose(32, 3, padding='same', activation=tf.nn.tanh),
        keras.layers.Conv2DTranspose(32, 3, padding='same', activation=tf.nn.tanh),
        keras.layers.BatchNormalization(),

        keras.layers.UpSampling2D(),
        keras.layers.Conv2DTranspose(64, 3, padding='same', activation=tf.nn.tanh),
        keras.layers.Conv2DTranspose(64, 3, padding='same', activation=tf.nn.tanh),
        keras.layers.BatchNormalization(),

        keras.layers.UpSampling2D(),
        keras.layers.Conv2DTranspose(64, 3, padding='valid', activation=tf.nn.tanh),
        keras.layers.Conv2DTranspose(64, 3, padding='valid', activation=tf.nn.tanh),
        keras.layers.BatchNormalization(),

        keras.layers.Conv2DTranspose(1, 3, padding='same', activation='sigmoid')
    ], name='decoder')
    return model


def get_disc_latent(input_shape=(288,)):
    model = keras.Sequential([
        keras.layers.Dense(128, input_shape=input_shape),
        keras.layers.BatchNormalization(),
        keras.layers.ReLU(),

        keras.layers.Dense(64),
        keras.layers.BatchNormalization(),
        keras.layers.ReLU(),

        keras.layers.Dense(32),
        keras.layers.BatchNormalization(),
        keras.layers.ReLU(),

        keras.layers.Dense(16),
        keras.layers.BatchNormalization(),
        keras.layers.ReLU(),

        keras.layers.Dense(1)
    ], name='discriminator_latent')
    return model


def get_disc_visual(input_shape=(28, 28, 1)):
    model = keras.Sequential([
        keras.layers.Conv2D(16, 5, (2, 2), padding='same', input_shape=input_shape),
        keras.layers.BatchNormalization(),
        keras.layers.LeakyReLU(0.2),

        keras.layers.Conv2D(16, 5, (2, 2), padding='same'),
        keras.layers.BatchNormalization(),
        keras.layers.LeakyReLU(0.2),

        keras.layers.Conv2D(16, 5, (2, 2), padding='same'),
        keras.layers.BatchNormalization(),
        keras.layers.LeakyReLU(0.2),

        keras.layers.Conv2D(1, 5, (2, 2), padding='same'),
        keras.layers.GlobalAveragePooling2D()
    ], name='discriminator_visual')
    return model


def get_classifier(input_shape=(28, 28, 1)):
    model = keras.Sequential([
        keras.layers.Conv2D(32, 5, (2, 2), padding='same', input_shape=input_shape),
        keras.layers.BatchNormalization(),
        keras.layers.LeakyReLU(0.2),

        keras.layers.Conv2D(64, 5, (2, 2), padding='same'),
        keras.layers.BatchNormalization(),
        keras.layers.LeakyReLU(0.2),

        keras.layers.Conv2D(64, 5, (2, 2), padding='same'),
        keras.layers.BatchNormalization(),
        keras.layers.LeakyReLU(0.2),

        keras.layers.Conv2D(1, 5, (2, 2), padding='same'),
        keras.layers.GlobalAveragePooling2D()
    ], name='classifier')
    return model


class Policy():
    def __init__(self):
        latent_shape = [288]

        self.enc = get_encoder()
        self.dec = get_decoder()
        self.disc_v = get_disc_visual()
        self.disc_l = get_disc_latent(latent_shape)
        self.cl = get_classifier()
        self.BS = BS

        X = tf.placeholder(tf.float32, [BS, 28, 28, 1])
        z = tf.random.normal(tf.shape(X), stddev=1e-5)

        l2 = tf.random.uniform([BS] + latent_shape, minval=-1, maxval=1)
        Xz = X + z
        l1 = self.enc(Xz)
        self.recon = self.dec(self.enc(X))
        dec_l2 = self.dec(l2)
        self.gen = dec_l2

class Model():
    def __init__(self):
            with  ('Loss'):
                loss_op = attrdict()
                with task('1. Classifier loss'):
                    logits_C_l1 = self.cl(self.dec(l1))
                    logits_C_l2 = self.cl(dec_l2)

                    loss_op.C_l1 = tf.losses.sigmoid_cross_entropy(tf.ones_like(logits_C_l1), logits=logits_C_l1)
                    loss_op.C_l2 = tf.losses.sigmoid_cross_entropy(tf.zeros_like(logits_C_l2), logits=logits_C_l2)
                    loss_op.C = (loss_op.C_l1 + loss_op.C_l2) / 2

                with task('2. Discriminator latent loss'):
                    logits_Dl_l1 = self.disc_l(l1)
                    logits_Dl_l2 = self.disc_l(l2)

                    loss_op.Dl_l1 = tf.losses.sigmoid_cross_entropy(tf.zeros_like(logits_Dl_l1), logits=logits_Dl_l1)
                    loss_op.Dl_l2 = tf.losses.sigmoid_cross_entropy(tf.ones_like(logits_Dl_l2), logits=logits_Dl_l2)
                    loss_op.Dl = (loss_op.Dl_l1 + loss_op.Dl_l2) / 2

                with task('3. Discriminator visual loss'):
                    logits_Dv_X = self.disc_v(X)
                    logits_Dv_l2 = self.disc_v(self.dec(l2))

                    loss_op.Dv_X = tf.losses.sigmoid_cross_entropy(tf.ones_like(logits_Dv_X), logits=logits_Dv_X)
                    loss_op.Dv_l2 = tf.losses.sigmoid_cross_entropy(tf.zeros_like(logits_Dv_l2), logits=logits_Dv_l2)
                    loss_op.Dv = (loss_op.Dv_X + loss_op.Dv_l2) / 2

                with task('4. Informative-negative mining'):
                    l2_mine = tf.get_variable('l2_mine', [BS] + latent_shape, tf.float32)
                    logits_C_l2_mine = self.cl(self.dec(l2_mine))
                    loss_C_l2_mine = tf.losses.sigmoid_cross_entropy(tf.zeros_like(logits_C_l2_mine), logits=logits_C_l2_mine)
                    opt = tf.train.GradientDescentOptimizer(1)

                    def cond(i):
                        return i < self.steps.IM

                    def body(i):
                        descent_op = opt.minimize(loss_C_l2_mine, var_list=[l2_mine])
                        with tf.control_dependencies([descent_op]):
                            return i + 1

                    self.l2_mine_descent = tf.while_loop(cond, body, [tf.constant(0)])

                with task('5. AE loss'):
                    Xh = self.dec(l1)
                    loss_AE_recon = tf.reduce_mean(tf.square(X - Xh), axis=[1, 2, 3])
                    loss_op.AE_recon = tf.reduce_mean(loss_AE_recon)

                    loss_op.AE_l = tf.losses.sigmoid_cross_entropy(tf.ones_like(logits_Dl_l1), logits=logits_Dl_l1)

                    logits_Dv_l2_mine = self.disc_v(self.dec(l2_mine))
                    loss_op.AE_v = tf.losses.sigmoid_cross_entropy(tf.ones_like(logits_Dv_l2_mine), logits=logits_Dv_l2_mine)

                    self.lamb = tf.placeholder_with_default(10., [])
                    loss_op.AE = loss_op.AE_l + loss_op.AE_v + self.lamb * loss_op.AE_recon

            with task('Optimize'):
                Opt = tf.train.AdamOptimizer
                train_op = attrdict()
                ae_vars = self.enc.trainable_variables + self.dec.trainable_variables
                train_op.C = Opt(lr.C).minimize(loss_op.C, var_list=self.cl.trainable_variables)
                train_op.Dl = Opt(lr.Dl).minimize(loss_op.Dl, var_list=self.disc_l.trainable_variables)
                train_op.Dv = Opt(lr.Dv).minimize(loss_op.Dv, var_list=self.disc_v.trainable_variables)
                train_op.AE = Opt(lr.AE).minimize(loss_op.AE, var_list=ae_vars)
                train_op.recon = Opt(lr.recon).minimize(loss_op.AE_recon, var_list=ae_vars)

            with task('Placeholders'):
                self.loss_op = loss_op
                self.train_op = train_op
                self.X = X
                self.Xz = Xz
                self.l2_mine = l2_mine
                self.l2 = l2
                self.anomaly_score = tf.reduce_mean(tf.square(X - self.recon), axis=[1, 2, 3])



    
    def train(self, epoch, lr, cliprange, obs, imgs, returns, masks, actions, values, neglogpacs,  exp_actions=None, states=None):
        # Here we calculate advantage A(s,a) = R + yV(s') - V(s)
        # Returns = R + yV(s')
        advs = returns - values
        # Normalize the advantages
        advs = (advs - advs.mean()) / (advs.std() + 1e-8)
        td_map = {
        self.train_model.ob : obs,
        self.A : actions,
        self.ADV : advs,
        self.R : returns,
        self.LR : lr,
        self.CLIPRANGE : cliprange,
        self.OLDNEGLOGPAC : neglogpacs,
        self.OLDVPRED : values
        }
        td_map[self.train_model.im] = imgs
    
        if states is not None:
            td_map[self.train_model.S] = states
            td_map[self.train_model.M] = masks
            
        return self.sess.run(self.stats_list + [self._train_op],td_map)[:-1]

    def run_train(self, data, last_value, last_done):
        self.finalise_buffer(data, last_value, last_done)
        self.train_model.ob_rms.update(self.data['ob'])
        if self.args.const_lr:
            self.cur_lrmult =  1.0
        else:
            self.cur_lrmult =  max(1.0 - float(self.timesteps_so_far) / self.max_timesteps, 0)
        lrnow = self.learning_rate*self.cur_lrmult
        self.lr = lrnow
        cliprangenow = 0.2
        self.n = self.data['done'].shape[0]
        inds = np.arange(self.n)
        # print(self.epochs)
        t1 = time.time()
        for epoch in range(self.epochs):
            if self.enable_shuffle:
                np.random.shuffle(inds)
            self.loss = [] 
            for start in range(0, self.n, self.batch_size):
                end = start + self.batch_size
                # print(epoch, start, end)
                mbinds = inds[start:end]
                slices = (self.data[key][mbinds] for key in self.training_input)
                # print([self.data[key][mbinds].shape for key in self.training_input])
                # print( self.train(epoch, lrnow, cliprangenow, vf_only, *slices))
                outs = self.train(epoch, lrnow, cliprangenow, *slices)
                self.loss.append(outs[:len(self.loss_names)])
        # print("train", time.time()- t1)
        self.loss = np.mean(self.loss, axis=0)
        self.evaluate(data['ep_rets'], data['ep_lens'])
        self.init_buffer()
        
    # def step(self, ob, im, stochastic=False, multi=False): 
    #     if not multi:
    #         actions, values, self.states, neglogpacs =  self.train_model.step(ob[None], im[None], stochastic=stochastic)
    #         return actions[0], values, self.states, neglogpacs
    #     else:
    #         actions, values, self.states, neglogpacs =  self.train_model.step(ob, im, stochastic=stochastic)
    #         return actions, values, self.states, neglogpacs        

    def get_advantage(self, last_value, last_done):    
        gamma = 0.99; lam = 0.95
        advs = np.zeros_like(self.data['rew'])
        lastgaelam = 0
        for t in reversed(range(len(self.data['rew']))):
            if t == len(self.data['rew']) - 1:
                nextnonterminal = 1.0 - last_done
                nextvalues = last_value
            else:
                nextnonterminal = 1.0 - self.data['done'][t+1]
                nextvalues = self.data['value'][t+1]
            delta = self.data['rew'][t] + gamma * nextvalues * nextnonterminal - self.data['value'][t]
            advs[t] = lastgaelam = delta + gamma * lam * nextnonterminal * lastgaelam
        self.data['return'] = advs + self.data['value']
    
    def init_buffer(self):
        self.data_input = ['ob', 'im', 'ac', 'rew', 'done', 'value', 'neglogpac']
        self.training_input = ['ob', 'im', 'return', 'done', 'ac', 'value', 'neglogpac']
        self.data = {t:[] for t in self.data_input}

    def log_stuff(self, things):
        if self.rank == 0:
            for thing in things:
                self.writer.add_scalar(thing, things[thing], self.iters_so_far)
                logger.record_tabular(thing, things[thing])
        
    def add_to_buffer(self, data):
        ''' data needs to be a list of lists, same length as self.data'''
        for d,key in zip(data, self.data):
            self.data[key].append(d)

    def finalise_buffer(self, data, last_value, last_done):
        ''' data must be dict'''
        for key in self.data_input:
            if key == 'done':
                self.data[key] = np.asarray(self.data[key], dtype=np.bool)
            else:
                self.data[key] = np.asarray(self.data[key])
        self.n = next(iter(self.data.values())).shape[0]
        for key in data:
            self.data[key] = data[key]
        self.get_advantage(last_value, last_done)
        # if self.rank == 0:
        # self.log_stuff()
