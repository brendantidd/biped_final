'''
From OpenAI Baselines
'''
from models.base import Base
import os   
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' 
import tensorflow as tf
tf.get_logger().setLevel('DEBUG')
from scripts.distributions import make_pdtype
from scripts.mpi_running_mean_std import RunningMeanStd
from scripts.mpi_moments import mpi_moments
# from baselines.common import explained_variance, fmt_row, zipsame
import numpy as np
from gym import spaces
# import scripts.utils as U
import scripts.utils as TF_U
from scripts.mpi_adam_optimizer import MpiAdamOptimizer
from mpi4py import MPI
from scripts import logger
import time
import cv2
comm = MPI.COMM_WORLD
from collections import deque
import math
from ops import *
from utils import *

def conv_out_size_same(size, stride):
    return int(math.ceil(float(size) / float(stride)))

class batch_norm(object):
    def __init__(self, epsilon=1e-5, momentum = 0.9, name="batch_norm"):
        with tf.variable_scope(name):
            self.epsilon  = epsilon
            self.momentum = momentum
            self.name = name

    def __call__(self, x, train=True):
        return tf.contrib.layers.batch_norm(x,
                      decay=self.momentum, 
                      updates_collections=None,
                      epsilon=self.epsilon,
                      scale=True,
                      is_training=train,
                      scope=self.name)

def display_images(im1, im2=None):
    im1 = im1.reshape([-1] + list(im1.shape))
    if im2 is None:
        for im in im1:
            im = cv2.resize(im,dsize=(int(im.shape[1] * 6), int(im.shape[0] * 6)))
            cv2.imshow("frame", im)
            cv2.waitKey(1)
    else:
        im2 = im2.reshape([-1] + list(im2.shape))
        for i1, i2 in zip(im1, im2):
            i1 = cv2.resize(i1,dsize=(int(i1.shape[1] * 6), int(i1.shape[0] * 6)))
            i2 = cv2.resize(i2,dsize=(int(i2.shape[1] * 6), int(i2.shape[0] * 6)))
            cv2.imshow("frame", np.hstack([i1,i2]))
            cv2.waitKey(1)

def get_vars(scope):
    return [x for x in tf.global_variables() if scope in x.name]

# def neglogp(self, x):
#     return 0.5 * tf.reduce_sum(tf.square((x - self.mean) / self.std), axis=-1) \
#             + 0.5 * np.log(2.0 * np.pi) * tf.to_float(tf.shape(x)[-1]) \
#             + tf.reduce_sum(self.logstd, axis=-1)

def log_normal_pdf(sample, mean, logvar, raxis=1):
    log2pi = tf.math.log(2. * np.pi)
    return tf.reduce_sum(
            -.5 * ((sample - mean) ** 2. * tf.exp(-logvar) + logvar + log2pi),
            axis=raxis)

def reparameterize(mean, logvar):
    eps = tf.random.normal(shape=tf.shape(mean))
    return eps * tf.exp(logvar * .5) + mean 

class Policy():
    def __init__(self, name, ob, im, vae_im, ob_space, ob_size, im_size, vae_im_size, ac_size, env, ac_space, z_ph, latent_dim, sess, args, hid_size, normalize=True, const_std=False):
        self.name = name
        self.ob = ob
        self.im = im
        self.vae_im = vae_im
        self.ob_size = ob_size
        self.im_size = im_size
        self.vae_im_size = vae_im_size
        self.ac_size = ac_size
        self.env = env
        self.sess = sess
        self.args = args
        self.pdtype = pdtype = make_pdtype(ac_space)
        self.const_std = const_std
        self.z_ph = z_ph
        self.latent_dim = latent_dim

        if self.args.batch_norm:
            self.e_bn1 = batch_norm(name='e_bn1')
            self.e_bn2 = batch_norm(name='e_bn2')
            self.e_bn3 = batch_norm(name='e_bn3')
            self.e_bn4 = batch_norm(name='e_bn4')

            self.d_bn1 = batch_norm(name='d_bn1')
            self.d_bn2 = batch_norm(name='d_bn2')
            self.d_bn3 = batch_norm(name='d_bn3')

            self.g_bn0 = batch_norm(name='g_bn0')
            self.g_bn1 = batch_norm(name='g_bn1')
            self.g_bn2 = batch_norm(name='g_bn2')
            self.g_bn3 = batch_norm(name='g_bn3')

        sequence_length = None
        if normalize:
            with tf.variable_scope("obfilter"):
                self.ob_rms = RunningMeanStd(shape=ob_space.shape)
        
        self.quarter = int(self.vae_im_size[0]/4)
        # mnist and titan
        self.min_val = 0
        self.max_val = 1
        
        # walking
        # self.min_val = 0.25
        # self.max_val = 2
        
        self.data_range = self.max_val - self.min_val
        # first_layer = 32
        # second_layer = 64
        self.first_layer = 16
        self.second_layer = 32
        self.third_layer = 64

        self.y_dim = 1
        self.output_height, self.output_width = 28, 28
        # self.gf_dim = 64
        # self.dfc_dim = 1024
        # self.gfc_dim = 1024
        # self.df_dim = 64
        self.gf_dim = 16
        self.dfc_dim = 128
        self.gfc_dim = 128
        self.df_dim = 16
        self.c_dim = 1
        self.batch_size = 32
        self.s_h,  self.s_w = self.vae_im_size[0], self.vae_im_size[1]
        self.s_h2, self.s_h4 = int(self.s_h/2), int(self.s_h/4)
        self.s_w2, self.s_w4 = int(self.s_w/2), int(self.s_w/4)     
        sample_dim = 100

        self.enc_mean, self.enc_logvar, self.enc, self.enc_err = self.encoder(self.vae_im)
        self.decoded_from_im = self.generator(self.enc_mean)
        self.decoded_from_z = self.generator(self.z_ph, reuse=True)
        self.discrim, self.discrim_logits = self.discriminator(self.vae_im)

        if args.gan_type == "vae_gan" or args.gan_type == "vae_gan_with_enc":
            self.discrim_gen, self.discrim_logits_gen = self.discriminator(self.decoded_from_im, reuse=True)
        else:
            self.discrim_gen, self.discrim_logits_gen = self.discriminator(self.decoded_from_z, reuse=True)
        
        # self.gen_mse = tf.reduce_mean(tf.square(self.decoded_from_im - self.vae_im))
        self.gen_mse = tf.reduce_mean(tf.square(self.decoded_from_im - self.vae_im), axis=[1,2,3])

        # self.gen_mse = tf.reduce_mean(tf.reduce_sum(tf.square(self.vae_im - self.decoded_from_im), axis=[1, 2, 3]))

        if normalize:
            obz = tf.clip_by_value((ob - tf.stop_gradient(self.ob_rms.mean)) / tf.stop_gradient(self.ob_rms.std), -5.0, 5.0)
        else:
            obz = ob
        
        with tf.variable_scope('qf'):
            x = tf.nn.relu(tf.layers.conv2d(self.im, self.first_layer, 3, strides=2, padding="same", name="q_vis1"))
            x = tf.nn.relu(tf.layers.conv2d(x, self.second_layer, 3, strides=2, padding="same", name="q_vis2"))
            x = tf.layers.flatten(x)
            x = tf.nn.relu(tf.layers.dense(x, 16, name="q_vis3"))
            last_out = obz
            last_out = tf.concat(axis=1,values=[last_out, x])
            last_out = tf.nn.tanh(tf.layers.dense(last_out, hid_size, name="q_fc1", kernel_initializer=TF_U.normc_initializer(1.0)))
            last_out = tf.nn.tanh(tf.layers.dense(last_out, hid_size, name="q_fc2", kernel_initializer=TF_U.normc_initializer(1.0)))
            self.qs = tf.layers.dense(last_out, 2, name='q_final', kernel_initializer=TF_U.normc_initializer(1.0))
            self.vpred = tf.reduce_max(self.qs, name='q_vpred')


        # self.recon = tf.reduce_sum(tf.square(self.decoded - self.vae_im), axis=[1,2,3])
        self.recon = 0.0
        self.stochastic = tf.placeholder(dtype=tf.bool, shape=())
        self.state = tf.constant([])
        self.neglogp = tf.constant([])
        self.steps = 0
        self.eps_start = 0.5
        self.eps_end = 0.001
        self.eps = self.eps_start
        self.eps_decay = 50000
        self.recon_error_buffer = []
        self.recon_thres = 0.0

    def generator(self, inputs, reuse=False):
        self.quarter = int(self.vae_im_size[0]/4)
        if self.args.robot == "titan":
            self.min_val = 0
            self.max_val = 1
        else:
            # walking
            self.min_val = 0.25
            self.max_val = 2
        
        self.data_range = self.max_val - self.min_val
        # first_layer = 32
        # second_layer = 64
        self.first_layer = 16
        self.second_layer = 32
        self.third_layer = 64
        with tf.variable_scope("generator") as scope:
            if reuse:
                scope.reuse_variables()
            if self.args.batch_norm:
                h0 = tf.nn.relu(self.g_bn0(tf.layers.dense(inputs, self.gfc_dim, name='g_h0_lin')))
                h1 = tf.nn.relu(self.g_bn1(tf.layers.dense(h0, self.gf_dim*2*self.s_h4*self.s_w4, name='g_h1_lin')))
                h1 = tf.reshape(h1, [-1, self.s_h4, self.s_w4, self.gf_dim * 2])
                h2 = tf.nn.relu(self.g_bn2(tf.layers.conv2d_transpose(h1, self.first_layer, 3, strides=2, padding='same',name='g_h2')))
                h3 = tf.nn.relu(self.g_bn3(tf.layers.conv2d_transpose(h2, self.second_layer, 3, strides=2, padding='same', name='g_h3')))
            else:
                h0 = tf.nn.relu(tf.layers.dense(inputs, self.gfc_dim, name='g_h0_lin'))
                h1 = tf.nn.relu(tf.layers.dense(h0, self.gf_dim*2*self.s_h4*self.s_w4, name='g_h1_lin'))
                h1 = tf.reshape(h1, [-1, self.s_h4, self.s_w4, self.gf_dim * 2])
                h2 = tf.nn.relu(tf.layers.conv2d_transpose(h1, self.first_layer, 3, strides=2, padding='same',name='g_h2'))
                h3 = tf.nn.relu(tf.layers.conv2d_transpose(h2, self.second_layer, 3, strides=2, padding='same',name='g_h3'))
            h4 = tf.layers.conv2d_transpose(h3, 1, 3, padding='same', name='g_h4')
            return ((tf.nn.tanh(h4) + 1 )/ 2) * self.data_range + self.min_val

    def discriminator(self, inputs, reuse=False):
        # df_dim = 64
        # dfc_dim = self.args.hid_size
        with tf.variable_scope("discriminator") as scope:
            if reuse:
                scope.reuse_variables()
            h0 = lrelu(tf.layers.conv2d(inputs, 32, 3, strides=2, padding='same', name='d_h0_conv'))
            if self.args.batch_norm:
                h1 = lrelu(self.d_bn1(tf.layers.conv2d(h0, self.df_dim, 3, strides=2, padding='same', name='d_h1_conv')))
                h1 = tf.layers.flatten(h1)
                h2 = lrelu(self.d_bn2(tf.layers.dense(h1, self.dfc_dim, name='d_h2_lin')))
            else:
                h1 = lrelu(tf.layers.conv2d(h0, self.df_dim, 3, strides=2, padding='same', name='d_h1_conv'))
                h1 = tf.layers.flatten(h1)
                h2 = lrelu(tf.layers.dense(h1, self.dfc_dim, name='d_h2_lin'))
            h3 = tf.layers.dense(h2, 1, name='d_h3_lin')
            return tf.nn.sigmoid(h3), h3
    
    def encoder(self, inputs, reuse=False):
        df_dim = 64
        dfc_dim = self.args.hid_size
        c_dim = 3
        with tf.variable_scope("encoder") as scope:
            if reuse:
                scope.reuse_variables()
            # h0 = lrelu(tf.layers.conv2d(inputs, c_dim, 5, strides=2, padding='same', name='e_h0_conv', kernel_initializer=self.initializer()))
            # if self.args.batch_norm:
            #     h1 = lrelu(self.e_bn1(tf.layers.conv2d(h0, df_dim, 5, strides=2, padding='same', name='e_h1_conv',kernel_initializer=self.initializer())))
            #     h2 = lrelu(self.e_bn2(tf.layers.conv2d(h1, df_dim, 5, strides=2, padding='same', name='e_h2_conv',kernel_initializer=self.initializer())))
            #     h3 = tf.layers.flatten(h2)
            #     h3 = tf.nn.tanh(self.e_bn3(tf.layers.dense(h3, dfc_dim, name='e_h2_lin', kernel_initializer=self.initializer())))
            #     h3 = tf.nn.tanh(self.e_bn4(tf.layers.dense(h3, dfc_dim, name='e_h3_lin',kernel_initializer=self.initializer())))
            # else:
            # h1 = lrelu(tf.layers.conv2d(h0, df_dim, 5, strides=2, padding='same', name='e_h1_conv', kernel_initializer=self.initializer()))
            # h2 = lrelu(tf.layers.conv2d(h1, df_dim, 5, strides=2, padding='same', name='e_h2_conv', kernel_initializer=self.initializer()))
            
            h0 = lrelu(tf.layers.conv2d(inputs, c_dim, 5, strides=2, padding='same', name='e_h0_conv', kernel_initializer=self.initializer()))
            h1 = lrelu(tf.layers.conv2d(h0, self.df_dim, 5, strides=2, padding='same', name='e_h1_conv', kernel_initializer=self.initializer()))
            h2 = lrelu(tf.layers.conv2d(h1, self.df_dim, 5, strides=2, padding='same', name='e_h2_conv', kernel_initializer=self.initializer()))

            # z_mean = tf.layers.flatten(h2)
            h2 = tf.layers.flatten(h2)
            h3 = tf.nn.tanh(tf.layers.dense(h2, self.dfc_dim, name='e_h2_lin', kernel_initializer=self.initializer()))
            z_mean = tf.layers.dense(h3, self.latent_dim, name='e_mean', kernel_initializer=self.initializer())
            z_log_var = tf.layers.dense(h3, self.latent_dim, name='e_log_var')            

            z = reparameterize(z_mean, z_log_var)
            err = tf.nn.tanh(tf.layers.dense(tf.nn.tanh(z_mean), self.dfc_dim, name='e_err1'))
            err = tf.layers.dense(err, 1, name='e_err2')
            return z_mean, z_log_var, z, err
    
    def initializer(self):
        # return tf.keras.initializers.RandomUniform(minval=-0.1, maxval=0.1)
        return tf.keras.initializers.RandomUniform(minval=-0.1, maxval=0.1)
        # Default
        # 70%
        # return tf.keras.initializers.RandomNormal()
        
        # return tf.keras.initializers.RandomNormal(mean=0.0, stddev=0.05)
        # 57%
        # return tf.keras.initializers.RandomNormal(mean=0.0, stddev=0.5) 57%
        # return tf.keras.initializers.RandomNormal(mean=0.0, stddev=0.01)
        # return tf.keras.initializers.Orthogonal( gain=1.0, seed=None)

    def update_eps(self):
        self.eps =  max(self.eps_start - (float(self.steps) / (self.eps_decay)), self.eps_end)
        self.steps += 1

    def detect(self, im):
        detection = self.sess.run(self.detection,feed_dict={self.z_ph:z})
        return detection

    def encode(self, im):
        z = self.sess.run(self.enc_mean, feed_dict={self.vae_im:im})
        return z
    
    def decode_from_z(self, z):
        decoded = self.sess.run(self.decoded_from_z,feed_dict={self.z_ph:z})
        # decoded = self.sess.run(self.decoded,feed_dict={self.z_ph:z})
        return decoded

    def decode_from_im(self, im):
        # decoded = self.sess.run(self.decoded, feed_dict={self.vae_im:im})
        decoded = self.sess.run(self.decoded_from_im, feed_dict={self.vae_im:im})
        return decoded
        
    def update_eps(self):
        self.eps =  max(self.eps_start - (float(self.steps) / (self.eps_decay)), self.eps_end)
        self.steps += 1

    def step(self, ob, im, stochastic=False):
        im_copy = im.copy()
        vae_im = im[:self.vae_im_size[0],int((self.im_size[0]-self.vae_im_size[0])/2):int(self.im_size[0]-(self.im_size[0]-self.vae_im_size[0])/2),:]
        self.recon_loss = self.sess.run([self.gen_mse], feed_dict={ self.vae_im: vae_im.reshape([1]+self.vae_im_size)})
        self.recon_loss_pred = [0]
        self.scaled_recon_loss = self.args.lam * (self.recon_loss[0]*self.recon_loss[0])
        q, v, state, neglogp, recon_im = self.sess.run([self.qs, self.vpred, self.state, self.neglogp, self.decoded_from_im], 
                                    feed_dict={self.ob: ob.reshape([1,self.ob_size]), 
                                                self.im: im.reshape([1]+self.im_size), 
                                                self.vae_im: vae_im.reshape([1]+self.vae_im_size), 
                                                self.stochastic:stochastic})
        
        im_copy[:self.vae_im_size[0],int((self.im_size[1]-self.vae_im_size[1])/2):int(self.im_size[1]-(self.im_size[1]-self.vae_im_size[1])/2),:] = recon_im[0,::]
        new_qs = q[0]
        self.update_eps()
        self.recon_error_buffer.append(self.recon_loss[0])
        if np.random.random() > self.eps or not stochastic: 
            # This is where we apply the thresholding.
            if not stochastic:
                act_from_q = np.argmax(new_qs)
                if act_from_q == 1 and self.recon_loss[0] < self.recon_thres:
                    act = 1
                else:
                    act = 0
            else:
                act = np.argmax(new_qs)
        else:
            
            act = np.random.randint(2)
        return [act], v, state, neglogp, im_copy, self.scaled_recon_loss, self.recon_loss[0], np.clip(new_qs,0.0, None), self.recon_loss_pred

class Model(Base):
    def __init__(self, name, env, ac_size, ob_size, im_size=[48,48,1], vae_im_size=[48,48,1], args=None, PATH=None, writer=None, hid_size=256, vis=False, normalize=True, ent_coef=0.0, vf_coef=0.5, max_grad_norm=0.5, mpi_rank_weight=1, max_timesteps=int(1e6), lr=3e-4, horizon=2048, batch_size=32, const_std=False, latent_dim=2):
        self.max_timesteps = max_timesteps
        self.horizon = horizon
        self.batch_size = batch_size
        self.learning_rate = lr
        self.env = env
        self.ob_size = ob_size
        self.im_size = im_size
        self.vae_im_size = vae_im_size
        self.ac_size = ac_size
        self.latent_dim = latent_dim

        self.sess = sess = TF_U.get_session()
        self.name = name
        high = np.inf*np.ones(ac_size)
        low = -high
        ac_space = spaces.Box(low, high, dtype=np.float32)
        high = np.inf*np.ones(ob_size)
        low = -high
        ob_space = spaces.Box(low, high, dtype=np.float32)
        self.args = args   
        im_size = im_size
        self.PATH = PATH
        self.hid_size = args.hid_size
        self.writer = writer

        self.ob = TF_U.get_placeholder(name="ob_" + self.name, dtype=tf.float32, shape=[None, self.ob_size])
        self.im = TF_U.get_placeholder(name="im_" + self.name, dtype=tf.float32, shape=[None] + self.im_size)
        self.vae_im = TF_U.get_placeholder(name="vae_im_" + self.name, dtype=tf.float32, shape=[None] + self.vae_im_size)
        self.z_ph = TF_U.get_placeholder(name="z_ph_" + self.name, dtype=tf.float32, shape=[None,self.latent_dim])
        self.detect_labels = TF_U.get_placeholder(name="det_" + self.name, dtype=tf.float32, shape=[None])
    
        with tf.variable_scope(name, reuse=tf.AUTO_REUSE):
            self.train_model = Policy(name, self.ob, self.im, self.vae_im, ob_space, ob_size, im_size, vae_im_size, ac_size, env, ac_space, self.z_ph, self.latent_dim, sess, args=self.args, normalize=normalize, hid_size=hid_size, const_std=const_std)

        Base.__init__(self)

        # CREATE THE PLACEHOLDERS
        self.LR = LR = tf.placeholder(tf.float32, [], name="LR")
        self.A = A = tf.placeholder(tf.float32, [None,1], name="A")
        # self.A = A = train_model.pdtype.sample_placeholder([None])
        self.ACTION = tf.placeholder(tf.int32, [None,1], name="ACTION")
        self.ADV = ADV = tf.placeholder(tf.float32, [None], name="ADV")
        self.R = R = tf.placeholder(tf.float32, [None], name="R")
        
        one_hot = tf.one_hot(tf.squeeze(self.ACTION), 2)
        current_q_t = tf.reduce_sum(self.train_model.qs * one_hot, 1)
        self.vf_loss = 0.5*tf.reduce_mean(tf.square(current_q_t - self.R))
        
        # self.decoder_train_loss = 0.5*tf.reduce_mean(self.train_model.recon)
        # self.decoder_train_loss2 = tf.reduce_sum(tf.square(self.train_model.decoded_from_im - self.train_model.vae_im), axis=[1,2,3])
        self.decoder_train_loss2 = tf.reduce_mean(tf.square(self.train_model.decoded_from_im - self.train_model.vae_im), axis=[1,2,3])
        self.ssim_loss = tf.image.ssim(self.train_model.vae_im, self.train_model.decoded_from_im, max_val=1.0, filter_size=11,filter_sigma=1.5, k1=0.01, k2=0.03)

        if self.args.gan_type == "vae" or self.args.gan_type == "vae_with_enc":
            self.mse_g_loss = tf.reduce_mean(tf.square(self.train_model.decoded_from_im - self.vae_im))
            self.g_loss = self.mse_g_loss
            self.g_stats_list = [self.g_loss]
        else:
            if self.args.gan_type == "vae_gan" or self.args.gan_type == "vae_gan_with_enc":
                self.G = self.train_model.decoded_from_im
            else:
                self.G = self.train_model.decoded_from_z
            self.D, self.D_logits = self.train_model.discrim, self.train_model.discrim_logits
            self.D_, self.D_logits_ = self.train_model.discrim_gen, self.train_model.discrim_logits_gen
            
            def sigmoid_cross_entropy_with_logits(x, y):
                try:
                    return tf.nn.sigmoid_cross_entropy_with_logits(logits=x, labels=y)
                except:
                    return tf.nn.sigmoid_cross_entropy_with_logits(logits=x, targets=y)

            self.d_loss_real = tf.reduce_mean(
            sigmoid_cross_entropy_with_logits(self.D_logits, tf.ones_like(self.D)))
            self.d_loss_fake = tf.reduce_mean(
            sigmoid_cross_entropy_with_logits(self.D_logits_, tf.zeros_like(self.D_)))
            
            self.d_loss = self.d_loss_real + self.d_loss_fake
            

            self.g_loss = tf.reduce_mean(sigmoid_cross_entropy_with_logits(self.D_logits_, tf.ones_like(self.D_)))
            # self.e_loss = tf.reduce_mean(tf.square(self.train_model.enc_mean - self.z_ph))
            # logp_enc = log_normal_pdf(self.train_model.enc, self.train_model.enc_mean, self.train_model.enc_logvar)

            if args.gan_type == "vae_gan" or args.gan_type == "vae_gan_with_enc":
                # self.mse_g_loss = tf.reduce_mean(tf.square(self.train_model.decoded_from_im - self.vae_im))
                self.g_loss = self.g_loss + tf.reduce_mean(self.train_model.gen_mse)
            else:
                self.g_loss = self.g_loss
            self.d_stats_list = [self.d_loss]
            self.g_stats_list = [self.g_loss]
        # self.e_stats_list = [self.e_loss]
        self.loss_names = ['q_loss','learning_rate','gan_d_loss', 'gan_g_loss']
        self.stats_list = [self.vf_loss, LR]

        all_variables = tf.trainable_variables()
        if "gan" in self.args.gan_type:
            d_var = [var for var in all_variables if 'd_' in var.name]
            if args.gan_type == "vae_gan_with_enc":
                g_var = [var for var in all_variables if 'g_' in var.name or 'e_' in var.name]
            else:
                g_var = [var for var in all_variables if 'g_' in var.name]
        else:
            if args.gan_type == "vae_with_enc":
                g_var = [var for var in all_variables if 'g_' in var.name or 'e_' in var.name]
            else:
                g_var = [var for var in all_variables if 'g_' in var.name]
            # g_var = [var for var in all_variables if 'g_' in var.name or 'e_' in var.name]
        e_var = [var for var in all_variables if 'e_' in var.name]
        q_var = [var for var in all_variables if 'q_' in var.name]

        self._dis_train_op = self.get_train_ops(self.d_loss, name + "_d", params=d_var)
        self._gen_train_op = self.get_train_ops(self.g_loss, name + "_g", params=g_var)
        self._train_op = self.get_train_ops(self.vf_loss, name, params=q_var)

        self.encode = self.train_model.encode
        self.decode_from_im = self.train_model.decode_from_im
        self.decode_from_z = self.train_model.decode_from_z
        self.step = self.train_model.step
        self.init_buffer()

    def get_mse(self, im):
        
        if self.args.mse:
            # mse = 1 - self.sess.run(self.train_model.discrim_gen_enc, feed_dict={self.train_model.vae_im2:im}).reshape([im.shape[0],])
            # print(im.shape, mse.shape)
            # exit()
            mse = self.sess.run(self.decoder_train_loss2, feed_dict={self.train_model.vae_im:im})
        else:
            mse = []
            for i in im:
                mse.append(self.sess.run(self.ssim_loss, feed_dict={self.train_model.vae_im:i.reshape([-1] + self.vae_im_size)})[0]*-1)
            mse = np.array(mse)
            # mse = self.sess.run(self.ssim_loss, feed_dict={self.train_model.vae_im2:im})[0]*-1
        return -mse

    def get_train_ops(self, loss, name, params=None, mpi_rank_weight=1, max_grad_norm=0.5): 
        if params is None:
            params = tf.trainable_variables(name)
        # 2. Build our trainer
        with tf.variable_scope(name, reuse=tf.AUTO_REUSE):
            self.trainer = MpiAdamOptimizer(comm, learning_rate=self.LR, mpi_rank_weight=mpi_rank_weight, epsilon=1e-5)
        # 3. Calculate the gradients
        with tf.variable_scope(name, reuse=tf.AUTO_REUSE):
            grads_and_var = self.trainer.compute_gradients(loss, params)
            grads, var = zip(*grads_and_var)
        grads, _grad_norm = tf.clip_by_global_norm(grads, max_grad_norm)
        
        grads_and_var = list(zip(grads, var))
        # zip aggregate each gradient with parameters associated
        # For instance zip(ABCD, xyza) => Ax, By, Cz, Da

        self.grads = grads
        self.var = var
        with tf.variable_scope(name, reuse=tf.AUTO_REUSE):
            train_op = self.trainer.apply_gradients(grads_and_var)
        return train_op

    def train_gan(self, epoch, lr, im, z=None):
        # z = self.sess.run(self.train_model.enc_mean,feed_dict={self.train_model.vae_im:im})
        # z = np.zeros_like()
        td_map = {
            self.train_model.vae_im : im,
            # self.inputs : im,
            # self.z : z,

            self.LR : lr
        }
        if self.args.gan_type == "gan":
            td_map[self.train_model.z_ph] = z

        if self.args.gan_type in ["vae", "vae_with_enc"]:
            g_stats = self.sess.run(self.g_stats_list + [self._gen_train_op],td_map)[:-1]
            return [0] + g_stats
        else:
            d_stats = self.sess.run(self.d_stats_list + [self._dis_train_op],td_map)[:-1]
            g_stats = self.sess.run(self.g_stats_list + [self._gen_train_op],td_map)[:-1]
            # Run g twice? from DCGAN code
            g_stats = self.sess.run(self.g_stats_list + [self._gen_train_op],td_map)[:-1]
            return d_stats + g_stats

    def train(self, epoch, lr, cliprange, obs, imgs, returns, masks, actions, values, neglogpacs, lam=None,  exp_actions=None, states=None):
        # Here we calculate advantage A(s,a) = R + yV(s') - V(s)
        # Returns = R + yV(s')
        advs = returns - values
        # Normalize the advantages
        advs = (advs - advs.mean()) / (advs.std() + 1e-8)
        td_map = {
        self.train_model.ob : obs,
        self.A : actions,
        self.ACTION : actions,
        self.ADV : advs,
        self.R : returns,
        self.LR : lr,
        # self.CLIPRANGE : cliprange,
        # self.OLDNEGLOGPAC : neglogpacs,
        # self.OLDVPRED : values
        }
        td_map[self.train_model.im] = imgs
        # td_map[self.train_model.vae_im] = imgs
        
        return self.sess.run(self.stats_list + [self._train_op],td_map)[:-1]

    def run_train(self, data, last_value, last_done):
        self.finalise_buffer(data, last_value, last_done)
        self.train_model.ob_rms.update(self.data['ob'])
        if self.args.const_lr:
            self.cur_lrmult =  1.0
        else:
            self.cur_lrmult =  max(1.0 - float(self.timesteps_so_far) / self.max_timesteps, 0)
       
        lrnow = self.learning_rate*self.cur_lrmult
        self.lr = lrnow
        cliprangenow = 0.2
        self.n = self.data['done'].shape[0]
        inds = np.arange(self.n)

        t1 = time.time()
        if "decoded" not in self.args.tests:
            if "pre" in self.args.tests:
                if self.iters_so_far < 10:    
                    vae_epochs = 10
                    # vae_epochs = 5
                    # vae_epochs = 1
                else:
                    vae_epochs = 1
            else:
                vae_epochs = 1
            # vae_epochs = 0
            vae_loss = [0,0,0]
            if self.args.train_vae:
                if "roi" in self.args.tests:
                    vae_images = self.data["im"][:,:self.vae_im_size[0],int((self.im_size[0]-self.vae_im_size[1])/2):int(self.im_size[1]-(self.im_size[1]-self.vae_im_size[1])/2),:]
                else:
                    vae_images = self.data["im"]
                if "pos" in self.args.tests:
                    if self.iters_so_far == 0:
                        self.vae_images = vae_images
                        self.vae_images_idx = 0
                    else:
                        image_idx = np.where(self.data["ac"] == 1)[0]
                        if len(image_idx) + self.vae_images_idx >= self.vae_images.shape[0]:
                            initial = self.vae_images.shape[0] - self.vae_images_idx
                            final =  len(image_idx) + self.vae_images_idx - self.vae_images.shape[0]
                            self.vae_images[self.vae_images_idx:,::] = vae_images[image_idx[:initial]]
                            self.vae_images[:final,::] = vae_images[image_idx[initial:]]
                            self.vae_images_idx = final
                        else:
                            self.vae_images[self.vae_images_idx:len(image_idx)+self.vae_images_idx,::] = vae_images[image_idx]
                            self.vae_images_idx += len(image_idx) 
                else:
                    self.vae_images = vae_images
                vae_inds = [i for i in range(self.vae_images.shape[0])]
                for epoch in range(vae_epochs):
                    if self.enable_shuffle:
                        np.random.shuffle(vae_inds)
                    self.loss = [] 
                    # for start in range(0, vae_images.shape[0]//self.batch_size, self.batch_size):
                    for start in range(0, self.n, self.batch_size):
                        end = start + self.batch_size
                        
                        mbinds = vae_inds[start:end]
                        vae_loss = self.train_gan(epoch, lrnow, self.vae_images[mbinds])
        else:
            self.epochs = 5               
       
        ninety = np.percentile(self.train_model.recon_error_buffer, 90)
        all_maxes = MPI.COMM_WORLD.allgather(ninety) # list of tuples
        self.train_model.recon_thres = max(all_maxes)
        self.train_model.recon_error_buffer = []
        
        for epoch in range(self.epochs):
            if self.enable_shuffle:
                np.random.shuffle(inds)
            # self.loss = [] 
            for start in range(0, self.n, self.batch_size):
                end = start + self.batch_size
                # print(epoch, start, end)
                mbinds = inds[start:end]
                
                # im_slices = (self.data[key][mbinds] for key in ["im"])
                # vae_loss = self.train_vae(epoch, lrnow, cliprangenow, *im_slices)
                
                slices = (self.data[key][mbinds] for key in self.training_input)
                # print([self.data[key][mbinds].shape for key in self.training_input])
                # print( self.train(epoch, lrnow, cliprangenow, vf_only, *slices))
                

                outs = self.train(epoch, lrnow, cliprangenow, *slices)
                self.loss = outs[:len(self.loss_names)]
        

        # print("train", time.time()- t1)
        # self.loss = np.mean(self.loss, axis=0)
        if "decoded" not in self.args.tests:
            self.loss = self.loss + vae_loss
        # self.log_stuff(things={"Eps":self.train_model.eps, "vae_lam":self.train_model.lam, "vae_scaled_recon":self.train_model.scaled_recon_loss, "vae_recon_loss": self.train_model.recon_loss,"vae_recon_thres": self.train_model.recon_thres, "vae_recon_loss_pred":self.train_model.recon_loss_pred[0]})
        self.log_stuff(things={"Eps":self.train_model.eps,"vae_scaled_recon":self.train_model.scaled_recon_loss, "vae_recon_thres": self.train_model.recon_thres, "vae_recon_loss_pred":self.train_model.recon_loss_pred[0]})
        self.evaluate(data['ep_rets'], data['ep_lens'])

        if "setup" in self.name:
            self.re_init_buffer()
        else:
            self.init_buffer()
        
    # def step(self, ob, im, stochastic=False, multi=False): 
    #     if not multi:
    #         actions, values, self.states, neglogpacs =  self.train_model.step(ob[None], im[None], stochastic=stochastic)
    #         return actions[0], values, self.states, neglogpacs
    #     else:
    #         actions, values, self.states, neglogpacs =  self.train_model.step(ob, im, stochastic=stochastic)
    #         return actions, values, self.states, neglogpacs        

    def get_advantage(self, last_value, last_done):    
        gamma = 0.99; lam = 0.95
        advs = np.zeros_like(self.data['rew'])
        lastgaelam = 0
        for t in reversed(range(len(self.data['rew']))):
            if t == len(self.data['rew']) - 1:
                nextnonterminal = 1.0 - last_done
                nextvalues = last_value
            else:
                nextnonterminal = 1.0 - self.data['done'][t+1]
                nextvalues = self.data['value'][t+1]
            delta = self.data['rew'][t] + gamma * nextvalues * nextnonterminal - self.data['value'][t]
            advs[t] = lastgaelam = delta + gamma * lam * nextnonterminal * lastgaelam
        self.data['return'] = advs + self.data['value']
    
    def init_buffer(self):
        if "lam" in self.args.tests:
            self.data_input = ['ob', 'im', 'ac', 'rew', 'done', 'value', 'neglogpac', "lam"]
            self.training_input = ['ob', 'im', 'return', 'done', 'ac', 'value', 'neglogpac', "lam"]
        else:
            if "setup" in self.name:
                self.data_input = ['ob', 'im', 'ac', 'actions', 'rew', 'done', 'value', 'neglogpac', 'neglogpac_select']   
                self.training_input = ['ob', 'im', 'return', 'done', 'ac', 'value', 'neglogpac', 'actions', 'neglogpac_select']
            else:
                self.data_input = ['ob', 'im', 'ac', 'rew', 'done', 'value', 'neglogpac']
                self.training_input = ['ob', 'im', 'return', 'done', 'ac', 'value', 'neglogpac']
        self.data = {t:[] for t in self.data_input}

    def re_init_buffer(self):
        # self.old_last_data = {t:self.data[t][-1] for t in self.data_input}
        # print("before reinit")
        ob, im, ac, actions, rew, done = self.data['ob'][-1], self.data['im'][-1], self.data['ac'][-1], self.data['actions'][-1], self.data['rew'][-1], self.data['done'][-1]
        value, neglogpac, neglogpac_select = self.train_model.get_vpred_and_nlogps(ob, im, ac, actions)
        self.data = {t:[] for t in self.data_input}
        self.data['ob'].append(ob)
        self.data['im'].append(im)
        self.data['ac'].append(ac)
        self.data['actions'].append(actions)
        self.data['rew'].append(rew)
        self.data['done'].append(done)
        self.data['value'].append(value)
        self.data['neglogpac'].append(neglogpac)
        self.data['neglogpac_select'].append(neglogpac_select)        
        # print("after reinit", actions, neglogpac_select)

    def log_stuff(self, things):
        if self.rank == 0:
            for thing in things:
                self.writer.add_scalar(thing, things[thing], self.iters_so_far)
                logger.record_tabular(thing, things[thing])
        
    def add_to_buffer(self, data):
        ''' data needs to be a list of lists, same length as self.data'''
        for d,key in zip(data, self.data):
            if key == "im":
                im = d[:self.im_size[0],int((d.shape[0]-self.im_size[0])/2):int(d.shape[0]-(d.shape[0]-self.im_size[0])/2),:]
                # print("adding to buff", d.shape, im.shape)
                self.data[key].append(im)
            else:
                self.data[key].append(d)

    def finalise_buffer(self, data, last_value, last_done):
        ''' data must be dict'''
        for key in self.data_input:
            if key == 'done':
                self.data[key] = np.asarray(self.data[key], dtype=np.bool)
            else:
                self.data[key] = np.asarray(self.data[key])
        self.n = next(iter(self.data.values())).shape[0]
        for key in data:
            self.data[key] = data[key]
        self.get_advantage(last_value, last_done)
        # if self.rank == 0:
        # self.log_stuff()
