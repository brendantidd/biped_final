'''
From OpenAI Baselines
'''
from tensorflow.python.ops.nn_ops import softmax_cross_entropy_with_logits_v2
from models.base import Base
import os   
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' 
import tensorflow as tf
tf.get_logger().setLevel('DEBUG')
from scripts.distributions import make_pdtype
from scripts.mpi_running_mean_std import RunningMeanStd
from scripts.mpi_moments import mpi_moments
# from baselines.common import explained_variance, fmt_row, zipsame
import numpy as np
from gym import spaces
# import scripts.utils as U
import scripts.utils as TF_U
from scripts.mpi_adam_optimizer import MpiAdamOptimizer
from mpi4py import MPI
from scripts import logger
import time
import cv2
comm = MPI.COMM_WORLD
from collections import deque

def lrelu(x, leak=0.2, name="lrelu"):
  return tf.maximum(x, leak*x)

class batch_norm(object):
    def __init__(self, epsilon=1e-5, momentum = 0.9, name="batch_norm"):
        with tf.variable_scope(name):
            self.epsilon  = epsilon
            self.momentum = momentum
            self.name = name

    def __call__(self, x, train=True):
        return tf.contrib.layers.batch_norm(x,
                      decay=self.momentum, 
                      updates_collections=None,
                      epsilon=self.epsilon,
                      scale=True,
                      is_training=train,
                      scope=self.name)

def get_vars(scope):
    return [x for x in tf.global_variables() if scope in x.name]

# def neglogp(self, x):
#     return 0.5 * tf.reduce_sum(tf.square((x - self.mean) / self.std), axis=-1) \
#             + 0.5 * np.log(2.0 * np.pi) * tf.to_float(tf.shape(x)[-1]) \
#             + tf.reduce_sum(self.logstd, axis=-1)

def log_normal_pdf(sample, mean, logvar, raxis=1):
    log2pi = tf.math.log(2. * np.pi)
    return tf.reduce_sum(
            -.5 * ((sample - mean) ** 2. * tf.exp(-logvar) + logvar + log2pi),
            axis=raxis)

def gaussian_nll(mu, log_sigma, x):
    return 0.5 * ((x - mu) / tf.exp(log_sigma)) ** 2 + log_sigma + 0.5 * np.log(2 * np.pi)
    
def reparameterize(mean, logvar):
    eps = tf.random.normal(shape=tf.shape(mean))
    return eps * tf.exp(logvar * .5) + mean

class Policy():
    def __init__(self, name, ob, im, last_q, vae_im, z_ph_ob, z_ph_im, ob_space, ob_size, im_size, vae_im_size, ac_size, env, LAM, ac_space, latent_dim, sess, vis,  args, hid_size, normalize=True, const_std=False):
        self.name = name
        self.ob = ob
        self.im = im
        self.last_q = last_q
        self.vae_im = vae_im
        self.z_ph_ob = z_ph_ob
        self.z_ph_im = z_ph_im
        self.ob_size = ob_size
        self.im_size = im_size
        self.vae_im_size = vae_im_size
        self.ac_size = ac_size
        self.hid_size = hid_size
        self.env = env
        self.LAM = LAM
        self.sess = sess
        self.args = args
        self.pdtype = pdtype = make_pdtype(ac_space)
        self.vis = vis
        self.const_std = const_std
        self.latent_dim = latent_dim
        sequence_length = None
        # if normalize:
        with tf.variable_scope("obfilter"):
            self.ob_rms = RunningMeanStd(shape=ob_space.shape)
        obz = tf.clip_by_value((ob - tf.stop_gradient(self.ob_rms.mean)) / tf.stop_gradient(self.ob_rms.std), -5.0, 5.0)

        self.quarter = int(self.vae_im_size[0]/4)
        if self.args.robot == "titan":
            self.min_val = 0
            self.max_val = 1
        else:
            # walking
            self.min_val = 0.25
            self.max_val = 2
                
        self.data_range = self.max_val - self.min_val
        # first_layer = 32
        # second_layer = 64
        self.first_layer = 16
        self.second_layer = 32
        self.third_layer = 64
        self.y_dim = 1
        self.output_height, self.output_width = 28, 28
        # self.gf_dim = 64
        # self.dfc_dim = 1024
        # self.gfc_dim = 1024
        # self.df_dim = 64
        self.gf_dim = 16
        self.dfc_dim = 64
        self.gfc_dim = 64
        self.df_dim = 16
        self.c_dim = 1
        self.batch_size = 32
        self.s_h,  self.s_w = self.vae_im_size[0], self.vae_im_size[1]
        self.s_h2, self.s_h4 = int(self.s_h/2), int(self.s_h/4)
        self.s_w2, self.s_w4 = int(self.s_w/2), int(self.s_w/4) 

        # self.zs, _, _ = self.encoder(self.im)
        
        # self.class_logits = tf.layers.dense(self.zs, 2, name="e_class_logits")
        # self.classifier = tf.nn.softmax(self.class_logits)

        # self.z_mean, self.z_logstd, self.z = self.encoder(self.vae_im, obz)
        self.z_im = self.encoder(self.vae_im)
        # print(self.z_mean.shape); exit()
        # self.decoded, decoded_log_sigma = self.generator(self.z_mean)
        
        # self.decoded_ob_from_z, self.decoded_im_from_z = self.generator(self.z_ph_im)
        self.decoded_im_from_z = self.generator(self.z_ph_im)
        # if self.args.use_kl:
        #     self.decoded_from_im, decoded_log_sigma = self.generator(self.z)
        # else:
        self.decoded_im_from_im = self.generator(self.z_im)
        # self.decoded_from_im, decoded_log_sigma = self.generator(self.last_q)

        # self.z_mean_decoded, _, _ = self.encoder(self.decoded)

        # self.decoded2, _ = self.generator2(self.z_mean)
        # self.decoded2_from_z, _ = self.generator2(self.z_ph, reuse=True)

        self.discrim, self.discrim_logits = self.discriminator(self.vae_im)
        self.discrim_gen, self.discrim_logits_gen = self.discriminator(self.decoded_im_from_z, reuse=True)
            

        # self.decoded = self.generator(self.z)
        # self.decoded_from_z, _ = self.generator(self.z_ph, reuse=True)
        # self.decoded_from_im, _ = self.generator2(self.z_mean, reuse=True)

        # self.recon = tf.reduce_sum(gaussian_nll(self.decoded, decoded_log_sigma, self.vae_im), axis=[1, 2, 3])
        # self.recon_z = tf.reduce_sum(tf.square(self.z_ph - self.z_mean), axis=[1, 2, 3])
        # self.recon_z = tf.reduce_sum(tf.square(self.z_ph - self.z_mean), axis=[1])
        # self.recon = tf.reduce_sum(tf.square(self.vae_im - self.decoded), axis=[1, 2, 3])
        self.recon_im = tf.reduce_sum(tf.square(self.vae_im - self.decoded_im_from_im), axis=[1, 2, 3])
        # self.recon_ob = tf.reduce_sum(tf.square(obz - self.decoded_ob_from_im), axis=1)
        # self.total_recon = self.recon_im + self.recon_ob
        
        # self.recon_im = tf.reduce_sum(tf.square(self.vae_im - self.decoded_from_im), axis=[1, 2, 3])

        # self.rec_loss = tf.reduce_sum(gaussian_nll(img_rec, log_sigma, img))
        # if self.args.use_kl:
        #     self.kld_loss = -tf.reduce_sum(0.5 * (1 + self.z_logstd - self.z_mean ** 2 - tf.exp(self.z_logstd)))

        # Consider adding observations
        # if normalize:
        #     obz = tf.clip_by_value((ob - tf.stop_gradient(self.ob_rms.mean)) / tf.stop_gradient(self.ob_rms.std), -5.0, 5.0)
        # else:
        #     obz = ob

        self.stochastic = tf.placeholder(dtype=tf.bool, shape=())

        self.max_lam = 0.1

        self.state = tf.constant([])
        self.initial_state = None

        self.steps = 0
        self.eps_start = 0.5
        self.eps_end = 0.001
        self.eps = self.eps_start
        self.eps_decay = 50000

        self.lam_steps = 0
        self.lam_decay = 100000
        self.scaled_recon_loss = 0.0
        self.recon_loss = 0.0

        self.lam = self.args.lam
        self.recon_error_buffer = []
        self.q_error_buffer = []
        self.recon_thres = 0.0
        self.q_thres = 0.0
        self.latent_thres = 0.0

    def generator(self, im_inputs, reuse=False):
        with tf.variable_scope("decode") as scope:
            if reuse:
                scope.reuse_variables()
            last_out = tf.nn.relu(tf.layers.dense(im_inputs, int(self.vae_im_size[0]/4) * int(self.vae_im_size[1]/4) * self.second_layer, name="g_1"))
            # print("gen", last_out.shape)
            last_out = tf.reshape(last_out, (-1, int(self.vae_im_size[0]/4), int(self.vae_im_size[1]/4), self.second_layer))
            # print("gen", last_out.shape)

            last_out = tf.nn.relu(tf.layers.conv2d_transpose(last_out, self.second_layer, 3, strides=2, padding='same', name="g_2"))
            # print("gen", last_out.shape)

            last_out = tf.nn.relu(tf.layers.conv2d_transpose(last_out, self.first_layer, 3, strides=2, padding='same', name="g_3"))
            # print("gen", last_out.shape)
            decoded_logits = tf.layers.conv2d_transpose(last_out, 1, 3, padding="same", name="g_4")
            decoded_im = ((tf.nn.tanh(decoded_logits, name="g_final") + 1 )/ 2) * self.data_range + self.min_val
            # print(decoded.shape); exit()
            # last_out = tf.nn.relu(tf.layers.dense(inputs, 16, name="g_logvar1"))
            # decoded_logvar = tf.layers.dense(last_out, 1, name="g_logvar2")[:,0]

            # last_out = tf.nn.tanh(tf.layers.dense(ob_inputs, 32, name="g_ob1"))
            # last_out = tf.nn.tanh(tf.layers.dense(last_out, 32, name="g_ob2"))
            # decoded_ob = tf.nn.tanh(tf.layers.dense(last_out, self.ob_size, name="g_ob_final"))
            decoded_ob = np.zeros(self.ob_size)
            # decoded_logvar = 0.0
            # decoded_logvar = tf.Variable(0.0, trainable=True)

            return decoded_im

    def discriminator(self, inputs, reuse=False):
        with tf.variable_scope("discriminator") as scope:
            if reuse:
                scope.reuse_variables()
            x = tf.nn.relu(tf.layers.conv2d(inputs, self.first_layer, 3, strides=2, padding="same", name="dis_vis1"))
            x = tf.nn.relu(tf.layers.conv2d(x, self.second_layer, 3, strides=2, padding="same", name="dis_vis2"))
            x = tf.layers.flatten(x)
            x = tf.layers.dense(x, self.hid_size, name="dis_lin1")
            x = tf.layers.dense(x, self.hid_size, name="dis_lin2")
            dis_logits = tf.layers.dense(x, 1, name="dis_final")
            return tf.nn.sigmoid(dis_logits), dis_logits

    def encoder(self, im_inputs, reuse=False):
        with tf.variable_scope("encoder") as scope:
            if reuse:
                scope.reuse_variables()
            # x = lrelu(tf.layers.conv2d(inputs, self.first_layer, 3, strides=2, padding="same", name="e_vis1"))
            # x = lrelu(tf.layers.conv2d(x, self.second_layer, 3, strides=2, padding="same", name="e_vis2"))
            x = tf.nn.relu(tf.layers.conv2d(im_inputs, self.first_layer, 3, strides=2, padding="same", name="e_vis1"))
            x = tf.nn.relu(tf.layers.conv2d(x, self.second_layer, 3, strides=2, padding="same", name="e_vis2"))
            # x = tf.nn.relu(tf.layers.conv2d(x, self.third_layer, 3, strides=2, padding="same", name="e_vis3"))

            # x = tf.layers.flatten(inputs)
            x = tf.layers.flatten(x)

# 
            x = tf.nn.tanh(tf.layers.dense(x, self.hid_size, name="e_lin1"))
            # x = tf.nn.tanh(tf.layers.dense(x, self.hid_size, name="e_lin2"))
            # x = tf.layers.dense(x, self.hid_size, name="e_lin3")
            z_im = tf.layers.dense(x, self.latent_dim, name="e_z_mean")


            # last_out = tf.nn.tanh(tf.layers.dense(ob_inputs, 32, name="e_ob1"))
            # last_out = tf.nn.tanh(tf.layers.dense(last_out, 32, name="e_ob2"))
            # z_ob = tf.nn.tanh(tf.layers.dense(last_out, self.latent_dim, name="e_ob_final"))
            
            # z_ob = tf.constant([0.0], shape=self.ob_size)

            # z_mean = tf.concat(axis=1,values=[im_z, ob_z])

            # z_logvar = tf.layers.dense(x, self.latent_dim, name="e_logvar")
            # z = reparameterize(z_mean, z_logvar)
            z_logvar = 0.0
            z = 0.0

            

            # return im_z, z_logvar, z, ob_z
            return z_im
    
    def initializer(self):
        return tf.keras.initializers.RandomNormal()

    def decode_from_z(self, ob, z_im):
        # decoded = self.sess.run(self.decoded_from_z,feed_dict={self.z_ph:z})
        # decoded = self.sess.run(self.decoded2_from_z,feed_dict={self.z_ph:z})
        decoded_im = self.sess.run(self.decoded_im_from_z,feed_dict={self.z_ph_im:z_im})
        return decoded_im

    def encode(self, ob, im):
        z_mean, z_log_var = self.sess.run([self.z_mean, self.z_log_var], feed_dict={self.im:im})
        # z_im = self.sess.run(self.z_im, feed_dict={self.ob:ob, self.im:im, self.vae_im:im})
        return z_mean
    
    def decode(self, ob, im):
        decoded = self.sess.run(self.decoded_im_from_im, feed_dict={self.im:im, self.vae_im:im})
        # decoded = self.sess.run(self.decoded_im_from_im, feed_dict={self.ob:ob, self.im:im, self.vae_im:im})
        # decoded = self.sess.run(self.decoded2, feed_dict={self.im:im, self.vae_im:im})
        return decoded

    def update_eps(self):
        self.eps =  max(self.eps_start - (float(self.steps) / (self.eps_decay)), self.eps_end)
        self.steps += 1

    def step(self, ob, im, stochastic=False, eval_pol=False):

        if self.args.train_vae:
            im_copy = im.copy()
            vae_im = im[:self.vae_im_size[0],int((self.im_size[0]-self.vae_im_size[0])/2):int(self.im_size[0]-(self.im_size[0]-self.vae_im_size[0])/2),:]

            q, v, recon_im, self.err_pred, self.q_err, recon_loss = self.sess.run([self.qs, self.vpred, self.decoded, self.err, self.qs_error, self.recon], 
                                feed_dict={self.ob: ob.reshape([1,self.ob_size]), 
                                            self.im: im.reshape([1]+self.im_size), 
                                            self.vae_im: vae_im.reshape([1]+self.vae_im_size), 
                                            self.im: im.reshape([1]+self.im_size), 
                                            self.stochastic:stochastic})

            self.q_err = [self.q_err]
            self.q_loss = (self.q_err[0] - self.err_pred[0])**2             
            
            new_qs = q[0]
            im_copy[:self.vae_im_size[0],int((self.im_size[1]-self.vae_im_size[1])/2):int(self.im_size[1]-(self.im_size[1]-self.vae_im_size[1])/2),:] = recon_im[0,::]
            

            self.update_eps()
            if np.random.random() > self.eps or not stochastic: 
                if eval_pol:
                    act_from_q = np.argmax(new_qs)
                    if act_from_q == 1 and recon_loss < self.recon_thres and self.q_loss < self.q_thres:
                        act = 1
                    else:
                        act = 0
                else:
                    act = np.argmax(new_qs)
            else:
                act = np.random.randint(2)
            return [act], v, im_copy, self.err_pred[0], self.q_err[0], np.clip(new_qs,0.0, None), self.q_loss, recon_loss

    def update_err_thres(self):
        if len(self.recon_error_buffer) == 0:
            self.recon_error_buffer.append(self.recon_thres)
        if "max" in self.args.ae_type:
            # thres = max(self.recon_error_buffer)
            thres = np.percentile(self.recon_error_buffer, 99)
        else:
            thres = np.percentile(self.recon_error_buffer, 95)
        all_maxes = MPI.COMM_WORLD.allgather(thres) 
        self.recon_thres = max(all_maxes)
        self.recon_error_buffer = []
        
        if len(self.q_error_buffer) == 0:
            self.q_error_buffer.append(self.q_thres)
        if "max" in self.args.ae_type:
            # thres = max(self.q_error_buffer)
            thres = np.percentile(self.q_error_buffer, 99)
        else:
            thres = np.percentile(self.q_error_buffer, 95)
        all_maxes = MPI.COMM_WORLD.allgather(thres) 
        self.q_thres = max(all_maxes)
        self.q_error_buffer = []

class Model(Base):
    def __init__(self, name, env, ac_size, ob_size, im_size=[48,48,1], vae_im_size=[48,48,1], args=None, PATH=None, writer=None, hid_size=256, vis=False, normalize=True, ent_coef=0.0, vf_coef=0.5, max_grad_norm=0.5, mpi_rank_weight=1, max_timesteps=int(1e6), lr=3e-4, horizon=2048, batch_size=32, const_std=False):
        self.max_timesteps = max_timesteps
        self.horizon = horizon
        self.batch_size = batch_size
        self.learning_rate = lr
        self.env = env
        self.ob_size = ob_size
        self.im_size = im_size
        self.vae_im_size = vae_im_size
        self.ac_size = ac_size

        self.sess = sess = TF_U.get_session()
        self.name = name
        high = np.inf*np.ones(ac_size)
        low = -high
        ac_space = spaces.Box(low, high, dtype=np.float32)
        high = np.inf*np.ones(ob_size)
        low = -high
        ob_space = spaces.Box(low, high, dtype=np.float32)
        self.args = args   
        im_size = im_size
        self.PATH = PATH
        self.hid_size = hid_size
        self.writer = writer
        self.latent_dim = self.args.latent_dim
        ob = TF_U.get_placeholder(name="ob_" + name, dtype=tf.float32, shape=[None] + list(ob_space.shape))
        im = TF_U.get_placeholder(name="im_" + name, dtype=tf.float32, shape=[None] + im_size)
        last_q = TF_U.get_placeholder(name="last_q_" + name, dtype=tf.float32, shape=[None, self.latent_dim])
        vae_im = TF_U.get_placeholder(name="vae_im_" + name, dtype=tf.float32, shape=[None] + vae_im_size)
        z_ph_ob = TF_U.get_placeholder(name="z_ph_ob_" + name, dtype=tf.float32, shape=[None, self.latent_dim])
        z_ph_im = TF_U.get_placeholder(name="z_ph_im_" + name, dtype=tf.float32, shape=[None, self.latent_dim])
        self.LAM = TF_U.get_placeholder(name="lam_" + self.name, dtype=tf.float32, shape=[None])
        
        self.args = args
        if ('vel' in name or 'hex' in name) and not args.vis:
            self.vis = False
        else:
            self.vis = vis
        with tf.variable_scope(name, reuse=tf.AUTO_REUSE):
            self.train_model = train_model = Policy(name, ob, im, last_q, vae_im, z_ph_ob, z_ph_im, ob_space, ob_size, im_size, vae_im_size, ac_size, env, self.LAM, ac_space, self.latent_dim, sess, self.vis, args=args, normalize=normalize, hid_size=hid_size, const_std=const_std)
        Base.__init__(self)

        # CREATE THE PLACEHOLDERS
        self.A = A = train_model.pdtype.sample_placeholder([None])
        self.ADV = ADV = tf.placeholder(tf.float32, [None])
        self.R = R = tf.placeholder(tf.float32, [None])
        # Keep track of old critic
        self.OLDVPRED = OLDVPRED = tf.placeholder(tf.float32, [None])
        self.LR = LR = tf.placeholder(tf.float32, [])
        # self.ERR = ERR = tf.placeholder(tf.float32, [None], name="ERR")

        def sigmoid_cross_entropy_with_logits(x, y):
            try:
                return tf.nn.sigmoid_cross_entropy_with_logits(logits=x, labels=y)
            except Exception as e:
                print(e); exit()
                return tf.nn.sigmoid_cross_entropy_with_logits(logits=x, targets=y)

        if "gan" in args.ae_type:
            self.G = self.train_model.decoded_im_from_z
            self.D, self.D_logits = self.train_model.discrim, self.train_model.discrim_logits
            self.D_, self.D_logits_ = self.train_model.discrim_gen, self.train_model.discrim_logits_gen
            

            self.d_loss_real = tf.reduce_mean(
            sigmoid_cross_entropy_with_logits(self.D_logits, tf.ones_like(self.D)))
            self.d_loss_fake = tf.reduce_mean(
            sigmoid_cross_entropy_with_logits(self.D_logits_, tf.zeros_like(self.D_)))
            
            self.d_loss = self.d_loss_real + self.d_loss_fake
            self.gan_loss = tf.reduce_mean(sigmoid_cross_entropy_with_logits(self.D_logits_, tf.ones_like(self.D_)))
            
        if "super" in args.ae_type:
            self.CLASS = CLASS = tf.placeholder(tf.int32, [None,1])
            self.super_loss = tf.nn.softmax_cross_entropy_with_logits_v2(labels=tf.one_hot(self.CLASS,2), logits=self.train_model.class_logits)

        # if self.args.use_kl:
        #     self.gen_mse_loss = tf.reduce_mean(train_model.recon) + train_model.kld_loss
        # else:
        # self.gen_mse_loss = tf.reduce_mean(train_model.recon_im) + tf.reduce_mean(train_model.recon_ob)
        self.gen_mse_loss = tf.reduce_mean(train_model.recon_im)
        # self.gen2_mse_loss = tf.reduce_mean(train_model.recon2)
        # self.gen_z_mse_loss = tf.reduce_mean(train_model.recon_z)

        # self.err_loss = 0.5*tf.reduce_mean(tf.square(train_model.err - self.ERR))
        # self.err_loss = 0.5*tf.reduce_mean(tf.square(train_model.err - tf.stop_gradient(train_model.qs_error)))

        all_variables = tf.trainable_variables()

        # self.g_loss = self.gen_mse_loss + 0.01*train_model.kld_loss
        # self.g_loss = self.gen_mse_loss + self.gen_z_mse_loss
        # self.g_loss = self.gen_mse_loss 
        # self.g_stats_list = [self.gen_mse_loss]
        # self.g2_loss = self.gen2_mse_loss 
        # self.g2_stats_list = [self.gen2_mse_loss]
        # self.e_stats_list = [self.err_loss]
        # self.loss_names = ['value_loss', 'learning_rate', 'q_err_loss', 'recon_loss']
        # self.loss_names = ['value_loss', 'learning_rate', 'recon_loss']
    
        # gen_var = [var for var in all_variables if 'g_' in var.name] 
        # err_var = [var for var in all_variables if 'err_' in var.name]
        # if "enc" in args.ae_type:
        # gen_var += [var for var in all_variables if 'e_' in var.name]
        # self._gen_train_op = self.get_train_ops(self.g_loss, name + "/gen", params=gen_var)            
        learning_rate = 0.001


        if self.args.ae_type == "super":
            loss = self.super_loss
            self.super_stats_list = [self.super_loss]
            # gen_var = [var for var in all_variables if 'g_' in var.name] 
            super_var = [var for var in all_variables if 'e_' in var.name] 
            # self._gen_train_op = self.get_train_ops(self.gen_mse_loss, name + "/gen", params=gen_var)            
            # self._gen_train_op = self.get_train_ops(self.gen_mse_loss, name + "/gen", params=gen_var)            
            self._super_train_op = tf.train.AdamOptimizer(learning_rate).minimize(loss, var_list=super_var)


        if self.args.ae_type == "super_ae":
            loss = self.gen_mse_loss + self.super_loss
            self.gen_stats_list = [self.gen_mse_loss,self.super_loss]
            gen_var = [var for var in all_variables if 'g_' in var.name] 
            gen_var += [var for var in all_variables if 'e_' in var.name] 
            # self._gen_train_op = self.get_train_ops(self.gen_mse_loss, name + "/gen", params=gen_var)            
            # self._gen_train_op = self.get_train_ops(self.gen_mse_loss, name + "/gen", params=gen_var)            
            self._gen_train_op = tf.train.AdamOptimizer(learning_rate).minimize(loss, var_list=gen_var)


        if self.args.ae_type == "ae":
            self.gen_stats_list = [self.gen_mse_loss]
            gen_var = [var for var in all_variables if 'g_' in var.name] 
            gen_var += [var for var in all_variables if 'e_' in var.name] 
            # self._gen_train_op = self.get_train_ops(self.gen_mse_loss, name + "/gen", params=gen_var)            
            # self._gen_train_op = self.get_train_ops(self.gen_mse_loss, name + "/gen", params=gen_var)            
            self._gen_train_op = tf.train.AdamOptimizer(learning_rate).minimize(self.gen_mse_loss, var_list=gen_var)

        if self.args.ae_type == "gan":
            self.gan_stats_list = [self.gan_loss, self.d_loss]
            gan_var = [var for var in all_variables if 'g_' in var.name] 
            # self._gan_train_op = self.get_train_ops(self.gan_loss, name + "/gan", params=gan_var)            
            self._gan_train_op = tf.train.AdamOptimizer(learning_rate).minimize(self.gan_loss, var_list=gan_var)

            dis_var = [var for var in all_variables if 'dis_' in var.name] 
            # self._dis_train_op = self.get_train_ops(self.d_loss, name + "/dis", params=dis_var)            
            self._dis_train_op = tf.train.AdamOptimizer(learning_rate).minimize(self.d_loss, var_list=dis_var)
        
        if self.args.ae_type == "ae_gan":
            loss = self.gan_loss + self.gen_mse_loss
            self.gan_stats_list = [self.gen_mse_loss,self.gan_loss, self.d_loss]
            gan_var = [var for var in all_variables if 'g_' in var.name] 
            gan_var += [var for var in all_variables if 'e_' in var.name] 
            # self._gan_train_op = self.get_train_ops(self.gan_loss, name + "/gan", params=gan_var)            
            self._gan_train_op = tf.train.AdamOptimizer(learning_rate).minimize(loss, var_list=gan_var)

            dis_var = [var for var in all_variables if 'dis_' in var.name] 
            # self._dis_train_op = self.get_train_ops(self.d_loss, name + "/dis", params=dis_var)            
            self._dis_train_op = tf.train.AdamOptimizer(learning_rate).minimize(self.d_loss, var_list=dis_var)
        

        # self.enc_loss = 0.5*tf.reduce_mean(tf.square(train_model.z_mean - self.train_model.z_ph))
        # self.enc_stats_list = [self.enc_loss]
        # enc_var = [var for var in all_variables if 'e_' in var.name] 
        # self._enc_train_op = self.get_train_ops(self.enc_loss, name + "/enc", params=enc_var)            


        # gen2_var = [var for var in all_variables if 'g2_' in var.name] 
        # self._gen2_train_op = self.get_train_ops(self.g2_loss, name + "/gen2", params=gen2_var)            

        self.step = train_model.step

        self.decode_from_z = train_model.decode_from_z
        self.train_model = train_model
        self.init_buffer()

    def get_train_ops(self, loss, name, params=None, mpi_rank_weight=1, max_grad_norm=0.5): 
        if params is None:
            params = tf.trainable_variables(name)
        # print(name)
        # print(params)
        # 2. Build our trainer
        with tf.variable_scope(name, reuse=tf.AUTO_REUSE):
            self.trainer = MpiAdamOptimizer(comm, learning_rate=self.LR, mpi_rank_weight=mpi_rank_weight, epsilon=1e-5)
        # 3. Calculate the gradients
        with tf.variable_scope(name, reuse=tf.AUTO_REUSE):
            grads_and_var = self.trainer.compute_gradients(loss, params)
            grads, var = zip(*grads_and_var)
        grads, _grad_norm = tf.clip_by_global_norm(grads, max_grad_norm)
        
        grads_and_var = list(zip(grads, var))
        # zip aggregate each gradient with parameters associated
        # For instance zip(ABCD, xyza) => Ax, By, Cz, Da

        self.grads = grads
        self.var = var
        with tf.variable_scope(name, reuse=tf.AUTO_REUSE):
            train_op = self.trainer.apply_gradients(grads_and_var)
        return train_op

    def get_pred(self,im, labels):
        pred = self.sess.run(self.train_model.classifier, feed_dict={self.train_model.vae_im:im, self.train_model.im:im})
        # print(np.argmax(pred,axis=1))
        return np.argmax(pred,axis=1), np.mean(1.0 - np.abs(labels - np.argmax(pred,axis=1)))

    def get_mse(self, ob, im):
        
        if self.args.mse:
            
            # mse = self.sess.run(self.train_model.recon, feed_dict={self.train_model.vae_im:im, self.train_model.im:im})
            mse = self.sess.run(self.train_model.recon_im, feed_dict={self.train_model.ob:ob, self.train_model.vae_im:im, self.train_model.im:im})
            # mse = self.sess.run(self.train_model.total_recon, feed_dict={self.train_model.ob:ob, self.train_model.vae_im:im, self.train_model.im:im})
            # mse = []
            # for i in im:
            #     grad = self.sess.run(self._grad_op, feed_dict={self.train_model.vae_im:im, self.train_model.im:im})
            #     # print(self.var)
            #     # print([g.shape for g in grad])
            #     mse.append(sum(np.array([g for gr in grad[-2] for g in gr])))
            #     # print(mse[-1])
            #     # print(len(grad)); exit()
            # mse = np.array(mse)
        else:
            mse = []
            for i in im:
                mse.append(self.sess.run(self.ssim_loss, feed_dict={self.train_model.vae_im:i.reshape([-1] + self.vae_im_size)})[0]*-1)
            mse = np.array(mse)
        return -mse

    def train_ae(self, epoch, lr, ob, imgs, vae_imgs, z, labels=None, last_q=None):
        self.train_model.ob_rms.update(ob)
        
        td_map = {
        self.LR : lr,
        self.train_model.ob:ob,
        self.train_model.im:imgs,
        self.train_model.vae_im:vae_imgs,
        self.train_model.z_ph_ob: z,
        self.train_model.z_ph_im: z
        # self.train_model.last_q: last_q
        }
        # if "rand" in self.args.ae_type:
        #     g_stats = [0.0]
        # else:
        # print(self.g_stats_list); exit()
        # g_stats = self.sess.run(self.g_stats_list + [self._gen_train_op],td_map)[:-1]
        # g2_stats = self.sess.run(self.g2_stats_list + [self._gen2_train_op],td_map)[:-1]
        if self.args.ae_type == "ae":
            gen_stats = self.sess.run(self.gen_stats_list + [self._gen_train_op],td_map)[:-1]
            # print([gen_stats[0]] + [0.0,0.0] + [gen_stats[1]])
            return gen_stats + [0.0,0.0,0.0]
        elif self.args.ae_type == "super":
            td_map[self.CLASS] = labels
            super_stats = self.sess.run(self.super_stats_list + [self._super_train_op],td_map)[:-1]
            return [0.0, 0.0,0.0] + super_stats
        elif self.args.ae_type == "super_ae":
            td_map[self.CLASS] = labels
            gen_stats = self.sess.run(self.gen_stats_list + [self._gen_train_op],td_map)[:-1]
            return [gen_stats[0]] + [0.0,0.0] + [gen_stats[1]]
        elif self.args.ae_type == "gan":
            self.sess.run(self._dis_train_op,td_map)
            gan_stats = self.sess.run(self.gan_stats_list + [self._gan_train_op],td_map)[:-1]
            return [0.0] + gan_stats + [0.0]
        elif self.args.ae_type == "ae_gan":
            self.sess.run(self._dis_train_op,td_map)
            gan_stats = self.sess.run(self.gan_stats_list + [self._gan_train_op],td_map)[:-1]
            gan_stats = self.sess.run(self.gan_stats_list + [self._gan_train_op],td_map)[:-1]
            return gan_stats + [0.0]
        else:
            gen_stats = self.sess.run(self.gen_stats_list + [self._gen_train_op],td_map)[:-1]
            gen_stats = self.sess.run(self.gen_stats_list + [self._gen_train_op],td_map)[:-1]
            gen_stats = self.sess.run(self.gen_stats_list + [self._gen_train_op],td_map)[:-1]
            self.sess.run(self._dis_train_op,td_map)
            gan_stats = self.sess.run(self.gan_stats_list + [self._gan_train_op],td_map)[:-1]
            return gen_stats + gan_stats + [0.0]

        # decoded_images = self.sess.run(self.train_model.decoded_from_z,feed_dict={self.train_model.z_ph:z})
        # td_map = {self.train_model.vae_im:decoded_images, self.train_model.z_ph:z, self.LR:lr}
        # enc_stats = self.sess.run(self.enc_stats_list + [self._enc_train_op],td_map)[:-1]
        # return gen_stats + gan_stats + enc_stats
        # return g_stats + g2_stats
       
    # def train(self, epoch, lr, obs, imgs, returns, masks, actions, values):
    #     # Here we calculate advantage A(s,a) = R + yV(s') - V(s)
    #     # Returns = R + yV(s')
    #     # print(lam.shape)
    #     advs = returns - values
    #     # Normalize the advantages
    #     advs = (advs - advs.mean()) / (advs.std() + 1e-8)
    #     td_map = {
    #     self.train_model.ob : obs,
    #     self.A : actions,
    #     self.ADV : advs,
    #     self.R : returns,
    #     self.LR : lr,
    #     self.OLDVPRED : values
    #     }
    #     td_map[self.train_model.im] = imgs
    #     td_map[self.ACTION] = actions
            
    #     outputs = self.sess.run(self.stats_list + [self._train_op],td_map)[:-1]
    #     # td_map[self.ERR] = outputs[-1]
    #     # e_stats = self.sess.run(self.e_stats_list + [self._err_train_op],td_map)[:-1]
    #     # return outputs[-1] + e_stats
    #     return outputs

    def run_train(self, data, last_value, last_done):
        self.finalise_buffer(data, last_value, last_done)
        self.train_model.ob_rms.update(self.data['ob'])
        if self.args.const_lr:
            self.cur_lrmult =  1.0
        else:
            self.cur_lrmult =  max(1.0 - float(self.timesteps_so_far) / self.max_timesteps, 0)
       
        lrnow = self.learning_rate*self.cur_lrmult
        self.lr = lrnow
        cliprangenow = 0.2
        self.n = self.data['done'].shape[0]
        inds = np.arange(self.n)
        # print(self.epochs)
        t1 = time.time()
        if "decoded" not in self.args.tests:
            if "pre" in self.args.tests:
                if self.iters_so_far < 10:    
                    vae_epochs = 10
                    # vae_epochs = 5
                    # vae_epochs = 1
                else:
                    vae_epochs = 2
            else:
                vae_epochs = 2
                # vae_epochs = 1
            # vae_epochs = 0
            vae_loss = [0,0,0]
            if self.args.train_vae:
                if "roi" in self.args.tests:
                    vae_images = self.data["im"][:,:self.vae_im_size[0],int((self.im_size[0]-self.vae_im_size[1])/2):int(self.im_size[1]-(self.im_size[1]-self.vae_im_size[1])/2),:]
                else:
                    vae_images = self.data["im"]
                if "pos" in self.args.tests:
                    if self.iters_so_far == 0:
                        self.vae_images = vae_images
                        self.vae_images_idx = 0
                    else:
                        image_idx = np.where(self.data["ac"] == 1)[0]
                        if len(image_idx) + self.vae_images_idx >= self.vae_images.shape[0]:
                            initial = self.vae_images.shape[0] - self.vae_images_idx
                            final =  len(image_idx) + self.vae_images_idx - self.vae_images.shape[0]
                            self.vae_images[self.vae_images_idx:,::] = vae_images[image_idx[:initial]]
                            self.vae_images[:final,::] = vae_images[image_idx[initial:]]
                            self.vae_images_idx = final
                        else:
                            self.vae_images[self.vae_images_idx:len(image_idx)+self.vae_images_idx,::] = vae_images[image_idx]
                            self.vae_images_idx += len(image_idx) 
                else:
                    self.vae_images = vae_images

                vae_inds = [i for i in range(self.vae_images.shape[0])]
                for epoch in range(vae_epochs):
                    if self.enable_shuffle:
                        np.random.shuffle(vae_inds)
                    self.loss = [] 
                    for start in range(0, self.n, self.batch_size):
                        end = start + self.batch_size
                        mbinds = vae_inds[start:end]
                        vae_loss = self.train_ae(epoch, lrnow, self.vae_images[mbinds])               
        else:
            self.epochs = 5                     
        # if self.rank == 0:print("ae train", time.time() - t1)
        t1 = time.time()
        for epoch in range(self.epochs):
            if self.enable_shuffle:
                np.random.shuffle(inds)
            for start in range(0, self.n, self.batch_size):
                end = start + self.batch_size
                mbinds = inds[start:end]
                slices = (self.data[key][mbinds] for key in self.training_input)       
                outs = self.train(epoch, lrnow, *slices)
                self.loss = outs[:len(self.loss_names)]
        # if self.rank == 0: print("q train", time.time() - t1)

        if "decoded" not in self.args.tests:
            self.loss = self.loss + vae_loss
        self.log_stuff(things={"Eps":self.train_model.eps, "recon_thres": self.train_model.recon_thres, "q_thres": self.train_model.q_thres})
        self.evaluate(data['ep_rets'], data['ep_lens'])

        self.init_buffer()

    def get_advantage(self, last_value, last_done):    
        gamma = 0.99; lam = 0.95
        advs = np.zeros_like(self.data['rew'])
        lastgaelam = 0
        for t in reversed(range(len(self.data['rew']))):
            if t == len(self.data['rew']) - 1:
                nextnonterminal = 1.0 - last_done
                nextvalues = last_value
            else:
                nextnonterminal = 1.0 - self.data['done'][t+1]
                nextvalues = self.data['value'][t+1]
            delta = self.data['rew'][t] + gamma * nextvalues * nextnonterminal - self.data['value'][t]
            advs[t] = lastgaelam = delta + gamma * lam * nextnonterminal * lastgaelam
        self.data['return'] = advs + self.data['value']
    
    def init_buffer(self):
        self.data_input = ['ob', 'im', 'ac', 'rew', 'done', 'value']
        self.training_input = ['ob', 'im', 'return', 'done', 'ac', 'value']
        self.data = {t:[] for t in self.data_input}

    def log_stuff(self, things):
        if self.rank == 0:
            for thing in things:
                self.writer.add_scalar(thing, things[thing], self.iters_so_far)
                logger.record_tabular(thing, things[thing])
        
    def add_to_buffer(self, data):
        ''' data needs to be a list of lists, same length as self.data'''
        for d,key in zip(data, self.data):
            if key == "im":
                im = d[:self.im_size[0],int((d.shape[0]-self.im_size[0])/2):int(d.shape[0]-(d.shape[0]-self.im_size[0])/2),:]
                # print("adding to buff", d.shape, im.shape)
                self.data[key].append(im)
            else:
                self.data[key].append(d)

    def finalise_buffer(self, data, last_value, last_done):
        ''' data must be dict'''
        for key in self.data_input:
            if key == 'done':
                self.data[key] = np.asarray(self.data[key], dtype=np.bool)
            else:
                self.data[key] = np.asarray(self.data[key])
        self.n = next(iter(self.data.values())).shape[0]
        for key in data:
            self.data[key] = data[key]
        self.get_advantage(last_value, last_done)
        # if self.rank == 0:
        # self.log_stuff()
