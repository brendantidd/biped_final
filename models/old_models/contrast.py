'''
From OpenAI Baselines
'''
from tensorflow.python.ops.nn_ops import softmax_cross_entropy_with_logits_v2
from models.base import Base
import os   
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' 
import tensorflow as tf
tf.get_logger().setLevel('DEBUG')
from scripts.distributions import make_pdtype
from scripts.mpi_running_mean_std import RunningMeanStd
from scripts.mpi_moments import mpi_moments
# from baselines.common import explained_variance, fmt_row, zipsame
import numpy as np
from gym import spaces
# import scripts.utils as U
import scripts.utils as TF_U
from scripts.mpi_adam_optimizer import MpiAdamOptimizer
from mpi4py import MPI
from scripts import logger
import time
import cv2
comm = MPI.COMM_WORLD
from collections import deque

def lrelu(x, leak=0.2, name="lrelu"):
  return tf.maximum(x, leak*x)

class batch_norm(object):
    def __init__(self, epsilon=1e-5, momentum = 0.9, name="batch_norm"):
        with tf.variable_scope(name):
            self.epsilon  = epsilon
            self.momentum = momentum
            self.name = name

    def __call__(self, x, train=True):
        return tf.contrib.layers.batch_norm(x,
                      decay=self.momentum, 
                      updates_collections=None,
                      epsilon=self.epsilon,
                      scale=True,
                      is_training=train,
                      scope=self.name)

def get_vars(scope):
    return [x for x in tf.global_variables() if scope in x.name]

# def neglogp(self, x):
#     return 0.5 * tf.reduce_sum(tf.square((x - self.mean) / self.std), axis=-1) \
#             + 0.5 * np.log(2.0 * np.pi) * tf.to_float(tf.shape(x)[-1]) \
#             + tf.reduce_sum(self.logstd, axis=-1)

def log_normal_pdf(sample, mean, logvar, raxis=1):
    log2pi = tf.math.log(2. * np.pi)
    return tf.reduce_sum(
            -.5 * ((sample - mean) ** 2. * tf.exp(-logvar) + logvar + log2pi),
            axis=raxis)

def gaussian_nll(mu, log_sigma, x):
    return 0.5 * ((x - mu) / tf.exp(log_sigma)) ** 2 + log_sigma + 0.5 * np.log(2 * np.pi)
    
def reparameterize(mean, logvar):
    eps = tf.random.normal(shape=tf.shape(mean))
    return eps * tf.exp(logvar * .5) + mean

class Policy():
    def __init__(self, name, im_size, vae_im_size, ac_size, env, ac_space, latent_dim, sess, vis,  args, hid_size, normalize=True, const_std=False):
        self.name = name
        self.im_size = im_size
        self.vae_im_size = vae_im_size
        self.ac_size = ac_size
        self.hid_size = hid_size
        self.env = env
        self.sess = sess
        self.args = args
        self.pdtype = pdtype = make_pdtype(ac_space)
        self.vis = vis
        self.const_std = const_std
        self.latent_dim = latent_dim
        sequence_length = None

        self.Z_PH = TF_U.get_placeholder(name="z_" + name, dtype=tf.float32, shape=[None, self.latent_dim])
        self.IM_PH1 = TF_U.get_placeholder(name="im1_" + name, dtype=tf.float32, shape=[None] + im_size)
        self.IM_PH2 = TF_U.get_placeholder(name="im2_" + name, dtype=tf.float32, shape=[None] + im_size)
        self.RHO_PH1 = TF_U.get_placeholder(name="rho1_" + name, dtype=tf.float32, shape=[None, 3])
        self.RHO_PH2 = TF_U.get_placeholder(name="rho2_" + name, dtype=tf.float32, shape=[None, 3])
  
        self.quarter = int(self.vae_im_size[0]/4)
        if self.args.robot == "titan":
            self.min_val = 0
            self.max_val = 1
        else:
            # walking
            self.min_val = 0.25
            self.max_val = 2
                
        self.data_range = self.max_val - self.min_val
        self.first_layer = 16
        self.second_layer = 32
        self.third_layer = 64

        self.z_im, self.rho_im = self.encoder(self.IM_PH1)

        self.decoded_im_from_z = self.generator(tf.concat([self.Z_PH, self.RHO_PH1], axis=1))

        self.decoded_im_from_ph = self.generator(tf.concat([self.z_im, self.RHO_PH2], axis=1), reuse=True)

        self.decoded_im_from_im = self.generator(tf.concat([self.z_im, self.rho_im], axis=1), reuse=True)
            

        self.recon_rho = tf.reduce_sum(tf.square(self.RHO_PH1 - self.rho_im), axis=1)
        self.recon_im = tf.reduce_sum(tf.square(self.IM_PH2 - self.decoded_im_from_ph), axis=[1, 2, 3])
        self.recon_im_from_im = tf.reduce_sum(tf.square(self.IM_PH1 - self.decoded_im_from_im), axis=[1, 2, 3])


        self.stochastic = tf.placeholder(dtype=tf.bool, shape=())

        self.max_lam = 0.1

        self.state = tf.constant([])
        self.initial_state = None

        self.steps = 0
        self.eps_start = 0.5
        self.eps_end = 0.001
        self.eps = self.eps_start
        self.eps_decay = 50000

        self.lam_steps = 0
        self.lam_decay = 100000
        self.scaled_recon_loss = 0.0
        self.recon_loss = 0.0

        self.lam = self.args.lam
        self.recon_error_buffer = []
        self.q_error_buffer = []
        self.recon_thres = 0.0
        self.q_thres = 0.0
        self.latent_thres = 0.0

    def generator(self, im_inputs, reuse=False):
        with tf.variable_scope("decode") as scope:
            if reuse:
                scope.reuse_variables()
            last_out = tf.nn.relu(tf.layers.dense(im_inputs, int(self.vae_im_size[0]/4) * int(self.vae_im_size[1]/4) * self.second_layer, name="g_1"))
            last_out = tf.reshape(last_out, (-1, int(self.vae_im_size[0]/4), int(self.vae_im_size[1]/4), self.second_layer))
            last_out = tf.nn.relu(tf.layers.conv2d_transpose(last_out, self.second_layer, 3, strides=2, padding='same', name="g_2"))
            last_out = tf.nn.relu(tf.layers.conv2d_transpose(last_out, self.first_layer, 3, strides=2, padding='same', name="g_3"))
            decoded_logits = tf.layers.conv2d_transpose(last_out, 1, 3, padding="same", name="g_4")
            decoded_im = ((tf.nn.tanh(decoded_logits, name="g_final") + 1 )/ 2) * self.data_range + self.min_val
            
            return decoded_im

    def encoder(self, im_inputs, reuse=False):
        with tf.variable_scope("encoder") as scope:
            if reuse:
                scope.reuse_variables()
            x = tf.nn.relu(tf.layers.conv2d(im_inputs, self.first_layer, 3, strides=2, padding="same", name="e_vis1"))
            x = tf.nn.relu(tf.layers.conv2d(x, self.second_layer, 3, strides=2, padding="same", name="e_vis2"))
            # x = tf.nn.relu(tf.layers.conv2d(x, self.third_layer, 3, strides=2, padding="same", name="e_vis3"))
            x = tf.layers.flatten(x)
            z_im = tf.layers.dense(x, self.latent_dim, name="e_z_mean")
            rho = tf.layers.dense(x, 3, name="e_z_rho")
            return z_im, rho
    
    def initializer(self):
        return tf.keras.initializers.RandomNormal()

    def decode_from_z(self, z):
        # decoded = self.sess.run(self.decoded_from_z,feed_dict={self.z_ph:z})
        # decoded = self.sess.run(self.decoded2_from_z,feed_dict={self.z_ph:z})
        decoded_im = self.sess.run(self.decoded_im_from_z,feed_dict={self.Z_PH:z, self.RHO_PH1:np.zeros([z.shape[0], 3])})
        return decoded_im

    def encode(self, im):
        z_mean = self.sess.run(self.z_im, feed_dict={self.IM_PH1:im})
        # z_im = self.sess.run(self.z_im, feed_dict={self.ob:ob, self.im:im, self.vae_im:im})
        return z_mean
    
    def decode(self, im):
        decoded = self.sess.run(self.decoded_im_from_im, feed_dict={self.IM_PH1:im})
        # decoded = self.sess.run(self.decoded_im_from_im, feed_dict={self.ob:ob, self.im:im, self.vae_im:im})
        # decoded = self.sess.run(self.decoded2, feed_dict={self.im:im, self.vae_im:im})
        return decoded

    def update_eps(self):
        self.eps =  max(self.eps_start - (float(self.steps) / (self.eps_decay)), self.eps_end)
        self.steps += 1

    def step(self, ob, im, stochastic=False, eval_pol=False):

        if self.args.train_vae:
            im_copy = im.copy()
            vae_im = im[:self.vae_im_size[0],int((self.im_size[0]-self.vae_im_size[0])/2):int(self.im_size[0]-(self.im_size[0]-self.vae_im_size[0])/2),:]

            q, v, recon_im, self.err_pred, self.q_err, recon_loss = self.sess.run([self.qs, self.vpred, self.decoded, self.err, self.qs_error, self.recon], 
                                feed_dict={self.ob: ob.reshape([1,self.ob_size]), 
                                            self.im: im.reshape([1]+self.im_size), 
                                            self.vae_im: vae_im.reshape([1]+self.vae_im_size), 
                                            self.im: im.reshape([1]+self.im_size), 
                                            self.stochastic:stochastic})

            self.q_err = [self.q_err]
            self.q_loss = (self.q_err[0] - self.err_pred[0])**2             
            
            new_qs = q[0]
            im_copy[:self.vae_im_size[0],int((self.im_size[1]-self.vae_im_size[1])/2):int(self.im_size[1]-(self.im_size[1]-self.vae_im_size[1])/2),:] = recon_im[0,::]
            

            self.update_eps()
            if np.random.random() > self.eps or not stochastic: 
                if eval_pol:
                    act_from_q = np.argmax(new_qs)
                    if act_from_q == 1 and recon_loss < self.recon_thres and self.q_loss < self.q_thres:
                        act = 1
                    else:
                        act = 0
                else:
                    act = np.argmax(new_qs)
            else:
                act = np.random.randint(2)
            return [act], v, im_copy, self.err_pred[0], self.q_err[0], np.clip(new_qs,0.0, None), self.q_loss, recon_loss

    def update_err_thres(self):
        if len(self.recon_error_buffer) == 0:
            self.recon_error_buffer.append(self.recon_thres)
        if "max" in self.args.ae_type:
            # thres = max(self.recon_error_buffer)
            thres = np.percentile(self.recon_error_buffer, 99)
        else:
            thres = np.percentile(self.recon_error_buffer, 95)
        all_maxes = MPI.COMM_WORLD.allgather(thres) 
        self.recon_thres = max(all_maxes)
        self.recon_error_buffer = []
        
        if len(self.q_error_buffer) == 0:
            self.q_error_buffer.append(self.q_thres)
        if "max" in self.args.ae_type:
            # thres = max(self.q_error_buffer)
            thres = np.percentile(self.q_error_buffer, 99)
        else:
            thres = np.percentile(self.q_error_buffer, 95)
        all_maxes = MPI.COMM_WORLD.allgather(thres) 
        self.q_thres = max(all_maxes)
        self.q_error_buffer = []

class Model(Base):
    def __init__(self, name, env, ac_size, ob_size, im_size=[48,48,1], vae_im_size=[48,48,1], args=None, PATH=None, writer=None, hid_size=256, vis=False, normalize=True, ent_coef=0.0, vf_coef=0.5, max_grad_norm=0.5, mpi_rank_weight=1, max_timesteps=int(1e6), lr=3e-4, horizon=2048, batch_size=32, const_std=False):
        self.max_timesteps = max_timesteps
        self.horizon = horizon
        self.batch_size = batch_size
        self.learning_rate = lr
        self.env = env
        self.ob_size = ob_size
        self.im_size = im_size
        self.vae_im_size = vae_im_size
        self.ac_size = ac_size

        self.sess = sess = TF_U.get_session()
        self.name = name
        high = np.inf*np.ones(ac_size)
        low = -high
        ac_space = spaces.Box(low, high, dtype=np.float32)
        high = np.inf*np.ones(ob_size)
        low = -high
        ob_space = spaces.Box(low, high, dtype=np.float32)
        self.args = args   
        im_size = im_size
        self.PATH = PATH
        self.hid_size = hid_size
        self.writer = writer
        self.latent_dim = self.args.latent_dim
        self.args = args
        if ('vel' in name or 'hex' in name) and not args.vis:
            self.vis = False
        else:
            self.vis = vis
        with tf.variable_scope(name, reuse=tf.AUTO_REUSE):
            self.train_model = train_model = Policy(name, im_size, vae_im_size, ac_size, env, ac_space, self.latent_dim, sess, self.vis, args=args, normalize=normalize, hid_size=hid_size, const_std=const_std)
        Base.__init__(self)

        # CREATE THE PLACEHOLDERS
        self.A = A = train_model.pdtype.sample_placeholder([None])
        self.ADV = ADV = tf.placeholder(tf.float32, [None])
        self.R = R = tf.placeholder(tf.float32, [None])
        # Keep track of old critic
        self.OLDVPRED = OLDVPRED = tf.placeholder(tf.float32, [None])
        self.LR = LR = tf.placeholder(tf.float32, [])
        # self.ERR = ERR = tf.placeholder(tf.float32, [None], name="ERR")

        self.gen_mse_loss = tf.reduce_mean(train_model.recon_im)
        self.rho_mse_loss = tf.reduce_mean(train_model.recon_rho)

        all_variables = tf.trainable_variables()

        learning_rate = 0.001

        loss = self.gen_mse_loss + self.rho_mse_loss
        self.gen_stats_list = [self.gen_mse_loss, self.rho_mse_loss]
        gen_var = [var for var in all_variables if 'g_' in var.name] 
        gen_var += [var for var in all_variables if 'e_' in var.name] 
        self._gen_train_op = tf.train.AdamOptimizer(learning_rate).minimize(loss, var_list=gen_var)

        self.step = train_model.step

        self.decode_from_z = train_model.decode_from_z
        self.train_model = train_model
        self.init_buffer()

    def get_train_ops(self, loss, name, params=None, mpi_rank_weight=1, max_grad_norm=0.5): 
        if params is None:
            params = tf.trainable_variables(name)
        # print(name)
        # print(params)
        # 2. Build our trainer
        with tf.variable_scope(name, reuse=tf.AUTO_REUSE):
            self.trainer = MpiAdamOptimizer(comm, learning_rate=self.LR, mpi_rank_weight=mpi_rank_weight, epsilon=1e-5)
        # 3. Calculate the gradients
        with tf.variable_scope(name, reuse=tf.AUTO_REUSE):
            grads_and_var = self.trainer.compute_gradients(loss, params)
            grads, var = zip(*grads_and_var)
        grads, _grad_norm = tf.clip_by_global_norm(grads, max_grad_norm)
        
        grads_and_var = list(zip(grads, var))
        # zip aggregate each gradient with parameters associated
        # For instance zip(ABCD, xyza) => Ax, By, Cz, Da

        self.grads = grads
        self.var = var
        with tf.variable_scope(name, reuse=tf.AUTO_REUSE):
            train_op = self.trainer.apply_gradients(grads_and_var)
        return train_op

    def get_pred(self,im, labels):
        pred = self.sess.run(self.train_model.classifier, feed_dict={self.train_model.vae_im:im, self.train_model.im:im})
        # print(np.argmax(pred,axis=1))
        return np.argmax(pred,axis=1), np.mean(1.0 - np.abs(labels - np.argmax(pred,axis=1)))

    def get_mse(self, im):
        
        if self.args.mse:
            # mse = self.sess.run(self.train_model.recon_im_from_im, feed_dict={self.train_model.IM_PH1:im})[:,0]
            mse = self.sess.run(self.train_model.recon_im_from_im, feed_dict={self.train_model.IM_PH1:im})
            # print(mse.shape); exit()
        else:
            mse = []
            for i in im:
                mse.append(self.sess.run(self.ssim_loss, feed_dict={self.train_model.vae_im:i.reshape([-1] + self.vae_im_size)})[0]*-1)
            mse = np.array(mse)
        return -mse

    def train_ae(self, lr, img1, img2, rho1, rho2):
        # self.train_model.ob_rms.update(ob)
        
        td_map = {
        self.LR : lr,
        self.train_model.IM_PH1:img1,
        self.train_model.IM_PH2:img2,
        self.train_model.RHO_PH1:rho1,
        self.train_model.RHO_PH2:rho2
        }
        gen_stats = self.sess.run(self.gen_stats_list + [self._gen_train_op],td_map)[:-1]
        return gen_stats + [0.0,0.0]

    def run_train(self, data, last_value, last_done):
        self.finalise_buffer(data, last_value, last_done)
        self.train_model.ob_rms.update(self.data['ob'])
        if self.args.const_lr:
            self.cur_lrmult =  1.0
        else:
            self.cur_lrmult =  max(1.0 - float(self.timesteps_so_far) / self.max_timesteps, 0)
       
        lrnow = self.learning_rate*self.cur_lrmult
        self.lr = lrnow
        cliprangenow = 0.2
        self.n = self.data['done'].shape[0]
        inds = np.arange(self.n)
        # print(self.epochs)
        t1 = time.time()
        if "decoded" not in self.args.tests:
            if "pre" in self.args.tests:
                if self.iters_so_far < 10:    
                    vae_epochs = 10
                    # vae_epochs = 5
                    # vae_epochs = 1
                else:
                    vae_epochs = 2
            else:
                vae_epochs = 2
                # vae_epochs = 1
            # vae_epochs = 0
            vae_loss = [0,0,0]
            if self.args.train_vae:
                if "roi" in self.args.tests:
                    vae_images = self.data["im"][:,:self.vae_im_size[0],int((self.im_size[0]-self.vae_im_size[1])/2):int(self.im_size[1]-(self.im_size[1]-self.vae_im_size[1])/2),:]
                else:
                    vae_images = self.data["im"]
                if "pos" in self.args.tests:
                    if self.iters_so_far == 0:
                        self.vae_images = vae_images
                        self.vae_images_idx = 0
                    else:
                        image_idx = np.where(self.data["ac"] == 1)[0]
                        if len(image_idx) + self.vae_images_idx >= self.vae_images.shape[0]:
                            initial = self.vae_images.shape[0] - self.vae_images_idx
                            final =  len(image_idx) + self.vae_images_idx - self.vae_images.shape[0]
                            self.vae_images[self.vae_images_idx:,::] = vae_images[image_idx[:initial]]
                            self.vae_images[:final,::] = vae_images[image_idx[initial:]]
                            self.vae_images_idx = final
                        else:
                            self.vae_images[self.vae_images_idx:len(image_idx)+self.vae_images_idx,::] = vae_images[image_idx]
                            self.vae_images_idx += len(image_idx) 
                else:
                    self.vae_images = vae_images

                vae_inds = [i for i in range(self.vae_images.shape[0])]
                for epoch in range(vae_epochs):
                    if self.enable_shuffle:
                        np.random.shuffle(vae_inds)
                    self.loss = [] 
                    for start in range(0, self.n, self.batch_size):
                        end = start + self.batch_size
                        mbinds = vae_inds[start:end]
                        vae_loss = self.train_ae(epoch, lrnow, self.vae_images[mbinds])               
        else:
            self.epochs = 5                     
        # if self.rank == 0:print("ae train", time.time() - t1)
        t1 = time.time()
        for epoch in range(self.epochs):
            if self.enable_shuffle:
                np.random.shuffle(inds)
            for start in range(0, self.n, self.batch_size):
                end = start + self.batch_size
                mbinds = inds[start:end]
                slices = (self.data[key][mbinds] for key in self.training_input)       
                outs = self.train(epoch, lrnow, *slices)
                self.loss = outs[:len(self.loss_names)]
        # if self.rank == 0: print("q train", time.time() - t1)

        if "decoded" not in self.args.tests:
            self.loss = self.loss + vae_loss
        self.log_stuff(things={"Eps":self.train_model.eps, "recon_thres": self.train_model.recon_thres, "q_thres": self.train_model.q_thres})
        self.evaluate(data['ep_rets'], data['ep_lens'])

        self.init_buffer()

    def get_advantage(self, last_value, last_done):    
        gamma = 0.99; lam = 0.95
        advs = np.zeros_like(self.data['rew'])
        lastgaelam = 0
        for t in reversed(range(len(self.data['rew']))):
            if t == len(self.data['rew']) - 1:
                nextnonterminal = 1.0 - last_done
                nextvalues = last_value
            else:
                nextnonterminal = 1.0 - self.data['done'][t+1]
                nextvalues = self.data['value'][t+1]
            delta = self.data['rew'][t] + gamma * nextvalues * nextnonterminal - self.data['value'][t]
            advs[t] = lastgaelam = delta + gamma * lam * nextnonterminal * lastgaelam
        self.data['return'] = advs + self.data['value']
    
    def init_buffer(self):
        self.data_input = ['ob', 'im', 'ac', 'rew', 'done', 'value']
        self.training_input = ['ob', 'im', 'return', 'done', 'ac', 'value']
        self.data = {t:[] for t in self.data_input}

    def log_stuff(self, things):
        if self.rank == 0:
            for thing in things:
                self.writer.add_scalar(thing, things[thing], self.iters_so_far)
                logger.record_tabular(thing, things[thing])
        
    def add_to_buffer(self, data):
        ''' data needs to be a list of lists, same length as self.data'''
        for d,key in zip(data, self.data):
            if key == "im":
                im = d[:self.im_size[0],int((d.shape[0]-self.im_size[0])/2):int(d.shape[0]-(d.shape[0]-self.im_size[0])/2),:]
                # print("adding to buff", d.shape, im.shape)
                self.data[key].append(im)
            else:
                self.data[key].append(d)

    def finalise_buffer(self, data, last_value, last_done):
        ''' data must be dict'''
        for key in self.data_input:
            if key == 'done':
                self.data[key] = np.asarray(self.data[key], dtype=np.bool)
            else:
                self.data[key] = np.asarray(self.data[key])
        self.n = next(iter(self.data.values())).shape[0]
        for key in data:
            self.data[key] = data[key]
        self.get_advantage(last_value, last_done)
        # if self.rank == 0:
        # self.log_stuff()
