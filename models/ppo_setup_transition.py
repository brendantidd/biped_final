from models.base_setup import Base
import os   
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' 
import tensorflow as tf
tf.get_logger().setLevel('DEBUG')
from baselines.common.distributions import make_pdtype
from baselines.common.mpi_running_mean_std import RunningMeanStd
from baselines.common.distributions import CategoricalPdType
from baselines.common.mpi_moments import mpi_moments
from baselines.common import explained_variance, fmt_row, zipsame
import numpy as np
from gym import spaces
import scripts.utils as U
from scripts.mpi_adam_optimizer import MpiAdamOptimizer
from mpi4py import MPI
from baselines import logger
import time

def get_vars(scope):
  return [x for x in tf.global_variables() if scope in x.name]

class Policy():
  def __init__(self, ob, im, ob_space, ob_size, im_size, ac_space, sess, vis,  args, hid_size, normalize=True, const_std=False):
    # with tf.variable_scope(name, reuse=tf.AUTO_REUSE):
    #   self.model(name, ob, im, ob_space, im_size, ac_space, vis, hid_size)
    #   self.scope = tf.get_variable_scope().name

  # def model(self, name, ob, im, ob_space, im_size, ac_space, vis, hid_size):
    self.ob = ob
    self.im = im
    self.ob_size = ob_size
    self.im_size = im_size
    self.sess = sess
    self.args = args
    self.pdtype = pdtype = make_pdtype(ac_space)
    self.vis = vis
    self.const_std = const_std
    sequence_length = None
    if normalize:
      with tf.variable_scope("obfilter"):
        self.ob_rms = RunningMeanStd(shape=ob_space.shape)
    if self.vis:
      x = tf.nn.relu(U.conv2d(im, 16, "vis_l1", [8, 8], [4, 4], pad="VALID"))
      # print("------------")
      # print("convolutional shape for policy", x.shape)
      # print("------------")

      x = tf.nn.relu(U.conv2d(x, 32, "vis_l2", [4, 4], [2, 2], pad="VALID"))
      x = U.flattenallbut0(x)
      # x = tf.nn.tanh(tf.layers.dense(x, 64, name='vis_lin', kernel_initializer=U.normc_initializer(0.01)))
      x = tf.nn.tanh(tf.layers.dense(x, 64, name='vis_lin', kernel_initializer=U.normc_initializer(1.0)))
      
      # x = tf.nn.relu(U.conv2d(im, 16, "vis_l1", [8, 8], [4, 4], pad="VALID"))
      # x = tf.nn.relu(U.conv2d(x, 16, "vis_l2", [4, 4], [2, 2], pad="VALID"))

      # x = tf.nn.relu(U.conv2d(im, 8, "vis_l1", [4, 4], [2, 2], pad="VALID"))
      # x = tf.nn.relu(U.conv2d(x, 8, "vis_l2", [4, 4], [2, 2], pad="VALID"))
      
      # x = tf.nn.relu(U.conv2d(im, 16, "vis_l1", [4, 4], [2, 2], pad="VALID"))
      # x = tf.nn.relu(U.conv2d(x, 32, "vis_l2", [4, 4], [2, 2], pad="VALID"))

      # x = tf.nn.relu(U.conv2d(im, 16, "vis_l1", [8, 8], [4, 4], pad="VALID"))
      # x = tf.nn.relu(U.conv2d(x, 32, "vis_l1", [4, 4], [2, 2], pad="VALID"))

      # x = U.flattenallbut0(x)
      # x = tf.nn.tanh(tf.layers.dense(x, 32, name='vis_lin', kernel_initializer=U.normc_initializer(0.01)))
    else:
      x = tf.nn.tanh(tf.layers.dense(im, 32, name="vis_fc0", kernel_initializer=U.normc_initializer(1.0)))
      x = tf.nn.tanh(tf.layers.dense(x, 32, name="vis_fc1", kernel_initializer=U.normc_initializer(1.0)))

      # print("-----------------------------------------------------------------")
      # print()
      # print("there is a bug that needs fixing next run")
      # print()
      # print("-----------------------------------------------------------------")

      # x = tf.nn.tanh(tf.layers.dense(x, hid_size, name="vis_fc1", kernel_initializer=U.normc_initializer(1.0)))
      # x = tf.nn.tanh(tf.layers.dense(x, hid_size, name="vis_fc2", kernel_initializer=U.normc_initializer(1.0)))

    if normalize:
      obz = tf.clip_by_value((ob - tf.stop_gradient(self.ob_rms.mean)) / tf.stop_gradient(self.ob_rms.std), -5.0, 5.0)
    else:
      obz = ob
    with tf.variable_scope('vf'):
      last_out = obz
      # if self.vis:
      #   x = tf.nn.relu(U.conv2d(im, 16, "vis_l1", [8, 8], [4, 4], pad="VALID"))
      #   x = tf.nn.relu(U.conv2d(x, 32, "vis_l2", [4, 4], [2, 2], pad="VALID"))
      #   x = U.flattenallbut0(x)
      #   x = tf.nn.tanh(tf.layers.dense(x, 64, name='vis_lin', kernel_initializer=U.normc_initializer(0.01)))
      # else:
      #   x = tf.nn.tanh(tf.layers.dense(im, hid_size, name="fc0", kernel_initializer=U.normc_initializer(1.0)))
      # print("ppo", last_out.shape, x.shape)
      last_out = tf.concat(axis=1,values=[last_out, x])

      if self.args.dual_value:
        last_out1 = tf.nn.tanh(tf.layers.dense(last_out, hid_size, name="fc1_1", kernel_initializer=U.normc_initializer(1.0)))
        last_out1 = tf.nn.tanh(tf.layers.dense(last_out1, hid_size, name="fc2_1", kernel_initializer=U.normc_initializer(1.0)))
        self.vpred1 = tf.layers.dense(last_out1, 1, name='final_1', kernel_initializer=U.normc_initializer(1.0))[:,0]
        last_out2 = tf.nn.tanh(tf.layers.dense(last_out, hid_size, name="fc1_2", kernel_initializer=U.normc_initializer(1.0)))
        last_out2 = tf.nn.tanh(tf.layers.dense(last_out2, hid_size, name="fc2_2", kernel_initializer=U.normc_initializer(1.0)))
        self.vpred2 = tf.layers.dense(last_out2, 1, name='final_2', kernel_initializer=U.normc_initializer(1.0))[:,0]
        self.vpred = tf.minimum(self.vpred1, self.vpred2)
      else:
        last_out = tf.nn.tanh(tf.layers.dense(last_out, hid_size, name="fc1", kernel_initializer=U.normc_initializer(1.0)))
        last_out = tf.nn.tanh(tf.layers.dense(last_out, hid_size, name="fc2", kernel_initializer=U.normc_initializer(1.0)))
        # if self.args.same_value:
        #   self.vpreds = tf.layers.dense(last_out, 2, name='final1', kernel_initializer=U.normc_initializer(1.0))
        #   self.vpred = tf.reduce_max(self.vpreds)
        # else:
        self.vpred = tf.layers.dense(last_out, 1, name='final', kernel_initializer=U.normc_initializer(1.0))[:,0]
          # self.vpreds = tf.layers.dense(last_out, 2, name='final2', kernel_initializer=U.normc_initializer(1.0))

        # print(self.vpred.shape)
        # exit()

    with tf.variable_scope('pol'):
      last_out = obz
      # if self.vis:
      #   x = tf.nn.relu(U.conv2d(im, 16, "vis_l1", [8, 8], [4, 4], pad="VALID"))
      #   x = tf.nn.relu(U.conv2d(x, 32, "vis_l2", [4, 4], [2, 2], pad="VALID"))
      #   x = U.flattenallbut0(x)
      #   x = tf.nn.tanh(tf.layers.dense(x, 64, name='vis_lin', kernel_initializer=U.normc_initializer(0.01)))
      # else:
      #   x = tf.nn.tanh(tf.layers.dense(im, hid_size, name="fc0", kernel_initializer=U.normc_initializer(1.0)))

      last_out = tf.concat(axis=1,values=[last_out, x])
      last_out = tf.nn.tanh(tf.layers.dense(last_out, hid_size, name='fc1', kernel_initializer=U.normc_initializer(1.0)))
      last_out_pol = tf.nn.tanh(tf.layers.dense(last_out, hid_size, name='fc2', kernel_initializer=U.normc_initializer(1.0)))

      self.mean = tf.layers.dense(last_out_pol, pdtype.param_shape()[0]//2, name='final', kernel_initializer=U.normc_initializer(0.01))
      # self.mean = tf.clip_by_value(tf.layers.dense(last_out, pdtype.param_shape()[0]//2, name='final', kernel_initializer=U.normc_initializer(0.01)),-50,50)
      # self.mean = tf.nn.tanh(tf.layers.dense(last_out, pdtype.param_shape()[0]//2, name='final', kernel_initializer=U.normc_initializer(0.01)))
      # logstd = tf.get_variable(name="logstd", shape=[1, pdtype.param_shape()[0]//2], initializer=tf.zeros_initializer())
      # logstd = tf.clip_by_value(tf.get_variable(name="logstd", shape=[1, pdtype.param_shape()[0]//2], initializer=tf.zeros_initializer()), -5.0, 0.0)
      # init = tf.constant_initializer([np.log(0.7)]*int(pdtype.param_shape()[0]//2))
      if self.const_std:
        # if self.args.obstacle_type == 'high_jumps':
        #   logstd = tf.constant([np.log(1.0)]*int(pdtype.param_shape()[0]//2))
        # else:
        logstd = tf.constant([np.log(0.75)]*int(pdtype.param_shape()[0]//2))
      else:      
        init = tf.constant_initializer([np.log(1.0)]*int(pdtype.param_shape()[0]//2))
        logstd = tf.get_variable(name="logstd", shape=[1, pdtype.param_shape()[0]//2],initializer=init)
      # logstd = tf.layers.dense(last_out, pdtype.param_shape()[0]//2, name='final', kernel_initializer=init)
      # if self.args.std_clip:
      #   logstd = tf.clip_by_value(logstd, np.log(0.5), 0.0)

    pdparam = tf.concat([self.mean, self.mean * 0.0 + logstd], axis=1)
    self.pd = pdtype.pdfromflat(pdparam)

    # if self.args.train_select:
    with tf.variable_scope('select'):
      select_hid_size = hid_size
      if not self.args.share_vis:
        x = tf.nn.relu(U.conv2d(im, 16, "vis_l1", [8, 8], [4, 4], pad="VALID"))
        x = tf.nn.relu(U.conv2d(x, 32, "vis_l2", [4, 4], [2, 2], pad="VALID"))
        x = U.flattenallbut0(x)
        x = tf.nn.tanh(tf.layers.dense(x, 64, name='vis_lin', kernel_initializer=U.normc_initializer(1.0)))
        last_out = tf.concat(axis=1,values=[obz, x])
        last_out = tf.nn.tanh(tf.layers.dense(last_out, select_hid_size, name='fc1', kernel_initializer=U.normc_initializer(1.0)))
        last_out_select = tf.nn.tanh(tf.layers.dense(last_out, select_hid_size, name='fc2', kernel_initializer=U.normc_initializer(1.0)))
      else:
        last_out_select = tf.nn.tanh(tf.layers.dense(last_out, select_hid_size, name='fc2', kernel_initializer=U.normc_initializer(1.0)))
      # self.selects = tf.layers.dense(last_out, 3, name="select_final", kernel_initializer=U.normc_initializer(1.0))
      
      # self.term_pred = tf.layers.dense(last_out, 2, name="term_final", kernel_initializer=U.normc_initializer(0.01))
      # self.term_pdtype = term_pdtype = CategoricalPdType(2)
      # self.term_pd = term_pdtype.pdfromflat(self.term_pred)
      
      
      if self.args.categorical:
        # last_out = tf.nn.tanh(tf.layers.dense(last_out, hid_size, name='fc', kernel_initializer=U.normc_initializer(1.0)))
        self.select_pred = tf.layers.dense(last_out_select, 2, name="select_final", kernel_initializer=U.normc_initializer(0.01))
        self.select_pdtype = select_pdtype = CategoricalPdType(2)
        self.select_pd = select_pdtype.pdfromflat(self.select_pred)
      else:
        high = np.inf*np.ones(1)
        low = -high
        select_ac_space = spaces.Box(low, high, dtype=np.float32)
        self.select_pdtype = select_pdtype = make_pdtype(select_ac_space)
        # last_out = tf.nn.tanh(tf.layers.dense(tf.stop_gradient(last_out), hid_size, name='fc', kernel_initializer=U.normc_initializer(1.0)))
        self.select_mean = tf.layers.dense(last_out_select, 1, name="select_final", kernel_initializer=U.normc_initializer(0.01))
        # self.select_pdtype = select_pdtype = CategoricalPdType(2)
        
        select_init = tf.constant_initializer([np.log(1.0)]*int(select_pdtype.param_shape()[0]//2))
        select_logstd = tf.get_variable(name="select_logstd", shape=[1, select_pdtype.param_shape()[0]//2],initializer=select_init)
        select_pdparam = tf.concat([self.select_mean, self.select_mean * 0.0 + select_logstd], axis=1)
        self.select_pd = pdtype.pdfromflat(select_pdparam)

      stochastic_act = self.select_pd.sample()
      determinstic_act = self.select_pd.mode()
  

    self.state_in = []
    self.state_out = []
    self.stochastic = tf.placeholder(dtype=tf.bool, shape=())

    # if self.args.train_select:
    # self.select = tf.nn.sigmoid(U.switch(self.stochastic, stochastic_act, determinstic_act))
    self.select = U.switch(self.stochastic, stochastic_act, determinstic_act)
    # self.select = tf.placeholder(tf.int32, [None])
    self.neglogp_select = self.select_pd.neglogp(self.select)
    if self.args.categorical:
      self.NLOGP_PH = tf.placeholder(tf.int32, [None])
      self.neglogp_select2 = self.select_pd.neglogp(self.NLOGP_PH)
    else:
      self.NLOGP_SELECT_PH = self.select_pdtype.sample_placeholder([None], name="neglogp_select")    
      self.neglogp_select2 = self.select_pd.neglogp(self.NLOGP_SELECT_PH)
      self.NLOGP_PH = self.pdtype.sample_placeholder([None])
      self.neglogp2 = self.pd.neglogp(self.NLOGP_PH)

    self.action = U.switch(self.stochastic, self.pd.sample(), self.pd.mode())
    # self.action = self.pd.sample()
    self.neglogp = self.pd.neglogp(self.action)
    self.state = tf.constant([])
    self.initial_state = None
    
    if self.args.all_setup:
      self.action_ph = self.pdtype.sample_placeholder([None])
      self.neglogpac = self.pd.neglogp(self.action_ph)

    with tf.variable_scope('prox'):
      x = tf.nn.relu(U.conv2d(im, 16, "vis_l1", [8, 8], [4, 4], pad="VALID"))
      x = tf.nn.relu(U.conv2d(x, 32, "vis_l2", [4, 4], [2, 2], pad="VALID"))
      x = U.flattenallbut0(x)
      x = tf.nn.tanh(tf.layers.dense(x, 64, name='vis_lin', kernel_initializer=U.normc_initializer(1.0)))
      last_out = tf.concat(axis=1,values=[obz, x])
      last_out = tf.nn.tanh(tf.layers.dense(last_out, hid_size, name="fc1", kernel_initializer=U.normc_initializer(1.0)))
      last_out = tf.nn.tanh(tf.layers.dense(last_out, hid_size, name="fc2", kernel_initializer=U.normc_initializer(1.0)))
      self.prox = tf.layers.dense(last_out, 1, name='final', kernel_initializer=U.normc_initializer(1.0))[:,0]

  def get_nlogp(self, ob, im, action):
    neglogp = self.sess.run(self.neglogpac, feed_dict={self.ob: ob.reshape([1,self.ob_size]), self.im: im.reshape([1]+self.im_size), self.action_ph: action.reshape([-1,12])})[0]
    # print(neglogp)
    return neglogp

  def _evaluate(self, variables, observation, stochastic, im=None, **extra_feed):
    sess = self.sess
    # if self.vis:
    feed_dict = {self.ob: U.adjust_shape(self.ob, observation),self.im: im, self.stochastic:stochastic}
    # else:
    #   feed_dict = {self.ob: U.adjust_shape(self.ob, observation), self.stochastic:stochastic}
    for inpt_name, data in extra_feed.items():
      if inpt_name in self.__dict__.keys():
        inpt = self.__dict__[inpt_name]
        if isinstance(inpt, tf.Tensor) and inpt._op.type == 'Placeholder':
          feed_dict[inpt] = U.adjust_shape(inpt, data)
    return sess.run(variables, feed_dict)

  def step(self, ob, im=None, stochastic=False):

    state = None   
    
    if self.args.train_select:
      a, v, state, neglogp, select_raw, neglogp_select = self.sess.run([self.action, self.vpred, self.state, self.neglogp, self.select, self.neglogp_select], feed_dict={self.ob: ob.reshape([1,self.ob_size]), self.im: im.reshape([1]+self.im_size), self.stochastic:stochastic})
    else:
      a, v, state, neglogp = self.sess.run([self.action, self.vpred, self.state, self.neglogp], feed_dict={self.ob: ob.reshape([1,self.ob_size]), self.im: im.reshape([1]+self.im_size), self.stochastic:stochastic})
      select_raw = [[0]]
      neglogp_select = [[0]]
    # print(select_raw, neglogp_select)
    if self.args.categorical:
      return a[0], v[0], state, neglogp[0], select_raw[0], neglogp_select[0]
    else:
      return a[0], v[0], state, neglogp[0], select_raw[0], neglogp_select[0]

  def value(self, ob, *args, **kwargs):
    return self._evaluate(self.vpred, ob, *args, **kwargs)

  def get_value(self, ob, im):
    return self.sess.run(self.vpred, feed_dict={self.ob: ob.reshape([1,self.ob_size]), self.im: im.reshape([1]+self.im_size)})[0]
  
  def get_prox(self, ob, im):
    return self.sess.run(self.prox, feed_dict={self.ob: ob.reshape([1,self.ob_size]), self.im: im.reshape([1]+self.im_size)})[0]
  
  def get_vpreds(self, ob, im):
    vpreds = self.sess.run(self.vpreds, feed_dict={self.ob: ob.reshape([1,self.ob_size]), self.im: im.reshape([1]+self.im_size)})[0]
    # print(vpreds)
    return vpreds
  
  def get_select_vpreds(self, ob, im):
    vpreds = self.sess.run(self.selects, feed_dict={self.ob: ob.reshape([1,self.ob_size]), self.im: im.reshape([1]+self.im_size)})[0]
    # print(vpreds)
    return vpreds
  
  def get_vpred_and_nlogps(self, ob, im, ac, action):
    vpreds, neglogp, neglogp_select = self.sess.run([self.vpred, self.neglogp2, self.neglogp_select2], feed_dict={self.ob: ob.reshape([1,self.ob_size]), self.im: im.reshape([1]+self.im_size), self.NLOGP_PH:ac.reshape([1,12]), self.NLOGP_SELECT_PH:action.reshape([1,1]) })
    # print(vpreds, neglogp, neglogp_select)
    return vpreds[0], neglogp[0], neglogp_select[0]
  
  def get_neglogp_select(self, ob, im, select):
    neglogp = self.sess.run(self.neglogp_select2, feed_dict={self.ob: ob.reshape([1,self.ob_size]), self.im: im.reshape([1]+self.im_size), self.NLOGP_PH:select.reshape([1,])})
    # print("neglogp", neglogp)
    return neglogp[0]
  
  def get_term(self, ob, im, stochastic):
    # print("get_term", ob.shape, im.shape)
    # term, neglogp = self.sess.run([self.term, self.neglogp_term], feed_dict={self.ob: ob.reshape([1,self.ob_size]), self.im: im.reshape([1]+self.im_size), self.stochastic:stochastic})
    term = self.sess.run(self.term, feed_dict={self.ob: ob.reshape([1,self.ob_size]), self.im: im.reshape([1]+self.im_size), self.stochastic:stochastic})
    # vpreds = self.sess.run(self.term, feed_dict={self.ob: ob.reshape([1,self.ob_size]), self.im: im.reshape([1]+self.im_size)})[0]
    # print(term, neglogp)
    # print(term)
    return term[0]

class Model(Base):
  def __init__(self, name, env, ac_size, ob_size, im_size=[48,48,4], args=None, PATH=None, writer=None, hid_size=256, vis=False, normalize=True, ent_coef=0.0, vf_coef=0.5, max_grad_norm=0.5, mpi_rank_weight=1, max_timesteps=int(1e6), lr=3e-4, horizon=2048, batch_size=32, const_std=False):
    self.max_timesteps = max_timesteps
    self.horizon = horizon
    self.batch_size = batch_size
    self.learning_rate = lr
    self.env = env
    self.ac_size = ac_size
    self.ob_size = ob_size
    self.im_size = im_size
    self.sess = sess = U.get_session()
    comm = MPI.COMM_WORLD
    self.name = name
    high = np.inf*np.ones(ac_size)
    low = -high
    ac_space = spaces.Box(low, high, dtype=np.float32)
    high = np.inf*np.ones(ob_size)
    low = -high
    ob_space = spaces.Box(low, high, dtype=np.float32)
    self.args = args   
    im_size = im_size
    self.PATH = PATH
    self.hid_size = hid_size
    self.writer = writer
    ob = U.get_placeholder(name="sp_ob" + name, dtype=tf.float32, shape=[None] + list(ob_space.shape))
    im = U.get_placeholder(name="sp_im" + name, dtype=tf.float32, shape=[None] + im_size)
    self.args = args
    if ('vel' in name or 'hex' in name) and not args.vis:
      self.vis = False
    else:
      self.vis = vis
    with tf.variable_scope(name, reuse=tf.AUTO_REUSE):
      # May need some more params when using recurrent
      # act_model = Policy(ob, im, ob_space, im_size, ac_space, sess, self.vis, normalize=normalize, hid_size=hid_size)
      train_model = Policy(ob, im, ob_space, ob_size, im_size, ac_space, sess, self.vis, args=args, normalize=normalize, hid_size=hid_size, const_std=const_std)
    Base.__init__(self)

    # CREATE THE PLACEHOLDERS
    self.A = A = train_model.pdtype.sample_placeholder([None])
    self.ADV = ADV = tf.placeholder(tf.float32, [None])
    self.R = R = tf.placeholder(tf.float32, [None])
    self.PROX = PROX = tf.placeholder(tf.float32, [None])
    # Keep track of old actor
    self.OLDNEGLOGPAC = OLDNEGLOGPAC = tf.placeholder(tf.float32, [None])
    # self.OLDNEGLOGPAC_TERM = OLDNEGLOGPAC_TERM = tf.placeholder(tf.float32, [None])
    # Keep track of old critic
    self.OLDVPRED = OLDVPRED = tf.placeholder(tf.float32, [None])
    self.LR = LR = tf.placeholder(tf.float32, [])
    # self.VF_LR = VF_LR = tf.placeholder(tf.float32, [])
    # Cliprange
    self.CLIPRANGE = CLIPRANGE = tf.placeholder(tf.float32, [])

    neglogpac = train_model.pd.neglogp(A)


    # Calculate the entropy
    # Entropy is used to improve exploration by limiting the premature convergence to suboptimal policy.
    entropy = tf.reduce_mean(train_model.pd.entropy())

    # Get the predicted value
    # if self.args.dual_value:
    #   vf_loss1 = 0.5*tf.reduce_mean(tf.square(train_model.vpred1 - R))
    #   vf_loss2 = 0.5*tf.reduce_mean(tf.square(train_model.vpred2 - R))
    #   vf_loss = vf_loss1 + vf_loss2
    # else:
    
  
    # one_hot = tf.one_hot(self.ACTION, 2)
    # current_q_t = tf.reduce_sum(train_model.vpreds * one_hot, 1)
    # vf_loss = 0.5*tf.reduce_mean(tf.square(current_q_t - self.R))

    # if not self.args.same_value:
    # Original paper minimises the expectation 
    # (Success - v)**2 + failure**2
    self.prox_loss = 0.5*tf.reduce_mean(tf.square(train_model.prox - PROX ))

    vf_loss = 0.5*tf.reduce_mean(tf.square(train_model.vpred - R))

    # Calculate ratio (pi current policy / pi old policy)
    ratio = tf.exp(OLDNEGLOGPAC - neglogpac)
    # Defining Loss = - J is equivalent to max J
    pg_losses = -ADV * ratio
    pg_losses2 = -ADV * tf.clip_by_value(ratio, 1.0 - CLIPRANGE, 1.0 + CLIPRANGE)
    pg_loss_pol = tf.reduce_mean(tf.maximum(pg_losses, pg_losses2))
    pg_loss = pg_loss_pol

    if self.args.train_select:
      self.OLDNEGLOGPAC_SELECT = OLDNEGLOGPAC_SELECT = tf.placeholder(tf.float32, [None])
      if args.categorical:
        self.ACTION = tf.placeholder(tf.int32, [None])
      else:
        self.ACTION = train_model.select_pdtype.sample_placeholder([None])
      neglogpac_select = train_model.select_pd.neglogp(self.ACTION)

      ratio_select = tf.exp(OLDNEGLOGPAC_SELECT - neglogpac_select)
      # Defining Loss = - J is equivalent to max J
      pg_losses_select = -ADV * ratio_select
      pg_losses2_select = -ADV * tf.clip_by_value(ratio_select, 1.0 - CLIPRANGE, 1.0 + CLIPRANGE)
      pg_loss_pol_select = tf.reduce_mean(tf.maximum(pg_losses_select, pg_losses2_select))

      pg_loss += pg_loss_pol_select

    approxkl = .5 * tf.reduce_mean(tf.square(neglogpac - OLDNEGLOGPAC))
    clipfrac = tf.reduce_mean(tf.to_float(tf.greater(tf.abs(ratio - 1.0), CLIPRANGE)))

    loss = pg_loss - entropy * ent_coef + vf_loss
    # vf_loss = vf_loss
    # UPDATE THE PARAMETERS USING LOSS
    
    # Prox only train op
    prox_params = tf.trainable_variables(name+"/prox/")
    with tf.variable_scope(name + "_prox", reuse=tf.AUTO_REUSE):
      # self.trainer = MpiAdamOptimizer(comm, learning_rate=prox_LR, mpi_rank_weight=mpi_rank_weight, epsilon=1e-5)
      self.trainer = MpiAdamOptimizer(comm, learning_rate=LR, mpi_rank_weight=mpi_rank_weight, epsilon=1e-5)
    with tf.variable_scope(name + "_prox", reuse=tf.AUTO_REUSE):
      prox_grads_and_var = self.trainer.compute_gradients(self.prox_loss, prox_params)
    prox_grads, prox_var = zip(*prox_grads_and_var)
    prox_grads, prox_grad_norm = tf.clip_by_global_norm(prox_grads, max_grad_norm)
    prox_grads_and_var = list(zip(prox_grads, prox_var))
    self.prox_grads = prox_grads
    self.prox_var = prox_var
    with tf.variable_scope(name + "_prox", reuse=tf.AUTO_REUSE):
      self._prox_train_op = self.trainer.apply_gradients(prox_grads_and_var)

    params = tf.trainable_variables(name)
    # 2. Build our trainer
    with tf.variable_scope(name, reuse=tf.AUTO_REUSE):
      self.trainer = MpiAdamOptimizer(comm, learning_rate=LR, mpi_rank_weight=mpi_rank_weight, epsilon=1e-5)

    # 3. Calculate the gradients
    with tf.variable_scope(name, reuse=tf.AUTO_REUSE):
      grads_and_var = self.trainer.compute_gradients(loss, params)
    grads, var = zip(*grads_and_var)
    
    grads, _grad_norm = tf.clip_by_global_norm(grads, max_grad_norm)
      
    grads_and_var = list(zip(grads, var))
    # zip aggregate each gradient with parameters associated
    # For instance zip(ABCD, xyza) => Ax, By, Cz, Da

    self.grads = grads
    self.var = var
    with tf.variable_scope(name, reuse=tf.AUTO_REUSE):
      self._train_op = self.trainer.apply_gradients(grads_and_var)
    
    std = tf.reduce_mean(train_model.pd.std)
    mean_ratio = tf.reduce_mean(ratio)
    mean_adv = tf.reduce_mean(self.ADV)

    if args.train_select:
      mean_select_ratio = tf.reduce_mean(ratio_select)
      select_std = tf.reduce_mean(train_model.select_pd.std)
      old_select_neglp = tf.reduce_mean(self.OLDNEGLOGPAC_SELECT)
      select_neglp = tf.reduce_mean(neglogpac_select)
      old_pol_neglp = tf.reduce_mean(self.OLDNEGLOGPAC)
      pol_neglp = tf.reduce_mean(neglogpac)
      self.loss_names = ['policy_loss', 'policy_loss_pol', 'policy_loss_select','select_old_neg', 'select_neg','pol_old_neg', 'pol_neg', 'value_loss', 'policy_entropy', 'approxkl', 
      'clipfrac', 'std', 'select_std', 'ratio_select', 'ratio', 'adv', 'cliprange', 'learning_rate','prox_loss']
      self.stats_list = [pg_loss, pg_loss_pol, pg_loss_pol_select, old_select_neglp, select_neglp, old_pol_neglp, pol_neglp, vf_loss, entropy, approxkl, clipfrac, std, select_std, mean_select_ratio, mean_ratio, mean_adv, CLIPRANGE, LR]
    else:
      self.loss_names = ['policy_loss', 'value_loss', 'policy_entropy', 'approxkl', 
      'clipfrac', 'std', 'ratio', 'adv', 'cliprange', 'learning_rate']
      self.stats_list = [pg_loss, vf_loss, entropy, approxkl, clipfrac, std, mean_ratio, mean_adv, CLIPRANGE, LR]

    self.train_model = train_model
    # self.act_model = act_model
    # Not sure where weights for act_model and train_model are synced. For safety using train_model
    self.step = train_model.step
    # self.value = train_model.value
    self.get_value = train_model.get_value
    self.init_buffer()
    if self.args.train_prox:
      self.init_static_buffer()


  def train(self, epoch, lr, cliprange, vf_only, obs, imgs, returns, masks, act, values, neglogpacs, actions, neglogpacs_select, prox, prox_obs, prox_im, exp_actions=None, states=None):

    prox_td_map = {
      self.train_model.ob : prox_obs,
      self.train_model.im : prox_im,
      self.PROX : prox,
      self.LR : lr
    }

    prox_loss =  list(self.sess.run([self.prox_loss, self._prox_train_op], prox_td_map)[:-1])

    # Here we calculate advantage A(s,a) = R + yV(s') - V(s)
    # Returns = R + yV(s')
    advs = returns - values
    # Normalize the advantages
    advs = (advs - advs.mean()) / (advs.std() + 1e-8)
    # act = torque
    # actions = setup or policy

    td_map = {
      self.train_model.ob : obs,
      self.A : act,
      self.ADV : advs,
      self.R : returns,
      self.LR : lr,
      self.CLIPRANGE : cliprange,
      self.OLDNEGLOGPAC : neglogpacs,
      self.PROX : prox,
      self.OLDVPRED : values
    }
    td_map[self.train_model.im] = imgs
    if self.args.train_select:
      td_map[self.ACTION] = actions
      td_map[self.OLDNEGLOGPAC_SELECT] = neglogpacs_select

    if states is not None:
      td_map[self.train_model.S] = states
      td_map[self.train_model.M] = masks
    
    # if vf_only:
    losses = list(self.sess.run(self.stats_list + [self._train_op],td_map)[:-1])
    return losses + prox_loss

  def run_train(self, ep_rets, ep_lens, vf_only=False, name="", evaluate=True, train=True):
    max_grad_norm=0.5

    self.train_model.ob_rms.update(self.data['ob'])
    if self.args.const_lr:
      self.cur_lrmult =  1.0
    else:
      self.cur_lrmult =  max(1.0 - float(self.timesteps_so_far) / self.max_timesteps, 0)
    # lr = lambda f:self.learning_rate*f
    # lrnow = lr(self.cur_lrmult)
    lrnow = self.learning_rate*self.cur_lrmult
    # lrnow = self.learning_rate
    self.lr = lrnow
    #**PPO2 doesn't seem to anneal clipping, may need to try with and without: works better without**#
    # cr = lambda f:0.2*f
    # if self.args.const_clip:
    #   # cliprangenow = cr(1.0)
    cliprangenow = 0.2
    # else:
    # for key in self.training_input:
      # print(key, self.data[key].shape)
      # cliprangenow = cr(self.cur_lrmult)
    # cliprangenow = 0.2*self.cur_lrmult
    self.sample_success_buffer()
    if train:
      self.n = self.data['done'].shape[0]
      inds = np.arange(self.n)
      for epoch in range(self.epochs):
        if self.enable_shuffle:
          np.random.shuffle(inds)
        self.loss = [] 
        for start in range(0, self.n, self.batch_size):
          end = start + self.batch_size
          mbinds = inds[start:end]
          # print([key for key in self.training_input + self.prox_input])
          # print([self.data[key].shape for key in self.training_input + self.prox_input])
          slices = (self.data[key][mbinds] for key in self.training_input + self.prox_input)
          outs = self.train(epoch, lrnow, cliprangenow, vf_only, *slices)
          self.loss.append(outs[:len(self.loss_names)])
      self.loss = np.mean(self.loss, axis=0)
    else:
      self.loss = [0 for _ in self.loss_names]
    if evaluate:
      self.evaluate(ep_rets, ep_lens, display_name=name)
    if self.args.extra_on_terminate:
      self.re_init_buffer()
    else:
      self.init_buffer()
    if not evaluate:
      return {"Setup " + name:loss for loss, name in zip(self.loss, self.loss_names)}

  # def step(self, ob, im, stochastic=False, multi=False): 
  #   if not multi:
  #     actions, values, self.states, neglogpacs =  self.train_model.step(ob[None], im[None], stochastic=stochastic)
  #     return actions[0], values, self.states, neglogpacs
  #   else:
  #     actions, values, self.states, neglogpacs =  self.train_model.step(ob, im, stochastic=stochastic)
  #     return actions, values, self.states, neglogpacs        


  def get_advantage(self, last_value, last_done):    
    gamma = 0.99; lam = 0.95
    advs = np.zeros_like(self.data['rew'])
    lastgaelam = 0
    for t in reversed(range(len(self.data['rew']))):
      if t == len(self.data['rew']) - 1:
        nextnonterminal = 1.0 - last_done
        nextvalues = last_value
      else:
        nextnonterminal = 1.0 - self.data['done'][t+1]
        nextvalues = self.data['value'][t+1]
      delta = self.data['rew'][t] + gamma * nextvalues * nextnonterminal - self.data['value'][t]
      advs[t] = lastgaelam = delta + gamma * lam * nextnonterminal * lastgaelam
    self.data['return'] = advs + self.data['value']
  
  def get_advantage2(self, last_value, last_done):    
    # print("running advantage2")
    gamma = 0.99; lam = 0.95
    advs = np.zeros_like(self.data['rew'])
    lastgaelam = 0
    for t in reversed(range(len(self.data['rew']))):
      reset_gae_lam = False
      if t == len(self.data['rew']) - 1:
        if last_done == 1:
          nextnonterminal = 0.0
        else:
          if last_done == 2:
            lastgaelam = 0
            print("reseting gae_lam")
          nextnonterminal = 1.0 
        nextvalues = last_value
      else:
        if self.data['done'][t+1] == 1:
          nextnonterminal = 0.0
        else:
          if self.data['done'][t+1] == 2:
            lastgaelam = 0
            print("reseting gae_lam")
          nextnonterminal = 1.0 
        # nextvalues = self.data['value'][t+1]
        nextvalues = self.data['next_value'][t]
      delta = self.data['rew'][t] + gamma * nextvalues * nextnonterminal - self.data['value'][t]
      advs[t] = lastgaelam = delta + gamma * lam * nextnonterminal * lastgaelam
    self.data['return'] = advs + self.data['value']

  def init_static_buffer(self, buffer_size = 25000):
    self.prox_data_input = ['prox_ob', 'prox_im', 'prox']
    self.prox_data = {}
    self.prox_data['pos_prox_ob'] = np.zeros([buffer_size,self.ob_size])
    self.prox_data['pos_prox_im'] = np.zeros([buffer_size] + self.im_size)
    self.prox_data['pos_prox'] = np.zeros([buffer_size,])
    self.prox_data['neg_prox_ob'] = np.zeros([buffer_size,self.ob_size])
    self.prox_data['neg_prox_im'] = np.zeros([buffer_size] + self.im_size)
    self.prox_data['neg_prox'] = np.zeros([buffer_size,])
    self.prox_data_pointer = {'pos':0, 'neg':0}
    self.buffer_size = buffer_size
    self.full = {'pos':False, 'neg':False}

  def add_to_success_buffer(self, obs, imgs, success=False, switch=None):
    if success:
      tag = 'pos'
    else:
      tag = 'neg'
    # print(success, switch, np.array(obs).shape)
    if not success:
      # Only save data from the switch point
      obs = np.array(obs)[switch:,:]
      imgs = np.array(imgs)[switch:,::]
      success = np.ones([obs.shape[0], ])*success
      # [switch:,:]*success
      # print(success)
      # print(success.shape, switch, obs.shape[0])
    else:
      # If episode was successful, work out how far each data point is from the switch point
      obs = np.array(obs)
      imgs = np.array(imgs)      
      new_success = [1]
      for i in reversed(range(switch)):
        new_success.append(new_success[-1]*0.95)
      # print(switch, obs.shape)
      # print(new_success)
      success = np.array(new_success[::-1] + [1 for _ in range(switch, obs.shape[0]-1)]).reshape([-1,])
      # print(success.shape)
      # print(success)

    if self.prox_data_pointer[tag] + obs.shape[0] < self.buffer_size:
      # print("pre overflow")
      start = self.prox_data_pointer[tag]
      end = start + obs.shape[0] 
      # print(end-start, obs.shape[0], success.shape, switch)
      self.prox_data[tag + '_prox_ob'][start:end,:] = obs
      self.prox_data[tag + '_prox_im'][start:end,::] = imgs
      # print(success.shape)
      self.prox_data[tag + '_prox'][start:end,] = success
      self.prox_data_pointer[tag] += obs.shape[0]
    else:
      # print("post overflow")
      self.full[tag] = True
      remainder = self.buffer_size - self.prox_data_pointer[tag]
      start = self.prox_data_pointer[tag]
      end = self.buffer_size
      self.prox_data[tag + '_prox_ob'][start:end,:] = obs[:remainder, :]
      self.prox_data[tag + '_prox_im'][start:end,::] = imgs[:remainder, ::]
      self.prox_data[tag + '_prox'][start:end,] = success[:remainder, ]
      start = 0
      end = obs.shape[0] - remainder
      self.prox_data[tag + '_prox_ob'][start:end,:] = obs[remainder:, :]
      self.prox_data[tag + '_prox_im'][start:end,::] = imgs[remainder:, ::]
      self.prox_data[tag + '_prox'][start:end,] = success[remainder:, ]
      self.prox_data_pointer[tag] = end
    # print(self.prox_data_pointer, obs.shape[0])

  def sample_success_buffer(self):
    if self.full['pos']:
      pos_idx = [i for i in range(self.buffer_size)]
    else:
      pos_idx = [i for i in range(self.prox_data_pointer['pos'])]
    np.random.shuffle(pos_idx)
    if self.full['neg']:
      neg_idx = [i for i in range(self.buffer_size)]
    else:
      neg_idx = [i for i in range(self.prox_data_pointer['neg'])]
    np.random.shuffle(neg_idx)

    assert (len(pos_idx) + len(neg_idx)) > (self.data['ob'].shape[0])

    # Not enough negs to make even split
    if len(neg_idx) < self.data['ob'].shape[0]//2:
      pos_idx = pos_idx[:(self.data['ob'].shape[0] - len(neg_idx))]
    # Not enough pos to make even split
    elif len(pos_idx) < self.data['ob'].shape[0]//2:
      neg_idx = neg_idx[:(self.data['ob'].shape[0] - len(pos_idx))]
    # Even split
    else:
      pos_idx = pos_idx[:(self.data['ob'].shape[0]//2)]
      neg_idx = neg_idx[:(self.data['ob'].shape[0] - len(pos_idx))]

    self.data['prox_ob'] = np.concatenate([self.prox_data['pos_prox_ob'][pos_idx,:], self.prox_data['neg_prox_ob'][neg_idx,:]])
    self.data['prox_im'] = np.concatenate([self.prox_data['pos_prox_im'][pos_idx,::], self.prox_data['neg_prox_im'][neg_idx,::]])
    self.data['prox'] =    np.concatenate([self.prox_data['pos_prox'][pos_idx,], self.prox_data['neg_prox'][neg_idx,]]) 

    # pos_idx = pos_idx[:self.data['ob'].shape[0]//2]
    # if len(neg_idx) < (self.data['ob'].shape[0] - self.data['ob'].shape[0]//2):
    #   print(" should be lenght of ", len(pos_idx) * 2)
    #   # print("not enough negatives", self.data["ob"].shape, len(pos_idx), len(neg_idx))
    #   self.data['prox_ob'] = np.concatenate([self.prox_data['pos_prox_ob'][pos_idx,:], self.prox_data['pos_prox_ob'][pos_idx,:]])
    #   self.data['prox_im'] = np.concatenate([self.prox_data['pos_prox_im'][pos_idx,::], self.prox_data['pos_prox_im'][pos_idx,::]])
    #   self.data['prox'] =    np.concatenate([self.prox_data['pos_prox'][pos_idx,], self.prox_data['pos_prox'][pos_idx,]]) 

    # else:
    #   # print("pos and neg number ", self.data["ob"].shape, len(pos_idx), len(neg_idx))
    #   print(" should be lenght of ", len(pos_idx) + len(neg_idx))

    #   neg_idx = neg_idx[:(self.data['ob'].shape[0] - self.data['ob'].shape[0]//2)]
    #   self.data['prox_ob'] = np.concatenate([self.prox_data['pos_prox_ob'][pos_idx,:], self.prox_data['neg_prox_ob'][neg_idx,:]])
    #   self.data['prox_im'] = np.concatenate([self.prox_data['pos_prox_im'][pos_idx,::], self.prox_data['neg_prox_im'][neg_idx,::]])
    #   self.data['prox'] =    np.concatenate([self.prox_data['pos_prox'][pos_idx,], self.prox_data['neg_prox'][neg_idx,]]) 
    # print("prox ob, im and ob: ", self.data['prox_ob'].shape, self.data['prox_im'].shape, self.data['ob'].shape)
  
  def init_buffer(self):  
    # Make sure reward is 'rew' and value/vpred is 'value', for get_advantage function in base.py
    # self.data_input = ['ob', 'im', 'ac', 'rew', 'done', 'value', 'neglogpac', 'roa_pred', 'roa_done']
    if self.args.advantage2:
      print("not using advantage2")
      # self.data_input = ['ob', 'im', 'ac', 'rew', 'done', 'value', 'neglogpac','next_value']
      # self.training_input = ['ob', 'im', 'return', 'done', 'ac', 'value', 'neglogpac']
    else:
      # self.data_input = ['ob', 'im', 'ac', 'actions', 'rew', 'done', 'value', 'neglogpac']
      # self.training_input = ['ob', 'im', 'return', 'done', 'ac', 'actions', 'value', 'neglogpac']
      # self.data_input = ['ob', 'im', 'ac', 'actions', 'rew', 'done', 'value', 'neglogpac']
      # self.training_input = ['ob', 'im', 'return', 'done', 'ac', 'actions', 'value', 'neglogpac']
      self.data_input = ['ob', 'im', 'ac', 'actions', 'rew', 'done', 'value', 'neglogpac', 'neglogpac_select']     
      if self.args.train_select:
        self.training_input = ['ob', 'im', 'return', 'done', 'ac', 'value', 'neglogpac', 'actions', 'neglogpac_select']
      else:
        self.training_input = ['ob', 'im', 'return', 'done', 'ac', 'value', 'neglogpac']
      self.prox_input = ['prox', 'prox_ob', 'prox_im']
    self.data = {t:[] for t in self.data_input}

  def re_init_buffer(self):
    # self.old_last_data = {t:self.data[t][-1] for t in self.data_input}
    # print("before reinit")
    ob, im, ac, actions, rew, done = self.data['ob'][-1], self.data['im'][-1], self.data['ac'][-1], self.data['actions'][-1], self.data['rew'][-1], self.data['done'][-1]
    value, neglogpac, neglogpac_select = self.train_model.get_vpred_and_nlogps(ob, im, ac, actions)
    self.data = {t:[] for t in self.data_input}
    self.data['ob'].append(ob)
    self.data['im'].append(im)
    self.data['ac'].append(ac)
    self.data['actions'].append(actions)
    self.data['rew'].append(rew)
    self.data['done'].append(done)
    self.data['value'].append(value)
    self.data['neglogpac'].append(neglogpac)
    self.data['neglogpac_select'].append(neglogpac_select)    
    # print("after reinit")

  def log_stuff(self, things):
    if self.rank == 0:
      for thing in things:
        self.writer.add_scalar(thing, things[thing], self.iters_so_far)
        logger.record_tabular(thing, things[thing])
    
  def add_to_buffer(self, data):
    ''' data needs to be a list of lists, same length as self.data'''
    for d,key in zip(data, self.data):
      self.data[key].append(d)

  def finalise_buffer(self, data, last_value, last_done, last_roa_done=None, last_roa=None):
    ''' data must be dict'''
    for key in self.data_input:
      if key == 'done':
        self.data[key] = np.asarray(self.data[key], dtype=np.bool)
      else:
        self.data[key] = np.asarray(self.data[key])
    self.n = next(iter(self.data.values())).shape[0]
    for key in data:
      self.data[key] = data[key]
    if self.args.advantage2:
      self.get_advantage2(last_value, last_done)
    else:
      self.get_advantage(last_value, last_done)
    # if self.rank == 0:
      # self.log_stuff()
