"""
Base class for all networks.
For now includes training parameters..
"""
# import scripts.utils as U
import tensorflow as tf
from scripts import logger
from mpi4py import MPI
from scripts.distributions import make_pdtype
import time
import numpy as np
from collections import deque
import scripts.utils as U

class Base():
    epochs = 10
    batch_size = 32
    horizon = 2048
    enable_shuffle = True
    episodes_so_far = 0
    timesteps_so_far = 0
    iters_so_far = 0
    best_reward = 0
    tstart = time.time()
    t1 = time.time()
    all_rewards = []
    lenbuffer = deque(maxlen=100) # rolling buffer for episode lengths
    rewbuffer = deque(maxlen=100) # rolling buffer for episode rewards
    comm = MPI.COMM_WORLD
    rank = comm.Get_rank()
    def __init__(self):
        vars_with_adam = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, scope=self.name+"/")
        self.vars = [v for v in vars_with_adam if "Adam" not in v.name]
        self.policy_saver = tf.train.Saver(var_list=self.vars)
        self.t1 = time.time()

    def save(self, best=False, early=False):
        if best:
            self.policy_saver.save(tf.get_default_session(), self.PATH + '/best/model.ckpt', write_meta_graph=False)
        elif early:
            self.policy_saver.save(tf.get_default_session(), self.PATH + '/early/model.ckpt', write_meta_graph=False)
        else:
            self.policy_saver.save(tf.get_default_session(), self.PATH + 'model.ckpt', write_meta_graph=False)
        # if self.rank == 0:
        print("Saving weights for " + self.name)

    def load(self, WEIGHT_PATH):
        self.policy_saver.restore(tf.get_default_session(), WEIGHT_PATH + "model.ckpt")
        print("Loaded weights for " + self.name + " module, from " + WEIGHT_PATH)

    def load_pol(self, base_name, WEIGHTS_PATH, reset_std=True, vis=True, vf=True):
        temp_vars = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, scope=base_name + "/")
        # base_vars = [v for v in temp_vars if "Adam" not in v.name and "select" not in v.name    and "logstd" not in v.name and ("obfilter" in v.name or "vis" in v.name or "pol" in v.name)]
        # if self.args.run_state == "train_setup":
        if "setup" in self.name:
            base_vars = [v for v in temp_vars if "Adam" not in v.name and "logstd" not in v.name and ("obfilter" in v.name or "vis" in v.name or "pol" in v.name)]
        else:
            base_vars = [v for v in temp_vars if "Adam" not in v.name and "logstd" not in v.name and ("obfilter" in v.name or "vis" in v.name or "pol" in v.name or "select" in v.name)]
        if vf:
            base_vars += [v for v in temp_vars if "Adam" not in v.name and "vf" in v.name]
        # target_func_vars = [v for v in self.vars if "Adam" not in v.name and "select" not in v.name    and "logstd" not in v.name and ("obfilter" in v.name or "vis" in v.name or "pol" in v.name)]
        # if self.args.run_state == "train_setup":
        if "setup" in self.name:
            target_func_vars = [v for v in self.vars if "Adam" not in v.name and "logstd" not in v.name and ("obfilter" in v.name or "vis" in v.name or "pol" in v.name)]
        else:
            target_func_vars = [v for v in self.vars if "Adam" not in v.name and "logstd" not in v.name and ("obfilter" in v.name or "vis" in v.name or "pol" in v.name or "select" in v.name)]
        if vf:
            target_func_vars += [v for v in self.vars if "Adam" not in v.name and "vf" in v.name]
        base_saver = tf.train.Saver(var_list=base_vars)        
        base_saver.restore(tf.get_default_session(), WEIGHTS_PATH + "model.ckpt")
        # if "setup" in self.name:
        #     print(target_func_vars)
        #     print(base_vars)
        update_target_expr = []
        for var, var_target in zip(sorted(base_vars, key=lambda v:v.name), sorted(target_func_vars, key=lambda v: v.name)):
            update_target_expr.append(var_target.assign(var))
        update_target_expr = tf.group(*update_target_expr)
        update_target = U.function([],[], updates=[update_target_expr])
        update_target()

        if self.rank == 0:
            print("Loaded weights for subpolicy: ", self.name)    
        if reset_std:
            for v in target_func_vars:
                if "logstd" in v.name:
                    assign_op = v.assign(np.log(np.ones([1,12])))
                    assign_op.op.run()

    def evaluate(self, rews, lens, log=True):

        lrlocal = (rews, lens) # local values
        listoflrpairs = MPI.COMM_WORLD.allgather(lrlocal) # list of tuples
        rews, lens = map(flatten_lists, zip(*listoflrpairs))
        self.lenbuffer.extend(lens)
        self.rewbuffer.extend(rews)
        if self.rank == 0 and log:
            logger.log("********** Iteration %i ************"%self.iters_so_far)
            for loss, name in zip(self.loss, self.loss_names):
                logger.record_tabular(name, loss)
                self.writer.add_scalar(name, loss, self.iters_so_far)
            logger.record_tabular("LearningRate", self.lr)
            logger.record_tabular("EpRewMean", np.mean(self.rewbuffer))
            logger.record_tabular("EpLenMean", np.mean(self.lenbuffer))
            logger.record_tabular("EpThisIter", len(self.lenbuffer))
            logger.record_tabular("EpThisIter", len(self.lenbuffer))
            logger.record_tabular("TimeThisIter", time.time() - self.t1)
            self.writer.add_scalar("time per rollout", time.time() - self.t1, self.iters_so_far)
            logger.record_tabular("TimeStepsSoFar", self.timesteps_so_far)
            logger.record_tabular("EnvTotalSteps", self.env.total_steps)
            self.t1 = time.time()
            self.writer.add_scalar("rewards", np.mean(self.rewbuffer), self.iters_so_far)
            self.writer.add_scalar("lengths", np.mean(self.lenbuffer), self.iters_so_far)
            self.all_rewards.append(np.mean(self.rewbuffer))
            np.save(self.PATH + "rewards.npy",self.all_rewards)
            logger.dump_tabular()
        
        self.env.log_stuff(logger, self.writer, self.iters_so_far, log)
        
        if self.rank == 0:
            try:
                self.save()
            except:
                print("Couldn't save training model")
        # if log:
        # print(lens)
        self.iters_so_far += 1
        self.timesteps_so_far += sum(lens)     
        # self.rewbuffer = []
        # self.lenbuffer = []

    def set_training_params(self, max_timesteps, learning_rate, horizon):
        self.max_timesteps = max_timesteps
        self.learning_rate = learning_rate
        self.horizon = horizon

    def log_stuff(self):
        pass

def flatten_lists(listoflists):
    return [el for list_ in listoflists for el in list_]