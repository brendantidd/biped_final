import os     
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' 
import tensorflow as tf
tf.get_logger().setLevel('DEBUG')
import numpy as np
import argparse
import tensorboardX
from collections import deque     
from scripts.utils import *
from baselines.common.mpi_util import sync_from_root
import random
from mpi4py import MPI
import time
from pathlib import Path
home = str(Path.home())
from baselines import logger
import matplotlib.pyplot as plt
plt.switch_backend('agg')
import pybullet as p
import cv2
import json
import git
from collections import Counter
np.set_printoptions(precision=3, suppress=True)

def run(args):

    PATH = home + '/results/biped_model/latest/' + args.folder + '/' + args.exp + '/'

    comm = MPI.COMM_WORLD
    rank = comm.Get_rank()
    # myseed = int(time.time() + 10000 * rank)
    myseed = int(args.seed + 10000 * rank)
    np.random.seed(myseed)
    random.seed(myseed)
    tf.set_random_seed(myseed)
    
    logger.configure(dir=PATH)
    if rank == 0:
        writer = tensorboardX.SummaryWriter(log_dir=PATH)
        repo = git.Repo(search_parent_directories=True)
        sha = repo.head.object.hexsha
        with open(PATH + 'commandline_args.txt', 'w') as f:
            f.write('Hash:')
            f.write(str(sha) + "\n")
            json.dump(args.__dict__, f, indent=2)
    else: 
        writer = None 

    # sess = tf.Session()
    gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction= 0.1)
    sess = tf.InteractiveSession(config=tf.ConfigProto(inter_op_parallelism_threads=1,
                                                                                    intra_op_parallelism_threads=1,                         
                                                                                    gpu_options=gpu_options), graph=None)
 
    horizon = 2048

    from assets.env_pb_biped_setup1 import Env
    from models import ppo_setup
    from models import ppo
    # from models.classifier import Classifier
    # if args.adv:
    #     from models.dqn_adv import DQN
    # else:
    #     from models.dqn import DQN

    # obstacle_types = ['gaps','base','jumps','stairs','zero']
    # obstacle_types = ['flat','high_jumps', 'gaps']
    # obstacle_types = ['flat','high_jumps', 'gaps', 'jumps', 'stairs', 'steps']
    # obstacle_types = ['flat','high_jumps', 'gaps', 'jumps', 'stairs']
    
    if args.obstacle_type == "mix":
        # obstacle_types = ['flat','high_jumps', 'gaps', 'jumps', 'stairs', 'steps']
        # obs_except_flat = ['high_jumps', 'gaps', 'jumps', 'stairs', 'steps']
        
        # obstacle_types = ['flat', 'gaps', 'jumps', 'stairs', 'steps']
        # obs_except_flat = ['gaps', 'jumps', 'stairs', 'steps']

        obstacle_types = ['flat', 'high_jumps']
        obs_except_flat = ['high_jumps']

    else:
        obstacle_types = ['flat',args.obstacle_type]
        obs_except_flat = [args.obstacle_type]


    # obstacle_types = ['flat','high_jumps']
    # obs_except_flat = ['high_jumps']

    # obstacle_types = ['flat', 'gaps', 'jumps', 'stairs', 'steps']
    # obs_except_flat = [ 'gaps', 'jumps', 'stairs', 'steps']

    # obstacle_types = ['flat','high_jumps']


    env = Env(render=args.render, PATH=PATH, args=args, display_im=args.display_im, obstacle_type=args.obstacle_type, vis=args.vis, doa=args.doa, disturbances=False, multi=args.multi, dqn=True, obstacle_types=obs_except_flat)
    # print(env.im_size)

    pol = {name:ppo.Model(name, env=env, ob_size=env.ob_size, ac_size=env.ac_size, im_size=env.im_size, args=args, PATH=PATH, horizon=horizon, writer=writer, max_timesteps=args.max_ts, vis=args.vis) for name in obstacle_types if name != 'zero'}    

    pol.update({name + '_setup': ppo_setup.Model(name + '_setup', env=env, ob_size=env.ob_size, ac_size=env.ac_size, im_size=env.im_size, args=args, PATH=PATH, horizon=horizon, writer=writer, max_timesteps=args.max_ts, vis=args.vis) for name in obstacle_types})
    

    # dqn = {name + "_dqn":DQN(name + "_dqn", env=env, ob_size=env.ob_size, ac_size=3, im_size=env.im_size, args=args, PATH=PATH, horizon=horizon, writer=writer, max_timesteps=args.max_ts, vis=args.vis, lr=args.lr, eps_decay=args.eps_decay) for name in obstacle_types if name != 'flat'}
    # dqn['dqn'] = DQN("dqn", env=env, ob_size=env.ob_size, ac_size=3, im_size=env.im_size, args=args, PATH=PATH, horizon=horizon, writer=writer, max_timesteps=args.max_ts, vis=args.vis, lr=args.lr, eps_decay=args.eps_decay) 

    # classifier = Classifier(args, sess=sess)

    if args.hpc:
        WEIGHTS_PATH = home + '/hpc-home/results/biped_model/weights/' + args.folder 
    else:
        # WEIGHTS_PATH = home + '/results/biped_model/weights/' + args.folder 
        WEIGHTS_PATH = './weights'
    
    for name in obstacle_types:
        # if name != 'flat':
            # pol[name + '_setup'].load(WEIGHTS_PATH='./weights/' + name + '_select/')
        # pol[name + '_setup'].load(WEIGHTS_PATH='./weights/' + name + '_setup/')
        if name == "high_jumps":
            # pol[name + '_setup'].load(WEIGHTS_PATH=home + '/results/biped_final/latest/setup/high_jumps_new_setup3/')
            # pol[name + '_setup'].load(WEIGHTS_PATH=home + '/hpc-home/results/biped_final/latest/final/high_jumps_new_rand3/')
            # pol[name + '_setup'].load(WEIGHTS_PATH=home + '/hpc-home/results/biped_final/latest/final/high_jumps_old/')
            pol[name + '_setup'].load(WEIGHTS_PATH='./weights/' + name + '_setup/')
        else:
            pol[name + '_setup'].load(WEIGHTS_PATH='./weights/' + name + '_setup/')
        pol[name].load_pol(base_name=name, WEIGHTS_PATH=WEIGHTS_PATH + '/' + name + '/')

        # pol[name].load_pol(base_name=name, WEIGHTS_PATH=WEIGHTS_PATH + '/' + name + '/')
        # if name != 'flat':
            # pol[name + '_setup'].load_pol(base_name=name + '_setup', WEIGHTS_PATH=WEIGHTS_PATH + '/' + name + '_setup/')
        #     # dqn[name + '_dqn'].load_dqn(base_name='dqn', WEIGHTS_PATH=WEIGHTS_PATH + '/' + name + '_dqn/')

    # classifier.load('./weights/classifier/new_classifier2/')
    # classifier.load(home + '/results/biped_model/latest/classifier/20000/')

    # if args.test_pol:
    #     dqn.load(home + '/hpc-home/results/biped_model/latest/dqn/dqn_adv_baseline_reward/')
    #     # dqn.load(home + '/results/biped_model/latest/dqn/longer_exp2/')

    initialize_uninitialized()


    # Throw an error if the graph grows (shouldn't change once everything is initialised)
    # tf.get_default_graph().finalize()

    prev_done = True

    # env.obstacle_type = 'mix'
    # env.obstacle_type = 'high_jumps'
    env.difficulty = args.difficulty
    env.height_coeff = args.height_coeff
    prev_done = True
    ob = env.reset(evaluate=True)
    im = env.get_im()
    desired_pol = obstacle_types.index(env.order[env.box_num])
    if args.no_state:
        # dqn_ob = np.array([desired_pol])
        dqn_ob = np.array([0])
        # dqn_ob[-1] = desired_pol 
        desired = np.zeros(len(obstacle_types))
        desired[desired_pol] = 1
        dqn_ob = np.concatenate([dqn_ob, desired])
        print(dqn_ob.shape)
        # dqn_im = np.array([0])
        if args.use_boxes:
            boxes = []
            for box in env.box_nums:
                info = []
                if (env.box_num + box < 1) or (env.box_num + box > len(env.box_info[0]) - 1):
                    info.append(0)
                else:
                    info.append(env.body_xyz[2] - (env.box_info[1][env.box_num+box][2] + env.box_info[2][env.box_num+box][2]))
                info.append(env.body_xyz[0] - (env.box_info[1][env.box_num+box][0] + env.box_info[2][env.box_num+box][0]))
                info.append(env.body_xyz[1])
                info.append(env.yaw)
                boxes.extend(info)
            dqn_im = np.array(boxes)
        else:
            dqn_im = env.get_im()
    else:

        dqn_ob = ob
        # desired = np.zeros(len(obstacle_types))
        # desired[desired_pol] = 1
        # dqn_ob = np.concatenate([dqn_ob, desired])
        # print(dqn_ob.shape)
        # dqn_im = im
        dqn_im = env.get_im()

    ep_ret = 0
    ep_len = 0
    ep_rets = []
    ep_lens = []
    ep_steps = 0
    ep_count = 0
    # timesteps_so_far = 0
    stochastic = False

    vpred_data = []
    vpred_orig_data = []
    probs_data = []
    stable_vpred = []
    terrain_data = []
    current_pol_data = []
    next_pol_data = []
    baseline_data = []
    switch_data = []
    feet_data = {'left':[],'right':[]}
    com_data = []
    switch_points = []

    success = []
    dists = []

    stable_count = 120

    # eval_length = 20
    # eval_length = 500
    eval_length = 1000

    # print(dqn_acts)
    # current_pol = np.argmax(vpreds)
    # vpred = vpreds[current_pol]

    # pols = [i for i in range(len(obstacle_types))]                         
    box_cross_steps = None

    # next_pol = current_pol
    prev_pol  =current_pol = obstacle_types[0]
    active = None

    pol_change = False    

    if args.render:
        replace_Id2 = p.addUserDebugText(current_pol,[env.body_xyz[0], env.body_xyz[1], env.body_xyz[2]+0.5],[0,0,1],textSize=3)
    
    current_base_pol = current_pol
    terminate_setup = False
    select_raw = 0
    total_steps = 0
    if rank == 0:
        writer.add_scalar("Eval_dists", 0, 0)
        writer.add_scalar("Eval_succes", 0, 0)
        t1 = time.time()
        eval_so_far = 0
    
    max_z_height = 0
    
    # stats = {'before':{'gaps':[],'jumps':[],'stairs':[], 'flat':[], 'steps':[], 'high_jumps':[]},
    #         'current':{'gaps':[],'jumps':[],'stairs':[], 'flat':[], 'steps':[], 'high_jumps':[]},
    #           'after':{'gaps':[],'jumps':[],'stairs':[], 'flat':[], 'steps':[], 'high_jumps':[]}}

    stats = {'before':[], 'current':[], 'after':[]}
    stat = {}
    
    # counts = {'success':{'gaps':[],'jumps':[],'stairs':[], 'flat':[], 'steps':[], 'high_jumps':[]}, 'failure':{'gaps':[],'jumps':[],'stairs':[], 'flat':[], 'steps':[], 'high_jumps':[]}}

    prev_artifact = artifact = None
    counts = {'success':[], 'failure':[]}

    while True:
        if env.body_xyz[2] > max_z_height:
            max_z_height = env.body_xyz[2]
    
        total_steps += 1
        # if eval_next and total_steps % 1000 == 0:
        if total_steps % 2000 == 0:
            all_dists = MPI.COMM_WORLD.allgather(dists)
            # all_rews = MPI.COMM_WORLD.allgather(rews)
            all_success = MPI.COMM_WORLD.allgather(success)
            all_dists = [h for d in all_dists for h in d]
            # all_rews = [h for d in all_rews for h in d]
            all_success = [h for d in all_success for h in d]
            # print(rank, len(all_dists))
            
                # eval_next = False
                # done = True
            if rank == 0:
                eval_so_far += 1
                writer.add_scalar("Eval_dists", np.mean(all_dists), eval_so_far)
                writer.add_scalar("Eval_success", np.mean(all_success), eval_so_far)
                # pol.writer.add_scalar("Eval_rews", np.mean(all_rews), pol.iters_so_far)
                # print("resuming training", len(all_dists), "evaluation took ", time.time() - t_eval, "eval dist ", np.mean(all_dists), " eval success ", np.mean(all_success))
                print("num {0:d} Dists {1:.4f} Success {2:.4f} Time {3:.4f} ".format(len(all_dists), np.mean(all_dists), np.mean(all_success), time.time  () - t1))
                # t1 = time.time()
            if len(all_dists) > eval_length:
                if rank == 0:
                    print("done.. ")
                all_stats = {}
                for key in stats:
                    temp_stat = MPI.COMM_WORLD.allgather(stats[key])
                    all_stats[key] = [h for d in temp_stat for h in d]
                all_counts = {}
                for key in counts:
                    temp_stat = MPI.COMM_WORLD.allgather(counts[key])
                    all_counts[key] = [h for d in temp_stat for h in d]
                
                if rank == 0:
                    # for _ in stats['before']:
                    #     # print(key, all_stats[key])
                    #     print("before: ", all_stats['before'])
                    
                    # for key in stats:
                        # print(key, [thing + " : " + str(len(all_stats[key])) for thing in all_stats[key]])
                        # print(key, all_stats[key])
                    # for o in obstacle_types:
                    #     for key in stats:
                    #         if key == 'after':
                    #             continue
                    #         print(key, "count for ", o, all_stats[key].count(o))

                    new = []
                    for i in range(len(all_stats['before'])):
                        new.append(all_stats['before'][i] + " " + all_stats['current'][i])
                    # print(new)
                    
                    print("All counts", all_counts)

                    # print(Counter(new))
                    print("success", Counter(all_counts['success']))
                    print("failure", Counter(all_counts['failure']))
                    counted_success = Counter(all_counts['success'])
                    counted_failure = Counter(all_counts['failure'])
                    for key in counted_success:
                        print(key, "success: ", counted_success[key]/(counted_success[key] + counted_failure[key]), "failure : ",  counted_failure[key]/(counted_success[key] + counted_failure[key]), "total:", counted_success[key] + counted_failure[key])
                    # print(stat)
                    # print(new.sort())
                    # for o in obstacle_types:
                    #     if o == 'after':
                    #         continue
                    #     for key in stats:
                    #         print(key, "count for ", o, all_stats[key].count(o))
                    print("============================================================================================================================")
                break

        # ===================================================================================
        # Step env ==========================================================================
        # ===================================================================================
        # print(classifier.obstacle_types[classifier_current_pol], current_base_pol, current_pol)
        if 'setup' in current_pol:
            act, _, _, _, select_raw, _ = pol[current_pol].step(ob, im, stochastic=False)
            terminate_setup = select_raw[0] > 3
            # print(select_raw)
            # print(terminate_setup, select_raw, env.steps - box_cross_steps)
            # if terminate_setup:
                # print('steps', env.steps - box_cross_step)
        else:
            act, _, _, _ = pol[current_pol].step(ob, im, stochastic=False)

        if args.render:
            # if 'setup' in current_pol:
                # replace_Id2 = p.addUserDebugText("Setting up for " + current_pol,[env.body_xyz[0], env.body_xyz[1], env.body_xyz[2]+0.5],[0,0,1],textSize=3,replaceItemUniqueId=replace_Id2)
            # else:
            replace_Id2 = p.addUserDebugText(current_pol,[env.body_xyz[0], env.body_xyz[1], env.body_xyz[2]+0.5],[0,0,1],textSize=3,replaceItemUniqueId=replace_Id2)
        
        torques = act
        
        next_ob, rew, done, _ = env.step(torques)
        next_im = env.get_im()

        # ===================================================================================
        # Terrain stuff =====================================================================
        # ===================================================================================

        
        # if not box_cross_steps and env.order[env.box_num] == args.obstacle_type:
        #     box_cross_steps = env.steps

        # if not box_cross_steps:
        #     desired_pol = obstacle_types.index('flat') 
        # elif box_cross_steps and (box_cross_steps + max_setup_length > env.steps):
        #     desired_pol = obstacle_types.index(args.obstacle_type + '_setup') 
        # elif box_cross_steps and (box_cross_steps + max_setup_length <= env.steps):
        #     if (env.order[env.box_num] == args.obstacle_type):
        #         # print(max_setup_length + box_cross_steps, env.steps - box_cross_steps)
        #         desired_pol = obstacle_types.index(args.obstacle_type) 
        #     elif (env.order[env.box_num-1] == args.obstacle_type and env.order[env.box_num] != args.obstacle_type and env.body_xyz[0] < env.box_info[1][env.box_num][0]):
        #         desired_pol = obstacle_types.index(args.obstacle_type) 
        #     else:
        #         desired_pol = obstacle_types.index('flat') 

        # if args.use_classifier:
        #     classifier_current_pol = classifier.obstacle_types[classifier.step(ob, im)]
        #     print(classifier_current_pol)
        # else:

        if artifact is None and env.order[env.box_num] != 'flat':
            prev_artifact = artifact = env.order[env.box_num]
        
        if (current_pol == 'high_jumps' and env.order[env.box_num] == 'flat') or (current_pol != 'high_jumps' and env.order[env.box_num] != 'flat'):
            # print(env.order)
            # if (current_pol == 'high_jumps' and env.order[env.box_num] == 'flat'):
            #     artifact = "jumps"
            # else:
            artifact = env.order[env.box_num]
            if prev_artifact != artifact:
                last_box_num = env.box_num
                counts['success'].append(prev_artifact)
            prev_artifact = artifact

        # classifier_current_pol = env.order[env.box_num]
        if artifact is not None:
            classifier_current_pol = artifact
        else:
            classifier_current_pol = 'flat'

        
        # if env.box_num > 0 and env.order[env.box_num] == 'flat' and env.order[env.box_num - 1] != 'flat':
        #     # if env.body_xyz[0] > env.box_info[1][env.box_num][0]:
        #     # if (env.ob_dict['left_foot_on_ground'] and env.ob_dict['right_foot_on_ground'] and abs(env.vz) < 0.05 and env.vx > 0):
        #     classifier_current_pol = env.order[env.box_num]
        # else:
        #     classifier_current_pol = env.order[env.box_num]
        # if classifier_current_pol not in obstacle_types:
        #     classifier_current_pol = 'flat'
        #     print(classifier_current_pol)



        # if not box_cross_steps and env.order[env.box_num] == args.obstacle_type:
        #     box_cross_steps = env.steps

        # if not box_cross_steps:
        #     desired_pol = obstacle_types.index('flat') 
        # elif box_cross_steps and (box_cross_steps + max_setup_length > env.steps):
        #     desired_pol = obstacle_types.index(args.obstacle_type + '_setup') 
        # elif box_cross_steps and (box_cross_steps + max_setup_length <= env.steps):
        #     if (env.order[env.box_num] == args.obstacle_type):
        #         # print(max_setup_length + box_cross_steps, env.steps - box_cross_steps)
        #         desired_pol = obstacle_types.index(args.obstacle_type) 
        #     elif (env.order[env.box_num-1] == args.obstacle_type and env.order[env.box_num] != args.obstacle_type and env.body_xyz[0] < env.box_info[1][env.box_num][0]):
        #         desired_pol = obstacle_types.index(args.obstacle_type) 
        #     else:
        #         desired_pol = obstacle_types.index('flat') 

        # if current_pol == 'flat_setup':
        #     print(current_pol, terminate_setup, select_raw)

        current_base_pol = classifier_current_pol
        # if args.no_setup:
        #     current_pol = current_base_pol
        # else:
        # print(env.order[env.box_num], current_base_pol, current_pol)
        if current_base_pol not in current_pol and 'setup' not in current_pol:
            # print(current_base_pol)
            # if ((env.x_min > (env.box_info[1][env.box_num][0] - env.box_info[2][env.box_num][0]) and (not env.ob_dict['left_foot_left_ground'] and not env.ob_dict['right_foot_left_ground']) and env.vx > 0) or current_pol == "flat"):
            # print("what?", current_base_pol, current_pol)
            # print(env.order, env.order[env.box_num])
            # if (current_pol == 'high_jumps' and (not env.ob_dict['left_foot_left_ground'] and not env.ob_dict['right_foot_left_ground'])) or (current_pol != 'high_jumps'):
            #     print("what?", env.vx)
            
            # This is what we were using:
            # if (current_pol == 'high_jumps' and env.x_min > (env.box_info[1][last_box_num][0] - env.box_info[2][last_box_num][0]) and (not env.ob_dict['left_foot_left_ground'] and not env.ob_dict['right_foot_left_ground']) and env.vx > 0) or (current_pol != 'high_jumps'):

            # I suggest taking out the env.vx condition, and think about foot size, cause x_min goes from com of base and foot
            if (current_pol == 'high_jumps' and (env.x_min - 0.09) > (env.box_info[1][last_box_num][0] - env.box_info[2][last_box_num][0]) and (not env.ob_dict['left_foot_left_ground'] and not env.ob_dict['right_foot_left_ground'])) or (current_pol != 'high_jumps'):

            # if (current_pol == 'high_jumps' and env.x_min > (env.box_info[1][env.box_num][0] - env.box_info[2][env.box_num][0]) and (not env.ob_dict['left_foot_left_ground'] and not env.ob_dict['right_foot_left_ground']) and env.vx > 0) or (current_pol != 'high_jumps'):
            # if (env.x_min > (env.box_info[1][env.box_num][0] - env.box_info[2][env.box_num][0]) and (not env.ob_dict['left_foot_left_ground'] and not env.ob_dict['right_foot_left_ground']) and env.vx > 0) or current_pol == "flat":
            

            # if (current_pol == 'high_jumps' and env.x_min > (env.box_info[1][env.box_num][0] - env.box_info[2][env.box_num][0]) and (env.ob_dict['left_foot_on_ground'] and env.ob_dict['right_foot_on_ground'])) or (current_pol != 'high_jumps'):
            # elif 'flat' in obstacle_types[current_pol] or (env.order[env.box_num-1] == args.obstacle_type and env.order[env.box_num] != args.obstacle_type and (env.ob_dict['left_foot_on_ground'] and env.ob_dict['right_foot_on_ground'] and env.vx > 0)):
            # if (current_pol == 'high_jumps' and np.sqrt(env.vx*env.vx + env.vy*env.vy + env.vz*env.vz) < 0.2) or (current_pol != 'high_jumps'):
            # if (current_pol == 'high_jumps' and env.x_min > (env.box_info[1][env.box_num][0] - env.box_info[2][env.box_num][0]) and (env.ob_dict['left_foot_on_ground'] and env.ob_dict['right_foot_on_ground']) and np.sqrt(env.vx*env.vx + env.vy*env.vy + env.vz*env.vz) < 0.1) or (current_pol != 'high_jumps'):
            # if (current_pol == 'high_jumps' and env.x_min > (env.box_info[1][env.box_num][0] - env.box_info[2][env.box_num][0]) and (env.ob_dict['left_foot_on_ground'] and env.ob_dict['right_foot_on_ground']) and env.vx > 0) or (current_pol != 'high_jumps'):
            # if (current_pol == 'high_jumps' and env.x_min > (env.box_info[1][env.box_num][0] - env.box_info[2][env.box_num][0]) and env.vx > 0 and np.sqrt(env.vx*env.vx + env.vy*env.vy + env.vz*env.vz) < 0.1 and (not env.ob_dict['left_foot_left_ground'] and not env.ob_dict['right_foot_left_ground'])) or (current_pol != 'high_jumps'):
            # if (current_pol == 'high_jumps' and env.x_min > (env.box_info[1][env.box_num][0] - env.box_info[2][env.box_num][0]) and env.vx > 0 and (not env.ob_dict['left_foot_left_ground'] and not env.ob_dict['right_foot_left_ground'])) or (current_pol != 'high_jumps'):
            # if (current_pol == 'high_jumps' and env.x_min > (env.box_info[1][env.box_num][0] - env.box_info[2][env.box_num][0]) and env.vx > 0 and np.sqrt(env.vx*env.vx + env.vy*env.vy + env.vz*env.vz) < 0.1) or (current_pol != 'high_jumps'):
            # if (current_pol == 'high_jumps' and env.x_min > (env.box_info[1][env.box_num][0] - env.box_info[2][env.box_num][0]) and (env.ob_dict['left_foot_on_ground'] and env.ob_dict['right_foot_on_ground']) and abs(env.vz) < 0.05 and env.vx > 0) or (current_pol != 'high_jumps'):
                
                # print("setup not in current_pol", classifier_current_pol)
                # if 'flat' in classifier_current_pol:
                #     # print("flat in classifier_current_pol?", classifier_current_pol)
                #     current_pol = 'flat'
                # # elif classifier_current_pol != current_base_pol:
                # else:
                current_pol = classifier_current_pol + '_setup'
                # print("setting up for ", current_pol, current_base_pol)
                terminate_setup = False
                box_cross_steps = env.steps
        elif 'setup' in current_pol and terminate_setup or (box_cross_steps and env.steps - box_cross_steps > 75):
            # print("switching from ", current_pol, " to ", current_base_pol)
            current_pol = current_base_pol
            box_cross_steps = None

        if args.no_setup and 'setup' in current_pol:
            # print("not using setup for",  current_pol, " instead: ", current_base_pol)
            current_pol = current_base_pol
        if args.no_flat_setup and current_pol == 'flat_setup':
            current_pol = 'flat'

        # # elif 
        # current_base_pol = classifier_current_pol
        # # print(current_base_pol, terminate_setup)
        # if ('setup' in current_pol and (terminate_setup or env.steps - box_cross_steps > 75)) or 'flat' in current_base_pol:
        #     # if not 'flat' in current_base_pol:
        #         # print("finished setting up, or flat")
        #     current_pol = current_base_pol
        #     box_cross_steps = None
        # ===================================================================================

        next_pol = current_pol
        env.set_current_pol(current_pol)

        if env.order[env.box_num] != 'zero':
            if 'setup' in current_pol:
                current_pol_data.append('setup')
                next_pol_data.append('setup')
            else:
                current_pol_data.append(current_pol)
                next_pol_data.append(next_pol)
            
            if env.body_xyz[2] > max_z_height:
                max_z_height = env.body_xyz[2]
            terrain_data.append(env.z_offset)

            if abs(env.foot_pos['left'][0] - env.foot_pos['right'][0]) < 0.1:
                feet_data['left'].append(env.foot_pos['left'][2])
                feet_data['right'].append(env.foot_pos['right'][2])
            else:
                feet_data['left'].append(np.nan)
                feet_data['right'].append(np.nan)
            
            if not env.ob_dict['left_foot_left_ground'] and abs(env.body_xyz[0] - env.foot_pos['left'][0]) < 0.1:
                com_data.append(env.body_xyz[2])
            elif not env.ob_dict['right_foot_left_ground'] and abs(env.body_xyz[0] - env.foot_pos['right'][0]) < 0.1:
                com_data.append(env.body_xyz[2])
            else:
                com_data.append(np.nan)

        if prev_pol != current_pol:
            switch_points.append(env.steps)
        prev_pol = current_pol
        # ===================================================================================
        # This is necessary to set the desired speed
        if args.obstacle_type == 'mix':
            if 'high_jumps' in current_pol:
                env.obstacle_type = 'high_jumps'
                env.original_speed = 0.0

            else:
                env.obstacle_type = 'mix'
                env.original_speed = 1.0

        ob = next_ob
        im = next_im
        ep_ret += rew
        ep_len += 1
        # ===================================================================================
        # done ==============================================================================
        # ===================================================================================
        if done:        
            # "Success", np.mean(np.array(self.success)), self.iters_so_far)
            # logger.record_tabular("Success", np.mean(np.array(self.success)))
            # if ((env.box_info[1][-1][0] - env.box_info[2][-1][0]) < env.body_xyz[0]) and env.steps >= env.max_steps - 2:
            # if env.box_num >= (len(env.box_info[1]) - 3):
            # if env.box_num >= (len(env.box_info[1]) - 4):
            last_artifact = 0
            for n, reverse_order in enumerate(env.order[::-1]):
                if reverse_order not in ["flat", "zero"]:
                    last_artifact = n
                    break
            last_box = len(env.order) - 1 - last_artifact
            if env.box_num > last_box:
                label = True    
            else:
                label = False
            success.append(label)
            # dists.append(env.box_num / (len(env.box_info[1]) - 1))
            # dists.append(env.body_xyz[0])
            dists.append(env.body_xyz[0]/(env.box_info[1][-1][0] + env.box_info[2][-1][0]))
            # env.obstacles.rand_order

            if not label:
                # print(stats['before'])
                # print(stats['current'])
                # print(stats['after'])
                # stats['before'][env.order[env.box_num-1]].append(ep_count)
                # stats['current'][env.order[env.box_num]].append(ep_count)
                # stats['after'][env.order[env.box_num+1]].append(ep_count)
                if env.order[env.box_num-1] + " " + env.order[env.box_num] in stat:
                    stat[env.order[env.box_num-1] + " " + env.order[env.box_num]].append(ep_count)
                else:
                    stat[env.order[env.box_num-1] + " " + env.order[env.box_num]] = [ep_count]

                stats['before'].append(env.order[env.box_num-1])
                stats['current'].append(env.order[env.box_num])
                stats['after'].append(env.order[env.box_num+1])
                
                if artifact == 'flat':
                    counts['failure'].append('high_jumps')
                else:
                    counts['failure'].append(artifact)
            
            # if rank == 0: 
            #     print(counts)

            # if args.record_failures and not metrics['success'][-1] and env.order[env.box_num] == 'gaps':    
            # if rank == 0:
            # if rank == 0 and args.record_failures and not metrics['success'][-1]:    
            if rank == 0 and args.record_failures:    
                try:
                    # os.mkdir(PATH + 'failure' + str(len(metrics['success']) - sum(metrics['success'])))
                    if not label:
                        os.mkdir(PATH + 'failure' + str(ep_count))
                        env.save_sim_data(PATH + 'failure' + str(ep_count) + '/', last_steps=True)
                    else:
                        os.mkdir(PATH + 'success' + str(ep_count))
                        env.save_sim_data(PATH + 'success' + str(ep_count) + '/')
                except:
                    pass
                # env.save_sim_data(PATH + 'failure' + str(len(metrics['success']) - sum(metrics['success'])) + '/', last_steps=True)

                num_axes = 2
                # fig, axes = plt.subplots(num_axes,    figsize=(10, 3*num_axes))            
                #     
                # fig, axes = plt.subplots(num_axes, figsize=(13, 4.5*num_axes), gridspec_kw = {'wspace':0, 'hspace':0.05})                
                fig, axes = plt.subplots(num_axes, figsize=(13, 3.0*num_axes), gridspec_kw = {'wspace':0, 'hspace':0.4})                
                # fig.subplots_adjust(hspace=0.01, wspace=0.01)
                # plt.subplots_adjust(wspace=0, hspace=0)
                # fig, axes = plt.subplots(num_axes, hspace=0.01, wspace=0.01, figsize=(13, 4.5*num_axes))                

                # fig.subplots_adjust(hspace=, wspace=)
                # colours = [np.array([51, 204, 51])/255.0, np.array([0, 102, 204])/255.0, np.array([200, 100, 51])/255.0, np.array([0, 102, 204])/255.0, np.array([255,165,0])/255.0, np.array([100, 204, 0])/255.0, ]
                colours = [np.array([51, 204, 51])/255.0, np.array([51, 51, 204])/255.0, np.array([204, 51, 51])/255.0, np.array([204, 102, 204])/255.0, np.array([204,102,51])/255.0,]
                # for i,name in enumerate(obs_except_flat):

                #     axes[0].plot([_ for _ in range(len(switch_data[name]))], switch_data[name], c=colours[i], linewidth=1.0, alpha=1.0)    
                # axes[0].legend(obs_except_flat)    
                # axes[0].set_title("Switch Estimate", loc='left')
                
                axes[0].plot([_ for _ in range(len(current_pol_data))], current_pol_data, c=np.array([0, 102, 204])/255.0, alpha=1.0)    
                axes[0].plot([_ for _ in range(len(next_pol_data))], next_pol_data, c=np.array([0, 204, 102])/255.0, alpha=1.0)    
                axes[0].set_ylim([-0.9,len(obstacle_types)-1 + 0.1])
                for s, n in enumerate(obstacle_types + ['setup']):
                    if n == 'zero': continue
                    axes[0].text(0,s*0.75+0.5,str(s) + ". " + n)
                axes[0].set_title("Current and Next Policy", loc='left')
                axes[0].legend(["current_policy", "next"], loc='upper center')    
                # axes[1].legend(["current_policy", "next"], loc='lower right')    


                axes[-1].plot([_ for _ in range(len(feet_data['left']))], feet_data['left'], c=colours[0], alpha=1.0)    
                axes[-1].plot([_ for _ in range(len(feet_data['right']))], feet_data['right'], c=colours[1], alpha=1.0)    
                axes[-1].plot([_ for _ in range(len(com_data))], com_data, c=colours[2], alpha=1.0)    
                axes[-1].plot([_ for _ in range(len(terrain_data))], terrain_data, c=colours[3], alpha=1.0)    
                axes[-1].set_title("Terrain Height (m)", loc='left')
                axes[-1].set_ylim([-0.1, max_z_height + 0.1])

                for i in range(num_axes):
                    for xc in switch_points:
                        # axes[i].axvline(x=xc, label='line at x = {}'.format(xc), c=c)
                        axes[i].axvline(x=xc,c=[colours[2][0],colours[2][1],colours[2][2],1.0], linestyle='--', linewidth=1.0)
                    # for xc in detections:
                    #     axes[i].axvline(x=xc,c=[colours[0][0],colours[0][1],colours[0][2],1.0], linestyle='--', linewidth=1.0)

                axes[0].set_xlabel('Timesteps')
                axes[1].set_xlabel('Timesteps')
                # axes[2].set_xlabel('Timesteps')

                # fig.tight_layout(pad=4.0)
                # plt.savefig(PATH + 'failure' + str(metrics["episodes"])    + '/plot.png', bbox_inches='tight')
                # plt.savefig(PATH + 'failure' + str(metrics["episodes"])    + '/plot.png', bbox_inches='tight')
                # plt.savefig(PATH + 'failure' + str(metrics["episodes"])    + '/plot.png')
                if not label:
                    plt.savefig(PATH + 'failure' + str(ep_count)    + '/plot.png')
                else:
                    plt.savefig(PATH + 'success' + str(ep_count)    + '/plot.png')
                plt.close()

            # if args.plot or (rank == 0 and ep_count % 100 == 0):
            #     num_axes = 3
            #     fig, axes = plt.subplots(num_axes,    figsize=(10, 3*num_axes))
            #     axes[0].plot([_ for _ in range(len(vpred_data))], vpred_data, alpha=1.0)    
                
            #     colours = [np.array([0, 102, 204])/255.0, np.array([0, 102, 204])/255.0, np.array([255,165,0])/255.0, np.array([51, 204, 51])/255.0, np.array([100, 204, 0])/255.0, np.array([200, 100, 51])/255.0]
                
            #     print(np.array(current_pol_data).shape, len(current_pol_data))
            #     axes[0].plot([_ for _ in range(len(current_pol_data))], current_pol_data, c=np.array([0, 102, 204])/255.0, alpha=1.0)    
            #     axes[0].set_ylim([-0.9,len(obstacle_types) + 0.1])
            #     for s, n in enumerate(obstacle_types):
            #         axes[0].text(0,s*0.5+0.5,str(s) + ". " + n)
            #     axes[0].set_title("Current policy", loc='left')


            #     axes[1].plot([_ for _ in range(len(baseline_data))], baseline_data, c=np.array([0, 102, 204])/255.0, alpha=1.0)    
            #     axes[1].set_ylim([-0.9,len(obstacle_types) + 0.1])
            #     for s, n in enumerate(obstacle_types):
            #         axes[1].text(0,s*0.5+0.5,str(s) + ". " + n)
            #     axes[1].set_title("baseline data", loc='left')

            #     axes[2].plot([_ for _ in range(len(terrain_data))], terrain_data, c=np.array([0, 102, 204])/255.0, alpha=1.0)    
            #     axes[2].set_title("terrain height", loc='left')
            #     fig.tight_layout(pad=4.0)
            #     print(PATH + 'dqn.png')
            #     plt.savefig(PATH + 'dqn.png', bbox_inches='tight') 

            vpred_data = []
            vpred_orig_data = []
            probs_data = []
            terrain_data = []
            current_pol_data = []
            next_pol_data = []
            baseline_data = []
            switch_data = []
            feet_data = {'left':[],'right':[]}
            com_data = []
            switch_points = []
            max_z_height = 0
            
            # env.obstacle_type = 'mix'
            # env.original_speed = 0.0

            # if rank == 0:
            #     print(ep_len, env.steps, env.max_steps)
            env.obstacle_type = 'mix'
            ob = env.reset(evaluate=True)
            im = env.get_im()
            # if env.obstacle_type != "mix":
            #     print("obstacle_type ", env.obstacle_type)
            # desired_pol = obstacle_types.index(env.order[env.box_num])
            # if args.no_state:
            #     # dqn_ob = np.array([desired_pol])
            #     dqn_ob = np.array([0])
            #     desired = np.zeros(len(obstacle_types))
            #     desired[desired_pol] = 1
            #     dqn_ob = np.concatenate([dqn_ob, desired])
            #     # dqn_im = np.array([0])
            #     if args.use_boxes:
            #         boxes = []
            #         for box in env.box_nums:
            #             info = []
            #             if (env.box_num + box < 1) or (env.box_num + box > len(env.box_info[0]) - 1):
            #                 info.append(0)
            #             else:
            #                 info.append(env.body_xyz[2] - (env.box_info[1][env.box_num+box][2] + env.box_info[2][env.box_num+box][2]))
            #             info.append(env.body_xyz[0] - (env.box_info[1][env.box_num+box][0] + env.box_info[2][env.box_num+box][0]))
            #             info.append(env.body_xyz[1])
            #             info.append(env.yaw)
            #             boxes.extend(info)
            #         dqn_im =np.array(boxes)
            #     else:
            #         dqn_im = env.get_im()
            # else:
            #     dqn_ob = ob
            #     # desired = np.zeros(len(obstacle_types))
            #     # desired[desired_pol] = 1
            #     # dqn_ob = np.concatenate([dqn_ob, desired])

            #     # dqn_im = im
            #     dqn_im = env.get_im()

            ep_rets.append(ep_ret)    
            ep_lens.append(ep_len)         
            ep_ret = 0
            ep_len = 0
            ep_count += 1    
            # print(env.order)
            

            # box_cross_steps = None
            # next_pol = current_pol
            
            # vpreds = dqn.get_vpreds(dqn_ob, dqn_im)
            # current_pol = np.argmax(vpreds)
            # vpred = vpreds[current_pol]
    
            prev_pol = current_pol = obstacle_types[0]
            active = None
            current_base_pol = current_pol
            terminate_setup = False
            prev_artifact = artifact = None

if __name__ == '__main__':
    
    # My hack for loading defaults, but still accepting run specific defaults
    import defaults
    from dotmap import DotMap
    args1, unknown1 = defaults.get_defaults() 
    parser = argparse.ArgumentParser()
    # Arguments that are specific for this run (including run specific defaults, ignore unknown arguments)
    parser.add_argument('--folder', default='multi')
    # parser.add_argument('--single_pol', default=True, action='store_false')
    parser.add_argument('--single_pol', default=False, action='store_true')
    parser.add_argument('--fixed_sequence', default=False, action='store_true')
    parser.add_argument('--new_rand', default=True, action='store_false')

    parser.add_argument('--obstacle_type', default='mix')
    # parser.add_argument('--obstacle_type', default='high_jumps')
    parser.add_argument('--difficulty', default=10, type=int)
    parser.add_argument('--jump_height', default=0.5, type=float)
    parser.add_argument('--terrain_count', default=7, type=int)
    parser.add_argument('--advantage2', default=False, action='store_true')
    parser.add_argument('--share_vis', default=True, action='store_false')
    parser.add_argument('--use_classifier', default=False, action='store_true')
    parser.add_argument('--no_setup', default=False, action='store_true')
    parser.add_argument('--no_flat_setup', default=False, action='store_true')
    parser.add_argument('--train_select', default=True, action='store_false')
    parser.add_argument('--record_failures', default=True, action='store_false')
    # parser.add_argument('--easy_flat', default=True, action='store_false')
    parser.add_argument('--easy_flat', default=False, action='store_true')
    parser.add_argument('--sleep', default=0.0001, type=float)
    parser.add_argument('--detection_dist', default=0.9, type=float)
    args2, unknown2 = parser.parse_known_args()
    args2 = vars(args2)
    # Replace any arguments from defaults with run specific defaults
    for key in args2:
        args1[key] = args2[key]
    # Look for any changes to defaults (unknowns) and replace values in args1
    for n, unknown in enumerate(unknown2):
        if "--" in unknown and n < len(unknown2)-1 and "--" not in unknown2[n+1]:
            arg_type = type(args1[unknown[2:]])
            args1[unknown[2:]] = arg_type(unknown2[n+1])
    args = DotMap(args1)
    # Check for dodgy arguments
    unknowns = []
    for unknown in unknown1 + unknown2:
        if "--" in unknown and unknown[2:] not in args:
            unknowns.append(unknown)
    if len(unknowns) > 0:
        print("Dodgy argument")
        print(unknowns)
        exit()
    os.environ["CUDA_VISIBLE_DEVICES"]="-1"
    run(args)