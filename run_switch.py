import os, inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' 
import tensorflow as tf
tf.get_logger().setLevel('DEBUG')
import numpy as np
import argparse
import tensorboardX
from collections import deque   
from scripts.utils import *
from baselines.common.mpi_util import sync_from_root
import random
from mpi4py import MPI
import time
from pathlib import Path
home = str(Path.home())
from baselines import logger


def run(args):

  PATH = home + '/results/biped_final/latest/' + args.folder + '/' + args.exp + '/'

  comm = MPI.COMM_WORLD
  rank = comm.Get_rank()
  myseed = args.seed + 10000 * rank
  np.random.seed(myseed)
  random.seed(myseed)
  tf.set_random_seed(myseed)
  
  logger.configure(dir=PATH)
  if rank == 0:
    writer = tensorboardX.SummaryWriter(log_dir=PATH)
  else: 
    writer = None 

  # sess = tf.Session()
  gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction= 0.1)
  sess = tf.InteractiveSession(config=tf.ConfigProto(inter_op_parallelism_threads=1,
                                          intra_op_parallelism_threads=1,             
                                          gpu_options=gpu_options), graph=None)
 
  horizon = 2048

  if args.mocap:
    from assets.env_mocap import Env
  else:
    from assets.env_pb_biped_switch import Env
  env = Env(render=args.render, PATH=PATH, args=args, cur=args.cur, obstacle_type=args.obstacle_type, control_type=args.control_type, display_hm=args.display_hm, vis=args.vis, speed_cur=args.speed_cur)

  if args.lstm_pol:
    from models.ppo_lstm import Model
    # from models.ppo_lstm_old import Model
  else:
    # if args.separate_vf:
    #   from models.ppo_vf import Model
    # else:
    #   from models.ppo import Model
    from models.ppo_switch import Model


  pol = Model(args.obstacle_type, env=env, ob_size=env.ob_size, ac_size=env.ac_size, im_size=env.im_size, args=args, PATH=PATH, horizon=horizon, writer=writer, max_timesteps=args.max_ts, vis=args.vis)
  
  initialize()
  sync_from_root(sess, pol.vars, comm=comm)
  if args.separate_vf:
    pol.set_training_params(max_timesteps=args.max_ts, learning_rate=args.lr, vf_learning_rate=args.vf_lr, horizon=horizon)
  else:
    pol.set_training_params(max_timesteps=args.max_ts, learning_rate=args.lr, horizon=horizon)

  if args.test_pol:
    if args.hpc:
      # pol.load(home + '/hpc-home/results/biped_model/latest/' + args.exp + '/')      
      # pol.load(home + '/hpc-home/results/biped_model/weights/w6/base_speed/')      
      # pol.load(home + '/hpc-home/results/biped_model/weights/w5/b/' + args.exp + '/')      
      pol.load(home + '/Dropbox/robot/mybot_ws/results_hpc/switch1/' + args.obstacle_type + '_no_overlap1/')      
      # pol.load(home + '/Dropbox/robot/mybot_ws/results_hpc/switch1/' + args.obstacle_type + '__/')      

    else:
      # pol.load(home + '/results/biped_model/weights/b2/' + args.exp + '/')      
      # pol.load(home + '/results/biped_model/weights/b10_vis/' + args.obstacle_type + '/')      
      # pol.load(home + '/Dropbox/robot/mybot_ws/results_hpc/switch/' + args.obstacle_type + '_no_overlap1/')      
      pol.load(home + '/results/biped_model/weights/b10_vis/' + args.obstacle_type    + '/')      


      # pol.load(PATH)

  if args.use_base:
    base = Model("base", env=env, ob_size=env.ob_size, ac_size=env.ac_size, im_size=env.im_size, args=args, PATH=PATH, vis=args.vis)
    if args.hpc:
      # pol.load_base(base, home + '/hpc-home/results/biped_model/weights/w6/base_speed/')      
      pol.load_base(base, home + '/hpc-home/results/biped_model/latest/' +  args.exp)      
    else:
      # pol.load_base(base, home + '/results/biped_model/weights/w5/b2/base_speed/')
      pol.load_base(base, home + '/results/biped_model/latest/b2/base/')      


  # Throw an error if the graph grows (shouldn't change once everything is initialised)
  # tf.get_default_graph().finalize()
  # print(env.ob_size)

  prev_done = True
  ob = env.reset()
  im = env.get_hm()
  ep_ret = 0
  ep_len = 0
  ep_rets = []
  ep_lens = []
  ep_steps = 0

  if args.lstm_pol:
    pol_states = [[np.zeros([1,128]), np.zeros([1,128])], [np.zeros([1,128]), np.zeros([1,128])]]

  if args.test_pol:
    stochastic = False
  else:
    stochastic = True
  
  while True:
    if pol.timesteps_so_far > pol.max_timesteps:
      break 
    if args.lstm_pol:
      act, vpred, pol_states, nlogp = pol.step(ob, im, stochastic=stochastic, states=pol_states)
    else:
      act, vpred, _, nlogp = pol.step(ob, im, stochastic=stochastic)

    torques = act

    next_ob, rew, done, _ = env.step(torques)
    next_im = env.get_hm()

    if not args.test_pol:
      pol.add_to_buffer([ob, im, act, rew, prev_done, vpred, nlogp])
    prev_done = done
    ob = next_ob
    im = next_im
    ep_ret += rew
    ep_len += 1
    ep_steps += 1
    
    if not args.test_pol and ep_steps % horizon == 0:
      if args.lstm_pol:
        _, vpred, _, _ = pol.step(next_ob, next_im, stochastic=True, states=pol_states)
      else:
        _, vpred, _, _ = pol.step(next_ob, next_im, stochastic=True)
      pol.finalise_buffer({"ep_rets":ep_rets, "ep_lens":ep_lens}, last_value=vpred, last_done=done)  
      pol.run_train(ep_rets,ep_lens)
      ep_rets = []
      ep_lens = []
      if args.vf_only and np.random.random() < 0.01:
        env.obstacle_type = 'mix'
      else:
        env.obstacle_type = args.obstacle_type
      
    if done:
      ob = env.reset()
      im = env.get_hm()
      if args.lstm_pol: 
        pol_states = [[np.zeros([1,128]), np.zeros([1,128])], [np.zeros([1,128]), np.zeros([1,128])]]  
      ep_rets.append(ep_ret)  
      ep_lens.append(ep_len)     
      ep_ret = 0
      ep_len = 0        

if __name__ == '__main__':

  parser = argparse.ArgumentParser()

  # parser.add_argument('--cur_len', default=1000, type=int)
  
  # Original from biped_model:
  parser.add_argument('--cur_len', default=1200, type=int)
  # Needed to increase:
  # parser.add_argument('--cur_len', default=2000, type=int)
  
  parser.add_argument('--inc', default=1, type=int)
  parser.add_argument('--difficulty', default=1, type=int)
  parser.add_argument('--no_overlap', default=False, action='store_true')
  parser.add_argument('--single_pol', default=False, action='store_true')
  parser.add_argument('--use_roa', default=False, action='store_true')
  parser.add_argument('--baseline', default=False, action='store_true')
  parser.add_argument('--rand_flat', default=False, action='store_true')

  parser.add_argument('--folder', default='switch')
  parser.add_argument('--nicks', default=False, action='store_true')
  parser.add_argument('--vf_only', default=False, action='store_true')
  parser.add_argument('--speed_cur', default=False, action='store_true')
  parser.add_argument('--expert', default=False, action='store_true')
  parser.add_argument('--use_base', default=False, action='store_true')
  parser.add_argument('--separate_vf', default=False, action='store_true')
  parser.add_argument('--display_doa', default=False, action='store_true')
  parser.add_argument('--act', default=False, action='store_true')
  parser.add_argument('--forces', default=False, action='store_true')
  parser.add_argument('--render', default=False, action='store_true')
  parser.add_argument('--hpc', default=False, action='store_true')
  parser.add_argument('--lstm_pol', default=False, action='store_true')
  parser.add_argument('--test_pol', default=False, action='store_true')
  # parser.add_argument('--vis', default=False, action='store_true')
  parser.add_argument('--vis', default=True, action='store_false')
  parser.add_argument('--cur', default=False, action='store_true')
  parser.add_argument('--mocap', default=False, action='store_true')
  parser.add_argument('--display_hm', default=False, action='store_true')
  parser.add_argument('--const_std', default=False, action='store_true')
  parser.add_argument('--exp', default="test")
  parser.add_argument('--obstacle_type', default="base", help="flat, stairs, path, jump")
  parser.add_argument('--control_type', default="walk", help="stop, slow,  walk, run")
  parser.add_argument('--seed', default=42, type=int)
  # parser.add_argument('--max_ts', default=10e7, type=int)
  parser.add_argument('--max_ts', default=20e7, type=int)
  parser.add_argument('--lr', default=3e-4, type=float)
  # parser.add_argument('--vf_lr', default=1e-2, type=float)
  parser.add_argument('--vf_lr', default=3e-4, type=float)
  args = parser.parse_args()
  os.environ["CUDA_VISIBLE_DEVICES"]="-1"
  run(args)