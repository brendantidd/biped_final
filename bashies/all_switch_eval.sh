#!/bin/bash

csiro=true
# csiro=false

# ==================================================================================
# Single terrain: default initial disturbance = 100, final = 1000
# ==================================================================================

Terrains="jumps gaps stairs"

declare -a Arguments=("_just_base")

for terrain in $Terrains; do 
    for (( i=0; i<${#Arguments[@]}; i++ )); do 
        mpirun -np 16 python3 run_switch_eval.py --folder switch_data --test_pol --obstacle_type $terrain --exp "$terrain${Arguments[$i]}" --difficulty 10
    done
done
