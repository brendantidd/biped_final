#!/bin/bash
# sbatch <<EOT
# #!/bin/bash
# # mpirun -np 16 python $4 --seed 84 --exp $1_$2 --obstacle_type $1 $3
echo mpirun -np 16 python $4 --seed 84 --exp $1_$2 --obstacle_type $1 $3

# exit 0
# EOT