#!/bin/bash

csiro=true
csiro=false

# ==================================================================================
# Ours
# ==================================================================================
# Terrains="high_jumps"
# declare -a Experiments=(
#                         "setup"
#                         "select"
#                         "select_share"
#                         "select_no_extra_rew"
#                         )
# declare -a Arguments=(
#                       "--folder all --max_ts 25000000"
#                       "--folder all --max_ts 25000000 --train_select"
#                       "--folder all --max_ts 25000000 --train_select --share_vis"
#                       "--folder all --max_ts 25000000 --train_select --no_extra_rew"
#                       )

# for terrain in $Terrains; do 
#     for (( i=0; i<${#Arguments[@]}; i++ )); do 
#       if [ "$csiro" = true ]
#       then
#         sbatch ./base_run_dqn_with_setup5.sh $terrain ${Experiments[$i]} "${Arguments[$i]}"
#       else
#         ./base_dqn_with_setup5.sh $terrain ${Experiments[$i]} "${Arguments[$i]}"
#       fi
#     done
# done

# ==================================================================================
# Rewards
# ==================================================================================
# Terrains="high_jumps"
# declare -a Experiments=(
#                         "tor"
#                         "const"
#                         "rew"
#                         )
# declare -a Arguments=(
#                       "--folder all --max_ts 25000000 --train_select --rew tor"
#                       "--folder all --max_ts 25000000 --train_select --rew const"
#                       "--folder all --max_ts 25000000 --train_select --rew rew"
#                       )

# for terrain in $Terrains; do 
#     for (( i=0; i<${#Arguments[@]}; i++ )); do 
#       if [ "$csiro" = true ]
#       then
#         sbatch ./base_run_dqn_with_setup5.sh $terrain ${Experiments[$i]} "${Arguments[$i]}"
#       else
#         ./base_dqn_with_setup5.sh $terrain ${Experiments[$i]} "${Arguments[$i]}"
#       fi
#     done
# done

# ==================================================================================
# Other terrains
# ==================================================================================
# Terrains="flat jumps gaps stairs steps"
# declare -a Experiments=(
#                         "select"
#                         )
# declare -a Arguments=(
#                       "--folder all --max_ts 25000000 --train_select"
#                       )

# for terrain in $Terrains; do 
#     for (( i=0; i<${#Arguments[@]}; i++ )); do 
#       if [ "$csiro" = true ]
#       then
#         sbatch ./base_run_dqn_with_setup5.sh $terrain ${Experiments[$i]} "${Arguments[$i]}"
#       else
#         ./base_dqn_with_setup5.sh $terrain ${Experiments[$i]} "${Arguments[$i]}"
#       fi
#     done
# done

# ==================================================================================
# Transition paper
# ==================================================================================
# Terrains="high_jumps"
# declare -a Experiments=("transition")
# declare -a Arguments=("--folder all --max_ts 25000000")

# for terrain in $Terrains; do 
#     for (( i=0; i<${#Arguments[@]}; i++ )); do 
#       if [ "$csiro" = true ]
#       then
#         sbatch ./base_run_transition.sh $terrain ${Experiments[$i]} "${Arguments[$i]}"
#       else
#         ./base_run_transition.sh $terrain ${Experiments[$i]} "${Arguments[$i]}"
#       fi
#     done
# done

# ==================================================================================
# End to end
# ==================================================================================
# Terrains="high_jumps"
# declare -a Experiments=(
#                         "e2e_cur_flat"
#                         "e2e_cur"
#                         "e2e_terrain_cur"
#                         "e2e_no_cur"
#                         )
# declare -a Arguments=(
#                       "--folder all --max_ts 100000000 --single_pol --cur --e2e --dqn --difficulty 1 --num_artifacts 1 --flat_e2e"
#                       "--folder all --max_ts 100000000 --single_pol --cur --e2e --dqn --difficulty 1 --num_artifacts 1"
#                       "--folder all --max_ts 100000000 --single_pol --e2e --dqn --difficulty 1 --num_artifacts 1"
#                       "--folder all --max_ts 100000000 --single_pol --e2e --dqn --difficulty 10 --num_artifacts 1"
#                       )

# for terrain in $Terrains; do 
#     for (( i=0; i<${#Arguments[@]}; i++ )); do 
#       if [ "$csiro" = true ]
#       then
#         sbatch ./base.sh $terrain ${Experiments[$i]} "${Arguments[$i]}"
#       else
#         ./base_run.sh $terrain ${Experiments[$i]} "${Arguments[$i]}"
#       fi
#     done
# done

# ==================================================================================
# DQN without setup
# ==================================================================================
# Terrains="high_jumps"
# declare -a Experiments=("dqn_no_setup")
# declare -a Arguments=("--folder all --max_ts 25000000")

# for terrain in $Terrains; do 
#     for (( i=0; i<${#Arguments[@]}; i++ )); do 
#       if [ "$csiro" = true ]
#       then
#         sbatch ./base_run_dqn_no_setup.sh $terrain ${Experiments[$i]} "${Arguments[$i]}"
#       else
#         ./base_run_dqn_no_setup.sh $terrain ${Experiments[$i]} "${Arguments[$i]}"
#       fi
#     done
# done
