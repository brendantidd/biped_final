#!/bin/bash

csiro=true
# csiro=false

# ==================================================================================
# Single terrain: default initial disturbance = 100, final = 1000
# ==================================================================================
# Terrains="base gaps jumps stairs"
# declare -a Experiments=(
#                         "_"
#                         )
# declare -a Arguments=(
#                       "--cur --max_ts 100000000"
#                       )

# for terrain in $Terrains; do 
#   for (( i=0; i<${#Arguments[@]}; i++ )); do 
#     if [ "$csiro" = true ]
#     then
#       sbatch ./base_csiro.sh "run_switch.py" $terrain ${Experiments[$i]} "${Arguments[$i]}" "20:00:00"
#     else
#       ./base_qut.sh "run_switch.py" $terrain ${Experiments[$i]} "${Arguments[$i]}" "20:00:00"
#     fi
#   done
# done

# ==================================================================================
# Single terrain, no overlap: default initial disturbance = 100, final = 1000
# ==================================================================================
Terrains="gaps jumps stairs"
declare -a Experiments=(
                        "no_overlap"
                        )
declare -a Arguments=(
                      "--cur --max_ts 100000000 --no_overlap"
                      )

for terrain in $Terrains; do 
  for (( i=0; i<${#Arguments[@]}; i++ )); do 
    if [ "$csiro" = true ]
    then
      sbatch ./base_csiro.sh "run_switch.py" $terrain ${Experiments[$i]} "${Arguments[$i]}" "20:00:00"
    else
      ./base_qut.sh "run_switch.py" $terrain ${Experiments[$i]} "${Arguments[$i]}" "20:00:00"
    fi
  done
done

# ==================================================================================
# End-to-end terrain
# ==================================================================================
# Terrains="mix"
# declare -a Experiments=(
#                         "_"
#                         )
# declare -a Arguments=(
#                       "--cur --max_ts 300000000"
#                       )

# for terrain in $Terrains; do 
#   for (( i=0; i<${#Arguments[@]}; i++ )); do 
#     if [ "$csiro" = true ]
#     then
#       sbatch ./base_csiro.sh "run_switch.py" $terrain ${Experiments[$i]} "${Arguments[$i]}" "40:00:00"
#     else
#       ./base_qut.sh "run_switch.py" $terrain ${Experiments[$i]} "${Arguments[$i]}" "40:00:00"
#     fi
#   done
# done

# ==================================================================================
# DQN terrain
# ==================================================================================
# Terrains="mix"
# declare -a Experiments=(
#                         ""
#                         )
# declare -a Arguments=(
#                       "--cur --max_ts 10000000"
#                       )

# for terrain in $Terrains; do 
#   for (( i=0; i<${#Arguments[@]}; i++ )); do 
#     if [ "$csiro" = true ]
#     then
#       sbatch ./base_csiro.sh "run_switch.py" $terrain ${Experiments[$i]} "${Arguments[$i]}" "20:00:00"
#     else
#       ./base_qut.sh "run_switch.py" $terrain ${Experiments[$i]} "${Arguments[$i]}" "20:00:00"
#     fi
#   done
# done