#!/bin/bash

# ==================================================================================
# Setup ----------------------------------------------------------------------------
# ==================================================================================
Terrains="high_jumps"
# Terrains="high_jumps gaps jumps stairs steps"
declare -a Experiments=(
                        # "4"
                        # "4_pause"
                        # "from_orig"
                        # "from_orig_w_vf"
                        # "new_hotness"
                        # "pause"
                        # "dont_pause"
                        # "pause"
                        # "dont_pause2"
                        # "pause2"
                        # "no_pause_disturb"
                        # "pause_disturb"
                        # "pause_disturb_2"
                        # "no_pause_disturb_30"
                        # "pause_disturb_30"
                        # "pause_disturb_window_90"
                        # "pause_disturb_window_75"
                        # "pause_disturb_final_60_vpred"
                        # "pause_disturb_final_60_rew"
                        # "pause_disturb_final_60"
                        # "pause_disturb_final_60_30"
                        # "pause_disturb_final_60_2"
                        # "pause_disturb_transition_no_extra"
                        # "pause_disturb_transition_extra"
                        # "no_pause"
                        # "a_08_b_002"
                        # "a_05_b_001"
                        # "a_04_b_001"
                        # "a_04_b_002"
                        # "old"
                        # "new"
                        # "old"
                        # "rand_dist_750"
                        # "rand_dist_500"
                        # "rand"
                        # "dist"

                        # "new_rand" 
                        # "new_rand2" 
                        # "old" 

                        # "new_rand3"
                        "new_rand4"
                        )
declare -a Arguments=(
                      # "--folder all --max_ts 20000000 --num_artifacts 1 --no_disturbance --final_disturbance 500 --seed 21"
                      # "--folder all --max_ts 20000000 --num_artifacts 1 --no_disturbance --final_disturbance 500 --seed 21 --pause_terminate"
                      # "--folder all --max_ts 20000000 --num_artifacts 1 --no_disturbance --final_disturbance 500 --seed 21 --from_original"
                      # "--folder all --max_ts 20000000 --num_artifacts 1 --no_disturbance --final_disturbance 500 --seed 21 --from_original --from_original_with_vf"
                      # "--folder all --max_ts 10000000 --num_artifacts 1 --new_hotness"
                      # "--folder all --max_ts 10000000 --num_artifacts 1 --pause_terminate"
                      # "--folder final --max_ts 10000000 --terrain_count 2"
                      # "--folder final --max_ts 10000000 --terrain_count 2 --pause_terminate"
                      # "--folder final --max_ts 15000000 --terrain_count 2"
                      # "--folder final --max_ts 15000000 --terrain_count 2 --pause_terminate"
                      # "--folder final --max_ts 20000000 --terrain_count 2"
                      # "--folder final --max_ts 20000000 --terrain_count 2 --pause_terminate"
                      # "--folder final --max_ts 30000000 --terrain_count 2"
                      # "--folder final --max_ts 30000000 --terrain_count 2 --pause_terminate"
                      # "--folder final --max_ts 20000000 --terrain_count 1 --no_disturbance --final_disturbance 500"
                      # "--folder final --max_ts 20000000 --terrain_count 1 --pause_terminate --no_disturbance --final_disturbance 500"
                      # "--folder final --max_ts 20000000 --terrain_count 2 --pause_terminate --no_disturbance --final_disturbance 500"
                      # "--folder final --max_ts 30000000 --terrain_count 1 --no_disturbance --final_disturbance 500"
                      # "--folder final --max_ts 30000000 --terrain_count 1 --pause_terminate --no_disturbance --final_disturbance 500"
                      # "--folder final --max_ts 20000000 --terrain_count 1 --pause_terminate --no_disturbance --final_disturbance 500 --initial_window 90"
                      # "--folder final --max_ts 20000000 --terrain_count 1 --pause_terminate --no_disturbance --final_disturbance 500 --initial_window 75"
                      # "--folder final --max_ts 20000000 --terrain_count 1 --pause_terminate --no_disturbance --rew vpred --final_disturbance 500 --final_window 60"
                      # "--folder final --max_ts 20000000 --terrain_count 1 --pause_terminate --no_disturbance --rew rew --final_disturbance 500 --final_window 60"
                      # "--folder final --max_ts 20000000 --terrain_count 1 --pause_terminate --no_disturbance --final_disturbance 500 --final_window 60"
                      # "--folder final --max_ts 30000000 --terrain_count 1 --pause_terminate --no_disturbance --final_disturbance 500 --final_window 60"
                      # "--folder final --max_ts 20000000 --terrain_count 2 --pause_terminate --no_disturbance --final_disturbance 500 --final_window 60"
                      # "--folder final --max_ts 20000000 --terrain_count 1 --pause_terminate --no_disturbance --final_disturbance 500 --extra_on_terminate --final_window 60 --transition"
                      # "--folder final --max_ts 20000000 --terrain_count 1 --pause_terminate --no_disturbance --final_disturbance 500 --final_window 60 --transition"
                      # "--folder final --max_ts 20000000 --terrain_count 1 --no_disturbance --final_disturbance 500 --final_window 60"
                      # "--folder final --max_ts 20000000 --terrain_count 1 --alpha 0.08 --beta 0.02 --pause_terminate --no_disturbance --final_disturbance 500 --final_window 60"
                      # "--folder final --max_ts 20000000 --terrain_count 1 --alpha 0.05 --beta 0.01 --pause_terminate --no_disturbance --final_disturbance 500 --final_window 60"
                      # "--folder final --max_ts 20000000 --terrain_count 1 --alpha 0.04 --beta 0.01 --pause_terminate --no_disturbance --final_disturbance 500 --final_window 60"
                      # "--folder final --max_ts 20000000 --terrain_count 1 --alpha 0.04 --beta 0.02 --pause_terminate --no_disturbance --final_disturbance 500 --final_window 60"
                      # "--folder final --max_ts 20000000 --terrain_count 1 --pause_terminate --no_disturbance --final_disturbance 500 --final_window 60"
                      # "--folder final --max_ts 20000000 --old_rew --terrain_count 1 --pause_terminate --no_disturbance --final_disturbance 500 --final_window 60"
                      # "--folder final --max_ts 20000000 --old_rew --terrain_count 1 --pause_terminate --no_disturbance --final_disturbance 750 --final_window 60 --new_rand --new_dist"
                      # "--folder final --max_ts 20000000 --old_rew --terrain_count 1 --pause_terminate --no_disturbance --final_disturbance 500 --final_window 60 --new_rand --new_dist"
                      # "--folder final --max_ts 20000000 --old_rew --terrain_count 1 --pause_terminate --no_disturbance --final_disturbance 500 --final_window 60 --new_rand"
                      # "--folder final --max_ts 20000000 --old_rew --terrain_count 1 --pause_terminate --no_disturbance --final_disturbance 500 --final_window 60 --new_dist"

                      # "--folder final --max_ts 20000000 --old_rew --terrain_count 1 --pause_terminate --new_rand"
                      # "--folder final --max_ts 20000000 --old_rew --terrain_count 1 --pause_terminate --new_rand"
                      # "--folder final --max_ts 20000000 --old_rew --terrain_count 1 --pause_terminate"

                      "--folder final --max_ts 20000000 --old_rew --terrain_count 1 --pause_terminate --new_rand"

                      )

for terrain in $Terrains; do 
    for (( i=0; i<${#Arguments[@]}; i++ )); do 
      ./base_qut.sh "run_setup_final.py" $terrain ${Experiments[$i]} "${Arguments[$i]}" "30:00:00"
    done
done


# ==================================================================================
# Transition -----------------------------------------------------------------------
# ==================================================================================
# Terrains="high_jumps"
# declare -a Experiments=(
#                         "pause_disturb_transition_no_extra"
#                         "pause_disturb_transition_extra"
#                         "no_pause_transition"
#                         )
# declare -a Arguments=(
#                       "--folder final --max_ts 20000000 --terrain_count 1 --new_rand --pause_terminate --no_disturbance --final_disturbance 500 --extra_on_terminate --final_window 60 --transition"
#                       "--folder final --max_ts 20000000 --terrain_count 1 --new_rand --pause_terminate --no_disturbance --final_disturbance 500 --final_window 60 --transition"
#                       "--folder final --max_ts 20000000 --terrain_count 1 --new_rand --no_disturbance --final_disturbance 500 --final_window 60 --transition"
#                       )

# for terrain in $Terrains; do 
#     for (( i=0; i<${#Arguments[@]}; i++ )); do 
#       ./base_qut.sh "run_setup_final.py" $terrain ${Experiments[$i]} "${Arguments[$i]}" "20:00:00"
#     done
# done

# ==================================================================================
# All terrains ---------------------------------------------------------------------
# ==================================================================================
# Terrains="gaps jumps stairs steps"
# declare -a Experiments=(
#                         "pause"
#                         )
# declare -a Arguments=(
#                       "--folder final --max_ts 20000000 --terrain_count 1 --pause_terminate --no_disturbance --final_disturbance 500 --final_window 60"
#                       )

# for terrain in $Terrains; do 
#     for (( i=0; i<${#Arguments[@]}; i++ )); do 
#       ./base_qut.sh "run_setup_final.py" $terrain ${Experiments[$i]} "${Arguments[$i]}" "20:00:00"
#     done
# done

# ==================================================================================
# E2e ------------------------------------------------------------------------------
# ==================================================================================
# Terrains="high_jumps"
# # Terrains="high_jumps gaps jumps stairs steps"
# declare -a Experiments=(
#                         "e2e"
#                         )
# declare -a Arguments=(
#                       "--folder final --terrain_count 1 --seed 21 --cur --terrain_first --train_setup"
#                       )

# for terrain in $Terrains; do 
#     for (( i=0; i<${#Arguments[@]}; i++ )); do 
#       ./base_qut.sh "run_single.py" $terrain ${Experiments[$i]} "${Arguments[$i]}" "30:00:00"
#     done
# done



