#!/bin/bash
sbatch <<EOT
#!/bin/bash
#SBATCH --job-name=$2_$3
#SBATCH --time=$5
#SBATCH --mem=48g
#SBATCH --ntasks-per-node=16
#SBATCH --nodes=1
ulimit -s 10240
module load openmpi python/3.6.1
cd /home/tid010/biped_final
mpirun -np 16 python $1 --seed 84 --exp $2_$3 --obstacle_type $2 $4

hostname

exit 0
EOT