#!/bin/bash

csiro=true
# csiro=false

# ==================================================================================
# Single terrain: default initial disturbance = 100, final = 1000
# ==================================================================================

Terrains="jumps gaps stairs"

# # Get data
# declare -a Arguments=("_data")

# for terrain in $Terrains; do 
#     for (( i=0; i<${#Arguments[@]}; i++ )); do 
#         mpirun -np 16 --oversubscribe python3 run_switch_get_data.py --folder switch_data --obstacle_type $terrain --exp "$terrain${Arguments[$i]}" 
#     done
# done

# declare -a Arguments=("_base_only_data")

# for terrain in $Terrains; do 
#     for (( i=0; i<${#Arguments[@]}; i++ )); do 
#         mpirun -np 16 --oversubscribe python3 run_switch_get_data.py --folder switch_data --base_only --obstacle_type $terrain --exp "$terrain${Arguments[$i]}" 
#     done
# done

# for terrain in $Terrains; do 
#     for (( i=0; i<${#Arguments[@]}; i++ )); do 
#         mpirun -np 16 --oversubscribe python3 run_switch_get_data.py --folder switch_data --base_only --use_no_overlap --obstacle_type $terrain --exp "$terrain${Arguments[$i]}" 
#     done
# done

# use_no_overlap

# # Train models
# declare -a Arguments=("_just_base")
# declare -a Arguments=("")

# for terrain in $Terrains; do 
#     for (( i=0; i<${#Arguments[@]}; i++ )); do 
#         python3 models/switch_doa.py --folder switch_data --obstacle_type $terrain --exp "$terrain${Arguments[$i]}" 
#     done
# done

# # Eval
# declare -a Arguments=("_just_base_eval")

# for terrain in $Terrains; do 
#     for (( i=0; i<${#Arguments[@]}; i++ )); do 
#         mpirun -np 16 --oversubscribe python3 run_switch_eval.py --folder switch_data --test_pol --obstacle_type $terrain --exp "$terrain${Arguments[$i]}" --difficulty 10
#     done
# done

# Eval
# mpirun -np 16 --oversubscribe 
python3 run_switch_multi.py --folder switch_data --use_roa
