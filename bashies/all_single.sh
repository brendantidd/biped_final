#!/bin/bash

csiro=true
# csiro=false

# ==================================================================================
# Ours single terrain
# ==================================================================================
Terrains="gaps jumps stairs"
declare -a Experiments=(
                        "1"
                        "2"
                        )
declare -a Arguments=(
                      "--cur --max_ts 70000000 --num_artifacts 1 --initial_disturbance 500 --final_disturbance 500"
                      )

for terrain in $Terrains; do 
    for (( i=0; i<${#Arguments[@]}; i++ )); do 
      if [ "$csiro" = true ]
      then
        sbatch ./base_csiro.sh "run_single.py" $terrain ${Experiments[$i]} "${Arguments[$i]}" "20:00:00"
      else
        ./base_qut.sh "run_single.py" $terrain ${Experiments[$i]} "${Arguments[$i]}" "20:00:00"
      fi
    done
done
