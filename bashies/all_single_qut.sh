#!/bin/bash

# csiro=true
csiro=false

# ==================================================================================
# Ours single terrain
# ==================================================================================
Terrains="high_jumps"
# Terrains="high_jumps gaps jumps stairs steps"
declare -a Experiments=(
                        "1"
                        "2"
                        "3"
                        )
declare -a Arguments=(
                        "--folder single --cur --no_x_and_z --train_hj --new_rand --difficulty 1 --jump_height 0.8 --set_done --new_image --max_ts 120000000 --more_power 1.5 --terrain_count 1"
                        "--folder single --cur --no_x_and_z --train_hj --new_rand --difficulty 1 --jump_height 0.8 --set_done --new_image --max_ts 120000000 --more_power 1.5 --terrain_count 2"
                        "--folder single --cur --no_x_and_z --train_hj --new_rand --difficulty 1 --jump_height 0.8 --set_done --new_image --max_ts 120000000 --more_power 1.5 --terrain_count 3"
                      )

for terrain in $Terrains; do 
    for (( i=0; i<${#Arguments[@]}; i++ )); do 
      ./base_qut.sh "run_single.py" $terrain ${Experiments[$i]} "${Arguments[$i]}" "24:00:00"
    done
done

declare -a Experiments=(
                        "2"
                      )


declare -a Arguments=( 
                      "--folder single --cur --new_rand --difficulty 1 --new_image --max_ts 120000000 --more_power 1.5 --terrain_count 2"
                      )

Terrains="gaps jumps stairs steps"
for terrain in $Terrains; do 
    for (( i=0; i<${#Arguments[@]}; i++ )); do 
      ./base_qut.sh "run_single.py" $terrain ${Experiments[$i]} "${Arguments[$i]}" "24:00:00"
    done
done

# "e2e1_old_cur"
                        # "e2e1"
                        # "single1"
                        # "terrain_first1_old_cur"
                        # "terrain_first1"
                        # "e2e2"
                        # "single2"
                        # "terrain_first2"

                        # "terrain_two"
                        # "terrain_one"
                        # "two"
                        # "one"
                        # "e2e_terrain_two"
                        # "e2e_terrain_one"
                        # "e2e_two"
                        # "e2e_one"

                        # "terrain_two_nox"
                        # "terrain_one_nox"
                        # "two_nox"
                        # "one_nox"

                        # "terrain_two_no_vel"
                        # "terrain_one_no_vel"
                        # "two_no_vel"
                        # "one_no_vel"
                        # "terrain_one_pitch"
                        # "one_pitch"
                        # "terrain_one_max_steps"
                        # "one_max_steps"
                        # "terrain_one_vel_limit"
                        # "one_vel_limit"

                        # "one_terrain"
                        # "one"
                        # "guide_terrain"
                        # "guide"
                        # "two_terrain"
                        # "two"
                        # "guide_terrain_two"
                        # "guide_two"

                        # "old_cur"
                        # "terrain_second"
                        # "joint_cur"
                        # "decay_rate_75"
                        # "decay_rate_80"
                        # "decay_rate_85"
                        # "cur_len_4"
                        # "success_len_2"
                        # "decay_rate_9"
                        # "decay_rate_95"
                        # "e2e_terrain"
                        # "e2e"

                        # "old"
                        # "old_longer"
                        # "old_longer_500"

                        # "longer"
                        # "fewer"
                        # "ten"
                        # "wider"
                        # "new_cur"
                        # "one_jump"

                        # "one_jump"
                        # "no_base"
                        # "no_base_terrain"
                        # "one_jump_75"
                        # "one_jump_fewer"
                        # "one_jump_terrain"
                        # "one_jump_terrain_fewer"

                        # "joint"
                        # "no_joint"
                        # "500"

                        # "75"
                        # "new_cur"
                        # "std"
                        # "85"
                        # "broken"
                        # "joint_cur"
                        # "terrain"
                        # "500"

                        # "dc_low"
                        # "dc1"
                        # "dc2"
                        # "dc10"
                        # "dc15"

                        # "100_1_5"
                        # "200_1_5"
                        # "300_1_5"
                        # "100_2"
                        # "200_2"
                        # "300_2"


                        # "75"
                        # "85"
                        # "new_cur"
                        # "no_body"
                        # "gain_40"
                        
                        # "full"
                        # "full_kill_vz"
                        # "guide_full_kill_vz"

 # "--folder single --new_rand --terrain_count 1 --max_ts 750000000 --jump_height 0.8 --seed 21 --cur --terrain_first --train_setup"
                      # "--folder single --new_rand --new_cur --terrain_count 1 --max_ts 750000000 --jump_height 0.8 --seed 21 --cur --terrain_first --train_setup"
                      # "--folder single --new_rand --new_cur --terrain_count 1 --max_ts 750000000 --jump_height 0.8 --seed 21 --cur --train_hj"
                      # "--folder single --new_rand --terrain_count 1 --max_ts 750000000 --jump_height 0.8 --seed 21 --cur --terrain_first --train_hj"
                      # "--folder single --new_rand --new_cur --terrain_count 1 --max_ts 750000000 --jump_height 0.8 --seed 21 --cur --terrain_first --train_hj"
                      # "--folder single --new_rand --new_cur --terrain_count 2 --max_ts 750000000 --jump_height 0.8 --seed 21 --cur --terrain_first --train_setup"
                      # "--folder single --new_rand --new_cur --terrain_count 2 --max_ts 750000000 --jump_height 0.8 --seed 21 --cur --train_hj"
                      # "--folder single --new_rand --new_cur --terrain_count 2 --max_ts 750000000 --jump_height 0.8 --seed 21 --cur --terrain_first --train_hj"
                      
                      # "--folder single --new_rand --new_cur --joint_cur --terrain_count 2 --max_ts 75000000 --jump_height 0.8 --cur --terrain_first --train_hj"
                      # "--folder single --new_rand --new_cur --joint_cur --terrain_count 1 --max_ts 75000000 --jump_height 0.8 --cur --terrain_first --train_hj"

                      # "--folder single --new_rand --new_cur --joint_cur --terrain_count 2 --max_ts 75000000 --jump_height 0.8 --cur --train_hj"
                      # "--folder single --new_rand --new_cur --joint_cur --terrain_count 1 --max_ts 75000000 --jump_height 0.8 --cur --train_hj"
                      
                      # "--folder single --new_rand --new_cur --joint_cur --terrain_count 2 --max_ts 75000000 --jump_height 0.8 --cur --terrain_first --train_setup"
                      # "--folder single --new_rand --new_cur --joint_cur --terrain_count 1 --max_ts 75000000 --jump_height 0.8 --cur --terrain_first --train_setup"

                      # "--folder single --new_rand --new_cur --joint_cur --terrain_count 2 --max_ts 75000000 --jump_height 0.8 --cur --train_setup"
                      # "--folder single --new_rand --new_cur --joint_cur --terrain_count 1 --max_ts 75000000 --jump_height 0.8 --cur --train_setup"
                      
                      # "--no_x --folder single --new_rand --new_cur --joint_cur --terrain_count 2 --max_ts 75000000 --jump_height 0.8 --cur --terrain_first --train_hj"
                      # "--no_x --folder single --new_rand --new_cur --joint_cur --terrain_count 1 --max_ts 75000000 --jump_height 0.8 --cur --terrain_first --train_hj"
                      # "--no_x --folder single --new_rand --new_cur --joint_cur --terrain_count 2 --max_ts 75000000 --jump_height 0.8 --cur --train_hj"
                      # "--no_x --folder single --new_rand --new_cur --joint_cur --terrain_count 1 --max_ts 75000000 --jump_height 0.8 --cur --train_hj"
                      

                      # "--folder single --new_rand --new_cur --joint_cur --terrain_count 2 --max_ts 75000000 --jump_height 0.8 --cur --terrain_first --train_hj"
                      # "--folder single --new_rand --new_cur --joint_cur --terrain_count 1 --max_ts 75000000 --jump_height 0.8 --cur --terrain_first --train_hj"
                      # "--folder single --new_rand --new_cur --joint_cur --terrain_count 2 --max_ts 75000000 --jump_height 0.8 --cur --train_hj"
                      # "--folder single --new_rand --new_cur --joint_cur --terrain_count 1 --max_ts 75000000 --jump_height 0.8 --cur --train_hj"
                      
                      # "--pitch_limit --folder single --new_rand --new_cur --joint_cur --terrain_count 1 --max_ts 75000000 --jump_height 0.8 --cur --terrain_first --train_hj"
                      # "--pitch_limit --folder single --new_rand --new_cur --joint_cur --terrain_count 1 --max_ts 75000000 --jump_height 0.8 --cur --train_hj"
                      
                      # "--use_max_steps --folder single --new_rand --new_cur --joint_cur --terrain_count 1 --max_ts 75000000 --jump_height 0.8 --cur --terrain_first --train_hj"
                      # "--use_max_steps --folder single --new_rand --new_cur --joint_cur --terrain_count 1 --max_ts 75000000 --jump_height 0.8 --cur --train_hj"

                      # "--vel_limit --folder single --new_rand --new_cur --joint_cur --terrain_count 1 --max_ts 75000000 --jump_height 0.8 --cur --terrain_first --train_hj"
                      # "--vel_limit --folder single --new_rand --new_cur --joint_cur --terrain_count 1 --max_ts 75000000 --jump_height 0.8 --cur --train_hj"

                      # "--folder single --new_rand --new_cur --joint_cur --terrain_count 1 --max_ts 75000000 --jump_height 0.8 --cur --terrain_first --train_hj"
                      # "--folder single --new_rand --new_cur --joint_cur --terrain_count 1 --max_ts 75000000 --jump_height 0.8 --cur --train_hj"
                      
                      # "--folder single --new_rand --new_cur --joint_cur --terrain_count 2 --max_ts 75000000 --jump_height 0.8 --cur --terrain_first --train_hj"
                      # "--folder single --new_rand --new_cur --joint_cur --terrain_count 2 --max_ts 75000000 --jump_height 0.8 --cur --train_hj"


                      # "--full_hj --folder single --new_rand --new_cur --joint_cur --terrain_count 1 --max_ts 75000000 --jump_height 0.8 --cur --terrain_first --train_hj"
                      # "--full_hj --folder single --new_rand --new_cur --joint_cur --terrain_count 1 --max_ts 75000000 --jump_height 0.8 --cur --train_hj" 
                      # "--guide_first --full_hj --folder single --new_rand --new_cur --joint_cur --terrain_count 1 --max_ts 75000000 --jump_height 0.8 --cur --terrain_first --train_hj"
                      # "--slow_cur --full_hj --folder single --new_rand --new_cur --joint_cur --terrain_count 1 --max_ts 75000000 --jump_height 0.8 --cur --train_hj" 
                      # "--full_hj --folder single --new_rand --new_cur --joint_cur --terrain_count 2 --max_ts 75000000 --jump_height 0.8 --cur --terrain_first --train_hj"
                      # "--full_hj --folder single --new_rand --new_cur --joint_cur --terrain_count 2 --max_ts 75000000 --jump_height 0.8 --cur --train_hj"

                      # "--difficulty 0 --full_hj --folder single --new_rand --new_cur --joint_cur --terrain_count 1 --max_ts 100000000 --jump_height 0.8 --cur --terrain_first --train_hj"
                      # "--difficulty 0 --full_hj --folder single --new_rand --new_cur --joint_cur --terrain_count 1 --max_ts 100000000 --jump_height 0.8 --cur --train_hj" 
                      # "--difficulty 0 --guide_first --full_hj --folder single --new_rand --new_cur --joint_cur --terrain_count 1 --max_ts 100000000 --jump_height 0.8 --cur --train_hj"
                      # "--difficulty 0 --guide_first --full_hj --folder single --new_rand --new_cur --joint_cur --terrain_count 1 --max_ts 100000000 --jump_height 0.8 --cur --terrain_first --train_hj"
                      # "--difficulty 0 --full_hj --folder single --new_rand --new_cur --joint_cur --terrain_count 2 --max_ts 100000000 --jump_height 0.8 --cur --terrain_first --train_hj"
                      # "--difficulty 0 --full_hj --folder single --new_rand --new_cur --joint_cur --terrain_count 2 --max_ts 100000000 --jump_height 0.8 --cur --train_hj" 
                      # "--difficulty 0 --guide_first --full_hj --folder single --new_rand --new_cur --joint_cur --terrain_count 2 --max_ts 100000000 --jump_height 0.8 --cur --train_hj"
                      # "--difficulty 0 --guide_first --full_hj --folder single --new_rand --new_cur --joint_cur --terrain_count 2 --max_ts 100000000 --jump_height 0.8 --cur --terrain_first --train_hj"
                       
                      # "--folder single --new_rand --new_cur --terrain_count 1 --max_ts 120000000 --jump_height 0.8 --cur --train_hj --joint_cur --body_decay_rate 0.75"
                      # "--folder single --new_rand --new_cur --terrain_count 1 --max_ts 120000000 --jump_height 0.8 --cur --train_hj --joint_cur --body_decay_rate 0.8"
                      # "--folder single --new_rand --new_cur --terrain_count 1 --max_ts 120000000 --jump_height 0.8 --cur --train_hj --joint_cur --body_decay_rate 0.85"
                      # "--folder single --new_rand --new_cur --terrain_count 1 --max_ts 120000000 --jump_height 0.8 --cur --train_hj --joint_cur --body_decay_rate 0.9"
                      # "--folder single --new_rand --new_cur --terrain_count 1 --max_ts 120000000 --jump_height 0.8 --cur --train_hj --joint_cur --body_decay_rate 0.95"

                      # "--folder single --new_rand --new_cur --terrain_count 1 --max_ts 120000000 --jump_height 0.8 --cur --train_setup --joint_cur --body_decay_rate 0.85 --terrain_first"
                      # "--folder single --new_rand --new_cur --terrain_count 1 --max_ts 120000000 --jump_height 0.8 --cur --train_setup --joint_cur --body_decay_rate 0.85"
                      # "--folder single --new_rand --terrain_count 1 --max_ts 120000000 --jump_height 0.8 --cur --train_hj --body_decay_rate 0.75 --difficulty 2"
                      # "--folder single --new_rand --new_cur --terrain_count 1 --max_ts 120000000 --jump_height 0.8 --cur --train_hj --body_decay_rate 0.75 --difficulty 2"
                      # "--folder single --new_rand --new_cur --terrain_count 1 --max_ts 120000000 --jump_height 0.8 --cur --train_hj --body_decay_rate 0.75 --difficulty 2 --joint_cur"
                      # "--folder single --new_rand --new_cur --terrain_count 1 --max_ts 120000000 --jump_height 0.8 --cur --train_hj --body_decay_rate 0.75 --difficulty 2 --terrain_first"
                      # "--folder single --new_rand --new_cur --terrain_count 1 --max_ts 120000000 --jump_height 0.8 --cur --train_hj --body_decay_rate 0.80 --difficulty 2 --terrain_first"
                      # "--folder single --new_rand --new_cur --terrain_count 1 --max_ts 120000000 --jump_height 0.8 --cur --train_hj --body_decay_rate 0.85 --difficulty 2 --terrain_first"
                      # "--folder single --new_rand --new_cur --terrain_count 1 --max_ts 120000000 --jump_height 0.8 --cur --train_hj --body_decay_rate 0.75 --difficulty 2 --terrain_first --cur_num 4"
                      # "--folder single --new_rand --new_cur --terrain_count 1 --max_ts 120000000 --jump_height 0.8 --cur --train_hj --body_decay_rate 0.75 --difficulty 2 --terrain_first --success_len 2"
                      
                      # "--folder single --initial_disturbance 100 --final_disturbance 100 --max_ts 50000000 --cur"
                      # "--folder single --initial_disturbance 100 --final_disturbance 100 --max_ts 100000000 --cur"
                      # "--folder single --initial_disturbance 500 --final_disturbance 500 --max_ts 100000000 --cur"

                      # "--folder single --initial_disturbance 500 --final_disturbance 500 --max_ts 100000000 --cur --longer"
                      # "--folder single --initial_disturbance 500 --final_disturbance 500 --max_ts 100000000 --cur --fewer_start"
                      # "--folder single --initial_disturbance 500 --final_disturbance 500 --max_ts 100000000 --cur --ten"
                      # "--folder single --initial_disturbance 500 --final_disturbance 500 --max_ts 100000000 --cur --wider"
                      # "--folder single --initial_disturbance 500 --final_disturbance 500 --max_ts 100000000 --cur --new_cur"
                      # "--folder single --initial_disturbance 500 --final_disturbance 500 --max_ts 100000000 --cur --one_jump"

                      # "--folder single --initial_disturbance 500 --final_disturbance 500 --max_ts 150000000 --cur --one_jump"
                      # "--folder single --initial_disturbance 500 --final_disturbance 500 --max_ts 150000000 --cur --one_jump --no_base"
                      # "--folder single --initial_disturbance 500 --final_disturbance 500 --max_ts 150000000 --cur --one_jump --no_base --terrain_first"
                      # "--folder single --initial_disturbance 500 --final_disturbance 500 --max_ts 150000000 --cur --one_jump --decay 0.75"
                      # "--folder single --initial_disturbance 500 --final_disturbance 500 --max_ts 150000000 --cur --one_jump --fewer_start"
                      # "--folder single --initial_disturbance 500 --final_disturbance 500 --max_ts 150000000 --cur --one_jump --terrain_first"
                      # "--folder single --initial_disturbance 500 --final_disturbance 500 --max_ts 150000000 --cur --one_jump --fewer_start --terrain_first"

                      # "--folder single --initial_disturbance 100 --final_disturbance 100 --max_ts 100000000 --cur --one_jump --new_rand --train_hj --set_done"
                      # "--folder single --initial_disturbance 100 --final_disturbance 100 --max_ts 100000000 --cur --one_jump --new_rand --train_hj --set_done --new_cur"
                      # "--folder single --initial_disturbance 100 --final_disturbance 100 --max_ts 100000000 --cur --one_jump --new_rand --train_hj --set_done --const_std"
                      # "--folder single --initial_disturbance 100 --final_disturbance 100 --max_ts 100000000 --cur --one_jump --new_rand --train_hj --set_done --body_decay_rate 0.85"
                      # "--folder single --initial_disturbance 100 --final_disturbance 100 --max_ts 100000000 --cur --one_jump --new_rand --train_hj --set_done --broken"
                      # "--folder single --initial_disturbance 100 --final_disturbance 100 --max_ts 100000000 --cur --one_jump --new_rand --train_hj --set_done --joint_cur"
                      # "--folder single --initial_disturbance 100 --final_disturbance 100 --max_ts 100000000 --cur --one_jump --new_rand --train_hj --set_done --terrain_first"
                      # "--folder single --initial_disturbance 500 --final_disturbance 500 --max_ts 100000000 --cur --one_jump --new_rand --train_hj --set_done"

                      # "--folder single --cur --one_jump --train_hj --new_rand --difficulty 10 --jump_height 0.32 --set_done --new_image --mean_success --success_mean 0.8 --cur_num 10 --new_cur --use_duty_cycle --no_x_and_z --max_ts 100000000 --max_duty_cycle 20"
                      # "--folder single --cur --one_jump --train_hj --new_rand --difficulty 4 --jump_height 0.8 --set_done --new_image --mean_success --success_mean 0.8 --cur_num 10 --new_cur --use_duty_cycle --no_x_and_z --max_ts 100000000 --max_duty_cycle 20"
                      # "--folder single --cur --one_jump --train_hj --new_rand --difficulty 4 --jump_height 0.8 --set_done --new_image --mean_success --success_mean 0.8 --cur_num 10 --new_cur --use_duty_cycle --no_x_and_z --max_ts 100000000 --max_duty_cycle 20"
                      # "--folder single --cur --one_jump --train_hj --new_rand --difficulty 4 --jump_height 0.8 --set_done --new_image --mean_success --success_mean 0.8 --cur_num 10 --new_cur --use_duty_cycle --no_x_and_z --max_ts 100000000 --max_duty_cycle 10"
                      # "--folder single --cur --one_jump --train_hj --new_rand --difficulty 4 --jump_height 0.8 --set_done --new_image --mean_success --success_mean 0.8 --cur_num 10 --new_cur --use_duty_cycle --no_x_and_z --max_ts 100000000 --max_duty_cycle 15"

                      # "--folder single --cur --one_jump --train_hj --new_rand --difficulty 4 --jump_height 0.8 --set_done --new_image --mean_success --success_mean 0.8 --cur_num 10 --max_ts 100000000 --initial_Kp 100 --more_power 1.5"
                      # "--folder single --cur --one_jump --train_hj --new_rand --difficulty 4 --jump_height 0.8 --set_done --new_image --mean_success --success_mean 0.8 --cur_num 10 --max_ts 100000000 --initial_Kp 200 --more_power 1.5"
                      # "--folder single --cur --one_jump --train_hj --new_rand --difficulty 4 --jump_height 0.8 --set_done --new_image --mean_success --success_mean 0.8 --cur_num 10 --max_ts 100000000 --initial_Kp 300 --more_power 1.5"
                      # "--folder single --cur --one_jump --train_hj --new_rand --difficulty 4 --jump_height 0.8 --set_done --new_image --mean_success --success_mean 0.8 --cur_num 10 --max_ts 100000000 --initial_Kp 100 --more_power 2.0"
                      # "--folder single --cur --one_jump --train_hj --new_rand --difficulty 4 --jump_height 0.8 --set_done --new_image --mean_success --success_mean 0.8 --cur_num 10 --max_ts 100000000 --initial_Kp 200 --more_power 2.0"
                      # "--folder single --cur --one_jump --train_hj --new_rand --difficulty 4 --jump_height 0.8 --set_done --new_image --mean_success --success_mean 0.8 --cur_num 10 --max_ts 100000000 --initial_Kp 300 --more_power 2.0"

                      # "--folder single --cur --no_x_and_y --train_hj --new_rand --difficulty 4 --jump_height 0.8 --set_done --new_image --max_ts 100000000 --more_power 1.5"
                      # "--folder single --cur --no_x_and_y --train_hj --new_rand --difficulty 4 --jump_height 0.8 --set_done --new_image --max_ts 100000000 --more_power 1.5 --body_decay_rate 0.85"
                      # "--folder single --cur --no_x_and_y --train_hj --new_rand --difficulty 4 --jump_height 0.8 --set_done --new_image --max_ts 100000000 --more_power 1.5 --new_cur"
                      # "--folder single --cur --no_x_and_y --train_hj --new_rand --difficulty 4 --jump_height 0.8 --set_done --new_image --max_ts 100000000 --more_power 1.5 --no_body_forces"
                      # "--folder single --cur --no_x_and_y --train_hj --new_rand --difficulty 4 --jump_height 0.8 --set_done --new_image --max_ts 100000000 --more_power 1.5 --joint_gain 40"
                      
                      # "--folder single --cur --no_x_and_z --train_hj --new_rand --difficulty 1 --jump_height 0.8 --set_done --new_image --max_ts 100000000 --more_power 1.5 --full_hj --terrain_count 2"
                      # "--folder single --cur --no_x_and_z --train_hj --new_rand --difficulty 1 --jump_height 0.8 --set_done --new_image --max_ts 100000000 --more_power 1.5 --full_hj --terrain_count 2 --kill_vz"
                      # "--folder single --cur --no_x_and_z --train_hj --new_rand --difficulty 1 --jump_height 0.8 --set_done --new_image --max_ts 100000000 --more_power 1.5 --full_hj --terrain_count 2 --kill_vz --guide_only"
 
                      # "--folder single --cur --no_x_and_z --train_hj --new_rand --difficulty 1 --jump_height 0.8 --set_done --new_image --max_ts 120000000 --more_power 1.5 --terrain_count 3"
                      # "--folder single --cur --no_x_and_z --train_hj --new_rand --difficulty 1 --jump_height 0.8 --set_done --new_image --max_ts 120000000 --more_power 1.5 --terrain_count 3 --foot_pen"

                      # "--folder single --cur --no_x_and_z --train_hj --new_rand --difficulty 1 --jump_height 0.8 --set_done --new_image --max_ts 120000000 --more_power 1.5 --terrain_count 3"

                      # "--folder single --cur --no_x_and_z --train_hj --new_rand --difficulty 1 --jump_height 0.8 --set_done --new_image --max_ts 120000000 --more_power 1.5 --terrain_count 1"
