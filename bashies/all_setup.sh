#!/bin/bash

csiro=true
# csiro=false

# ==================================================================================
# Ours single terrain
# ==================================================================================
Terrains="high_jumps flat gaps jumps up_stairs down_stairs steps"
declare -a Experiments=(
                        "1"
                        "2"
                        )
declare -a Arguments=(
                      "--cur --max_ts 70000000 --num_artifacts 1 --initial_disturbance 500 --final_disturbance 500"
                      )

for terrain in $Terrains; do 
    for (( i=0; i<${#Arguments[@]}; i++ )); do 
      if [ "$csiro" = true ]
      then
        sbatch ./base_csiro.sh "run_setup.py" $terrain ${Experiments[$i]} "${Arguments[$i]}" "20:00:00"
      else
        ./base_qut.sh "run_setup.py" $terrain ${Experiments[$i]} "${Arguments[$i]}" "20:00:00"
      fi
        
    done
done
