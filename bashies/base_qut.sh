#!/bin/bash
qsub <<EOT
#!/bin/bash -l
#PBS -N $2_$3
#PBS -l walltime=$5
# PBS -l select=1:ncpus=16:mpiprocs=16:mem=48Gb:cpuarch=avx2:ngpus=1:gputype=M40
# Remove the second # to enable gputype, valid types are M40,K40,K80,P100
# also for gpus you'll need to load the cuda module
# module load cuda/9.0.176
# module load tensorflow/1.6.0rc1-foss-2018a
# module load python/3.6.4-foss-2018a
# module load tensorflow/1.5.0-gpu-m40-foss-2018a-python-3.6.4
# module load tensorflow/1.5.0-gpu-m40-foss-2018a-python-3.6.4
# module load tensorflow/1.6.0rc1-foss-2018a-avx2
module load python/3.6.4-foss-2018a
cd /home/n8942251/biped_final
mpirun -np 16 python $1 --seed 84 --exp $2_$3 --obstacle_type $2 $4

hostname

exit 0
EOT
