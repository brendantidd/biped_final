import os     
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' 
import tensorflow as tf
tf.get_logger().setLevel('DEBUG')
import numpy as np
import argparse
import tensorboardX
from collections import deque     
from scripts.utils import *
from baselines.common.mpi_util import sync_from_root
import random
from mpi4py import MPI
import time
from pathlib import Path
home = str(Path.home())
from baselines import logger
import matplotlib.pyplot as plt
plt.switch_backend('agg')
import pybullet as p
import cv2

def run(args):

    PATH = home + '/results/biped_final/latest/' + args.folder + '/' + args.exp + '/'

    comm = MPI.COMM_WORLD
    rank = comm.Get_rank()
    num_workers = comm.Get_size()
    myseed = args.seed + 10000 * rank
    np.random.seed(myseed)
    random.seed(myseed)
    tf.set_random_seed(myseed)
    
    logger.configure(dir=PATH)
    if rank == 0:
        writer = tensorboardX.SummaryWriter(log_dir=PATH)
    else: 
        writer = None 

    # sess = tf.Session()
    gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction= 0.1)
    sess = tf.InteractiveSession(config=tf.ConfigProto(inter_op_parallelism_threads=1,
                                                                                    intra_op_parallelism_threads=1,                         
                                                                                    gpu_options=gpu_options), graph=None)
 
    horizon = 2048

    if args.multi_robots:
        from assets.env_multi import Env
    else:
        from assets.env_pb_biped_switch import Env

    env = Env(render=args.render, PATH=PATH, args=args, display_hm=args.display_hm, obstacle_type=args.obstacle_type, vis=args.vis, doa=args.doa, disturbances=False)

    from models.ppo_switch import Model
    from models.switch_doa import RoANet
    # from models.doa import RoANet
    
    if args.base_only:
        print("TRYING WITH BASE ONLY AS FIRST POLICY..")
        obstacle_types = [args.obstacle_type, 'base', 'zero']
        start_obstacle_types = ['base']
    else:
        obstacle_types = ['gaps', 'base', 'jumps','stairs','zero']
        start_obstacle_types = ['gaps', 'base', 'jumps','stairs']
        if args.obstacle_type != 'zero':
            start_obstacle_types.remove(args.obstacle_type)
    

    # pol = Model(obstacle_types[0], env=env, ob_size=env.ob_size, ac_size=env.ac_size, im_size=env.im_size, args=args, PATH=PATH, horizon=horizon, writer=writer, max_timesteps=args.max_ts, vis=args.vis)
    # initialize()
    # # pol.load(home + '/Dropbox/robot/mybot_ws/results_hpc/switch/' + obstacle_types[0]    + '_no_overlap1/')      
    # pol.load(home + '/results/biped_model/weights/b10_vis/' + obstacle_types[0]    + '/')      
    # exit()

    pol = {name:Model(name, env=env, ob_size=env.ob_size, ac_size=env.ac_size, im_size=env.im_size, args=args, PATH=PATH, horizon=horizon, writer=writer, max_timesteps=int(5e6), vis=args.vis) for name in obstacle_types if name != 'zero'}    
    
    
    data_size = env.ob_size
    # data_size = 31
    if args.test_pol or args.roa_table:

        roa = RoANet(args.obstacle_type, args=args, ob_size=data_size)

        # if args.vis:results_size[0])
    
    initialize()

    if args.hpc:
        WEIGHTS_PATH = home + '/hpc-home/results/biped_model/weights/b10_vis'
    else:
        if args.use_no_overlap:
            # WEIGHTS_PATH = home + '/Dropbox/robot/mybot_ws/results_hpc/switch'
            WEIGHTS_PATH = home + '/results/biped_final/latest/switch'
        else:
            WEIGHTS_PATH = home + '/results/biped_model/weights/b10_vis'
    
    for name in obstacle_types:
        if name == 'zero': continue
        if name == 'base':
            pol[name].load(home + '/results/biped_model/weights/b10_vis/' + name + '/')
        elif args.use_no_overlap:
            pol[name].load(WEIGHTS_PATH + '/' + name + '_no_overlap/')
            # pol[name].load(WEIGHTS_PATH + '/' + name + '_test_vis/')
        else:
            pol[name].load(WEIGHTS_PATH + '/' + name + '/')
    if args.test_pol or args.roa_table:
        # roa.load(home + '/results/biped_model/latest/' + args.folder + '/' + args.obstacle_type + '_small_balanced3/best/')
        # roa.load(home + '/results/biped_model/latest/' + args.folder + '/' + args.obstacle_type + '_dist/best/')
        # roa.load(home + '/results/biped_model/latest/' + args.folder + '/' + args.obstacle_type + '_orig/best/')
        roa.load(home + '/results/biped_final/latest/switch_data/switch_estimator_' + args.obstacle_type + '/best/')
    
    env.difficulty = args.difficulty
    env.height_coeff = args.height_coeff

    if args.render:
        eval_length = 100
    else:
        # eval_length = 1200
        eval_length = 500

    # data_length = 5000
    # data_length = 10000
    # if args.roa_table or args.q_table:
    #     data_length = 100000
    if args.single_point:
        # data_length = 5000
        # data_length = 15000
        # data_length = 5000
        # data_length = 20000
        # data_length = 80000
        data_length = 100000
        # data_length = 60000
        # data_length = 2000
        # data_length = 10000
        # data_length = 2500
    else:
        data_length = 100000

    prev_done = True
    ob = env.reset()
    im = env.get_hm()



    input_ob = np.zeros([data_length, data_size])
    if args.vis:
        input_im = np.zeros([data_length] + [60,40,1])
    else:
        input_im = np.zeros([data_length] + env.im_size)
    # if args.test_pol:
    labels = []
    #     baseline_labels = []
    # else:
    # labels = np.zeros([data_length, 1])

    # if rank == 0:
    #     all_ob = np.zeros([num_workers, data_length, env.ob_size])
    #     if args.vis:
    #         all_im = np.zeros([data_length] + [60,40,1])
    #     else:
    #         all_im = np.zeros([num_workers] + [data_length] + env.im_size)
    #     all_labels = np.zeros([num_workers, data_length, 1])
    # else:
    #     all_ob = None
    #     all_im = None
    #     all_labels = None
    total_data = 0
    data_pointer = 0

    ep_ret = 0
    ep_len = 0
    ep_rets = []
    ep_lens = []
    ep_steps = 0
    
    episode_count = 0
    
    switch_step = None
    if args.just_base:
        current_pol = "base" 
    else:
        current_pol = np.random.choice(start_obstacle_types) 
    initial_pol = start_obstacle_types.index(current_pol)
    
    # if args.render and args.debug:
        # replace_Id2 = p.addUserDebugText(current_pol,[env.body_xyz[0], env.body_xyz[1], env.body_xyz[2]+0.5],[0,0,1],textSize=3)

    obs = []
    imgs = []
    hms = []

    current_pol_data = []
    next_pol_data = []
    feet_data = {'left':[],'right':[]}
    com_data = []
    switch_data = []
    terrain_data = []
    detection_dist = 0.9
    # detection_dist = 1.5
    # detection_dist = 0.8

    t1 = time.time()
    t2 = time.time()
    count = 0
    initial_pos = None
    failed_to_switch = False
    if args.roa_table:
        switch_window = 100
    else:
        # switch_window = 180
        switch_window = 100
        # switch_window = 50

    box_cross_step = switch_step = None
    prev_switched = switched = False

    baseline_stats = {'zero':0.1800, 'base':0.9600, 'stairs':0.8700, 'jumps': 0.61 , 'gaps':0.5200}
    baseline_stat = baseline_stats[args.obstacle_type]
    if args.multi_robots:
        robot = {}
    if args.q_table or args.test_pol:
        # table = {terrain:{i:[] for i in range(switch_window)} for terrain in start_obstacle_types}
        # all_table = {i:[] for i in range(switch_window)}
        # all_table = np.zeros(switch_window)
        # all_table_count = np.zeros(switch_window)
        table = np.zeros([len(start_obstacle_types), int(detection_dist*100)])
        table_count = np.ones([len(start_obstacle_types), int(detection_dist*100)])
        all_tables = np.zeros([len(start_obstacle_types), int(detection_dist*100)])
        all_tables_count = np.ones([len(start_obstacle_types), int(detection_dist*100)])
    
    if args.roa_table:
        table = np.zeros([len(start_obstacle_types), 100])
        table_count = np.ones([len(start_obstacle_types), 100])
        all_tables = np.zeros([len(start_obstacle_types), 100])
        all_tables_count = np.ones([len(start_obstacle_types), 100])

    # max_disturbance = 3000
    max_disturbance = 1000
    
    if rank == 0:
        print("Getting goal set for ", current_pol, " in terrain type for ", current_pol, ", recording values for stable regions in velocity range. Collecting ", eval_length, " samples for each")
    
    if args.q_table:
        rand_step = int(detection_dist*100)-1
    #     rand_step = switch_window - 1
    else:
        rand_step = int(detection_dist*100)

    # if args.use_roa:
    roa_table = {}
    # roa_table['gaps'] = {'base':0.8912404358542619,'jumps':0.8796955116770484,'stairs':0.8967811886478415}
    # roa_table['jumps'] = {'gaps':0.8529617829817016,'base':0.8592505388789706,'stairs':0.8345029192706269}

    roa_table['jumps'] = {'gaps':33,'base':32,'stairs':32}
    roa_table['stairs'] = {'gaps':5,'base':3,'jumps':5}
    # roa_table['gaps'] = {'base':0,'jumps':3,'stairs':6}
    # roa_table['gaps'] = {'base':26,'jumps':26,'stairs':24}
    roa_table['gaps'] = {'base':84,'jumps':60,'stairs':30}

        # roa_table['jumps'] = {'gaps':0.6974131695637322,'base':0.5612809990803329,'stairs':0.5492723854602318}
        # roa_table['stairs'] = {'gaps':0.5782047725551963,'base':0.5138365214870609,'jumps':0.5696806930800914}
        # roa_table['gaps'] = {'base':0.5020249960316859,'jumps':0.5680257043220183,'stairs':0.4860258400664809}

    if args.use_q_table:
        q_table = {'gaps':{'jumps':18,'base':0,'stairs':18}, 'jumps':{'gaps':14,'base':9,'stairs':2}, 'stairs':{'jumps':8,'base':16,'gaps':17}}
    
    num_failures = 0
    next_pol = current_pol
    max_z_height = 0
    switch_point = 0
    detection = 0
    artifact_x = 0
    dist_to_artifact = 0
    obs_deque = deque(maxlen=5)
    imgs_deque = deque(maxlen=5)
    detected = None
    while True:
        
        if env.body_xyz[2] > max_z_height:
            max_z_height = env.body_xyz[2]

        dist_to_artifact = artifact_x - env.body_xyz[0]                
        if dist_to_artifact < 0 and env.box_num + 4 < len(env.box_info[0]):
            up_coming_edges = [env.box_info[1][env.box_num + 2][0] - env.box_info[2][env.box_num + 2][0], env.box_info[1][env.box_num + 3][0] - env.box_info[2][env.box_num + 3][0], env.box_info[1][env.box_num + 4][0] - env.box_info[2][env.box_num + 4][0]]
            distances = (np.array(up_coming_edges) - env.body_xyz[0])
            dists = np.array([d < detection_dist + 0.01 and d > detection_dist - 0.01 for d in distances])
            if (dists).any():
                # print(env.steps, distances)
                if dist_to_artifact < 0:
                    idx = np.argmax(dists)
                    height_diff = (env.box_info[1][env.box_num + idx + 2][2] + env.box_info[2][env.box_num + idx + 2][2]) - (env.box_info[1][env.box_num + idx + 2 - 1][2] + env.box_info[2][env.box_num + idx + 2 - 1][2])
                    if height_diff > 0.25 and height_diff < 0.4:
                        next_pol = 'jumps'
                        detection = env.steps
                        artifact_x = env.box_info[1][env.box_num + idx + 2][0] - env.box_info[2][env.box_num + idx + 2][0]
                        detected = True
                    elif height_diff < -0.4:
                        next_pol = 'gaps'
                        detection = env.steps
                        artifact_x = env.box_info[1][env.box_num + idx + 2][0] - env.box_info[2][env.box_num + idx + 2][0]
                        detected = True
                    elif abs(height_diff) > 0.12 and abs(height_diff) < 0.21:
                        next_pol = 'stairs'
                        detection = env.steps
                        artifact_x = env.box_info[1][env.box_num + idx + 2][0] - env.box_info[2][env.box_num + idx + 2][0]
                        detected = True

        feet = np.array(list(env.local_foot_pos['left']) + list(env.local_foot_pos['right']) + [env.body_xyz[2] - env.foot_pos['left'][2], env.body_xyz[2] - env.foot_pos['right'][2]] + env.contacts + [env.body_xyz[1], env.body_xyz[2]-env.z_offset, env.yaw, env.vx, env.vy, env.vz, dist_to_artifact])


        # if dist_to_artifact > 0 and dist_to_artifact < detection_dist:
        #     box_cross_step = 
        # else:
        #     box_cross_step = None

        if not switch_step and dist_to_artifact > 0 and dist_to_artifact < detection_dist:
            if not box_cross_step:
                box_cross_step = env.steps
            # Safe guard against getting too close
            if dist_to_artifact < 0.01:
                switch_step = dist_to_artifact
            elif args.test_pol:
                # if roa.step(ob,im) >= 0.8:
                if roa.step(ob,im) >= 0.5:
                    switch_step = dist_to_artifact
            elif args.roa_table:
                if roa.step(ob, im) >= rand_step/100:
                    switch_step = dist_to_artifact
            elif args.q_table:
                switch_step = rand_step/100
            elif args.in_sight:
                switch_step = dist_to_artifact
            else:
                # switch_step = 30/100
                switch_step = rand_step/100
        # print(dist_to_artifact, rand_step/100)
        # if not switched and ((args.q_table and dist_to_artifact < rand_step/100 + 0.01 and dist_to_artifact > rand_step/100 - 0.01) or (not args.q_table and switch_step and env.steps == switch_step) or (not args.q_table and box_cross_step and dist_to_artifact < 0)):
        # if not switched and ((switch_step and dist_to_artifact < switch_step + 0.01 and dist_to_artifact > switch_step - 0.01) or (box_cross_step and )):
        if not switched and ((switch_step and dist_to_artifact < switch_step + 0.01 and dist_to_artifact > switch_step - 0.01)):
            # print("switching bug", env.steps, dist_to_artifact, switch_step)
            current_pol = args.obstacle_type
            initial_pos = [env.body_xyz[0], env.body_xyz[1], env.z_offset]
            initial_heading = env.yaw
            switched = True
            switch_point = env.steps
            # if args.single_point:
                # obs.append(feet)
                # imgs.append(im)
            if args.multi_robots:
                robot['pos'] = env.body_xyz
                robot['orn'] = [env.qx, env.qy, env.qz, env.qw]
                robot['joints'] = env.joints
        
        
        if not switched or (switched and env.steps <= switch_point + 2):
            # print("adding to deque", env.steps, switch_point)
            # obs_deque.append(feet)
            obs_deque.append(ob)
            imgs_deque.append(im)
            
        # if not switched and box_cross_step and dist_to_artifact < 0:
        #     print("falling to death", switch_step, env.steps)
        #     current_pol = args.obstacle_type
        #     initial_pos = [env.body_xyz[0], env.body_xyz[1], env.z_offset]
        #     initial_heading = env.yaw
        #     switched = True
        #     switch_point = env.steps
        #     if args.single_point:
        #         obs.append(feet)
        #         imgs.append(im)


        # if not args.single_point and not (args.roa_table or args.q_table) and switched and env.steps <= (box_cross_step + switch_window):
        # if not args.single_point and switched and env.steps <= (box_cross_step + switch_window):
        #     obs.append(ob)
        #     imgs.append(im)
        
        
        # elif (args.roa_table or args.q_table) and box_cross_step and env.steps <= (box_cross_step + switch_window):
        #     obs.append(ob)
        #     imgs.append(im)

        # if not args.test_pol and (not args.q_table or args.roa_table) and np.random.random() < 0.02 and env.vx > 0.4 and not switched and (not switch_step or (switch_step and abs(dist_to_artifact - switch_step) > 0.2)):
        #     # print("disturbing")
        #     if args.debug:
        #         replace_Id2 = p.addUserDebugText("disturbing",[env.body_xyz[0], env.body_xyz[1], env.body_xyz[2]+0.7],[0,0,1],textSize=3,replaceItemUniqueId=replace_Id2)
        #     env.add_disturbance(max_disturbance, forward_only=True)
        # # if args.test_pol and args.vis and box_cross_step and not switched:
        #     # env.add_disturbance(max_disturbance, forward_only=True)


        if current_pol == 'zero':
            zero_ob = copy.copy(ob)
            zero_ob[-2] = 0
            act, vpred, _, nlogp = pol['base'].step(zero_ob, im, stochastic=False)
        else:
            act, vpred, _, nlogp = pol[current_pol].step(ob, im, stochastic=False)

        terrain_data.append(env.z_offset)

        torques = act

        # if (args.test_pol or args.roa_table) and box_cross_step and box_cross_step + 180 > env.steps:
        if args.test_pol and box_cross_step and dist_to_artifact > 0 and dist_to_artifact < detection_dist:
        # if args.test_pol and dist_to_artifact > 0 and dist_to_artifact < detection_dist:
            switch_thing = roa.step(ob, im)
            # switch_thing = roa.step(feet)
            switch_data.append(switch_thing)
            # if args.q_table:
            #     table[initial_pol][env.steps - box_cross_step] += switch_thing
            #     table_count[initial_pol][env.steps - box_cross_step] += 1
        else:
            switch_data.append(0)

        # if (detected and not prev_detected) or (switched and not prev_switched):
        #     if(detected and not prev_detected):
        #         print("detected")
        #     if (switched and not prev_switched):
        #         print("switched")
        #     next_ob, rew, done, _ = env.step(torques, freeze_robot=True)
        # else:
        next_ob, rew, done, _ = env.step(torques)

        # prev_switched = switched
        # prev_detected = detected


        next_im = env.get_hm()


        # image = (next_im*255.0).astype(np.uint8)
        # cv2.imshow('frame', image)
        # cv2.waitKey(1)

        if (args.obstacle_type != 'zero' and env.order[env.box_num] == 'zero') or (switch_step is not None and args.obstacle_type == 'zero' and env.steps - switch_step > 300):
            done = True

        prev_done = done
        ob = next_ob
        im = next_im
        ep_ret += rew
        ep_len += 1
        ep_steps += 1

        # if args.render and args.debug:
            # replace_Id2 = p.addUserDebugText(current_pol,[env.body_xyz[0], env.body_xyz[1], env.body_xyz[2]+0.5],[0,0,1],textSize=3,replaceItemUniqueId=replace_Id2)
            # replace_Id2 = p.addUserDebugText(current_pol,[env.body_xyz[0], env.body_xyz[1], env.body_xyz[2]+0.5],[0,0,1],textSize=3)
        
        if args.test_pol and episode_count > eval_length and rank == 0:
            print("Data: ", total_data, "of ", data_length,    "breakdown: ", round(np.mean(labels),2), "baseline: ", baseline_stat, "time: ", round(time.time() - t1,2), "total: ", round(time.time() - t2,2))

            break

        current_pol_data.append(obstacle_types.index(current_pol))
        next_pol_data.append(obstacle_types.index(next_pol))

        if abs(env.foot_pos['left'][0] - env.foot_pos['right'][0]) < 0.05:
            feet_data['left'].append(env.foot_pos['left'][2])
            feet_data['right'].append(env.foot_pos['right'][2])
        else:
            feet_data['left'].append(np.nan)
            feet_data['right'].append(np.nan)
        if not env.ob_dict['left_foot_left_ground'] and abs(env.body_xyz[0] - env.foot_pos['left'][0]) < 0.05:
            com_data.append(env.body_xyz[2])
        elif not env.ob_dict['right_foot_left_ground'] and abs(env.body_xyz[0] - env.foot_pos['right'][0]) < 0.05:
            com_data.append(env.body_xyz[2])
        else:
            com_data.append(np.nan)

        # print(rank, env.steps)
        if done:    
            # print(env.body_xyz[2] - env.z_offset)
            if args.vis:
                vel = np.sqrt(env.vx**2 + env.vy**2 + env.vz**2)
                # print("vel", vel)
                # if ((args.obstacle_type != 'zero' and env.order[env.box_num] == 'zero') or (switch_step is not None and args.obstacle_type == 'zero' and env.steps - switch_step > 300)):
                #     if not (env.body_xyz[2] - env.z_offset) > 0.88:
                #         print("got to the end but height.. ",(env.body_xyz[2] - env.z_offset))
                #         failure_path = PATH + 'failure_height' + str(num_failures)
                #     if not vel < 1.25:
                #         print("got to the end but velocity .. ", vel)
                #         failure_path = PATH + 'failure_vel' + str(num_failures)                    
                #     if ((args.obstacle_type != 'zero' and env.order[env.box_num] == 'zero') or (switch_step is not None and args.obstacle_type == 'zero' and env.steps - switch_step > 300)) and (not (env.body_xyz[2] - env.z_offset) > 0.88 or not vel < 1.25):
                #         print((env.body_xyz[2] - env.z_offset))
                #         try:
                #             os.mkdir(failure_path)
                #         except:
                #             pass
                #         env.save_sim_data(failure_path + '/', last_steps=True)
                #         num_failures += 1
                # if ((args.obstacle_type != 'zero' and env.order[env.box_num] == 'zero') or (switch_step is not None and args.obstacle_type == 'zero' and env.steps - switch_step > 300)) and (abs(env.body_xyz[1]) < 0.2 and abs(env.yaw) < 0.2 and    (env.body_xyz[2] - env.z_offset) > 0.94 ):
                if ((args.obstacle_type != 'zero' and env.order[env.box_num] == 'zero') or (switch_step is not None and args.obstacle_type == 'zero' and env.steps - switch_step > 300)) and    (env.body_xyz[2] - env.z_offset) > 0.88 and vel < 1.25:
                    label = True    
                    colour = [0.1,0.9,0.1,0.5]
                else:
                    label = False
                    colour = [0.9,0.1,0.1,0.5]
            else:
                if ((args.obstacle_type != 'zero' and env.order[env.box_num] == 'zero') or (switch_step is not None and args.obstacle_type == 'zero' and env.steps - switch_step > 300)) and    (env.body_xyz[2] - env.z_offset) > 0.88 and vel < 1.25:
                # if ((args.obstacle_type != 'zero' and env.order[env.box_num] == 'zero') or (switch_step is not None and args.obstacle_type == 'zero' and env.steps - switch_step > 300)) and (abs(env.body_xyz[1]) < 0.2 and abs(env.yaw) < 0.2 and (env.body_xyz[2] - env.z_offset) > 0.94 ):
                    label = True    
                    colour = [0.1,0.9,0.1,0.5]
                else:
                    label = False
                    colour = [0.9,0.1,0.1,0.5]
         
            if args.q_table:
                # table[initial_pol][rand_step].append(label)
                # all_table[rand_step].append(label)
                # print(initial_pol,    rand_step)
                table[initial_pol][rand_step] += label
                table_count[initial_pol][rand_step] += 1
                # print(table)
                # all_table[rand_step] += label
                # all_table_count[rand_step] += 1
                rand_step = np.random.randint(0,detection_dist*100)
                # print(rand_step)
                # rand_step += 1
                # if rand_step >= switch_window:
                #     rand_step = 0
                # if episode_count % 10 == 0 and rank == 0:
                #     np.save(PATH + "q_table.npy", all_table)
            elif args.roa_table:
                # print(rand_step)
                # print(initial_pol, rand_step)
                table[initial_pol][rand_step] += label
                table_count[initial_pol][rand_step] += 1
                # rand_step = np.random.randint(0,detection_dist*100)

                rand_step = np.random.randint(0,100)
                
                # rand_step += 1
                # if rand_step >= 100:
                #     rand_step = 0
            else:
                rand_step = np.random.randint(0,detection_dist*100)

                # rand_step = np.random.randint(0,switch_window)
                # rand_step = 80
                # print("rand always 80")

            if args.multi_robots:
                robot['success'] = label

            if args.render and args.debug and initial_pos is not None:
                visBoxId = p.createVisualShape(p.GEOM_SPHERE, radius=0.1, rgbaColor=colour)
                box_id = p.createMultiBody(baseMass=0, baseVisualShapeIndex=visBoxId, basePosition=initial_pos, baseOrientation= p.getQuaternionFromEuler([0.0,1.5709,initial_heading]))

            # Now doing 5 data points per point
            obs.extend(obs_deque)            
            imgs.extend(imgs_deque)            
            labels.extend([label]*len(obs_deque))


            if len(labels) > len(obs):
                del labels[-1]
                print("no data point", num_failures, rank, switch_step, env.steps, dist_to_artifact)
                failure_path = PATH + 'failure_vel' + str(rank) + "00" + str(num_failures)                    
                try:
                    os.mkdir(failure_path)
                except:
                    pass
                env.save_sim_data(failure_path + '/', last_steps=True)
                num_failures += 1

            assert(len(labels) == len(obs))
            # obs[-1][-1] = label
            # idx = min(data_pointer+len(obs), data_length) - data_pointer
            # print(idx)
            # if idx > 0 and ((args.test_pol or args.q_table or args.roa_table) or label == 0 or (label == 1 and np.mean(labels[:data_pointer]) < 0.6)):
            # if idx > 0:
            #     if not failed_to_switch:
            #         # print(idx, len(obs[:idx]), len(obs), data_pointer)
            #         input_ob[data_pointer:data_pointer+idx] = obs[:idx]
            #         input_im[data_pointer:data_pointer+idx] = imgs[:idx]

                    # for i in range(idx):
                    #     print(input_im[i+data_pointer].shape)
                    #     cv2.imshow('frame', input_im[i+data_pointer])
                    #     cv2.waitKey(1)
                    # print(data_pointer, idx, label)
                    # labels[data_pointer:data_pointer+idx] = label
                    # baseline_labels = np.zeros([data_length, 1])
                    # data_pointer += idx
                    # obs = []
                    # imgs = []
                    # print(np.mean(labels))
            # elif label == 1:
            #     print("throwing away", idx, np.mean(labels[:data_pointer]))
    
            # if count % 2 == 0:
            if count % 10 == 0:
                
                # comm.Gather(sendbuf, recvbuf, root=0)
                # print("at gather", rank, time.time() - t2)
                # comm.Gather(input_ob, all_ob, root=0)
                # comm.Gather(input_im, all_im, root=0)
                # comm.Gather(labels,all_labels, root=0)
                # all_ob = comm.gather(input_ob[:data_pointer,:], root=0)
                # all_im = comm.gather(input_im[:data_pointer,:], root=0)

                all_obs = comm.gather(obs, root=0)
                all_imgs = comm.gather(imgs, root=0)
                all_labels = comm.gather(labels, root=0)
                # print("rank", rank)
                # print("obs")
                # print( np.array(obs))
                # print("labels")
                # print(labels)
                # if rank == 0:
                #     print("all_obs")
                #     print( all_obs)
                #     print("all lables")
                #     print(all_labels)
                #     print()
                if args.test_pol or args.q_table or args.roa_table:
                    comm.Allreduce(table, all_tables, op=MPI.SUM)
                    comm.Allreduce(table_count, all_tables_count, op=MPI.SUM)
                    # all_tables = comm.gather(table, root=0)
                    # all_tables_count = comm.gather(table_count, root=0)

                # print(rank, input_ob[:data_pointer,:].shape, input_im[:data_pointer,:].shape, labels[:data_pointer,:].shape)
                # if rank == 1:
                    # print(input_ob[0,:10])
                if rank == 0:
                    all_obs = np.concatenate(all_obs)
                    all_imgs = np.concatenate(all_imgs)
                    all_labels = np.concatenate(all_labels)
                    # print(all_tables)
                    # print(all_obs.shape)
                    # print(all_labels.shape)
                    # if args.q_table:
                    #     all_tables = np.concatenate(all_tables, axis=1)
                    #     all_tables_count = np.concatenate(all_tables_count, axis=1)
                        # print("tables shape", all_tables.shape, all_tables_count.shape)

                    # print(all_ob[data_pointer,:10])                
                    # print(all_ob.shape, all_im.shape, all_labels.shape)

                    np.save(PATH + 'input_ob.npy', all_obs)
                    np.save(PATH + 'input_im.npy', all_imgs)
                    np.save(PATH + 'labels.npy', all_labels)
                    if args.q_table or args.roa_table or args.test_pol:
                        # np.save(PATH + 'table.npy', all_tables)
                        # np.save(PATH + 'table_count.npy', all_tables_count)
        
                        # print(start_obstacle_types, " to ",    args.obstacle_type)
                        # print(all_tables/all_tables_count)
                        # np.save(PATH + '', result)
                        if args.roa_table:
                            # result = np.max(all_tables/all_tables_count, axis=1)
                            result = np.argmax(all_tables/all_tables_count, axis=1)
                            print(result)
                            print('roa_table[\'' + args.obstacle_type + '\'] = {\''+ start_obstacle_types[0] + '\':' + str(result[0]) + ',\''+ start_obstacle_types[1] + '\':' + str(result[1]) + ',\''+ start_obstacle_types[2] + '\':' + str(result[2]) + '}')
                        elif args.q_table:
                            result = np.argmax(all_tables/all_tables_count, axis=1)
                            print(result)
                            print('q_table[\'' + args.obstacle_type + '\'] = {\''+ start_obstacle_types[0] + '\':' + str(result[0]) + ',\''+ start_obstacle_types[1] + '\':' + str(result[1]) + ',\''+ start_obstacle_types[2] + '\':' + str(result[2]) + '}')
        
                        colours = [np.array([51, 204, 51])/255.0, np.array([51, 51, 204])/255.0, np.array([204, 51, 51])/255.0, np.array([204, 102, 204])/255.0, np.array([204,102,51])/255.0,]
                        # q_data[j] = all_tables[i][j-box_cross_step]/all_tables_count[i][j-box_cross_step]
                        if args.q_table:
                            # num_axes = 2 + len(start_obstacle_types)
                            # fig, axes = plt.subplots(num_axes,    figsize=(10, 3*num_axes))
                            # xs = [_ for _ in range(len(terrain_data))]
                            # for i in range(len(start_obstacle_types)):
                            #     q_data = [0 for _ in range(len(terrain_data))]
                            #     if box_cross_step:
                            #         max_len = min(box_cross_step+switch_window, len(terrain_data))
                            #         for j in range(box_cross_step, max_len):
                            #             # if all_tables[initial_pol][i-box_cross_step]:
                            #             q_data[j] = all_tables[i][j-box_cross_step]/all_tables_count[i][j-box_cross_step]
                            #     axes[i].plot(xs, q_data, c=np.array([204, 102, 0])/255.0, alpha=1.0)    
                            #     axes[i].legend([start_obstacle_types[i]])
                            num_axes = 2 + len(start_obstacle_types)
                            fig, axes = plt.subplots(num_axes,    figsize=(10, 3*num_axes))
                            xs = [_ for _ in range(int(detection_dist*100))]
                            for i in range(len(start_obstacle_types)):
                                # q_data = [0 for _ in range(len(switch_window))]
                                        # if all_tables[initial_pol][i-box_cross_step]:
                                data = all_tables[i]/all_tables_count[i]
                                axes[i].plot(xs, data, c=np.array([204, 102, 0])/255.0, alpha=1.0)    
                                axes[i].legend([start_obstacle_types[i]])
                        elif args.roa_table:
                            num_axes = 2 + len(start_obstacle_types)
                            fig, axes = plt.subplots(num_axes,    figsize=(10, 3*num_axes))
                            xs = [_ for _ in range(switch_window)]
                            for i in range(len(start_obstacle_types)):
                                # q_data = [0 for _ in range(len(switch_window))]
                                        # if all_tables[initial_pol][i-box_cross_step]:
                                data = all_tables[i]/all_tables_count[i]
                                axes[i].plot(xs, data, c=np.array([204, 102, 0])/255.0, alpha=1.0)    
                                axes[i].legend([start_obstacle_types[i]])
                        else:
                            num_axes = 3
                            fig, axes = plt.subplots(num_axes,    figsize=(10, 3*num_axes))
                            axes[0].plot([s for s in range(len(switch_data))], switch_data, c=np.array([204, 102, 0])/255.0, alpha=1.0)    
                        
                        axes[num_axes-2].plot([_ for _ in range(len(current_pol_data))], current_pol_data, c=np.array([0, 102, 204])/255.0, alpha=1.0)    
                        axes[num_axes-2].plot([_ for _ in range(len(next_pol_data))], next_pol_data, c=np.array([0, 204, 102])/255.0, alpha=1.0)    
                        axes[num_axes-2].set_ylim([-0.9,len(obstacle_types) + 0.1])
                        for s, n in enumerate(obstacle_types):
                            axes[num_axes-2].text(0,s*0.75+0.5,str(s) + ". " + n)
                        axes[num_axes-2].set_title("Current policy", loc='left')
                        axes[num_axes-2].legend(["current_policy", "next"])    
                        

                        axes[num_axes-1].plot([_ for _ in range(len(feet_data['left']))], feet_data['left'], c=colours[0], alpha=1.0)    
                        axes[num_axes-1].plot([_ for _ in range(len(feet_data['right']))], feet_data['right'], c=colours[1], alpha=1.0)    
                        axes[num_axes-1].plot([_ for _ in range(len(com_data))], com_data, c=colours[2], alpha=1.0)    
                        axes[num_axes-1].plot([t for t in range(len(terrain_data))], terrain_data, c=np.array([0, 102, 204])/255.0, alpha=1.0)    
                        axes[num_axes-1].set_title("terrain height", loc='left')
                        axes[num_axes-1].set_ylim([-0.1, max_z_height + 0.1])
                        
                        
                        for i in range(num_axes-2,num_axes):
                            # for xc in switch_point:
                                # axes[i].axvline(x=xc, label='line at x = {}'.format(xc), c=c)
                            axes[i].axvline(x=switch_point,c=[colours[2][0],colours[2][1],colours[2][2],0.6], linestyle='--', linewidth=0.5)
                            # for xc in detection:
                            axes[i].axvline(x=detection,c=[colours[0][0],colours[0][1],colours[0][2],0.6], linestyle='--', linewidth=0.5)

                        fig.tight_layout(pad=4.0)
                        # plt.savefig(PATH + 'switch.png', bbox_inches='tight')
                                                
                        plt.savefig(PATH + args.obstacle_type + '_plot.png', bbox_inches='tight')
                        plt.close()
                
                    total_data = all_obs.shape[0]
                    if total_data > data_length:
                        break

                    # if args.q_table:
                    #     print(table)
                    print("Data: {0:5d} of {1:4d} rank 0: {2:0.4f} breakdown: {3:0.4f} time: {4:4.2f} total: {5:5.2f}".format(total_data,    data_length, round(np.mean(labels),2), round(np.mean(all_labels),2), round(time.time() - t1,2), round(time.time() - t2,2)))
                    t1 = time.time()

                    # np.save(PATH + 'input_ob.npy', input_ob[:data_pointer,:])
                    # np.save(PATH + 'input_im.npy', input_im[:data_pointer,:])
                    # np.save(PATH + 'labels.npy', labels[:data_pointer,:])
            count += 1
            # print(count)



            episode_count += 1
            if args.just_base:
                current_pol = "base" 
            else:
                current_pol = np.random.choice(start_obstacle_types) 
            initial_pol = start_obstacle_types.index(current_pol)
            
            # if args.roa_table:
            #     if np.random.random() < 0.5:
            #         rand_step = np.random.randint(0,switch_window)
            #     else:
            #         rand_step = np.argmax(all_tables[initial_pol]/all_tables_count[initial_pol])
            #         print(rand_step)


            # print(current_pol)

            current_pol_data = []
            next_pol_data = []
            feet_data = {'left':[],'right':[]}
            com_data = []
            switch_data = []
            terrain_data = []
    
            box_cross_step = switch_step = None
            prev_switched = switched = False
            
            myseed += 1
            np.random.seed(myseed)

            if args.multi_robots:
                ob = env.reset(robot=robot)
                robot = {}
            else:
                ob = env.reset()
            im = env.get_hm()
            ep_rets.append(ep_ret)    
            ep_lens.append(ep_len)         
            ep_ret = 0
            ep_len = 0
            initial_pos = None
            next_pol = current_pol
            switch_point = 0
            detection = 0
            artifact_x = 0
            dist_to_artifact = 0
            detected = None

            # print("starting with ", obstacle_types[next_pol])

    print("-------------------------------------------------------------------------------------------------------")
    print("Data: {0:4d} of {1:4d} time: {2:4.2f} total: {3:5.2f}".format(total_data,    data_length, round(time.time() - t1,2), round(time.time() - t2,2)))
    # print("{0:s} thres: {1:0.2f} breakdown: {2:0.2f}".format(args.obstacle_type, args.roa_thres, round(np.mean(labels[:data_pointer,0]),2)))
    print("{0:s} thres: {1:0.2f} breakdown: {2:3.2f} baseline: {3:3.2f}".format(args.obstacle_type, args.roa_thres, round(np.mean(labels),2), baseline_stat))
    print("-------------------------------------------------------------------------------------------------------")
    MPI.COMM_WORLD.Abort(1)



if __name__ == '__main__':

    parser = argparse.ArgumentParser()

    parser.add_argument('--sub_folder', default='b')
    # parser.add_argument('--folder', default='b4')
    # parser.add_argument('--folder', default='b9')
    parser.add_argument('--folder', default='switch')
    # parser.add_argument('--folder', default='b10_vis')

    parser.add_argument('--difficulty', default=10, type=int)
    parser.add_argument('--height_coeff', default=0.07)
    # parser.add_argument('--roa_thres', default=0.8, type=float)
    parser.add_argument('--roa_thres', default=0.5, type=float)

    parser.add_argument('--cur_len', default=1000, type=int)
    parser.add_argument('--inc', default=2, type=int)

    parser.add_argument('--use_no_overlap', default=False, action='store_true')
    parser.add_argument('--no_overlap', default=False, action='store_true')
    parser.add_argument('--just_base', default=False, action='store_true')
    parser.add_argument('--manual', default=False, action='store_true')
    parser.add_argument('--single_point', default=True, action='store_false')
    parser.add_argument('--rand_pol', default=False, action='store_true')
    parser.add_argument('--use_q_table', default=False, action='store_true')
    parser.add_argument('--in_sight', default=False, action='store_true')
    parser.add_argument('--two', default=False, action='store_true')
    # parser.add_argument('--two', default=True, action='store_false')
    parser.add_argument('--roa_table', default=False, action='store_true')
    parser.add_argument('--q_table', default=False, action='store_true')
    parser.add_argument('--debug', default=False, action='store_true')
    parser.add_argument('--multi_robots', default=False, action='store_true')
    parser.add_argument('--single_pol', default=True, action='store_false')
    parser.add_argument('--full', default=False, action='store_true')
    parser.add_argument('--push', default=False, action='store_true')
    parser.add_argument('--no_rand', default=False, action='store_true')
    parser.add_argument('--centre', default=False, action='store_true')
    parser.add_argument('--use_roa', default=False, action='store_true')
    parser.add_argument('--no_reg', default=False, action='store_true')
    parser.add_argument('--base_only', default=False, action='store_true')
    parser.add_argument('--baseline', default=False, action='store_true')
    parser.add_argument('--use_goal_set', default=False, action='store_true')

    parser.add_argument('--use_vf', default=False, action='store_true')
    parser.add_argument('--multi', default=True, action='store_false')
    parser.add_argument('--goal_set', default=False, action='store_true')
    parser.add_argument('--expert', default=False, action='store_true')
    parser.add_argument('--nicks', default=False, action='store_true')
    parser.add_argument('--act', default=False, action='store_true')
    parser.add_argument('--forces', default=False, action='store_true')
    parser.add_argument('--plot', default=False, action='store_true')
    parser.add_argument('--gae', default=False, action='store_true')
    parser.add_argument('--doa', default=False, action='store_true')

    parser.add_argument('--render', default=False, action='store_true')
    parser.add_argument('--hpc', default=False, action='store_true')
    parser.add_argument('--lstm_pol', default=False, action='store_true')
    parser.add_argument('--test_pol', default=False, action='store_true')
    # parser.add_argument('--vis', default=False, action='store_true')
    parser.add_argument('--vis', default=True, action='store_false')
    parser.add_argument('--cur', default=False, action='store_true')
    parser.add_argument('--mocap', default=False, action='store_true')
    parser.add_argument('--display_hm', default=False, action='store_true')
    parser.add_argument('--display_doa', default=False, action='store_true')
    # parser.add_argument('--const_std', default=True, action='store_false')
    parser.add_argument('--const_std', default=False, action='store_true')
    parser.add_argument('--exp', default="test")
    parser.add_argument('--obstacle_type', default="domain_train_no_stairs", help="flat, stairs, path, jump")
    parser.add_argument('--control_type', default="walk", help="stop, slow,    walk, run")
    parser.add_argument('--seed', default=42, type=int)
    parser.add_argument('--max_ts', default=1e5, type=int)
    parser.add_argument('--lr', default=1e-5, type=float)
    args = parser.parse_args()
    os.environ["CUDA_VISIBLE_DEVICES"]="-1"
    run(args)