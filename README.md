# README #

This repo is the code I used for the biped papers in my PhD, work on the titan is in a different repo. This code has mostly come from the disgusting mess that is my biped_model repo. While some of this code is still a disgusting mess, it is slightly more managable in this repo.

### What is this repository for? ###

* ACRA paper: Curriculum learning for individual policies, run for each terrain type (from repo biped_model:acra)
    - `run.py --cur --obstacle_type terrain`
* IROS paper: Learning to switch between policies (from repo biped_model:RA_L)
    Train single policy (slightly different / earlier than ACRA paper, uses height map)?
    - `run_switch.py --obstacle_type terrain` </br>
    Need to collect data for each policy type, then train switch estimator
    - `run_switch_get_data.py --obstacle_type terrain`
    - `models/switch_doa.py --obstacle_type terrain` </br>
    Evalute individual switch properties
    - `run_switch_eval.py --obstacle_type terrain` </br>
    or 
    - `bashies/all_switch_eval.sh`    
    Evaluate on multiple terrains
    - `run_switch_multi.py`
* Training setup policies (from repo biped_model:setup_pause)
    - `run_setup.py --obstacle_type terrain`
* Ensemble paper
    - `run_ensemble.py`

### How do I get set up? ###

* 

### Contribution guidelines ###

* 

### Who do I talk to? ###

* 

Current tests (args.tests):
"decoded" 
"extra" - add scaled_recon_loss to positive rew
"roi" - roi only for vae (only with pos)
"pos" - positive images only for vae
"lam" - lam
"err" - error subtracted from q, learn errer
"pre" - pretrain vae (10 epochs for 10 trains)


### Example uses: ###
#### Setup paper ####
<!-- Pause terminate to train for a fixed length for a magic number period of time (50)-->
`mpirun -np 16 --oversubscribe python3 run_setup.py --pause_terminate`

<!-- Train for fixed episodes (instead of fixed data length), i.e. as episode lengths decrease, training will occur at around the same wallclock time, with less data -->
`mpirun -np 16 --oversubscribe python3 run_setup.py --fixed_training_length`

<!-- Train from target policy, and uses terminate bit -3 to activate setup, and 3 to activate target.. -->
`mpirun -np 16 --oversubscribe python3 run_setup.py --new_hotness`

## Important lessons ##
Change one thing at a time!!

<!-- Train e2e policy setup environment -->
`mpirun -np 16 --oversubscribe python3 run_single.py --train_setup --obstacle_type high_jumps --folder setup --cur --exp e2e`


Pybullet on HPC was 2.5.5, on my computer was 2.8.4. For some reason my results weren't the same as the HPC (detrimentally so)

My computer:
pybullet 2.8.4
numpy 1.17
tensorflow-gpu 1.14
python 3.6.9

<!-- Setup bugs -->
- Originally was training with discrete starting location, this really killed performance on sequence of terrains
- get_expert() needs to be called because it sets swing foot in state. Performance suffered when changing terrains